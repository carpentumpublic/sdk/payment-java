//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** WALLET_TRANSFER
 *
 * Payment method for sending funds from your account to external wallet.
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public class WalletTransferMethodImpl implements WalletTransferMethod {
    private final AccountPayoutRequestWalletTransfer account;

    @Override
    public AccountPayoutRequestWalletTransfer getAccount() {
        return account;
    }


    /** One of following can serve as Payment Operator:

 * Financial or other institution (such as bank, card payment processor, ...) that manages transactions for your customers
 * Mobile wallet
 * Blockchain protocol for crypto currency payments

Customer is informed with the payment instructions where funds have to be transferred efficiently based on the selected Payment operator.
For getting list of the available payment options for payins use [POST /payins/!availablePaymentOptions](#operations-Incoming_payments-availablePaymentOptions) API, for payouts use [POST /payouts/!availablePaymentOptions](#operations-Outgoing_payments-availablePaymentOptions) API. */
    private final Optional<String> paymentOperatorCode;

    @Override
    public Optional<String> getPaymentOperatorCode() {
        return paymentOperatorCode;
    }


    /** Your customer mobile phone number in full international telephone number format, including country code.

If currency is GHS or KES then phoneNumber field is required. Otherwise, it is optional. */
    private final Optional<String> phoneNumber;

    @Override
    public Optional<String> getPhoneNumber() {
        return phoneNumber;
    }


    private final Optional<String> remark;

    @Override
    public Optional<String> getRemark() {
        return remark;
    }


    @Override public PaymentMethodCode getPaymentMethodCode() { return PAYMENT_METHOD_CODE; }

    private final int hashCode;
    private final String toString;

    private WalletTransferMethodImpl(BuilderImpl builder) {
        this.account = Objects.requireNonNull(builder.account, "Property 'account' is required.");
        this.paymentOperatorCode = Optional.ofNullable(builder.paymentOperatorCode);
        this.phoneNumber = Optional.ofNullable(builder.phoneNumber);
        this.remark = Optional.ofNullable(builder.remark);

        this.hashCode = Objects.hash(account, paymentOperatorCode, phoneNumber, remark);
        this.toString = builder.type + "(" +
                "account=" + account +
                ", paymentOperatorCode=" + paymentOperatorCode +
                ", phoneNumber=" + phoneNumber +
                ", remark=" + remark +
                ')';
    }

    @Override
    public int hashCode() {
        return hashCode;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }

        if (!(obj instanceof WalletTransferMethodImpl)) {
            return false;
        }

        WalletTransferMethodImpl that = (WalletTransferMethodImpl) obj;
        if (!Objects.equals(this.account, that.account)) return false;
        if (!Objects.equals(this.paymentOperatorCode, that.paymentOperatorCode)) return false;
        if (!Objects.equals(this.phoneNumber, that.phoneNumber)) return false;
        if (!Objects.equals(this.remark, that.remark)) return false;

        return true;
    }

    @Override
    public String toString() {
        return toString;
    }

    /** Builder for {@link WalletTransferMethod} model class. */
    public static class BuilderImpl implements WalletTransferMethod.Builder {
        private AccountPayoutRequestWalletTransfer account = null;
        private String paymentOperatorCode = null;
        private String phoneNumber = null;
        private String remark = null;

        private final String type;

        public BuilderImpl(String type) {
            this.type = type;
        }

        public BuilderImpl() {
            this("WalletTransferMethod");
        }

        /**
          * Set {@link WalletTransferMethod#getAccount} property.
          *
          * 
          */
        @Override
        public BuilderImpl account(AccountPayoutRequestWalletTransfer account) {
            this.account = account;
            return this;
        }

        @Override
        public boolean isAccountDefined() {
            return this.account != null;
        }

        /**
          * Set {@link WalletTransferMethod#getPaymentOperatorCode} property.
          *
          * One of following can serve as Payment Operator:

 * Financial or other institution (such as bank, card payment processor, ...) that manages transactions for your customers
 * Mobile wallet
 * Blockchain protocol for crypto currency payments

Customer is informed with the payment instructions where funds have to be transferred efficiently based on the selected Payment operator.
For getting list of the available payment options for payins use [POST /payins/!availablePaymentOptions](#operations-Incoming_payments-availablePaymentOptions) API, for payouts use [POST /payouts/!availablePaymentOptions](#operations-Outgoing_payments-availablePaymentOptions) API.
          */
        @Override
        public BuilderImpl paymentOperatorCode(String paymentOperatorCode) {
            this.paymentOperatorCode = paymentOperatorCode;
            return this;
        }

        @Override
        public boolean isPaymentOperatorCodeDefined() {
            return this.paymentOperatorCode != null;
        }

        /**
          * Set {@link WalletTransferMethod#getPhoneNumber} property.
          *
          * Your customer mobile phone number in full international telephone number format, including country code.

If currency is GHS or KES then phoneNumber field is required. Otherwise, it is optional.
          */
        @Override
        public BuilderImpl phoneNumber(String phoneNumber) {
            this.phoneNumber = phoneNumber;
            return this;
        }

        @Override
        public boolean isPhoneNumberDefined() {
            return this.phoneNumber != null;
        }

        /**
          * Set {@link WalletTransferMethod#getRemark} property.
          *
          * 
          */
        @Override
        public BuilderImpl remark(String remark) {
            this.remark = remark;
            return this;
        }

        @Override
        public boolean isRemarkDefined() {
            return this.remark != null;
        }

        /**
         * Create new instance of {@link WalletTransferMethod} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @Override
        public WalletTransferMethodImpl build() {
            return new WalletTransferMethodImpl(this);
        }

    }
}