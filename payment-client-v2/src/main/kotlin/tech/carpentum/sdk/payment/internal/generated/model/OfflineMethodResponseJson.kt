//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.JsonClass;
import tech.carpentum.sdk.payment.model.*;

@JsonClass(generateAdapter = true)
class OfflineMethodResponseJson {
    var idPayin: IdPayin? = null
    var idPayment: IdPayment? = null
    var account: AccountResponseOffline? = null
    var accountCustomer: AccountCustomerResponseOffline? = null
    var money: MoneyPaymentResponse? = null
    var moneyRequired: MoneyRequired? = null
    var vat: MoneyVat? = null
    var reference: String? = null
    var processor: PaymentProcessor? = null
    var qrName: String? = null
    var qrCode: String? = null
    var returnUrl: String? = null
    var paymentOperator: SelectedPaymentOperatorIncoming? = null
    var productId: String? = null
    var acceptedAt: java.time.OffsetDateTime? = null
    var expireAt: java.time.OffsetDateTime? = null
    var paymentMethodCode: String? = null
}