//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** FailureReasons
 *
 * 
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public class FailureReasonsImpl implements FailureReasons {
    /** Date and time of the payment failure. */
    private final java.time.OffsetDateTime failedAt;

    @Override
    public java.time.OffsetDateTime getFailedAt() {
        return failedAt;
    }


    private final FailureReasonCode failureReasonCode;

    @Override
    public FailureReasonCode getFailureReasonCode() {
        return failureReasonCode;
    }


    /** Additional description of the failure reason. */
    private final Optional<String> message;

    @Override
    public Optional<String> getMessage() {
        return message;
    }




    private final int hashCode;
    private final String toString;

    private FailureReasonsImpl(BuilderImpl builder) {
        this.failedAt = Objects.requireNonNull(builder.failedAt, "Property 'failedAt' is required.");
        this.failureReasonCode = Objects.requireNonNull(builder.failureReasonCode, "Property 'failureReasonCode' is required.");
        this.message = Optional.ofNullable(builder.message);

        this.hashCode = Objects.hash(failedAt, failureReasonCode, message);
        this.toString = builder.type + "(" +
                "failedAt=" + failedAt +
                ", failureReasonCode=" + failureReasonCode +
                ", message=" + message +
                ')';
    }

    @Override
    public int hashCode() {
        return hashCode;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }

        if (!(obj instanceof FailureReasonsImpl)) {
            return false;
        }

        FailureReasonsImpl that = (FailureReasonsImpl) obj;
        if (!Objects.equals(this.failedAt, that.failedAt)) return false;
        if (!Objects.equals(this.failureReasonCode, that.failureReasonCode)) return false;
        if (!Objects.equals(this.message, that.message)) return false;

        return true;
    }

    @Override
    public String toString() {
        return toString;
    }

    /** Builder for {@link FailureReasons} model class. */
    public static class BuilderImpl implements FailureReasons.Builder {
        private java.time.OffsetDateTime failedAt = null;
        private FailureReasonCode failureReasonCode = null;
        private String message = null;

        private final String type;

        public BuilderImpl(String type) {
            this.type = type;
        }

        public BuilderImpl() {
            this("FailureReasons");
        }

        /**
          * Set {@link FailureReasons#getFailedAt} property.
          *
          * Date and time of the payment failure.
          */
        @Override
        public BuilderImpl failedAt(java.time.OffsetDateTime failedAt) {
            this.failedAt = failedAt;
            return this;
        }

        @Override
        public boolean isFailedAtDefined() {
            return this.failedAt != null;
        }

        /**
          * Set {@link FailureReasons#getFailureReasonCode} property.
          *
          * 
          */
        @Override
        public BuilderImpl failureReasonCode(FailureReasonCode failureReasonCode) {
            this.failureReasonCode = failureReasonCode;
            return this;
        }

        @Override
        public boolean isFailureReasonCodeDefined() {
            return this.failureReasonCode != null;
        }

        /**
          * Set {@link FailureReasons#getMessage} property.
          *
          * Additional description of the failure reason.
          */
        @Override
        public BuilderImpl message(String message) {
            this.message = message;
            return this;
        }

        @Override
        public boolean isMessageDefined() {
            return this.message != null;
        }

        /**
         * Create new instance of {@link FailureReasons} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @Override
        public FailureReasonsImpl build() {
            return new FailureReasonsImpl(this);
        }

    }
}