//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** AuthTokenAction
 *
 * Action code specifying endpoints that can be called with the issued token.
 *
 * 
 *
 * The model class is immutable.
 *
 * Use static {@link #of} method to create a new model class instance.
 */
@JsonClass(generateAdapter = false)
public class AuthTokenActionImpl implements AuthTokenAction {
    private final @NotNull String value;

    private final int hashCode;
    private final String toString;

    public AuthTokenActionImpl(@NotNull String value) {
        this.value = Objects.requireNonNull(value, "Property 'AuthTokenAction' is required.");

        this.hashCode = value.hashCode();
        this.toString = String.valueOf(value);
    }

    @Override
    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return toString;
    }

    @Override
    public int hashCode() {
        return hashCode;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }

        if (!(obj instanceof AuthTokenActionImpl)) {
            return false;
        }

        AuthTokenActionImpl that = (AuthTokenActionImpl) obj;
        if (!this.value.equals(that.value)) return false;

        return true;
    }
}