//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** TopUpRequestedMoneyProvided
 *
 * Provide how much money (amount) and in what currency (currencyCode) you want to pay to have your account topped up in a different currency (currencyRequired). The API will return how much money will be added to your account.
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public class TopUpRequestedMoneyProvidedImpl implements TopUpRequestedMoneyProvided {
    private final Money moneyProvided;

    @Override
    public Money getMoneyProvided() {
        return moneyProvided;
    }


    private final Optional<CurrencyCode> currencyRequired;

    @Override
    public Optional<CurrencyCode> getCurrencyRequired() {
        return currencyRequired;
    }




    private final int hashCode;
    private final String toString;

    private TopUpRequestedMoneyProvidedImpl(BuilderImpl builder) {
        this.moneyProvided = Objects.requireNonNull(builder.moneyProvided, "Property 'moneyProvided' is required.");
        this.currencyRequired = Optional.ofNullable(builder.currencyRequired);

        this.hashCode = Objects.hash(moneyProvided, currencyRequired);
        this.toString = builder.type + "(" +
                "moneyProvided=" + moneyProvided +
                ", currencyRequired=" + currencyRequired +
                ')';
    }

    @Override
    public int hashCode() {
        return hashCode;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }

        if (!(obj instanceof TopUpRequestedMoneyProvidedImpl)) {
            return false;
        }

        TopUpRequestedMoneyProvidedImpl that = (TopUpRequestedMoneyProvidedImpl) obj;
        if (!Objects.equals(this.moneyProvided, that.moneyProvided)) return false;
        if (!Objects.equals(this.currencyRequired, that.currencyRequired)) return false;

        return true;
    }

    @Override
    public String toString() {
        return toString;
    }

    /** Builder for {@link TopUpRequestedMoneyProvided} model class. */
    public static class BuilderImpl implements TopUpRequestedMoneyProvided.Builder {
        private Money moneyProvided = null;
        private CurrencyCode currencyRequired = null;

        private final String type;

        public BuilderImpl(String type) {
            this.type = type;
        }

        public BuilderImpl() {
            this("TopUpRequestedMoneyProvided");
        }

        /**
          * Set {@link TopUpRequestedMoneyProvided#getMoneyProvided} property.
          *
          * 
          */
        @Override
        public BuilderImpl moneyProvided(Money moneyProvided) {
            this.moneyProvided = moneyProvided;
            return this;
        }

        @Override
        public boolean isMoneyProvidedDefined() {
            return this.moneyProvided != null;
        }

        /**
          * Set {@link TopUpRequestedMoneyProvided#getCurrencyRequired} property.
          *
          * 
          */
        @Override
        public BuilderImpl currencyRequired(CurrencyCode currencyRequired) {
            this.currencyRequired = currencyRequired;
            return this;
        }

        @Override
        public boolean isCurrencyRequiredDefined() {
            return this.currencyRequired != null;
        }

        /**
         * Create new instance of {@link TopUpRequestedMoneyProvided} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @Override
        public TopUpRequestedMoneyProvidedImpl build() {
            return new TopUpRequestedMoneyProvidedImpl(this);
        }

    }
}