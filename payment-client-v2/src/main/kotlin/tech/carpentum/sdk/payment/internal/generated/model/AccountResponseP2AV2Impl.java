//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** AccountResponseP2AV2
 *
 * Parameters of a bank account where we expect that your customer send funds to make a payment. These account parameters has to be provided to your customer in form of an payment instructions.
The returned parameters are depended on the payment method and currency your customer choose to pay.
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@Deprecated
@JsonClass(generateAdapter = false)
public class AccountResponseP2AV2Impl implements AccountResponseP2AV2 {
    /** Name of the bank account where we expect that your customer sends funds to make a payment.
If this parameter contains any value then show it to your customer in the payment instructions. */
    private final Optional<String> accountName;

    @Override
    public Optional<String> getAccountName() {
        return accountName;
    }


    /** Number of the bank account where we expect that your customer sends funds to make a payment.
If this parameter contains any value then show it to your customer in the payment instructions. */
    private final Optional<String> accountNumber;

    @Override
    public Optional<String> getAccountNumber() {
        return accountNumber;
    }


    /** Bank code of the bank where we expect that your customer sends funds to make a payment.
If this parameter contains any value then show it to your customer in the payment instructions. */
    private final Optional<String> bankCode;

    @Override
    public Optional<String> getBankCode() {
        return bankCode;
    }


    /** Name of the bank where we expect that your customer sends funds to make a payment.
If this parameter contains any value then show it to your customer in the payment instructions. */
    private final Optional<String> bankName;

    @Override
    public Optional<String> getBankName() {
        return bankName;
    }


    /** Branch name of the bank where we expect that your customer sends funds to make a payment.
If this parameter contains any value then show it to your customer in the payment instructions. */
    private final Optional<String> bankBranch;

    @Override
    public Optional<String> getBankBranch() {
        return bankBranch;
    }


    /** City of the bank where we expect that your customer sends funds to make a payment.
If this parameter contains any value then show it to your customer in the payment instructions. */
    private final Optional<String> bankCity;

    @Override
    public Optional<String> getBankCity() {
        return bankCity;
    }


    /** Province of the bank where we expect that your customer sends funds to make a payment.
If this parameter contains any value then show it to your customer in the payment instructions. */
    private final Optional<String> bankProvince;

    @Override
    public Optional<String> getBankProvince() {
        return bankProvince;
    }




    private final int hashCode;
    private final String toString;

    private AccountResponseP2AV2Impl(BuilderImpl builder) {
        this.accountName = Optional.ofNullable(builder.accountName);
        this.accountNumber = Optional.ofNullable(builder.accountNumber);
        this.bankCode = Optional.ofNullable(builder.bankCode);
        this.bankName = Optional.ofNullable(builder.bankName);
        this.bankBranch = Optional.ofNullable(builder.bankBranch);
        this.bankCity = Optional.ofNullable(builder.bankCity);
        this.bankProvince = Optional.ofNullable(builder.bankProvince);

        this.hashCode = Objects.hash(accountName, accountNumber, bankCode, bankName, bankBranch, bankCity, bankProvince);
        this.toString = builder.type + "(" +
                "accountName=" + accountName +
                ", accountNumber=" + accountNumber +
                ", bankCode=" + bankCode +
                ", bankName=" + bankName +
                ", bankBranch=" + bankBranch +
                ", bankCity=" + bankCity +
                ", bankProvince=" + bankProvince +
                ')';
    }

    @Override
    public int hashCode() {
        return hashCode;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }

        if (!(obj instanceof AccountResponseP2AV2Impl)) {
            return false;
        }

        AccountResponseP2AV2Impl that = (AccountResponseP2AV2Impl) obj;
        if (!Objects.equals(this.accountName, that.accountName)) return false;
        if (!Objects.equals(this.accountNumber, that.accountNumber)) return false;
        if (!Objects.equals(this.bankCode, that.bankCode)) return false;
        if (!Objects.equals(this.bankName, that.bankName)) return false;
        if (!Objects.equals(this.bankBranch, that.bankBranch)) return false;
        if (!Objects.equals(this.bankCity, that.bankCity)) return false;
        if (!Objects.equals(this.bankProvince, that.bankProvince)) return false;

        return true;
    }

    @Override
    public String toString() {
        return toString;
    }

    /** Builder for {@link AccountResponseP2AV2} model class. */
    public static class BuilderImpl implements AccountResponseP2AV2.Builder {
        private String accountName = null;
        private String accountNumber = null;
        private String bankCode = null;
        private String bankName = null;
        private String bankBranch = null;
        private String bankCity = null;
        private String bankProvince = null;

        private final String type;

        public BuilderImpl(String type) {
            this.type = type;
        }

        public BuilderImpl() {
            this("AccountResponseP2AV2");
        }

        /**
          * Set {@link AccountResponseP2AV2#getAccountName} property.
          *
          * Name of the bank account where we expect that your customer sends funds to make a payment.
If this parameter contains any value then show it to your customer in the payment instructions.
          */
        @Override
        public BuilderImpl accountName(String accountName) {
            this.accountName = accountName;
            return this;
        }

        @Override
        public boolean isAccountNameDefined() {
            return this.accountName != null;
        }

        /**
          * Set {@link AccountResponseP2AV2#getAccountNumber} property.
          *
          * Number of the bank account where we expect that your customer sends funds to make a payment.
If this parameter contains any value then show it to your customer in the payment instructions.
          */
        @Override
        public BuilderImpl accountNumber(String accountNumber) {
            this.accountNumber = accountNumber;
            return this;
        }

        @Override
        public boolean isAccountNumberDefined() {
            return this.accountNumber != null;
        }

        /**
          * Set {@link AccountResponseP2AV2#getBankCode} property.
          *
          * Bank code of the bank where we expect that your customer sends funds to make a payment.
If this parameter contains any value then show it to your customer in the payment instructions.
          */
        @Override
        public BuilderImpl bankCode(String bankCode) {
            this.bankCode = bankCode;
            return this;
        }

        @Override
        public boolean isBankCodeDefined() {
            return this.bankCode != null;
        }

        /**
          * Set {@link AccountResponseP2AV2#getBankName} property.
          *
          * Name of the bank where we expect that your customer sends funds to make a payment.
If this parameter contains any value then show it to your customer in the payment instructions.
          */
        @Override
        public BuilderImpl bankName(String bankName) {
            this.bankName = bankName;
            return this;
        }

        @Override
        public boolean isBankNameDefined() {
            return this.bankName != null;
        }

        /**
          * Set {@link AccountResponseP2AV2#getBankBranch} property.
          *
          * Branch name of the bank where we expect that your customer sends funds to make a payment.
If this parameter contains any value then show it to your customer in the payment instructions.
          */
        @Override
        public BuilderImpl bankBranch(String bankBranch) {
            this.bankBranch = bankBranch;
            return this;
        }

        @Override
        public boolean isBankBranchDefined() {
            return this.bankBranch != null;
        }

        /**
          * Set {@link AccountResponseP2AV2#getBankCity} property.
          *
          * City of the bank where we expect that your customer sends funds to make a payment.
If this parameter contains any value then show it to your customer in the payment instructions.
          */
        @Override
        public BuilderImpl bankCity(String bankCity) {
            this.bankCity = bankCity;
            return this;
        }

        @Override
        public boolean isBankCityDefined() {
            return this.bankCity != null;
        }

        /**
          * Set {@link AccountResponseP2AV2#getBankProvince} property.
          *
          * Province of the bank where we expect that your customer sends funds to make a payment.
If this parameter contains any value then show it to your customer in the payment instructions.
          */
        @Override
        public BuilderImpl bankProvince(String bankProvince) {
            this.bankProvince = bankProvince;
            return this;
        }

        @Override
        public boolean isBankProvinceDefined() {
            return this.bankProvince != null;
        }

        /**
         * Create new instance of {@link AccountResponseP2AV2} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @Override
        public AccountResponseP2AV2Impl build() {
            return new AccountResponseP2AV2Impl(this);
        }

    }
}