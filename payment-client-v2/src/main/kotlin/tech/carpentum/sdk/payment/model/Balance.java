//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** Balance
 *
 * 
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public interface Balance {

    @NotNull CurrencyCode getCurrencyCode();

    /** Current balance, may include pending payouts or settlements. */
    @NotNull java.math.BigDecimal getBalance();

    /** Available balance, ready to use for payouts, settlements or other movements. It is calculated as balance - (pendingSettlementAmount + pendingPayoutAmount). */
    @NotNull java.math.BigDecimal getAvailableBalance();

    /** Amount of settlements waiting to be processed. */
    @NotNull java.math.BigDecimal getPendingSettlementAmount();

    /** Amount of payouts waiting to be processed. */
    @NotNull java.math.BigDecimal getPendingPayoutAmount();

    /** Date and time of last update of balance or available balance on this account. */
    @NotNull java.time.OffsetDateTime getLastBalanceMovement();

    @NotNull static Builder builder(Balance copyOf) {
        Builder builder = builder();
        builder.currencyCode(copyOf.getCurrencyCode());
        builder.balance(copyOf.getBalance());
        builder.availableBalance(copyOf.getAvailableBalance());
        builder.pendingSettlementAmount(copyOf.getPendingSettlementAmount());
        builder.pendingPayoutAmount(copyOf.getPendingPayoutAmount());
        builder.lastBalanceMovement(copyOf.getLastBalanceMovement());
        return builder;
    }

    @NotNull static Builder builder() {
        return new BalanceImpl.BuilderImpl();
    }

    /** Builder for {@link Balance} model class. */
    interface Builder {

        /**
          * Set {@link Balance#getCurrencyCode} property.
          *
          * 
          */
        @NotNull Builder currencyCode(CurrencyCode currencyCode);

        boolean isCurrencyCodeDefined();


        /**
          * Set {@link Balance#getBalance} property.
          *
          * Current balance, may include pending payouts or settlements.
          */
        @NotNull Builder balance(java.math.BigDecimal balance);

        boolean isBalanceDefined();


        /**
          * Set {@link Balance#getAvailableBalance} property.
          *
          * Available balance, ready to use for payouts, settlements or other movements. It is calculated as balance - (pendingSettlementAmount + pendingPayoutAmount).
          */
        @NotNull Builder availableBalance(java.math.BigDecimal availableBalance);

        boolean isAvailableBalanceDefined();


        /**
          * Set {@link Balance#getPendingSettlementAmount} property.
          *
          * Amount of settlements waiting to be processed.
          */
        @NotNull Builder pendingSettlementAmount(java.math.BigDecimal pendingSettlementAmount);

        boolean isPendingSettlementAmountDefined();


        /**
          * Set {@link Balance#getPendingPayoutAmount} property.
          *
          * Amount of payouts waiting to be processed.
          */
        @NotNull Builder pendingPayoutAmount(java.math.BigDecimal pendingPayoutAmount);

        boolean isPendingPayoutAmountDefined();


        /**
          * Set {@link Balance#getLastBalanceMovement} property.
          *
          * Date and time of last update of balance or available balance on this account.
          */
        @NotNull Builder lastBalanceMovement(java.time.OffsetDateTime lastBalanceMovement);

        boolean isLastBalanceMovementDefined();


        /**
         * Create new instance of {@link Balance} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @NotNull Balance build();

    }
}