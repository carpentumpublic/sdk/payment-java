package tech.carpentum.sdk.payment.sample

import tech.carpentum.sdk.payment.AuthTokenOperations
import tech.carpentum.sdk.payment.IncomingPaymentsApi
import tech.carpentum.sdk.payment.IncomingPaymentsApi.Factory.definePayinAvailablePaymentOptionsEndpoint
import tech.carpentum.sdk.payment.IncomingPaymentsApi.Factory.defineCreatePayinEndpoint
import tech.carpentum.sdk.payment.IncomingPaymentsApi.Factory.defineGetPayinEndpoint
import tech.carpentum.sdk.payment.IncomingPaymentsApi.Factory.defineSetPayinExternalReferenceEndpoint
import tech.carpentum.sdk.payment.PaymentContext
import tech.carpentum.sdk.payment.ResponseException
import tech.carpentum.sdk.payment.model.AvailablePayinOptionList
import tech.carpentum.sdk.payment.model.Payin
import tech.carpentum.sdk.payment.model.PayinAcceptedResponse
import tech.carpentum.sdk.payment.model.PayinDetail
import tech.carpentum.sdk.payment.model.PaymentRequested
import java.io.InterruptedIOException
import java.time.Duration

/**
 * Wraps all Incoming Payments API operations for single `idPayin`.
 */
class CreatePayinScenario private constructor(
    private val incomingPaymentsApi: IncomingPaymentsApi,
    private val idPayin: String,
    authToken: String
) : AbstractScenario(authToken) {

    @Throws(ResponseException::class, InterruptedIOException::class)
    fun availablePaymentOptions(paymentRequested: PaymentRequested): AvailablePayinOptionList {
        return incomingPaymentsApi.payinAvailablePaymentOptions(paymentRequested)
    }

    @Throws(ResponseException::class, InterruptedIOException::class)
    fun createPayment(payin: Payin): PayinAcceptedResponse {
        return incomingPaymentsApi.createPayin(idPayin, payin)
    }

    @Throws(ResponseException::class, InterruptedIOException::class)
    fun setExternalReference(reference: String) {
        incomingPaymentsApi.setPayinExternalReference(idPayin, reference)
    }

    @Throws(ResponseException::class, InterruptedIOException::class)
    fun getDetail(): PayinDetail {
        return incomingPaymentsApi.getPayin(idPayin)
    }

    companion object {
        /**
         * Creates new instance of the scenario with newly created Authorization token with specified validity.
         */
        @Throws(ResponseException::class, InterruptedIOException::class)
        fun create(context: PaymentContext, idPayin: String, validity: Duration? = null): CreatePayinScenario {
            val token = context.createAuthToken(
                AuthTokenOperations()
                    .grant(definePayinAvailablePaymentOptionsEndpoint())
                    .grant(defineCreatePayinEndpoint().forId(idPayin))
                    .grant(defineGetPayinEndpoint().forId(idPayin))
                    .grant(defineSetPayinExternalReferenceEndpoint().forId(idPayin)),
                validity
            ).token
            return CreatePayinScenario(IncomingPaymentsApi.create(context, token), idPayin, token)
        }

        /**
         * Creates new instance of the scenario with existing Authorization token.
         */
        @Throws(ResponseException::class, InterruptedIOException::class)
        fun create(context: PaymentContext, idPayin: String, authToken: String): CreatePayinScenario {
            return CreatePayinScenario(IncomingPaymentsApi.create(context, authToken), idPayin, authToken)
        }
    }
}
