//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.CapitecPayMethod

class CapitecPayMethodJsonAdapter {
    @FromJson
    fun fromJson(json: CapitecPayMethodJson): CapitecPayMethod {
        val builder = CapitecPayMethod.builder()
        builder.account(json.account)
        builder.emailAddress(json.emailAddress)
        builder.phoneNumber(json.phoneNumber)
        builder.said(json.said)
        return builder.build()
    }

    @ToJson
    fun toJson(model: CapitecPayMethod): CapitecPayMethodJson {
        val json = CapitecPayMethodJson()
        json.account = model.account.orElse(null)
        json.emailAddress = model.emailAddress.orElse(null)
        json.phoneNumber = model.phoneNumber.orElse(null)
        json.said = model.said.orElse(null)
        json.paymentMethodCode = model.paymentMethodCode.name
        return json
    }

    @FromJson
    fun fromJsonImpl(model: CapitecPayMethod): CapitecPayMethodImpl {
        return model as CapitecPayMethodImpl
    }

    @ToJson
    fun toJsonImpl(impl: CapitecPayMethodImpl): CapitecPayMethod {
        return impl
    }

}