//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** TopUpRequestedMoneyRequired
 *
 * Provide the currency in which you want to pay (currencyCode) to have your account topped up with a certain amount of money (amount) in a different currency (currencyProvided). The API will return how much money you will pay in your original currency (currencyCode).
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public class TopUpRequestedMoneyRequiredImpl implements TopUpRequestedMoneyRequired {
    private final Money moneyRequired;

    @Override
    public Money getMoneyRequired() {
        return moneyRequired;
    }


    private final Optional<CurrencyCode> currencyProvided;

    @Override
    public Optional<CurrencyCode> getCurrencyProvided() {
        return currencyProvided;
    }




    private final int hashCode;
    private final String toString;

    private TopUpRequestedMoneyRequiredImpl(BuilderImpl builder) {
        this.moneyRequired = Objects.requireNonNull(builder.moneyRequired, "Property 'moneyRequired' is required.");
        this.currencyProvided = Optional.ofNullable(builder.currencyProvided);

        this.hashCode = Objects.hash(moneyRequired, currencyProvided);
        this.toString = builder.type + "(" +
                "moneyRequired=" + moneyRequired +
                ", currencyProvided=" + currencyProvided +
                ')';
    }

    @Override
    public int hashCode() {
        return hashCode;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }

        if (!(obj instanceof TopUpRequestedMoneyRequiredImpl)) {
            return false;
        }

        TopUpRequestedMoneyRequiredImpl that = (TopUpRequestedMoneyRequiredImpl) obj;
        if (!Objects.equals(this.moneyRequired, that.moneyRequired)) return false;
        if (!Objects.equals(this.currencyProvided, that.currencyProvided)) return false;

        return true;
    }

    @Override
    public String toString() {
        return toString;
    }

    /** Builder for {@link TopUpRequestedMoneyRequired} model class. */
    public static class BuilderImpl implements TopUpRequestedMoneyRequired.Builder {
        private Money moneyRequired = null;
        private CurrencyCode currencyProvided = null;

        private final String type;

        public BuilderImpl(String type) {
            this.type = type;
        }

        public BuilderImpl() {
            this("TopUpRequestedMoneyRequired");
        }

        /**
          * Set {@link TopUpRequestedMoneyRequired#getMoneyRequired} property.
          *
          * 
          */
        @Override
        public BuilderImpl moneyRequired(Money moneyRequired) {
            this.moneyRequired = moneyRequired;
            return this;
        }

        @Override
        public boolean isMoneyRequiredDefined() {
            return this.moneyRequired != null;
        }

        /**
          * Set {@link TopUpRequestedMoneyRequired#getCurrencyProvided} property.
          *
          * 
          */
        @Override
        public BuilderImpl currencyProvided(CurrencyCode currencyProvided) {
            this.currencyProvided = currencyProvided;
            return this;
        }

        @Override
        public boolean isCurrencyProvidedDefined() {
            return this.currencyProvided != null;
        }

        /**
         * Create new instance of {@link TopUpRequestedMoneyRequired} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @Override
        public TopUpRequestedMoneyRequiredImpl build() {
            return new TopUpRequestedMoneyRequiredImpl(this);
        }

    }
}