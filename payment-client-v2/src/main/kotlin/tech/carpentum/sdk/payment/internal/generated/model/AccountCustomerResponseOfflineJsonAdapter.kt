//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.AccountCustomerResponseOffline

class AccountCustomerResponseOfflineJsonAdapter {
    @FromJson
    fun fromJson(json: AccountCustomerResponseOfflineJson): AccountCustomerResponseOffline {
        val builder = AccountCustomerResponseOffline.builder()
        builder.accountNumber(json.accountNumber)
        return builder.build()
    }

    @ToJson
    fun toJson(model: AccountCustomerResponseOffline): AccountCustomerResponseOfflineJson {
        val json = AccountCustomerResponseOfflineJson()
        json.accountNumber = model.accountNumber.orElse(null)
        return json
    }

    @FromJson
    fun fromJsonImpl(model: AccountCustomerResponseOffline): AccountCustomerResponseOfflineImpl {
        return model as AccountCustomerResponseOfflineImpl
    }

    @ToJson
    fun toJsonImpl(impl: AccountCustomerResponseOfflineImpl): AccountCustomerResponseOffline {
        return impl
    }

}