//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** PayoutDetail
 *
 * 
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public interface PayoutDetail {

    @NotNull PaymentRequested getPaymentRequested();

    @NotNull PaymentProcess getProcess();

    @NotNull MoneyFee getFee();

    @NotNull PayoutMethodResponse getPaymentMethodResponse();

    @NotNull static Builder builder(PayoutDetail copyOf) {
        Builder builder = builder();
        builder.paymentRequested(copyOf.getPaymentRequested());
        builder.process(copyOf.getProcess());
        builder.fee(copyOf.getFee());
        builder.paymentMethodResponse(copyOf.getPaymentMethodResponse());
        return builder;
    }

    @NotNull static Builder builder() {
        return new PayoutDetailImpl.BuilderImpl();
    }

    /** Builder for {@link PayoutDetail} model class. */
    interface Builder {

        /**
          * Set {@link PayoutDetail#getPaymentRequested} property.
          *
          * 
          */
        @NotNull Builder paymentRequested(PaymentRequested paymentRequested);

        boolean isPaymentRequestedDefined();


        /**
          * Set {@link PayoutDetail#getProcess} property.
          *
          * 
          */
        @NotNull Builder process(PaymentProcess process);

        boolean isProcessDefined();


        /**
          * Set {@link PayoutDetail#getFee} property.
          *
          * 
          */
        @NotNull Builder fee(MoneyFee fee);

        boolean isFeeDefined();


        /**
          * Set {@link PayoutDetail#getPaymentMethodResponse} property.
          *
          * 
          */
        @NotNull Builder paymentMethodResponse(PayoutMethodResponse paymentMethodResponse);

        boolean isPaymentMethodResponseDefined();


        /**
         * Create new instance of {@link PayoutDetail} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @NotNull PayoutDetail build();

    }
}