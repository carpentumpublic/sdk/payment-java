//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** ProcessorStatus
 *
 * If present, this field specifies the status of the payment processing by an external processor.

Possible values:
 * `ACCEPTED` - The payment was accepted by the processor.
 * `PROCESSED` - The payment has been processed. For the result of the processing, check the "status" field in the response.
 *
 * 
 *
 * The model class is immutable.
 *
 * Use static {@link #of} method to create a new model class instance.
 */
@JsonClass(generateAdapter = false)
public class ProcessorStatusImpl implements ProcessorStatus {
    private final @NotNull String value;

    private final int hashCode;
    private final String toString;

    public ProcessorStatusImpl(@NotNull String value) {
        this.value = Objects.requireNonNull(value, "Property 'ProcessorStatus' is required.");

        this.hashCode = value.hashCode();
        this.toString = String.valueOf(value);
    }

    @Override
    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return toString;
    }

    @Override
    public int hashCode() {
        return hashCode;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }

        if (!(obj instanceof ProcessorStatusImpl)) {
            return false;
        }

        ProcessorStatusImpl that = (ProcessorStatusImpl) obj;
        if (!this.value.equals(that.value)) return false;

        return true;
    }
}