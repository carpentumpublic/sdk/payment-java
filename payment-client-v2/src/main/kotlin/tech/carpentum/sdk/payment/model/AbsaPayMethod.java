//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** ABSA_PAY
 *
 * Absa Pay offers a simple, quick and secure way to make payments without needing to input banking login details.

Customers can select their preferred account for payment and securely authorize the transaction through the banking app.
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public interface AbsaPayMethod extends PayinMethod {
    /** A discriminator value of property {@link #getPaymentMethodCode}. The model class extends {@link PayinMethod}. */
    PayinMethod.PaymentMethodCode PAYMENT_METHOD_CODE = PayinMethod.PaymentMethodCode.ABSA_PAY;

    @NotNull AccountPayinRequestAbsaPay getAccount();

    /** Your customer e-mail address in RFC 5322 format that is used for identification of the customer's payins. */
    @NotNull Optional<String> getEmailAddress();

    @NotNull static Builder builder(AbsaPayMethod copyOf) {
        Builder builder = builder();
        builder.account(copyOf.getAccount());
        builder.emailAddress(copyOf.getEmailAddress().orElse(null));
        return builder;
    }

    @NotNull static Builder builder() {
        return new AbsaPayMethodImpl.BuilderImpl();
    }

    /** Builder for {@link AbsaPayMethod} model class. */
    interface Builder {

        /**
          * Set {@link AbsaPayMethod#getAccount} property.
          *
          * 
          */
        @NotNull Builder account(AccountPayinRequestAbsaPay account);

        boolean isAccountDefined();


        /**
          * Set {@link AbsaPayMethod#getEmailAddress} property.
          *
          * Your customer e-mail address in RFC 5322 format that is used for identification of the customer's payins.
          */
        @NotNull Builder emailAddress(String emailAddress);

        boolean isEmailAddressDefined();


        /**
         * Create new instance of {@link AbsaPayMethod} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @NotNull AbsaPayMethod build();

    }
}