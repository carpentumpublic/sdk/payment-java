//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** Balance
 *
 * 
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public class BalanceImpl implements Balance {
    private final CurrencyCode currencyCode;

    @Override
    public CurrencyCode getCurrencyCode() {
        return currencyCode;
    }


    /** Current balance, may include pending payouts or settlements. */
    private final java.math.BigDecimal balance;

    @Override
    public java.math.BigDecimal getBalance() {
        return balance;
    }


    /** Available balance, ready to use for payouts, settlements or other movements. It is calculated as balance - (pendingSettlementAmount + pendingPayoutAmount). */
    private final java.math.BigDecimal availableBalance;

    @Override
    public java.math.BigDecimal getAvailableBalance() {
        return availableBalance;
    }


    /** Amount of settlements waiting to be processed. */
    private final java.math.BigDecimal pendingSettlementAmount;

    @Override
    public java.math.BigDecimal getPendingSettlementAmount() {
        return pendingSettlementAmount;
    }


    /** Amount of payouts waiting to be processed. */
    private final java.math.BigDecimal pendingPayoutAmount;

    @Override
    public java.math.BigDecimal getPendingPayoutAmount() {
        return pendingPayoutAmount;
    }


    /** Date and time of last update of balance or available balance on this account. */
    private final java.time.OffsetDateTime lastBalanceMovement;

    @Override
    public java.time.OffsetDateTime getLastBalanceMovement() {
        return lastBalanceMovement;
    }




    private final int hashCode;
    private final String toString;

    private BalanceImpl(BuilderImpl builder) {
        this.currencyCode = Objects.requireNonNull(builder.currencyCode, "Property 'currencyCode' is required.");
        this.balance = Objects.requireNonNull(builder.balance, "Property 'balance' is required.");
        this.availableBalance = Objects.requireNonNull(builder.availableBalance, "Property 'availableBalance' is required.");
        this.pendingSettlementAmount = Objects.requireNonNull(builder.pendingSettlementAmount, "Property 'pendingSettlementAmount' is required.");
        this.pendingPayoutAmount = Objects.requireNonNull(builder.pendingPayoutAmount, "Property 'pendingPayoutAmount' is required.");
        this.lastBalanceMovement = Objects.requireNonNull(builder.lastBalanceMovement, "Property 'lastBalanceMovement' is required.");

        this.hashCode = Objects.hash(currencyCode, balance, availableBalance, pendingSettlementAmount, pendingPayoutAmount, lastBalanceMovement);
        this.toString = builder.type + "(" +
                "currencyCode=" + currencyCode +
                ", balance=" + balance +
                ", availableBalance=" + availableBalance +
                ", pendingSettlementAmount=" + pendingSettlementAmount +
                ", pendingPayoutAmount=" + pendingPayoutAmount +
                ", lastBalanceMovement=" + lastBalanceMovement +
                ')';
    }

    @Override
    public int hashCode() {
        return hashCode;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }

        if (!(obj instanceof BalanceImpl)) {
            return false;
        }

        BalanceImpl that = (BalanceImpl) obj;
        if (!Objects.equals(this.currencyCode, that.currencyCode)) return false;
        if (!Objects.equals(this.balance, that.balance)) return false;
        if (!Objects.equals(this.availableBalance, that.availableBalance)) return false;
        if (!Objects.equals(this.pendingSettlementAmount, that.pendingSettlementAmount)) return false;
        if (!Objects.equals(this.pendingPayoutAmount, that.pendingPayoutAmount)) return false;
        if (!Objects.equals(this.lastBalanceMovement, that.lastBalanceMovement)) return false;

        return true;
    }

    @Override
    public String toString() {
        return toString;
    }

    /** Builder for {@link Balance} model class. */
    public static class BuilderImpl implements Balance.Builder {
        private CurrencyCode currencyCode = null;
        private java.math.BigDecimal balance = null;
        private java.math.BigDecimal availableBalance = null;
        private java.math.BigDecimal pendingSettlementAmount = null;
        private java.math.BigDecimal pendingPayoutAmount = null;
        private java.time.OffsetDateTime lastBalanceMovement = null;

        private final String type;

        public BuilderImpl(String type) {
            this.type = type;
        }

        public BuilderImpl() {
            this("Balance");
        }

        /**
          * Set {@link Balance#getCurrencyCode} property.
          *
          * 
          */
        @Override
        public BuilderImpl currencyCode(CurrencyCode currencyCode) {
            this.currencyCode = currencyCode;
            return this;
        }

        @Override
        public boolean isCurrencyCodeDefined() {
            return this.currencyCode != null;
        }

        /**
          * Set {@link Balance#getBalance} property.
          *
          * Current balance, may include pending payouts or settlements.
          */
        @Override
        public BuilderImpl balance(java.math.BigDecimal balance) {
            this.balance = balance;
            return this;
        }

        @Override
        public boolean isBalanceDefined() {
            return this.balance != null;
        }

        /**
          * Set {@link Balance#getAvailableBalance} property.
          *
          * Available balance, ready to use for payouts, settlements or other movements. It is calculated as balance - (pendingSettlementAmount + pendingPayoutAmount).
          */
        @Override
        public BuilderImpl availableBalance(java.math.BigDecimal availableBalance) {
            this.availableBalance = availableBalance;
            return this;
        }

        @Override
        public boolean isAvailableBalanceDefined() {
            return this.availableBalance != null;
        }

        /**
          * Set {@link Balance#getPendingSettlementAmount} property.
          *
          * Amount of settlements waiting to be processed.
          */
        @Override
        public BuilderImpl pendingSettlementAmount(java.math.BigDecimal pendingSettlementAmount) {
            this.pendingSettlementAmount = pendingSettlementAmount;
            return this;
        }

        @Override
        public boolean isPendingSettlementAmountDefined() {
            return this.pendingSettlementAmount != null;
        }

        /**
          * Set {@link Balance#getPendingPayoutAmount} property.
          *
          * Amount of payouts waiting to be processed.
          */
        @Override
        public BuilderImpl pendingPayoutAmount(java.math.BigDecimal pendingPayoutAmount) {
            this.pendingPayoutAmount = pendingPayoutAmount;
            return this;
        }

        @Override
        public boolean isPendingPayoutAmountDefined() {
            return this.pendingPayoutAmount != null;
        }

        /**
          * Set {@link Balance#getLastBalanceMovement} property.
          *
          * Date and time of last update of balance or available balance on this account.
          */
        @Override
        public BuilderImpl lastBalanceMovement(java.time.OffsetDateTime lastBalanceMovement) {
            this.lastBalanceMovement = lastBalanceMovement;
            return this;
        }

        @Override
        public boolean isLastBalanceMovementDefined() {
            return this.lastBalanceMovement != null;
        }

        /**
         * Create new instance of {@link Balance} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @Override
        public BalanceImpl build() {
            return new BalanceImpl(this);
        }

    }
}