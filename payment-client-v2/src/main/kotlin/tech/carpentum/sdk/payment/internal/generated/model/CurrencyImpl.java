//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** Currency
 *
 * 
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public class CurrencyImpl implements Currency {
    private final CurrencyCode code;

    @Override
    public CurrencyCode getCode() {
        return code;
    }




    private final int hashCode;
    private final String toString;

    private CurrencyImpl(BuilderImpl builder) {
        this.code = Objects.requireNonNull(builder.code, "Property 'code' is required.");

        this.hashCode = Objects.hash(code);
        this.toString = builder.type + "(" +
                "code=" + code +
                ')';
    }

    @Override
    public int hashCode() {
        return hashCode;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }

        if (!(obj instanceof CurrencyImpl)) {
            return false;
        }

        CurrencyImpl that = (CurrencyImpl) obj;
        if (!Objects.equals(this.code, that.code)) return false;

        return true;
    }

    @Override
    public String toString() {
        return toString;
    }

    /** Builder for {@link Currency} model class. */
    public static class BuilderImpl implements Currency.Builder {
        private CurrencyCode code = null;

        private final String type;

        public BuilderImpl(String type) {
            this.type = type;
        }

        public BuilderImpl() {
            this("Currency");
        }

        /**
          * Set {@link Currency#getCode} property.
          *
          * 
          */
        @Override
        public BuilderImpl code(CurrencyCode code) {
            this.code = code;
            return this;
        }

        @Override
        public boolean isCodeDefined() {
            return this.code != null;
        }

        /**
         * Create new instance of {@link Currency} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @Override
        public CurrencyImpl build() {
            return new CurrencyImpl(this);
        }

    }
}