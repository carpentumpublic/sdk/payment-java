//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.AccountPayinRequestCodePayment

class AccountPayinRequestCodePaymentJsonAdapter {
    @FromJson
    fun fromJson(json: AccountPayinRequestCodePaymentJson): AccountPayinRequestCodePayment {
        val builder = AccountPayinRequestCodePayment.builder()
        builder.accountName(json.accountName)
        return builder.build()
    }

    @ToJson
    fun toJson(model: AccountPayinRequestCodePayment): AccountPayinRequestCodePaymentJson {
        val json = AccountPayinRequestCodePaymentJson()
        json.accountName = model.accountName
        return json
    }

    @FromJson
    fun fromJsonImpl(model: AccountPayinRequestCodePayment): AccountPayinRequestCodePaymentImpl {
        return model as AccountPayinRequestCodePaymentImpl
    }

    @ToJson
    fun toJsonImpl(impl: AccountPayinRequestCodePaymentImpl): AccountPayinRequestCodePayment {
        return impl
    }

}