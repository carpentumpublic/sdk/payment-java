//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** CRYPTO_TRANSFER
 *
 * Payment method for sending funds from your crypto wallet to external crypto wallet.
It requires customer to provide a receiving crypto wallet details in format related to chosen blockchain protocol.
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public interface SettlementCryptoTransferMethod extends SettlementMethod {
    /** A discriminator value of property {@link #getPaymentMethodCode}. The model class extends {@link SettlementMethod}. */
    SettlementMethod.PaymentMethodCode PAYMENT_METHOD_CODE = SettlementMethod.PaymentMethodCode.CRYPTO_TRANSFER;

    @NotNull AccountSettlementRequestCryptoTransfer getAccount();

    /** One of following can serve as Payment Operator:

 * Financial or other institution (such as bank, card payment processor, ...) that manages transactions for your customers
 * Mobile wallet
 * Blockchain protocol for crypto currency payments

Customer is informed with the payment instructions where funds have to be transferred efficiently based on the selected Payment operator.
For getting list of the available payment options for payins use [POST /payins/!availablePaymentOptions](#operations-Incoming_payments-availablePaymentOptions) API, for payouts use [POST /payouts/!availablePaymentOptions](#operations-Outgoing_payments-availablePaymentOptions) API. */
    @NotNull String getPaymentOperatorCode();

    /** Reference number of transaction. */
    @NotNull Optional<String> getTransactionReference();

    @NotNull static Builder builder(SettlementCryptoTransferMethod copyOf) {
        Builder builder = builder();
        builder.account(copyOf.getAccount());
        builder.paymentOperatorCode(copyOf.getPaymentOperatorCode());
        builder.transactionReference(copyOf.getTransactionReference().orElse(null));
        return builder;
    }

    @NotNull static Builder builder() {
        return new SettlementCryptoTransferMethodImpl.BuilderImpl();
    }

    /** Builder for {@link SettlementCryptoTransferMethod} model class. */
    interface Builder {

        /**
          * Set {@link SettlementCryptoTransferMethod#getAccount} property.
          *
          * 
          */
        @NotNull Builder account(AccountSettlementRequestCryptoTransfer account);

        boolean isAccountDefined();


        /**
          * Set {@link SettlementCryptoTransferMethod#getPaymentOperatorCode} property.
          *
          * One of following can serve as Payment Operator:

 * Financial or other institution (such as bank, card payment processor, ...) that manages transactions for your customers
 * Mobile wallet
 * Blockchain protocol for crypto currency payments

Customer is informed with the payment instructions where funds have to be transferred efficiently based on the selected Payment operator.
For getting list of the available payment options for payins use [POST /payins/!availablePaymentOptions](#operations-Incoming_payments-availablePaymentOptions) API, for payouts use [POST /payouts/!availablePaymentOptions](#operations-Outgoing_payments-availablePaymentOptions) API.
          */
        @NotNull Builder paymentOperatorCode(String paymentOperatorCode);

        boolean isPaymentOperatorCodeDefined();


        /**
          * Set {@link SettlementCryptoTransferMethod#getTransactionReference} property.
          *
          * Reference number of transaction.
          */
        @NotNull Builder transactionReference(String transactionReference);

        boolean isTransactionReferenceDefined();


        /**
         * Create new instance of {@link SettlementCryptoTransferMethod} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @NotNull SettlementCryptoTransferMethod build();

    }
}