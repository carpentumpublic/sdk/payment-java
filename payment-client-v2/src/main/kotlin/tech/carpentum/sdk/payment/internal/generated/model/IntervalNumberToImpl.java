//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** IntervalNumberTo
 *
 * 
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public class IntervalNumberToImpl implements IntervalNumberTo {
    private final Optional<java.math.BigDecimal> from;

    @Override
    public Optional<java.math.BigDecimal> getFrom() {
        return from;
    }


    private final Optional<java.math.BigDecimal> to;

    @Override
    public Optional<java.math.BigDecimal> getTo() {
        return to;
    }




    private final int hashCode;
    private final String toString;

    private IntervalNumberToImpl(BuilderImpl builder) {
        this.from = Optional.ofNullable(builder.from);
        this.to = Optional.ofNullable(builder.to);

        this.hashCode = Objects.hash(from, to);
        this.toString = builder.type + "(" +
                "from=" + from +
                ", to=" + to +
                ')';
    }

    @Override
    public int hashCode() {
        return hashCode;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }

        if (!(obj instanceof IntervalNumberToImpl)) {
            return false;
        }

        IntervalNumberToImpl that = (IntervalNumberToImpl) obj;
        if (!Objects.equals(this.from, that.from)) return false;
        if (!Objects.equals(this.to, that.to)) return false;

        return true;
    }

    @Override
    public String toString() {
        return toString;
    }

    /** Builder for {@link IntervalNumberTo} model class. */
    public static class BuilderImpl implements IntervalNumberTo.Builder {
        private java.math.BigDecimal from = null;
        private java.math.BigDecimal to = null;

        private final String type;

        public BuilderImpl(String type) {
            this.type = type;
        }

        public BuilderImpl() {
            this("IntervalNumberTo");
        }

        /**
          * Set {@link IntervalNumberTo#getFrom} property.
          *
          * 
          */
        @Override
        public BuilderImpl from(java.math.BigDecimal from) {
            this.from = from;
            return this;
        }

        @Override
        public boolean isFromDefined() {
            return this.from != null;
        }

        /**
          * Set {@link IntervalNumberTo#getTo} property.
          *
          * 
          */
        @Override
        public BuilderImpl to(java.math.BigDecimal to) {
            this.to = to;
            return this;
        }

        @Override
        public boolean isToDefined() {
            return this.to != null;
        }

        /**
         * Create new instance of {@link IntervalNumberTo} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @Override
        public IntervalNumberToImpl build() {
            return new IntervalNumberToImpl(this);
        }

    }
}