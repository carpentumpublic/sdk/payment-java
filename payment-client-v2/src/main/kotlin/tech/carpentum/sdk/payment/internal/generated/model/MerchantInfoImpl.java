//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** MerchantInfo
 *
 * Merchant information.
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public class MerchantInfoImpl implements MerchantInfo {
    private final ChannelInfo channelInfo;

    @Override
    public ChannelInfo getChannelInfo() {
        return channelInfo;
    }




    private final int hashCode;
    private final String toString;

    private MerchantInfoImpl(BuilderImpl builder) {
        this.channelInfo = Objects.requireNonNull(builder.channelInfo, "Property 'channelInfo' is required.");

        this.hashCode = Objects.hash(channelInfo);
        this.toString = builder.type + "(" +
                "channelInfo=" + channelInfo +
                ')';
    }

    @Override
    public int hashCode() {
        return hashCode;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }

        if (!(obj instanceof MerchantInfoImpl)) {
            return false;
        }

        MerchantInfoImpl that = (MerchantInfoImpl) obj;
        if (!Objects.equals(this.channelInfo, that.channelInfo)) return false;

        return true;
    }

    @Override
    public String toString() {
        return toString;
    }

    /** Builder for {@link MerchantInfo} model class. */
    public static class BuilderImpl implements MerchantInfo.Builder {
        private ChannelInfo channelInfo = null;

        private final String type;

        public BuilderImpl(String type) {
            this.type = type;
        }

        public BuilderImpl() {
            this("MerchantInfo");
        }

        /**
          * Set {@link MerchantInfo#getChannelInfo} property.
          *
          * 
          */
        @Override
        public BuilderImpl channelInfo(ChannelInfo channelInfo) {
            this.channelInfo = channelInfo;
            return this;
        }

        @Override
        public boolean isChannelInfoDefined() {
            return this.channelInfo != null;
        }

        /**
         * Create new instance of {@link MerchantInfo} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @Override
        public MerchantInfoImpl build() {
            return new MerchantInfoImpl(this);
        }

    }
}