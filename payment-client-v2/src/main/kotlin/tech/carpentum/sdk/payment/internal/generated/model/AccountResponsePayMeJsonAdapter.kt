//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.AccountResponsePayMe

class AccountResponsePayMeJsonAdapter {
    @FromJson
    fun fromJson(json: AccountResponsePayMeJson): AccountResponsePayMe {
        val builder = AccountResponsePayMe.builder()
        builder.accountName(json.accountName)
        return builder.build()
    }

    @ToJson
    fun toJson(model: AccountResponsePayMe): AccountResponsePayMeJson {
        val json = AccountResponsePayMeJson()
        json.accountName = model.accountName.orElse(null)
        return json
    }

    @FromJson
    fun fromJsonImpl(model: AccountResponsePayMe): AccountResponsePayMeImpl {
        return model as AccountResponsePayMeImpl
    }

    @ToJson
    fun toJsonImpl(impl: AccountResponsePayMeImpl): AccountResponsePayMe {
        return impl
    }

}