//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** SettlementMethodCode
 *
 * The `SettlementMethodCode` represents settlement payment method available for a payment.

 - CRYPTO_TRANSFER - A payment method for sending funds from your crypto wallet to external crypto wallet.
 *
 * 
 *
 * The model class is immutable.
 *
 * Use static {@link #of} method to create a new model class instance.
 */
public interface SettlementMethodCode {


    @NotNull String getValue();

    static SettlementMethodCode of(@NotNull String value) {
        return new SettlementMethodCodeImpl(value);
    }
}