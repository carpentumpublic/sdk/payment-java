//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** PayoutMethodCode
 *
 * Payment method is a payment instrument that your customers use (such as online bank transfers, VA payments, UPI and so on).
Select a payment method from the list of the methods that you have configured in your account.
The `PaymentMethodCode` represents payment method available for a payment.

 - BANK_TRANSFER - A payment method for sending funds from your account to external bank account using a country specific payment system to process bank transfers. It requires customer to provide a receiving bank account details which are required by local payment system to process bank transfers.
 - CRYPTO_TRANSFER - A payment method for sending funds from your crypto wallet to external crypto wallet. It requires customer to provide a receiving crypto wallet details in format related to a chosen blockchain protocol.
 - WALLET_TRANSFER - A payment method for sending funds from your account to external wallet. It requires customer to provide a receiving wallet details in format related to the provider of a wallet.
 *
 * 
 *
 * The model class is immutable.
 *
 * Use static {@link #of} method to create a new model class instance.
 */
@JsonClass(generateAdapter = false)
public class PayoutMethodCodeImpl implements PayoutMethodCode {
    private final @NotNull String value;

    private final int hashCode;
    private final String toString;

    public PayoutMethodCodeImpl(@NotNull String value) {
        this.value = Objects.requireNonNull(value, "Property 'PayoutMethodCode' is required.");

        this.hashCode = value.hashCode();
        this.toString = String.valueOf(value);
    }

    @Override
    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return toString;
    }

    @Override
    public int hashCode() {
        return hashCode;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }

        if (!(obj instanceof PayoutMethodCodeImpl)) {
            return false;
        }

        PayoutMethodCodeImpl that = (PayoutMethodCodeImpl) obj;
        if (!this.value.equals(that.value)) return false;

        return true;
    }
}