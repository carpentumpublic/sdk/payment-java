//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** IMPS
 *
 * It is India market version of the Offline Bank Transfer which differs in the data which need to be collected and used during a Payment process.
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public class IMPSMethodImpl implements IMPSMethod {
    private final AccountPayinRequestIMPS account;

    @Override
    public AccountPayinRequestIMPS getAccount() {
        return account;
    }


    /** Your customer e-mail address in RFC 5322 format that is used for identification of the customer's payins. */
    private final Optional<String> emailAddress;

    @Override
    public Optional<String> getEmailAddress() {
        return emailAddress;
    }


    @Override public PaymentMethodCode getPaymentMethodCode() { return PAYMENT_METHOD_CODE; }

    private final int hashCode;
    private final String toString;

    private IMPSMethodImpl(BuilderImpl builder) {
        this.account = Objects.requireNonNull(builder.account, "Property 'account' is required.");
        this.emailAddress = Optional.ofNullable(builder.emailAddress);

        this.hashCode = Objects.hash(account, emailAddress);
        this.toString = builder.type + "(" +
                "account=" + account +
                ", emailAddress=" + emailAddress +
                ')';
    }

    @Override
    public int hashCode() {
        return hashCode;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }

        if (!(obj instanceof IMPSMethodImpl)) {
            return false;
        }

        IMPSMethodImpl that = (IMPSMethodImpl) obj;
        if (!Objects.equals(this.account, that.account)) return false;
        if (!Objects.equals(this.emailAddress, that.emailAddress)) return false;

        return true;
    }

    @Override
    public String toString() {
        return toString;
    }

    /** Builder for {@link IMPSMethod} model class. */
    public static class BuilderImpl implements IMPSMethod.Builder {
        private AccountPayinRequestIMPS account = null;
        private String emailAddress = null;

        private final String type;

        public BuilderImpl(String type) {
            this.type = type;
        }

        public BuilderImpl() {
            this("IMPSMethod");
        }

        /**
          * Set {@link IMPSMethod#getAccount} property.
          *
          * 
          */
        @Override
        public BuilderImpl account(AccountPayinRequestIMPS account) {
            this.account = account;
            return this;
        }

        @Override
        public boolean isAccountDefined() {
            return this.account != null;
        }

        /**
          * Set {@link IMPSMethod#getEmailAddress} property.
          *
          * Your customer e-mail address in RFC 5322 format that is used for identification of the customer's payins.
          */
        @Override
        public BuilderImpl emailAddress(String emailAddress) {
            this.emailAddress = emailAddress;
            return this;
        }

        @Override
        public boolean isEmailAddressDefined() {
            return this.emailAddress != null;
        }

        /**
         * Create new instance of {@link IMPSMethod} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @Override
        public IMPSMethodImpl build() {
            return new IMPSMethodImpl(this);
        }

    }
}