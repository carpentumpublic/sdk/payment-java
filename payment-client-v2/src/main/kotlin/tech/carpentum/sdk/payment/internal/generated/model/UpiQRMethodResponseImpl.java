//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** UPIQR
 *
 * 
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public class UpiQRMethodResponseImpl implements UpiQRMethodResponse {
    private final IdPayin idPayin;

    @Override
    public IdPayin getIdPayin() {
        return idPayin;
    }


    private final IdPayment idPayment;

    @Override
    public IdPayment getIdPayment() {
        return idPayment;
    }


    private final Optional<AccountCustomerResponseUpiQR> accountCustomer;

    @Override
    public Optional<AccountCustomerResponseUpiQR> getAccountCustomer() {
        return accountCustomer;
    }


    private final MoneyPaymentResponse money;

    @Override
    public MoneyPaymentResponse getMoney() {
        return money;
    }


    private final Optional<MoneyVat> vat;

    @Override
    public Optional<MoneyVat> getVat() {
        return vat;
    }


    private final String merchantName;

    @Override
    public String getMerchantName() {
        return merchantName;
    }


    /** Reference number of transaction. */
    private final String reference;

    @Override
    public String getReference() {
        return reference;
    }


    private final Optional<ExternalReference> externalReference;

    @Override
    public Optional<ExternalReference> getExternalReference() {
        return externalReference;
    }


    private final Optional<PaymentProcessor> processor;

    @Override
    public Optional<PaymentProcessor> getProcessor() {
        return processor;
    }


    /** The name of the QR code image to be scanned by a wallet or payment service compatible with this payment method. The QR code of the image can be labeled by qrName to increase the clarity of the payment instruction.
If this parameter contains any value, include it in the payment instructions for your customer. */
    private final String qrName;

    @Override
    public String getQrName() {
        return qrName;
    }


    /** The URL of the QR code image to be scanned by a wallet or payment service compatible with this payment method. The QR code encodes the instructions how make a payment.
If this parameter contains any value, include it in the payment instructions for your customer. */
    private final String qrCode;

    @Override
    public String getQrCode() {
        return qrCode;
    }


    /** It can be used as deep link button target (what is typically known as an intent trigger)
or to generate a QR code that can be scanned with any UPI enabled app. */
    private final Optional<String> upiQrDeepLink;

    @Override
    public Optional<String> getUpiQrDeepLink() {
        return upiQrDeepLink;
    }


    private final Optional<SelectedPaymentOperatorIncoming> paymentOperator;

    @Override
    public Optional<SelectedPaymentOperatorIncoming> getPaymentOperator() {
        return paymentOperator;
    }


    /** Virtual payment address where we expect that your customer sends funds to make a payment. This parameter is to be shown to your customer. */
    private final Optional<String> upiId;

    @Override
    public Optional<String> getUpiId() {
        return upiId;
    }


    /** Virtual payment address of the customer on UPI (Unified Payment Interface) */
    private final Optional<String> upiIdCustomer;

    @Override
    public Optional<String> getUpiIdCustomer() {
        return upiIdCustomer;
    }


    /** Your customer mobile phone number in full international telephone number format, including country code. */
    private final Optional<String> phoneNumber;

    @Override
    public Optional<String> getPhoneNumber() {
        return phoneNumber;
    }


    /** This is the URL where the customers will be redirected after completing a payment.

The URL must be either IP or domain-based. */
    private final String returnUrl;

    @Override
    public String getReturnUrl() {
        return returnUrl;
    }


    /** Date and time when payment was accepted. */
    private final java.time.OffsetDateTime acceptedAt;

    @Override
    public java.time.OffsetDateTime getAcceptedAt() {
        return acceptedAt;
    }


    /** Date and time of payment expiration. If no money has been transferred to this time, payment is considered failed and callback with status change event will shortly follow. */
    private final java.time.OffsetDateTime expireAt;

    @Override
    public java.time.OffsetDateTime getExpireAt() {
        return expireAt;
    }


    @Override public PaymentMethodCode getPaymentMethodCode() { return PAYMENT_METHOD_CODE; }

    private final int hashCode;
    private final String toString;

    private UpiQRMethodResponseImpl(BuilderImpl builder) {
        this.idPayin = Objects.requireNonNull(builder.idPayin, "Property 'idPayin' is required.");
        this.idPayment = Objects.requireNonNull(builder.idPayment, "Property 'idPayment' is required.");
        this.accountCustomer = Optional.ofNullable(builder.accountCustomer);
        this.money = Objects.requireNonNull(builder.money, "Property 'money' is required.");
        this.vat = Optional.ofNullable(builder.vat);
        this.merchantName = Objects.requireNonNull(builder.merchantName, "Property 'merchantName' is required.");
        this.reference = Objects.requireNonNull(builder.reference, "Property 'reference' is required.");
        this.externalReference = Optional.ofNullable(builder.externalReference);
        this.processor = Optional.ofNullable(builder.processor);
        this.qrName = Objects.requireNonNull(builder.qrName, "Property 'qrName' is required.");
        this.qrCode = Objects.requireNonNull(builder.qrCode, "Property 'qrCode' is required.");
        this.upiQrDeepLink = Optional.ofNullable(builder.upiQrDeepLink);
        this.paymentOperator = Optional.ofNullable(builder.paymentOperator);
        this.upiId = Optional.ofNullable(builder.upiId);
        this.upiIdCustomer = Optional.ofNullable(builder.upiIdCustomer);
        this.phoneNumber = Optional.ofNullable(builder.phoneNumber);
        this.returnUrl = Objects.requireNonNull(builder.returnUrl, "Property 'returnUrl' is required.");
        this.acceptedAt = Objects.requireNonNull(builder.acceptedAt, "Property 'acceptedAt' is required.");
        this.expireAt = Objects.requireNonNull(builder.expireAt, "Property 'expireAt' is required.");

        this.hashCode = Objects.hash(idPayin, idPayment, accountCustomer, money, vat, merchantName, reference, externalReference, processor, qrName, qrCode, upiQrDeepLink, paymentOperator, upiId, upiIdCustomer, phoneNumber, returnUrl, acceptedAt, expireAt);
        this.toString = builder.type + "(" +
                "idPayin=" + idPayin +
                ", idPayment=" + idPayment +
                ", accountCustomer=" + accountCustomer +
                ", money=" + money +
                ", vat=" + vat +
                ", merchantName=" + merchantName +
                ", reference=" + reference +
                ", externalReference=" + externalReference +
                ", processor=" + processor +
                ", qrName=" + qrName +
                ", qrCode=" + qrCode +
                ", upiQrDeepLink=" + upiQrDeepLink +
                ", paymentOperator=" + paymentOperator +
                ", upiId=" + upiId +
                ", upiIdCustomer=" + upiIdCustomer +
                ", phoneNumber=" + phoneNumber +
                ", returnUrl=" + returnUrl +
                ", acceptedAt=" + acceptedAt +
                ", expireAt=" + expireAt +
                ')';
    }

    @Override
    public int hashCode() {
        return hashCode;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }

        if (!(obj instanceof UpiQRMethodResponseImpl)) {
            return false;
        }

        UpiQRMethodResponseImpl that = (UpiQRMethodResponseImpl) obj;
        if (!Objects.equals(this.idPayin, that.idPayin)) return false;
        if (!Objects.equals(this.idPayment, that.idPayment)) return false;
        if (!Objects.equals(this.accountCustomer, that.accountCustomer)) return false;
        if (!Objects.equals(this.money, that.money)) return false;
        if (!Objects.equals(this.vat, that.vat)) return false;
        if (!Objects.equals(this.merchantName, that.merchantName)) return false;
        if (!Objects.equals(this.reference, that.reference)) return false;
        if (!Objects.equals(this.externalReference, that.externalReference)) return false;
        if (!Objects.equals(this.processor, that.processor)) return false;
        if (!Objects.equals(this.qrName, that.qrName)) return false;
        if (!Objects.equals(this.qrCode, that.qrCode)) return false;
        if (!Objects.equals(this.upiQrDeepLink, that.upiQrDeepLink)) return false;
        if (!Objects.equals(this.paymentOperator, that.paymentOperator)) return false;
        if (!Objects.equals(this.upiId, that.upiId)) return false;
        if (!Objects.equals(this.upiIdCustomer, that.upiIdCustomer)) return false;
        if (!Objects.equals(this.phoneNumber, that.phoneNumber)) return false;
        if (!Objects.equals(this.returnUrl, that.returnUrl)) return false;
        if (!Objects.equals(this.acceptedAt, that.acceptedAt)) return false;
        if (!Objects.equals(this.expireAt, that.expireAt)) return false;

        return true;
    }

    @Override
    public String toString() {
        return toString;
    }

    /** Builder for {@link UpiQRMethodResponse} model class. */
    public static class BuilderImpl implements UpiQRMethodResponse.Builder {
        private IdPayin idPayin = null;
        private IdPayment idPayment = null;
        private AccountCustomerResponseUpiQR accountCustomer = null;
        private MoneyPaymentResponse money = null;
        private MoneyVat vat = null;
        private String merchantName = null;
        private String reference = null;
        private ExternalReference externalReference = null;
        private PaymentProcessor processor = null;
        private String qrName = null;
        private String qrCode = null;
        private String upiQrDeepLink = null;
        private SelectedPaymentOperatorIncoming paymentOperator = null;
        private String upiId = null;
        private String upiIdCustomer = null;
        private String phoneNumber = null;
        private String returnUrl = null;
        private java.time.OffsetDateTime acceptedAt = null;
        private java.time.OffsetDateTime expireAt = null;

        private final String type;

        public BuilderImpl(String type) {
            this.type = type;
        }

        public BuilderImpl() {
            this("UpiQRMethodResponse");
        }

        /**
          * Set {@link UpiQRMethodResponse#getIdPayin} property.
          *
          * 
          */
        @Override
        public BuilderImpl idPayin(IdPayin idPayin) {
            this.idPayin = idPayin;
            return this;
        }

        @Override
        public boolean isIdPayinDefined() {
            return this.idPayin != null;
        }

        /**
          * Set {@link UpiQRMethodResponse#getIdPayment} property.
          *
          * 
          */
        @Override
        public BuilderImpl idPayment(IdPayment idPayment) {
            this.idPayment = idPayment;
            return this;
        }

        @Override
        public boolean isIdPaymentDefined() {
            return this.idPayment != null;
        }

        /**
          * Set {@link UpiQRMethodResponse#getAccountCustomer} property.
          *
          * 
          */
        @Override
        public BuilderImpl accountCustomer(AccountCustomerResponseUpiQR accountCustomer) {
            this.accountCustomer = accountCustomer;
            return this;
        }

        @Override
        public boolean isAccountCustomerDefined() {
            return this.accountCustomer != null;
        }

        /**
          * Set {@link UpiQRMethodResponse#getMoney} property.
          *
          * 
          */
        @Override
        public BuilderImpl money(MoneyPaymentResponse money) {
            this.money = money;
            return this;
        }

        @Override
        public boolean isMoneyDefined() {
            return this.money != null;
        }

        /**
          * Set {@link UpiQRMethodResponse#getVat} property.
          *
          * 
          */
        @Override
        public BuilderImpl vat(MoneyVat vat) {
            this.vat = vat;
            return this;
        }

        @Override
        public boolean isVatDefined() {
            return this.vat != null;
        }

        /**
          * Set {@link UpiQRMethodResponse#getMerchantName} property.
          *
          * 
          */
        @Override
        public BuilderImpl merchantName(String merchantName) {
            this.merchantName = merchantName;
            return this;
        }

        @Override
        public boolean isMerchantNameDefined() {
            return this.merchantName != null;
        }

        /**
          * Set {@link UpiQRMethodResponse#getReference} property.
          *
          * Reference number of transaction.
          */
        @Override
        public BuilderImpl reference(String reference) {
            this.reference = reference;
            return this;
        }

        @Override
        public boolean isReferenceDefined() {
            return this.reference != null;
        }

        /**
          * Set {@link UpiQRMethodResponse#getExternalReference} property.
          *
          * 
          */
        @Override
        public BuilderImpl externalReference(ExternalReference externalReference) {
            this.externalReference = externalReference;
            return this;
        }

        @Override
        public boolean isExternalReferenceDefined() {
            return this.externalReference != null;
        }

        /**
          * Set {@link UpiQRMethodResponse#getProcessor} property.
          *
          * 
          */
        @Override
        public BuilderImpl processor(PaymentProcessor processor) {
            this.processor = processor;
            return this;
        }

        @Override
        public boolean isProcessorDefined() {
            return this.processor != null;
        }

        /**
          * Set {@link UpiQRMethodResponse#getQrName} property.
          *
          * The name of the QR code image to be scanned by a wallet or payment service compatible with this payment method. The QR code of the image can be labeled by qrName to increase the clarity of the payment instruction.
If this parameter contains any value, include it in the payment instructions for your customer.
          */
        @Override
        public BuilderImpl qrName(String qrName) {
            this.qrName = qrName;
            return this;
        }

        @Override
        public boolean isQrNameDefined() {
            return this.qrName != null;
        }

        /**
          * Set {@link UpiQRMethodResponse#getQrCode} property.
          *
          * The URL of the QR code image to be scanned by a wallet or payment service compatible with this payment method. The QR code encodes the instructions how make a payment.
If this parameter contains any value, include it in the payment instructions for your customer.
          */
        @Override
        public BuilderImpl qrCode(String qrCode) {
            this.qrCode = qrCode;
            return this;
        }

        @Override
        public boolean isQrCodeDefined() {
            return this.qrCode != null;
        }

        /**
          * Set {@link UpiQRMethodResponse#getUpiQrDeepLink} property.
          *
          * It can be used as deep link button target (what is typically known as an intent trigger)
or to generate a QR code that can be scanned with any UPI enabled app.
          */
        @Override
        public BuilderImpl upiQrDeepLink(String upiQrDeepLink) {
            this.upiQrDeepLink = upiQrDeepLink;
            return this;
        }

        @Override
        public boolean isUpiQrDeepLinkDefined() {
            return this.upiQrDeepLink != null;
        }

        /**
          * Set {@link UpiQRMethodResponse#getPaymentOperator} property.
          *
          * 
          */
        @Override
        public BuilderImpl paymentOperator(SelectedPaymentOperatorIncoming paymentOperator) {
            this.paymentOperator = paymentOperator;
            return this;
        }

        @Override
        public boolean isPaymentOperatorDefined() {
            return this.paymentOperator != null;
        }

        /**
          * Set {@link UpiQRMethodResponse#getUpiId} property.
          *
          * Virtual payment address where we expect that your customer sends funds to make a payment. This parameter is to be shown to your customer.
          */
        @Override
        public BuilderImpl upiId(String upiId) {
            this.upiId = upiId;
            return this;
        }

        @Override
        public boolean isUpiIdDefined() {
            return this.upiId != null;
        }

        /**
          * Set {@link UpiQRMethodResponse#getUpiIdCustomer} property.
          *
          * Virtual payment address of the customer on UPI (Unified Payment Interface)
          */
        @Override
        public BuilderImpl upiIdCustomer(String upiIdCustomer) {
            this.upiIdCustomer = upiIdCustomer;
            return this;
        }

        @Override
        public boolean isUpiIdCustomerDefined() {
            return this.upiIdCustomer != null;
        }

        /**
          * Set {@link UpiQRMethodResponse#getPhoneNumber} property.
          *
          * Your customer mobile phone number in full international telephone number format, including country code.
          */
        @Override
        public BuilderImpl phoneNumber(String phoneNumber) {
            this.phoneNumber = phoneNumber;
            return this;
        }

        @Override
        public boolean isPhoneNumberDefined() {
            return this.phoneNumber != null;
        }

        /**
          * Set {@link UpiQRMethodResponse#getReturnUrl} property.
          *
          * This is the URL where the customers will be redirected after completing a payment.

The URL must be either IP or domain-based.
          */
        @Override
        public BuilderImpl returnUrl(String returnUrl) {
            this.returnUrl = returnUrl;
            return this;
        }

        @Override
        public boolean isReturnUrlDefined() {
            return this.returnUrl != null;
        }

        /**
          * Set {@link UpiQRMethodResponse#getAcceptedAt} property.
          *
          * Date and time when payment was accepted.
          */
        @Override
        public BuilderImpl acceptedAt(java.time.OffsetDateTime acceptedAt) {
            this.acceptedAt = acceptedAt;
            return this;
        }

        @Override
        public boolean isAcceptedAtDefined() {
            return this.acceptedAt != null;
        }

        /**
          * Set {@link UpiQRMethodResponse#getExpireAt} property.
          *
          * Date and time of payment expiration. If no money has been transferred to this time, payment is considered failed and callback with status change event will shortly follow.
          */
        @Override
        public BuilderImpl expireAt(java.time.OffsetDateTime expireAt) {
            this.expireAt = expireAt;
            return this;
        }

        @Override
        public boolean isExpireAtDefined() {
            return this.expireAt != null;
        }

        /**
         * Create new instance of {@link UpiQRMethodResponse} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @Override
        public UpiQRMethodResponseImpl build() {
            return new UpiQRMethodResponseImpl(this);
        }

    }
}