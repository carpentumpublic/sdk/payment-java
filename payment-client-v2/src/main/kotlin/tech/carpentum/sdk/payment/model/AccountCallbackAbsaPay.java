//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** AccountCallbackAbsaPay
 *
 * Parameters of your customer's bank or wallet account which your customer sends funds from. These account parameters are used for the sender's account verification in processing of the payment.
Which parameters are mandatory depends on the payment method and the currency your customer choose to pay.
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public interface AccountCallbackAbsaPay {

    /** Account Name is the name of the person who holds the bank or wallet account which your customer sends funds from to make his payment.
The name should be in the same format as the account holder name of the account. Allows numbers, some special characters and UNICODE symbols, see validation pattern. */
    @NotNull String getAccountName();

    /** Account Name is the name of the person who holds the bank or wallet account which your customer sends funds from to make his payment.
The name should be in the same format as the account holder name of the account. Allows numbers, some special characters and UNICODE symbols, see validation pattern. */
    @NotNull String getAccountNumber();

    @NotNull int getAccountIndex();

    @NotNull static Builder builder(AccountCallbackAbsaPay copyOf) {
        Builder builder = builder();
        builder.accountName(copyOf.getAccountName());
        builder.accountNumber(copyOf.getAccountNumber());
        builder.accountIndex(copyOf.getAccountIndex());
        return builder;
    }

    @NotNull static Builder builder() {
        return new AccountCallbackAbsaPayImpl.BuilderImpl();
    }

    /** Builder for {@link AccountCallbackAbsaPay} model class. */
    interface Builder {

        /**
          * Set {@link AccountCallbackAbsaPay#getAccountName} property.
          *
          * Account Name is the name of the person who holds the bank or wallet account which your customer sends funds from to make his payment.
The name should be in the same format as the account holder name of the account. Allows numbers, some special characters and UNICODE symbols, see validation pattern.
          */
        @NotNull Builder accountName(String accountName);

        boolean isAccountNameDefined();


        /**
          * Set {@link AccountCallbackAbsaPay#getAccountNumber} property.
          *
          * Account Name is the name of the person who holds the bank or wallet account which your customer sends funds from to make his payment.
The name should be in the same format as the account holder name of the account. Allows numbers, some special characters and UNICODE symbols, see validation pattern.
          */
        @NotNull Builder accountNumber(String accountNumber);

        boolean isAccountNumberDefined();


        /**
          * Set {@link AccountCallbackAbsaPay#getAccountIndex} property.
          *
          * 
          */
        @NotNull Builder accountIndex(Integer accountIndex);

        boolean isAccountIndexDefined();


        /**
         * Create new instance of {@link AccountCallbackAbsaPay} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @NotNull AccountCallbackAbsaPay build();

    }
}