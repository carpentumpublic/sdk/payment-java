//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.PaymentOption

class PaymentOptionJsonAdapter {
    @FromJson
    fun fromJson(json: PaymentOptionJson): PaymentOption {
        val builder = PaymentOption.builder()
        builder.paymentTypeCode(json.paymentTypeCode)
        builder.paymentMethodCode(json.paymentMethodCode)
        builder.currencyCode(json.currencyCode)
        builder.segmentCode(json.segmentCode)
        builder.transactionAmountLimit(json.transactionAmountLimit)
        builder.isAvailable(json.isAvailable)
        builder.paymentOperators(json.paymentOperators?.toList())
        return builder.build()
    }

    @ToJson
    fun toJson(model: PaymentOption): PaymentOptionJson {
        val json = PaymentOptionJson()
        json.paymentTypeCode = model.paymentTypeCode
        json.paymentMethodCode = model.paymentMethodCode
        json.currencyCode = model.currencyCode
        json.segmentCode = model.segmentCode.orElse(null)
        json.transactionAmountLimit = model.transactionAmountLimit
        json.isAvailable = model.isAvailable
        json.paymentOperators = model.paymentOperators.ifEmpty { null }
        return json
    }

    @FromJson
    fun fromJsonImpl(model: PaymentOption): PaymentOptionImpl {
        return model as PaymentOptionImpl
    }

    @ToJson
    fun toJsonImpl(impl: PaymentOptionImpl): PaymentOption {
        return impl
    }

}