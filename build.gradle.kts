import me.qoomon.gradle.gitversioning.GitVersioningPluginConfig
import me.qoomon.gradle.gitversioning.GitVersioningPluginConfig.*

plugins {
    id("me.qoomon.git-versioning") version "6.4.3"
    id("io.github.gradle-nexus.publish-plugin") version "2.0.0"
    id("com.github.ben-manes.versions") version "0.51.0"
    id("se.patrikerdes.use-latest-versions") version "0.2.18" // gradle useLatestVersions
}

tasks.register("printVersion") {
    group = "Development"
    doLast {
        println(project.version)
    }
}

gitVersioning.apply {
    refs {
        tag("v(?<version>[0-9].*)") {
            version = "\${ref.version}\${dirty.snapshot}"
        }
        tag("snapshot-(?<version>.+)") {
            version = "\${ref.version}\${dirty}-SNAPSHOT"
        }
        tag(".+") {
            version = "\${ref.slug}-\${commit.short}\${dirty.snapshot}"
        }
        branch("master") {
            version = "master-\${commit.short}\${dirty.snapshot}"
        }
        branch(".+") {
            version = "\${ref.slug}-\${commit.short}\${dirty.snapshot}"
        }
    }
}

nexusPublishing {
    repositories {
        create("mavenCentral") {
            val ossrhUsername: String? by project
            val ossrhPassword: String? by project
            // see https://status.maven.org/ in case of artifact publishing issues
            nexusUrl.set(uri("https://s01.oss.sonatype.org/service/local/"))
            snapshotRepositoryUrl.set(uri("https://s01.oss.sonatype.org/content/repositories/snapshots/"))
            username.set(ossrhUsername)
            password.set(ossrhPassword)
        }
    }
}
