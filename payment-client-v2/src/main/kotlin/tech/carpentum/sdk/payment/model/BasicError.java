//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** BasicError
 *
 * 
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public interface BasicError {

    @NotNull Optional<String> getDescription();

    @NotNull static BasicError ofDescription(String description) { return builder().description(description).build(); }

    @NotNull static Builder builder() {
        return new BasicErrorImpl.BuilderImpl();
    }

    /** Builder for {@link BasicError} model class. */
    interface Builder {

        /**
          * Set {@link BasicError#getDescription} property.
          *
          * 
          */
        @NotNull Builder description(String description);

        boolean isDescriptionDefined();


        /**
         * Create new instance of {@link BasicError} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @NotNull BasicError build();

    }
}