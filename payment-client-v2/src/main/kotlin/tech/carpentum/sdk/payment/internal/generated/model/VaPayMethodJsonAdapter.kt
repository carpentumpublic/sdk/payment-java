//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.VaPayMethod

class VaPayMethodJsonAdapter {
    @FromJson
    fun fromJson(json: VaPayMethodJson): VaPayMethod {
        val builder = VaPayMethod.builder()
        builder.account(json.account)
        builder.paymentOperatorCode(json.paymentOperatorCode)
        builder.emailAddress(json.emailAddress)
        return builder.build()
    }

    @ToJson
    fun toJson(model: VaPayMethod): VaPayMethodJson {
        val json = VaPayMethodJson()
        json.account = model.account.orElse(null)
        json.paymentOperatorCode = model.paymentOperatorCode.orElse(null)
        json.emailAddress = model.emailAddress.orElse(null)
        json.paymentMethodCode = model.paymentMethodCode.name
        return json
    }

    @FromJson
    fun fromJsonImpl(model: VaPayMethod): VaPayMethodImpl {
        return model as VaPayMethodImpl
    }

    @ToJson
    fun toJsonImpl(impl: VaPayMethodImpl): VaPayMethod {
        return impl
    }

}