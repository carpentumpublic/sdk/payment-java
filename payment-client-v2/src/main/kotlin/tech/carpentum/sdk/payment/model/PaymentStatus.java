//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** PaymentStatus
 *
 * * `PROCESSING` - payment process is not finished yet. Success or failure of payment cannot be determined at this time.
* `SUCCESS` - payment has been successfully processed.
* `FAILED` - payment has been accepted by platform and failed during processing. Details can be found in `failureReasons` attribute.
* `REFUSED` - payment has not been accepted by platform for processing. Details can be found in `failureReasons` attribute.

For every status change new callback `payment.statusChange` is sent. See more info about [callbacks](general.html#callbacks).
 *
 * 
 *
 * The model class is immutable.
 *
 * Use static {@link #of} method to create a new model class instance.
 */
public interface PaymentStatus {
    PaymentStatus PROCESSING = PaymentStatus.of("PROCESSING");
    PaymentStatus SUCCESS = PaymentStatus.of("SUCCESS");
    PaymentStatus FAILED = PaymentStatus.of("FAILED");
    PaymentStatus REFUSED = PaymentStatus.of("REFUSED");

    @NotNull String getValue();

    static PaymentStatus of(@NotNull String value) {
        return new PaymentStatusImpl(value);
    }
}