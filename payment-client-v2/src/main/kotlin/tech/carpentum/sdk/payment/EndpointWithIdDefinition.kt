package tech.carpentum.sdk.payment

/**
 * Definition of parametrized endpoint.
 *
 * Used by individual API classes:
 *
 * - [IncomingPaymentsApi]
 * - [OutgoingPaymentsApi]
 *
 * See also [EndpointDefinition].
 */
data class EndpointWithIdDefinition(
    val method: EndpointDefinition.Method,
    val resource: String
) {
    init {
        require(resource.contains(idPlaceholder)) { "Resource must contain '$idPlaceholder'!" }
    }

    fun forId(id: String): EndpointDefinition {
        return EndpointDefinition(method, resource.replace(idPlaceholder, id))
    }

    fun anyId(): EndpointDefinition {
        return EndpointDefinition(method, resource.replace(idPlaceholder, ".*"))
    }

    private companion object {
        private const val idPlaceholder = "{id}"
    }

}
