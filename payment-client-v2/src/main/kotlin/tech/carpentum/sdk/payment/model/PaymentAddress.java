//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** PaymentAddress
 *
 * Specifies the payment address (e.g. in UPI payment system) where customer can send the payment using theirs wallets or others banking services.
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public interface PaymentAddress {

    /** The name of the payment address ID e.g. UPI ID/VPA or GPAY ID. */
    @NotNull String getName();

    /** The payment address ID where customer can send the payment. */
    @NotNull String getIdAddress();

    @NotNull static Builder builder(PaymentAddress copyOf) {
        Builder builder = builder();
        builder.name(copyOf.getName());
        builder.idAddress(copyOf.getIdAddress());
        return builder;
    }

    @NotNull static Builder builder() {
        return new PaymentAddressImpl.BuilderImpl();
    }

    /** Builder for {@link PaymentAddress} model class. */
    interface Builder {

        /**
          * Set {@link PaymentAddress#getName} property.
          *
          * The name of the payment address ID e.g. UPI ID/VPA or GPAY ID.
          */
        @NotNull Builder name(String name);

        boolean isNameDefined();


        /**
          * Set {@link PaymentAddress#getIdAddress} property.
          *
          * The payment address ID where customer can send the payment.
          */
        @NotNull Builder idAddress(String idAddress);

        boolean isIdAddressDefined();


        /**
         * Create new instance of {@link PaymentAddress} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @NotNull PaymentAddress build();

    }
}