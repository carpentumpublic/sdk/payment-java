//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** SettlementPaymentOption
 *
 * 
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public interface SettlementPaymentOption {

    @NotNull SettlementMethodCode getPaymentMethodCode();

    @NotNull CurrencyCode getCurrencyCode();

    @NotNull IntervalNumberTo getTransactionAmountLimit();

    /** If set to false, the option is not currently available and must be activated in administration. */
    @NotNull Boolean getIsAvailable();

    /** Payment operators that can be used for paying via this settlement payment option. */
    @NotNull java.util.List<@NotNull PaymentOperatorOption> getPaymentOperators();

    @NotNull static Builder builder(SettlementPaymentOption copyOf) {
        Builder builder = builder();
        builder.paymentMethodCode(copyOf.getPaymentMethodCode());
        builder.currencyCode(copyOf.getCurrencyCode());
        builder.transactionAmountLimit(copyOf.getTransactionAmountLimit());
        builder.isAvailable(copyOf.getIsAvailable());
        builder.paymentOperators(copyOf.getPaymentOperators());
        return builder;
    }

    @NotNull static Builder builder() {
        return new SettlementPaymentOptionImpl.BuilderImpl();
    }

    /** Builder for {@link SettlementPaymentOption} model class. */
    interface Builder {

        /**
          * Set {@link SettlementPaymentOption#getPaymentMethodCode} property.
          *
          * 
          */
        @NotNull Builder paymentMethodCode(SettlementMethodCode paymentMethodCode);

        boolean isPaymentMethodCodeDefined();


        /**
          * Set {@link SettlementPaymentOption#getCurrencyCode} property.
          *
          * 
          */
        @NotNull Builder currencyCode(CurrencyCode currencyCode);

        boolean isCurrencyCodeDefined();


        /**
          * Set {@link SettlementPaymentOption#getTransactionAmountLimit} property.
          *
          * 
          */
        @NotNull Builder transactionAmountLimit(IntervalNumberTo transactionAmountLimit);

        boolean isTransactionAmountLimitDefined();


        /**
          * Set {@link SettlementPaymentOption#getIsAvailable} property.
          *
          * If set to false, the option is not currently available and must be activated in administration.
          */
        @NotNull Builder isAvailable(Boolean isAvailable);

        boolean isIsAvailableDefined();


        /**
          * Replace all items in {@link SettlementPaymentOption#getPaymentOperators} list property.
          *
          * Payment operators that can be used for paying via this settlement payment option.
          */
        @NotNull Builder paymentOperators(java.util.List<@NotNull PaymentOperatorOption> paymentOperators);
        /**
          * Add single item to {@link SettlementPaymentOption#getPaymentOperators} list property.
          *
          * Payment operators that can be used for paying via this settlement payment option.
          */
        @NotNull Builder paymentOperatorsAdd(PaymentOperatorOption item);
        /**
          * Add all items to {@link SettlementPaymentOption#getPaymentOperators} list property.
          *
          * Payment operators that can be used for paying via this settlement payment option.
          */
        @NotNull Builder paymentOperatorsAddAll(java.util.List<@NotNull PaymentOperatorOption> paymentOperators);


        /**
         * Create new instance of {@link SettlementPaymentOption} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @NotNull SettlementPaymentOption build();

    }
}