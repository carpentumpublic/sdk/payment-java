//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** TopUpRedirectResponse
 *
 * Customer should be redirected to specified URL for the next payment process step.
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public class TopUpAcceptedRedirectResponseImpl implements TopUpAcceptedRedirectResponse {
    private final TopUpRequested topUpRequested;

    @Override
    public TopUpRequested getTopUpRequested() {
        return topUpRequested;
    }


    private final Redirection redirectTo;

    @Override
    public Redirection getRedirectTo() {
        return redirectTo;
    }




    private final int hashCode;
    private final String toString;

    private TopUpAcceptedRedirectResponseImpl(BuilderImpl builder) {
        this.topUpRequested = Objects.requireNonNull(builder.topUpRequested, "Property 'topUpRequested' is required.");
        this.redirectTo = Objects.requireNonNull(builder.redirectTo, "Property 'redirectTo' is required.");

        this.hashCode = Objects.hash(topUpRequested, redirectTo);
        this.toString = builder.type + "(" +
                "topUpRequested=" + topUpRequested +
                ", redirectTo=" + redirectTo +
                ')';
    }

    @Override
    public int hashCode() {
        return hashCode;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }

        if (!(obj instanceof TopUpAcceptedRedirectResponseImpl)) {
            return false;
        }

        TopUpAcceptedRedirectResponseImpl that = (TopUpAcceptedRedirectResponseImpl) obj;
        if (!Objects.equals(this.topUpRequested, that.topUpRequested)) return false;
        if (!Objects.equals(this.redirectTo, that.redirectTo)) return false;

        return true;
    }

    @Override
    public String toString() {
        return toString;
    }

    /** Builder for {@link TopUpAcceptedRedirectResponse} model class. */
    public static class BuilderImpl implements TopUpAcceptedRedirectResponse.Builder {
        private TopUpRequested topUpRequested = null;
        private Redirection redirectTo = null;

        private final String type;

        public BuilderImpl(String type) {
            this.type = type;
        }

        public BuilderImpl() {
            this("TopUpAcceptedRedirectResponse");
        }

        /**
          * Set {@link TopUpAcceptedRedirectResponse#getTopUpRequested} property.
          *
          * 
          */
        @Override
        public BuilderImpl topUpRequested(TopUpRequested topUpRequested) {
            this.topUpRequested = topUpRequested;
            return this;
        }

        @Override
        public boolean isTopUpRequestedDefined() {
            return this.topUpRequested != null;
        }

        /**
          * Set {@link TopUpAcceptedRedirectResponse#getRedirectTo} property.
          *
          * 
          */
        @Override
        public BuilderImpl redirectTo(Redirection redirectTo) {
            this.redirectTo = redirectTo;
            return this;
        }

        @Override
        public boolean isRedirectToDefined() {
            return this.redirectTo != null;
        }

        /**
         * Create new instance of {@link TopUpAcceptedRedirectResponse} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @Override
        public TopUpAcceptedRedirectResponseImpl build() {
            return new TopUpAcceptedRedirectResponseImpl(this);
        }

    }
}