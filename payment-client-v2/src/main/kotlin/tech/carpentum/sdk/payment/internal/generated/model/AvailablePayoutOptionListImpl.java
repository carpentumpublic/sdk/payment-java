//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** AvailablePayoutOptionList
 *
 * The list of possible payment options.
The list always contains at least one payment option.
If no available payment options are found, an HTTP 406 response is returned with the error code.
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public class AvailablePayoutOptionListImpl implements AvailablePayoutOptionList {
    private final java.util.List<@NotNull AvailablePayoutOption> data;

    @Override
    public java.util.List<@NotNull AvailablePayoutOption> getData() {
        return data;
    }




    private final int hashCode;
    private final String toString;

    private AvailablePayoutOptionListImpl(BuilderImpl builder) {
        this.data = java.util.Collections.unmodifiableList(builder.data);

        this.hashCode = Objects.hash(data);
        this.toString = builder.type + "(" +
                "data=" + data +
                ')';
    }

    @Override
    public int hashCode() {
        return hashCode;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }

        if (!(obj instanceof AvailablePayoutOptionListImpl)) {
            return false;
        }

        AvailablePayoutOptionListImpl that = (AvailablePayoutOptionListImpl) obj;
        if (!Objects.equals(this.data, that.data)) return false;

        return true;
    }

    @Override
    public String toString() {
        return toString;
    }

    /** Builder for {@link AvailablePayoutOptionList} model class. */
    public static class BuilderImpl implements AvailablePayoutOptionList.Builder {
        private java.util.List<@NotNull AvailablePayoutOption> data = new java.util.ArrayList<>();

        private final String type;

        public BuilderImpl(String type) {
            this.type = type;
        }

        public BuilderImpl() {
            this("AvailablePayoutOptionList");
        }

        /**
          * Replace all items in {@link AvailablePayoutOptionList#getData} list property.
          *
          * 
          */
        @Override
        public BuilderImpl data(java.util.List<@NotNull AvailablePayoutOption> data) {
            this.data.clear();
            if (data != null) {
                this.data.addAll(data);
            }
            return this;
        }
        /**
          * Add single item to {@link AvailablePayoutOptionList#getData} list property.
          *
          * 
          */
        @Override
        public BuilderImpl dataAdd(AvailablePayoutOption item) {
            if (item != null) {
                this.data.add(item);
            }
            return this;
        }
        /**
          * Add all items to {@link AvailablePayoutOptionList#getData} list property.
          *
          * 
          */
        @Override
        public BuilderImpl dataAddAll(java.util.List<@NotNull AvailablePayoutOption> data) {
            if (data != null) {
                this.data.addAll(data);
            }
            return this;
        }


        /**
         * Create new instance of {@link AvailablePayoutOptionList} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @Override
        public AvailablePayoutOptionListImpl build() {
            return new AvailablePayoutOptionListImpl(this);
        }

    }
}