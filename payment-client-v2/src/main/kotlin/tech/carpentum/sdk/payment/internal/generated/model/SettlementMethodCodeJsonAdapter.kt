//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.SettlementMethodCode

class SettlementMethodCodeJsonAdapter {

    @FromJson
    fun fromJson(json: String): SettlementMethodCode {
        return SettlementMethodCode.of(json)
    }

    @ToJson
    fun toJson(model: SettlementMethodCode): String {
        return model.value
    }

    @FromJson
    fun fromJsonImpl(model: SettlementMethodCode): SettlementMethodCodeImpl {
        return model as SettlementMethodCodeImpl
    }

    @ToJson
    fun toJsonImpl(impl: SettlementMethodCodeImpl): SettlementMethodCode {
        return impl
    }

}