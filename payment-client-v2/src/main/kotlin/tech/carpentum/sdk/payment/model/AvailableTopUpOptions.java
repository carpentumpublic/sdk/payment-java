//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** AvailableTopUpOptions
 *
 * The list of possible top up options.
The list always contains at least one payment option.
If no available payment options are found, an HTTP 406 response is returned with the error code.
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public interface AvailableTopUpOptions {

    @NotNull java.util.List<@NotNull AvailableTopUpOption> getOptions();

    @NotNull static AvailableTopUpOptions ofOptions(java.util.List<@NotNull AvailableTopUpOption> options) { return builder().options(options).build(); }

    @NotNull static Builder builder() {
        return new AvailableTopUpOptionsImpl.BuilderImpl();
    }

    /** Builder for {@link AvailableTopUpOptions} model class. */
    interface Builder {

        /**
          * Replace all items in {@link AvailableTopUpOptions#getOptions} list property.
          *
          * 
          */
        @NotNull Builder options(java.util.List<@NotNull AvailableTopUpOption> options);
        /**
          * Add single item to {@link AvailableTopUpOptions#getOptions} list property.
          *
          * 
          */
        @NotNull Builder optionsAdd(AvailableTopUpOption item);
        /**
          * Add all items to {@link AvailableTopUpOptions#getOptions} list property.
          *
          * 
          */
        @NotNull Builder optionsAddAll(java.util.List<@NotNull AvailableTopUpOption> options);


        /**
         * Create new instance of {@link AvailableTopUpOptions} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @NotNull AvailableTopUpOptions build();

    }
}