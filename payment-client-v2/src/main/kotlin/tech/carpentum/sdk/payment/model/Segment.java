//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** Segment
 *
 * 
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public interface Segment {

    @NotNull SegmentCode getCode();

    @NotNull static Segment ofCode(SegmentCode code) { return builder().code(code).build(); }

    @NotNull static Builder builder() {
        return new SegmentImpl.BuilderImpl();
    }

    /** Builder for {@link Segment} model class. */
    interface Builder {

        /**
          * Set {@link Segment#getCode} property.
          *
          * 
          */
        @NotNull Builder code(SegmentCode code);

        boolean isCodeDefined();


        /**
         * Create new instance of {@link Segment} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @NotNull Segment build();

    }
}