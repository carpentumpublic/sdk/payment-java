//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** MOBILE_MONEY
 *
 * Mobile Money is a payment method which allows customers to simply complete a payment from their mobile money wallet.
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public class MobileMoneyMethodImpl implements MobileMoneyMethod {
    private final AccountPayinRequestMobileMoney account;

    @Override
    public AccountPayinRequestMobileMoney getAccount() {
        return account;
    }


    /** Your customer e-mail address in RFC 5322 format that is used for identification of the customer's payins. */
    private final Optional<String> emailAddress;

    @Override
    public Optional<String> getEmailAddress() {
        return emailAddress;
    }


    /** Your customer mobile phone number in full international telephone number format, including country code. */
    private final String phoneNumber;

    @Override
    public String getPhoneNumber() {
        return phoneNumber;
    }


    @Override public PaymentMethodCode getPaymentMethodCode() { return PAYMENT_METHOD_CODE; }

    private final int hashCode;
    private final String toString;

    private MobileMoneyMethodImpl(BuilderImpl builder) {
        this.account = Objects.requireNonNull(builder.account, "Property 'account' is required.");
        this.emailAddress = Optional.ofNullable(builder.emailAddress);
        this.phoneNumber = Objects.requireNonNull(builder.phoneNumber, "Property 'phoneNumber' is required.");

        this.hashCode = Objects.hash(account, emailAddress, phoneNumber);
        this.toString = builder.type + "(" +
                "account=" + account +
                ", emailAddress=" + emailAddress +
                ", phoneNumber=" + phoneNumber +
                ')';
    }

    @Override
    public int hashCode() {
        return hashCode;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }

        if (!(obj instanceof MobileMoneyMethodImpl)) {
            return false;
        }

        MobileMoneyMethodImpl that = (MobileMoneyMethodImpl) obj;
        if (!Objects.equals(this.account, that.account)) return false;
        if (!Objects.equals(this.emailAddress, that.emailAddress)) return false;
        if (!Objects.equals(this.phoneNumber, that.phoneNumber)) return false;

        return true;
    }

    @Override
    public String toString() {
        return toString;
    }

    /** Builder for {@link MobileMoneyMethod} model class. */
    public static class BuilderImpl implements MobileMoneyMethod.Builder {
        private AccountPayinRequestMobileMoney account = null;
        private String emailAddress = null;
        private String phoneNumber = null;

        private final String type;

        public BuilderImpl(String type) {
            this.type = type;
        }

        public BuilderImpl() {
            this("MobileMoneyMethod");
        }

        /**
          * Set {@link MobileMoneyMethod#getAccount} property.
          *
          * 
          */
        @Override
        public BuilderImpl account(AccountPayinRequestMobileMoney account) {
            this.account = account;
            return this;
        }

        @Override
        public boolean isAccountDefined() {
            return this.account != null;
        }

        /**
          * Set {@link MobileMoneyMethod#getEmailAddress} property.
          *
          * Your customer e-mail address in RFC 5322 format that is used for identification of the customer's payins.
          */
        @Override
        public BuilderImpl emailAddress(String emailAddress) {
            this.emailAddress = emailAddress;
            return this;
        }

        @Override
        public boolean isEmailAddressDefined() {
            return this.emailAddress != null;
        }

        /**
          * Set {@link MobileMoneyMethod#getPhoneNumber} property.
          *
          * Your customer mobile phone number in full international telephone number format, including country code.
          */
        @Override
        public BuilderImpl phoneNumber(String phoneNumber) {
            this.phoneNumber = phoneNumber;
            return this;
        }

        @Override
        public boolean isPhoneNumberDefined() {
            return this.phoneNumber != null;
        }

        /**
         * Create new instance of {@link MobileMoneyMethod} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @Override
        public MobileMoneyMethodImpl build() {
            return new MobileMoneyMethodImpl(this);
        }

    }
}