//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** GetPaymentOptionsError
 *
 * 
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public class GetPaymentOptionsError extends BusinessValidationError {
    // tag::codeEnum[]
    /** @see #getCode */
    public static final String CODE_MERCHANT_INACTIVE = "MERCHANT_INACTIVE";
    /** @see #getCode */
    public static final String CODE_PAYMENT_METHOD_CODE_INVALID = "PAYMENT_METHOD_CODE_INVALID";
    /** @see #getCode */
    public static final String CODE_PAYMENT_OPERATOR_CODE_INVALID = "PAYMENT_OPERATOR_CODE_INVALID";
    /** @see #getCode */
    public static final String CODE_SEGMENT_CODE_INVALID = "SEGMENT_CODE_INVALID";
    // end::codeEnum[]








    private GetPaymentOptionsError(GetPaymentOptionsError.Builder builder) {
        super(builder);
    }

    @NotNull public static Builder builder() {
        return new Builder();
    }

    /** Builder for {@link GetPaymentOptionsError} model class. */
    public static class Builder extends BusinessValidationError.Builder<GetPaymentOptionsError, Builder> {
        private Builder() {}

        /**
         * Create new instance of {@link GetPaymentOptionsError} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @NotNull public GetPaymentOptionsError build() {
            return new GetPaymentOptionsError(this);
        }
    }
}