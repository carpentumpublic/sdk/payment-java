//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** CRYPTO_TRANSFER
 *
 * Payment method for sending funds from your crypto wallet to external crypto wallet.
It requires customer to provide a receiving crypto wallet details in format related to chosen blockchain protocol.
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public interface CryptoTransferMethod extends PayoutMethod {
    /** A discriminator value of property {@link #getPaymentMethodCode}. The model class extends {@link PayoutMethod}. */
    PayoutMethod.PaymentMethodCode PAYMENT_METHOD_CODE = PayoutMethod.PaymentMethodCode.CRYPTO_TRANSFER;

    @NotNull AccountPayoutRequestCryptoTransfer getAccount();

    /** One of following can serve as Payment Operator:

 * Financial or other institution (such as bank, card payment processor, ...) that manages transactions for your customers
 * Mobile wallet
 * Blockchain protocol for crypto currency payments

Customer is informed with the payment instructions where funds have to be transferred efficiently based on the selected Payment operator.
For getting list of the available payment options for payins use [POST /payins/!availablePaymentOptions](#operations-Incoming_payments-availablePaymentOptions) API, for payouts use [POST /payouts/!availablePaymentOptions](#operations-Outgoing_payments-availablePaymentOptions) API. */
    @NotNull String getPaymentOperatorCode();

    @NotNull Optional<String> getRemark();

    @NotNull static Builder builder(CryptoTransferMethod copyOf) {
        Builder builder = builder();
        builder.account(copyOf.getAccount());
        builder.paymentOperatorCode(copyOf.getPaymentOperatorCode());
        builder.remark(copyOf.getRemark().orElse(null));
        return builder;
    }

    @NotNull static Builder builder() {
        return new CryptoTransferMethodImpl.BuilderImpl();
    }

    /** Builder for {@link CryptoTransferMethod} model class. */
    interface Builder {

        /**
          * Set {@link CryptoTransferMethod#getAccount} property.
          *
          * 
          */
        @NotNull Builder account(AccountPayoutRequestCryptoTransfer account);

        boolean isAccountDefined();


        /**
          * Set {@link CryptoTransferMethod#getPaymentOperatorCode} property.
          *
          * One of following can serve as Payment Operator:

 * Financial or other institution (such as bank, card payment processor, ...) that manages transactions for your customers
 * Mobile wallet
 * Blockchain protocol for crypto currency payments

Customer is informed with the payment instructions where funds have to be transferred efficiently based on the selected Payment operator.
For getting list of the available payment options for payins use [POST /payins/!availablePaymentOptions](#operations-Incoming_payments-availablePaymentOptions) API, for payouts use [POST /payouts/!availablePaymentOptions](#operations-Outgoing_payments-availablePaymentOptions) API.
          */
        @NotNull Builder paymentOperatorCode(String paymentOperatorCode);

        boolean isPaymentOperatorCodeDefined();


        /**
          * Set {@link CryptoTransferMethod#getRemark} property.
          *
          * 
          */
        @NotNull Builder remark(String remark);

        boolean isRemarkDefined();


        /**
         * Create new instance of {@link CryptoTransferMethod} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @NotNull CryptoTransferMethod build();

    }
}