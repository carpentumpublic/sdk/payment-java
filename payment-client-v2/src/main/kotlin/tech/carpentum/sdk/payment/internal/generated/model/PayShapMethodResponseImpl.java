//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** PAYSHAP
 *
 * 
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public class PayShapMethodResponseImpl implements PayShapMethodResponse {
    private final IdPayin idPayin;

    @Override
    public IdPayin getIdPayin() {
        return idPayin;
    }


    private final IdPayment idPayment;

    @Override
    public IdPayment getIdPayment() {
        return idPayment;
    }


    private final MoneyPaymentResponse money;

    @Override
    public MoneyPaymentResponse getMoney() {
        return money;
    }


    private final Optional<MoneyVat> vat;

    @Override
    public Optional<MoneyVat> getVat() {
        return vat;
    }


    private final String merchantName;

    @Override
    public String getMerchantName() {
        return merchantName;
    }


    private final Optional<AccountCustomerResponsePayShap> accountCustomer;

    @Override
    public Optional<AccountCustomerResponsePayShap> getAccountCustomer() {
        return accountCustomer;
    }


    /** Your customer mobile phone number. */
    private final Optional<String> phoneNumber;

    @Override
    public Optional<String> getPhoneNumber() {
        return phoneNumber;
    }


    /** Reference number of transaction. */
    private final String reference;

    @Override
    public String getReference() {
        return reference;
    }


    /** This is the URL where the customers will be redirected after completing a payment.

The URL must be either IP or domain-based. */
    private final String returnUrl;

    @Override
    public String getReturnUrl() {
        return returnUrl;
    }


    private final SelectedPaymentOperatorIncoming paymentOperator;

    @Override
    public SelectedPaymentOperatorIncoming getPaymentOperator() {
        return paymentOperator;
    }


    private final Optional<ExternalProvider> externalProvider;

    @Override
    public Optional<ExternalProvider> getExternalProvider() {
        return externalProvider;
    }


    /** Date and time when payment was accepted. */
    private final java.time.OffsetDateTime acceptedAt;

    @Override
    public java.time.OffsetDateTime getAcceptedAt() {
        return acceptedAt;
    }


    /** Date and time of payment expiration. If no money has been transferred to this time, payment is considered failed and callback with status change event will shortly follow. */
    private final java.time.OffsetDateTime expireAt;

    @Override
    public java.time.OffsetDateTime getExpireAt() {
        return expireAt;
    }


    @Override public PaymentMethodCode getPaymentMethodCode() { return PAYMENT_METHOD_CODE; }

    private final int hashCode;
    private final String toString;

    private PayShapMethodResponseImpl(BuilderImpl builder) {
        this.idPayin = Objects.requireNonNull(builder.idPayin, "Property 'idPayin' is required.");
        this.idPayment = Objects.requireNonNull(builder.idPayment, "Property 'idPayment' is required.");
        this.money = Objects.requireNonNull(builder.money, "Property 'money' is required.");
        this.vat = Optional.ofNullable(builder.vat);
        this.merchantName = Objects.requireNonNull(builder.merchantName, "Property 'merchantName' is required.");
        this.accountCustomer = Optional.ofNullable(builder.accountCustomer);
        this.phoneNumber = Optional.ofNullable(builder.phoneNumber);
        this.reference = Objects.requireNonNull(builder.reference, "Property 'reference' is required.");
        this.returnUrl = Objects.requireNonNull(builder.returnUrl, "Property 'returnUrl' is required.");
        this.paymentOperator = Objects.requireNonNull(builder.paymentOperator, "Property 'paymentOperator' is required.");
        this.externalProvider = Optional.ofNullable(builder.externalProvider);
        this.acceptedAt = Objects.requireNonNull(builder.acceptedAt, "Property 'acceptedAt' is required.");
        this.expireAt = Objects.requireNonNull(builder.expireAt, "Property 'expireAt' is required.");

        this.hashCode = Objects.hash(idPayin, idPayment, money, vat, merchantName, accountCustomer, phoneNumber, reference, returnUrl, paymentOperator, externalProvider, acceptedAt, expireAt);
        this.toString = builder.type + "(" +
                "idPayin=" + idPayin +
                ", idPayment=" + idPayment +
                ", money=" + money +
                ", vat=" + vat +
                ", merchantName=" + merchantName +
                ", accountCustomer=" + accountCustomer +
                ", phoneNumber=" + phoneNumber +
                ", reference=" + reference +
                ", returnUrl=" + returnUrl +
                ", paymentOperator=" + paymentOperator +
                ", externalProvider=" + externalProvider +
                ", acceptedAt=" + acceptedAt +
                ", expireAt=" + expireAt +
                ')';
    }

    @Override
    public int hashCode() {
        return hashCode;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }

        if (!(obj instanceof PayShapMethodResponseImpl)) {
            return false;
        }

        PayShapMethodResponseImpl that = (PayShapMethodResponseImpl) obj;
        if (!Objects.equals(this.idPayin, that.idPayin)) return false;
        if (!Objects.equals(this.idPayment, that.idPayment)) return false;
        if (!Objects.equals(this.money, that.money)) return false;
        if (!Objects.equals(this.vat, that.vat)) return false;
        if (!Objects.equals(this.merchantName, that.merchantName)) return false;
        if (!Objects.equals(this.accountCustomer, that.accountCustomer)) return false;
        if (!Objects.equals(this.phoneNumber, that.phoneNumber)) return false;
        if (!Objects.equals(this.reference, that.reference)) return false;
        if (!Objects.equals(this.returnUrl, that.returnUrl)) return false;
        if (!Objects.equals(this.paymentOperator, that.paymentOperator)) return false;
        if (!Objects.equals(this.externalProvider, that.externalProvider)) return false;
        if (!Objects.equals(this.acceptedAt, that.acceptedAt)) return false;
        if (!Objects.equals(this.expireAt, that.expireAt)) return false;

        return true;
    }

    @Override
    public String toString() {
        return toString;
    }

    /** Builder for {@link PayShapMethodResponse} model class. */
    public static class BuilderImpl implements PayShapMethodResponse.Builder {
        private IdPayin idPayin = null;
        private IdPayment idPayment = null;
        private MoneyPaymentResponse money = null;
        private MoneyVat vat = null;
        private String merchantName = null;
        private AccountCustomerResponsePayShap accountCustomer = null;
        private String phoneNumber = null;
        private String reference = null;
        private String returnUrl = null;
        private SelectedPaymentOperatorIncoming paymentOperator = null;
        private ExternalProvider externalProvider = null;
        private java.time.OffsetDateTime acceptedAt = null;
        private java.time.OffsetDateTime expireAt = null;

        private final String type;

        public BuilderImpl(String type) {
            this.type = type;
        }

        public BuilderImpl() {
            this("PayShapMethodResponse");
        }

        /**
          * Set {@link PayShapMethodResponse#getIdPayin} property.
          *
          * 
          */
        @Override
        public BuilderImpl idPayin(IdPayin idPayin) {
            this.idPayin = idPayin;
            return this;
        }

        @Override
        public boolean isIdPayinDefined() {
            return this.idPayin != null;
        }

        /**
          * Set {@link PayShapMethodResponse#getIdPayment} property.
          *
          * 
          */
        @Override
        public BuilderImpl idPayment(IdPayment idPayment) {
            this.idPayment = idPayment;
            return this;
        }

        @Override
        public boolean isIdPaymentDefined() {
            return this.idPayment != null;
        }

        /**
          * Set {@link PayShapMethodResponse#getMoney} property.
          *
          * 
          */
        @Override
        public BuilderImpl money(MoneyPaymentResponse money) {
            this.money = money;
            return this;
        }

        @Override
        public boolean isMoneyDefined() {
            return this.money != null;
        }

        /**
          * Set {@link PayShapMethodResponse#getVat} property.
          *
          * 
          */
        @Override
        public BuilderImpl vat(MoneyVat vat) {
            this.vat = vat;
            return this;
        }

        @Override
        public boolean isVatDefined() {
            return this.vat != null;
        }

        /**
          * Set {@link PayShapMethodResponse#getMerchantName} property.
          *
          * 
          */
        @Override
        public BuilderImpl merchantName(String merchantName) {
            this.merchantName = merchantName;
            return this;
        }

        @Override
        public boolean isMerchantNameDefined() {
            return this.merchantName != null;
        }

        /**
          * Set {@link PayShapMethodResponse#getAccountCustomer} property.
          *
          * 
          */
        @Override
        public BuilderImpl accountCustomer(AccountCustomerResponsePayShap accountCustomer) {
            this.accountCustomer = accountCustomer;
            return this;
        }

        @Override
        public boolean isAccountCustomerDefined() {
            return this.accountCustomer != null;
        }

        /**
          * Set {@link PayShapMethodResponse#getPhoneNumber} property.
          *
          * Your customer mobile phone number.
          */
        @Override
        public BuilderImpl phoneNumber(String phoneNumber) {
            this.phoneNumber = phoneNumber;
            return this;
        }

        @Override
        public boolean isPhoneNumberDefined() {
            return this.phoneNumber != null;
        }

        /**
          * Set {@link PayShapMethodResponse#getReference} property.
          *
          * Reference number of transaction.
          */
        @Override
        public BuilderImpl reference(String reference) {
            this.reference = reference;
            return this;
        }

        @Override
        public boolean isReferenceDefined() {
            return this.reference != null;
        }

        /**
          * Set {@link PayShapMethodResponse#getReturnUrl} property.
          *
          * This is the URL where the customers will be redirected after completing a payment.

The URL must be either IP or domain-based.
          */
        @Override
        public BuilderImpl returnUrl(String returnUrl) {
            this.returnUrl = returnUrl;
            return this;
        }

        @Override
        public boolean isReturnUrlDefined() {
            return this.returnUrl != null;
        }

        /**
          * Set {@link PayShapMethodResponse#getPaymentOperator} property.
          *
          * 
          */
        @Override
        public BuilderImpl paymentOperator(SelectedPaymentOperatorIncoming paymentOperator) {
            this.paymentOperator = paymentOperator;
            return this;
        }

        @Override
        public boolean isPaymentOperatorDefined() {
            return this.paymentOperator != null;
        }

        /**
          * Set {@link PayShapMethodResponse#getExternalProvider} property.
          *
          * 
          */
        @Override
        public BuilderImpl externalProvider(ExternalProvider externalProvider) {
            this.externalProvider = externalProvider;
            return this;
        }

        @Override
        public boolean isExternalProviderDefined() {
            return this.externalProvider != null;
        }

        /**
          * Set {@link PayShapMethodResponse#getAcceptedAt} property.
          *
          * Date and time when payment was accepted.
          */
        @Override
        public BuilderImpl acceptedAt(java.time.OffsetDateTime acceptedAt) {
            this.acceptedAt = acceptedAt;
            return this;
        }

        @Override
        public boolean isAcceptedAtDefined() {
            return this.acceptedAt != null;
        }

        /**
          * Set {@link PayShapMethodResponse#getExpireAt} property.
          *
          * Date and time of payment expiration. If no money has been transferred to this time, payment is considered failed and callback with status change event will shortly follow.
          */
        @Override
        public BuilderImpl expireAt(java.time.OffsetDateTime expireAt) {
            this.expireAt = expireAt;
            return this;
        }

        @Override
        public boolean isExpireAtDefined() {
            return this.expireAt != null;
        }

        /**
         * Create new instance of {@link PayShapMethodResponse} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @Override
        public PayShapMethodResponseImpl build() {
            return new PayShapMethodResponseImpl(this);
        }

    }
}