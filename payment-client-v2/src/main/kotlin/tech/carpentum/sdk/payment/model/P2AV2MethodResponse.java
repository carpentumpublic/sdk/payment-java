//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** P2A_V2
 *
 * The account parameters for this payment method are used to show payment instructions to the customer.
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public interface P2AV2MethodResponse extends PayinMethodResponse {
    /** A discriminator value of property {@link #getPaymentMethodCode}. The model class extends {@link PayinMethodResponse}. */
    PayinMethodResponse.PaymentMethodCode PAYMENT_METHOD_CODE = PayinMethodResponse.PaymentMethodCode.P2A_V2;

    @NotNull IdPayin getIdPayin();

    @NotNull IdPayment getIdPayment();

    @NotNull AccountResponseP2AV2 getAccount();

    @NotNull MoneyPaymentResponse getMoney();

    @NotNull Optional<MoneyVat> getVat();

    @NotNull String getMerchantName();

    @NotNull PaymentAddress getPaymentAddress();

    /** The name of the QR code image to be scanned by a wallet or payment service compatible with this payment method. The QR code of the image can be labeled by qrName to increase the clarity of the payment instruction.
If this parameter contains any value, include it in the payment instructions for your customer. */
    @NotNull String getQrName();

    /** The URL of the QR code image to be scanned by a wallet or payment service compatible with this payment method. The QR code encodes the instructions how make a payment.
If this parameter contains any value, include it in the payment instructions for your customer. */
    @NotNull String getQrCode();

    /** Reference number of transaction. */
    @NotNull String getReference();

    @NotNull Optional<ExternalReference> getExternalReference();

    /** This is the URL where the customers will be redirected after completing a payment.

The URL must be either IP or domain-based. */
    @NotNull String getReturnUrl();

    @NotNull SelectedPaymentOperatorIncoming getPaymentOperator();

    /** Date and time when payment was accepted. */
    @NotNull java.time.OffsetDateTime getAcceptedAt();

    /** Date and time of payment expiration. If no money has been transferred to this time, payment is considered failed and callback with status change event will shortly follow. */
    @NotNull java.time.OffsetDateTime getExpireAt();

    @NotNull static Builder builder(P2AV2MethodResponse copyOf) {
        Builder builder = builder();
        builder.idPayin(copyOf.getIdPayin());
        builder.idPayment(copyOf.getIdPayment());
        builder.account(copyOf.getAccount());
        builder.money(copyOf.getMoney());
        builder.vat(copyOf.getVat().orElse(null));
        builder.merchantName(copyOf.getMerchantName());
        builder.paymentAddress(copyOf.getPaymentAddress());
        builder.qrName(copyOf.getQrName());
        builder.qrCode(copyOf.getQrCode());
        builder.reference(copyOf.getReference());
        builder.externalReference(copyOf.getExternalReference().orElse(null));
        builder.returnUrl(copyOf.getReturnUrl());
        builder.paymentOperator(copyOf.getPaymentOperator());
        builder.acceptedAt(copyOf.getAcceptedAt());
        builder.expireAt(copyOf.getExpireAt());
        return builder;
    }

    @NotNull static Builder builder() {
        return new P2AV2MethodResponseImpl.BuilderImpl();
    }

    /** Builder for {@link P2AV2MethodResponse} model class. */
    interface Builder {

        /**
          * Set {@link P2AV2MethodResponse#getIdPayin} property.
          *
          * 
          */
        @NotNull Builder idPayin(IdPayin idPayin);

        boolean isIdPayinDefined();


        /**
          * Set {@link P2AV2MethodResponse#getIdPayment} property.
          *
          * 
          */
        @NotNull Builder idPayment(IdPayment idPayment);

        boolean isIdPaymentDefined();


        /**
          * Set {@link P2AV2MethodResponse#getAccount} property.
          *
          * 
          */
        @NotNull Builder account(AccountResponseP2AV2 account);

        boolean isAccountDefined();


        /**
          * Set {@link P2AV2MethodResponse#getMoney} property.
          *
          * 
          */
        @NotNull Builder money(MoneyPaymentResponse money);

        boolean isMoneyDefined();


        /**
          * Set {@link P2AV2MethodResponse#getVat} property.
          *
          * 
          */
        @NotNull Builder vat(MoneyVat vat);

        boolean isVatDefined();


        /**
          * Set {@link P2AV2MethodResponse#getMerchantName} property.
          *
          * 
          */
        @NotNull Builder merchantName(String merchantName);

        boolean isMerchantNameDefined();


        /**
          * Set {@link P2AV2MethodResponse#getPaymentAddress} property.
          *
          * 
          */
        @NotNull Builder paymentAddress(PaymentAddress paymentAddress);

        boolean isPaymentAddressDefined();


        /**
          * Set {@link P2AV2MethodResponse#getQrName} property.
          *
          * The name of the QR code image to be scanned by a wallet or payment service compatible with this payment method. The QR code of the image can be labeled by qrName to increase the clarity of the payment instruction.
If this parameter contains any value, include it in the payment instructions for your customer.
          */
        @NotNull Builder qrName(String qrName);

        boolean isQrNameDefined();


        /**
          * Set {@link P2AV2MethodResponse#getQrCode} property.
          *
          * The URL of the QR code image to be scanned by a wallet or payment service compatible with this payment method. The QR code encodes the instructions how make a payment.
If this parameter contains any value, include it in the payment instructions for your customer.
          */
        @NotNull Builder qrCode(String qrCode);

        boolean isQrCodeDefined();


        /**
          * Set {@link P2AV2MethodResponse#getReference} property.
          *
          * Reference number of transaction.
          */
        @NotNull Builder reference(String reference);

        boolean isReferenceDefined();


        /**
          * Set {@link P2AV2MethodResponse#getExternalReference} property.
          *
          * 
          */
        @NotNull Builder externalReference(ExternalReference externalReference);

        boolean isExternalReferenceDefined();


        /**
          * Set {@link P2AV2MethodResponse#getReturnUrl} property.
          *
          * This is the URL where the customers will be redirected after completing a payment.

The URL must be either IP or domain-based.
          */
        @NotNull Builder returnUrl(String returnUrl);

        boolean isReturnUrlDefined();


        /**
          * Set {@link P2AV2MethodResponse#getPaymentOperator} property.
          *
          * 
          */
        @NotNull Builder paymentOperator(SelectedPaymentOperatorIncoming paymentOperator);

        boolean isPaymentOperatorDefined();


        /**
          * Set {@link P2AV2MethodResponse#getAcceptedAt} property.
          *
          * Date and time when payment was accepted.
          */
        @NotNull Builder acceptedAt(java.time.OffsetDateTime acceptedAt);

        boolean isAcceptedAtDefined();


        /**
          * Set {@link P2AV2MethodResponse#getExpireAt} property.
          *
          * Date and time of payment expiration. If no money has been transferred to this time, payment is considered failed and callback with status change event will shortly follow.
          */
        @NotNull Builder expireAt(java.time.OffsetDateTime expireAt);

        boolean isExpireAtDefined();


        /**
         * Create new instance of {@link P2AV2MethodResponse} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @NotNull P2AV2MethodResponse build();

    }
}