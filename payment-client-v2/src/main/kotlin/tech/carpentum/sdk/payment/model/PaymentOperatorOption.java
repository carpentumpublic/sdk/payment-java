//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** PaymentOperatorOption
 *
 * 
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public interface PaymentOperatorOption {

    /** One of following can serve as Payment Operator:

 * Financial or other institution (such as bank, card payment processor, ...) that manages transactions for your customers
 * Mobile wallet
 * Blockchain protocol for crypto currency payments

Customer is informed with the payment instructions where funds have to be transferred efficiently based on the selected Payment operator.
For getting list of the available payment options for payins use [POST /payins/!availablePaymentOptions](#operations-Incoming_payments-availablePaymentOptions) API, for payouts use [POST /payouts/!availablePaymentOptions](#operations-Outgoing_payments-availablePaymentOptions) API. */
    @NotNull String getCode();

    @NotNull Optional<String> getName();

    /** If set to false, the payment operator is not actually available, please contact merchant support to solve this problem. */
    @NotNull Boolean getIsAvailable();

    @NotNull IntervalNumberTo getTransactionAmountLimit();

    @NotNull static Builder builder(PaymentOperatorOption copyOf) {
        Builder builder = builder();
        builder.code(copyOf.getCode());
        builder.name(copyOf.getName().orElse(null));
        builder.isAvailable(copyOf.getIsAvailable());
        builder.transactionAmountLimit(copyOf.getTransactionAmountLimit());
        return builder;
    }

    @NotNull static Builder builder() {
        return new PaymentOperatorOptionImpl.BuilderImpl();
    }

    /** Builder for {@link PaymentOperatorOption} model class. */
    interface Builder {

        /**
          * Set {@link PaymentOperatorOption#getCode} property.
          *
          * One of following can serve as Payment Operator:

 * Financial or other institution (such as bank, card payment processor, ...) that manages transactions for your customers
 * Mobile wallet
 * Blockchain protocol for crypto currency payments

Customer is informed with the payment instructions where funds have to be transferred efficiently based on the selected Payment operator.
For getting list of the available payment options for payins use [POST /payins/!availablePaymentOptions](#operations-Incoming_payments-availablePaymentOptions) API, for payouts use [POST /payouts/!availablePaymentOptions](#operations-Outgoing_payments-availablePaymentOptions) API.
          */
        @NotNull Builder code(String code);

        boolean isCodeDefined();


        /**
          * Set {@link PaymentOperatorOption#getName} property.
          *
          * 
          */
        @NotNull Builder name(String name);

        boolean isNameDefined();


        /**
          * Set {@link PaymentOperatorOption#getIsAvailable} property.
          *
          * If set to false, the payment operator is not actually available, please contact merchant support to solve this problem.
          */
        @NotNull Builder isAvailable(Boolean isAvailable);

        boolean isIsAvailableDefined();


        /**
          * Set {@link PaymentOperatorOption#getTransactionAmountLimit} property.
          *
          * 
          */
        @NotNull Builder transactionAmountLimit(IntervalNumberTo transactionAmountLimit);

        boolean isTransactionAmountLimitDefined();


        /**
         * Create new instance of {@link PaymentOperatorOption} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @NotNull PaymentOperatorOption build();

    }
}