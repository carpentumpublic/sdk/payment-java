//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** RedirectResponse
 *
 * Customer should be redirected to specified URL for the next payment process step.
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public class PayinAcceptedRedirectResponseImpl implements PayinAcceptedRedirectResponse {
    private final PaymentRequested paymentRequested;

    @Override
    public PaymentRequested getPaymentRequested() {
        return paymentRequested;
    }


    private final Redirection redirectTo;

    @Override
    public Redirection getRedirectTo() {
        return redirectTo;
    }




    private final int hashCode;
    private final String toString;

    private PayinAcceptedRedirectResponseImpl(BuilderImpl builder) {
        this.paymentRequested = Objects.requireNonNull(builder.paymentRequested, "Property 'paymentRequested' is required.");
        this.redirectTo = Objects.requireNonNull(builder.redirectTo, "Property 'redirectTo' is required.");

        this.hashCode = Objects.hash(paymentRequested, redirectTo);
        this.toString = builder.type + "(" +
                "paymentRequested=" + paymentRequested +
                ", redirectTo=" + redirectTo +
                ')';
    }

    @Override
    public int hashCode() {
        return hashCode;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }

        if (!(obj instanceof PayinAcceptedRedirectResponseImpl)) {
            return false;
        }

        PayinAcceptedRedirectResponseImpl that = (PayinAcceptedRedirectResponseImpl) obj;
        if (!Objects.equals(this.paymentRequested, that.paymentRequested)) return false;
        if (!Objects.equals(this.redirectTo, that.redirectTo)) return false;

        return true;
    }

    @Override
    public String toString() {
        return toString;
    }

    /** Builder for {@link PayinAcceptedRedirectResponse} model class. */
    public static class BuilderImpl implements PayinAcceptedRedirectResponse.Builder {
        private PaymentRequested paymentRequested = null;
        private Redirection redirectTo = null;

        private final String type;

        public BuilderImpl(String type) {
            this.type = type;
        }

        public BuilderImpl() {
            this("PayinAcceptedRedirectResponse");
        }

        /**
          * Set {@link PayinAcceptedRedirectResponse#getPaymentRequested} property.
          *
          * 
          */
        @Override
        public BuilderImpl paymentRequested(PaymentRequested paymentRequested) {
            this.paymentRequested = paymentRequested;
            return this;
        }

        @Override
        public boolean isPaymentRequestedDefined() {
            return this.paymentRequested != null;
        }

        /**
          * Set {@link PayinAcceptedRedirectResponse#getRedirectTo} property.
          *
          * 
          */
        @Override
        public BuilderImpl redirectTo(Redirection redirectTo) {
            this.redirectTo = redirectTo;
            return this;
        }

        @Override
        public boolean isRedirectToDefined() {
            return this.redirectTo != null;
        }

        /**
         * Create new instance of {@link PayinAcceptedRedirectResponse} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @Override
        public PayinAcceptedRedirectResponseImpl build() {
            return new PayinAcceptedRedirectResponseImpl(this);
        }

    }
}