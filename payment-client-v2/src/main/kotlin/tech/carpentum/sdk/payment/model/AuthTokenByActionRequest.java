//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** AuthTokenByActionRequest
 *
 * 
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public interface AuthTokenByActionRequest {

    /** Your Merchant ID. This Merchant ID is obtained upon [Merchant enrollment process](sandbox.html#sandbox-account-registration). */
    @NotNull String getMerchantCode();

    /** See [Merchant Secret Key](terminology.html#term-Merchant-secret). */
    @NotNull String getSecret();

    /** Validity of the token in seconds. Maximum value 3600, default value 1800 (30 minutes). */
    @NotNull Optional<Integer> getValiditySecs();

    @NotNull AuthTokenAction getAction();

    /** Id of the specific payin/payout to allow access to. */
    @NotNull String getIdPayment();

    @NotNull Optional<Money> getMoney();

    @NotNull Optional<Money> getMoneyProvided();

    @NotNull Optional<CurrencyCode> getCurrencyCodeRequired();

    @NotNull Optional<Money> getMoneyRequired();

    @NotNull Optional<CurrencyCode> getCurrencyCodeProvided();

    @NotNull Optional<SettlementMethod> getSettlementMethod();

    @NotNull static Builder builder(AuthTokenByActionRequest copyOf) {
        Builder builder = builder();
        builder.merchantCode(copyOf.getMerchantCode());
        builder.secret(copyOf.getSecret());
        builder.validitySecs(copyOf.getValiditySecs().orElse(null));
        builder.action(copyOf.getAction());
        builder.idPayment(copyOf.getIdPayment());
        builder.money(copyOf.getMoney().orElse(null));
        builder.moneyProvided(copyOf.getMoneyProvided().orElse(null));
        builder.currencyCodeRequired(copyOf.getCurrencyCodeRequired().orElse(null));
        builder.moneyRequired(copyOf.getMoneyRequired().orElse(null));
        builder.currencyCodeProvided(copyOf.getCurrencyCodeProvided().orElse(null));
        builder.settlementMethod(copyOf.getSettlementMethod().orElse(null));
        return builder;
    }

    @NotNull static Builder builder() {
        return new AuthTokenByActionRequestImpl.BuilderImpl();
    }

    /** Builder for {@link AuthTokenByActionRequest} model class. */
    interface Builder {

        /**
          * Set {@link AuthTokenByActionRequest#getMerchantCode} property.
          *
          * Your Merchant ID. This Merchant ID is obtained upon [Merchant enrollment process](sandbox.html#sandbox-account-registration).
          */
        @NotNull Builder merchantCode(String merchantCode);

        boolean isMerchantCodeDefined();


        /**
          * Set {@link AuthTokenByActionRequest#getSecret} property.
          *
          * See [Merchant Secret Key](terminology.html#term-Merchant-secret).
          */
        @NotNull Builder secret(String secret);

        boolean isSecretDefined();


        /**
          * Set {@link AuthTokenByActionRequest#getValiditySecs} property.
          *
          * Validity of the token in seconds. Maximum value 3600, default value 1800 (30 minutes).
          */
        @NotNull Builder validitySecs(Integer validitySecs);

        boolean isValiditySecsDefined();


        /**
          * Set {@link AuthTokenByActionRequest#getAction} property.
          *
          * 
          */
        @NotNull Builder action(AuthTokenAction action);

        boolean isActionDefined();


        /**
          * Set {@link AuthTokenByActionRequest#getIdPayment} property.
          *
          * Id of the specific payin/payout to allow access to.
          */
        @NotNull Builder idPayment(String idPayment);

        boolean isIdPaymentDefined();


        /**
          * Set {@link AuthTokenByActionRequest#getMoney} property.
          *
          * 
          */
        @NotNull Builder money(Money money);

        boolean isMoneyDefined();


        /**
          * Set {@link AuthTokenByActionRequest#getMoneyProvided} property.
          *
          * 
          */
        @NotNull Builder moneyProvided(Money moneyProvided);

        boolean isMoneyProvidedDefined();


        /**
          * Set {@link AuthTokenByActionRequest#getCurrencyCodeRequired} property.
          *
          * 
          */
        @NotNull Builder currencyCodeRequired(CurrencyCode currencyCodeRequired);

        boolean isCurrencyCodeRequiredDefined();


        /**
          * Set {@link AuthTokenByActionRequest#getMoneyRequired} property.
          *
          * 
          */
        @NotNull Builder moneyRequired(Money moneyRequired);

        boolean isMoneyRequiredDefined();


        /**
          * Set {@link AuthTokenByActionRequest#getCurrencyCodeProvided} property.
          *
          * 
          */
        @NotNull Builder currencyCodeProvided(CurrencyCode currencyCodeProvided);

        boolean isCurrencyCodeProvidedDefined();


        /**
          * Set {@link AuthTokenByActionRequest#getSettlementMethod} property.
          *
          * 
          */
        @NotNull Builder settlementMethod(SettlementMethod settlementMethod);

        boolean isSettlementMethodDefined();


        /**
         * Create new instance of {@link AuthTokenByActionRequest} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @NotNull AuthTokenByActionRequest build();

    }
}