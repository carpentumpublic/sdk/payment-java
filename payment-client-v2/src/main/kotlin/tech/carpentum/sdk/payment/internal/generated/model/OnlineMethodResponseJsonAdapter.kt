//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.OnlineMethodResponse

class OnlineMethodResponseJsonAdapter {
    @FromJson
    fun fromJson(json: OnlineMethodResponseJson): OnlineMethodResponse {
        val builder = OnlineMethodResponse.builder()
        builder.idPayin(json.idPayin)
        builder.idPayment(json.idPayment)
        builder.account(json.account)
        builder.money(json.money)
        builder.moneyRequired(json.moneyRequired)
        builder.vat(json.vat)
        builder.reference(json.reference)
        builder.returnUrl(json.returnUrl)
        builder.acceptedAt(json.acceptedAt)
        builder.expireAt(json.expireAt)
        return builder.build()
    }

    @ToJson
    fun toJson(model: OnlineMethodResponse): OnlineMethodResponseJson {
        val json = OnlineMethodResponseJson()
        json.idPayin = model.idPayin
        json.idPayment = model.idPayment
        json.account = model.account
        json.money = model.money
        json.moneyRequired = model.moneyRequired.orElse(null)
        json.vat = model.vat.orElse(null)
        json.reference = model.reference
        json.returnUrl = model.returnUrl
        json.acceptedAt = model.acceptedAt
        json.expireAt = model.expireAt
        json.paymentMethodCode = model.paymentMethodCode.name
        return json
    }

    @FromJson
    fun fromJsonImpl(model: OnlineMethodResponse): OnlineMethodResponseImpl {
        return model as OnlineMethodResponseImpl
    }

    @ToJson
    fun toJsonImpl(impl: OnlineMethodResponseImpl): OnlineMethodResponse {
        return impl
    }

}