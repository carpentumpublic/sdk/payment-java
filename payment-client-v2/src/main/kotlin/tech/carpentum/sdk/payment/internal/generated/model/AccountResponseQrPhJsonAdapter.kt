//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.AccountResponseQrPh

class AccountResponseQrPhJsonAdapter {
    @FromJson
    fun fromJson(json: AccountResponseQrPhJson): AccountResponseQrPh {
        val builder = AccountResponseQrPh.builder()
        builder.accountName(json.accountName)
        return builder.build()
    }

    @ToJson
    fun toJson(model: AccountResponseQrPh): AccountResponseQrPhJson {
        val json = AccountResponseQrPhJson()
        json.accountName = model.accountName
        return json
    }

    @FromJson
    fun fromJsonImpl(model: AccountResponseQrPh): AccountResponseQrPhImpl {
        return model as AccountResponseQrPhImpl
    }

    @ToJson
    fun toJsonImpl(impl: AccountResponseQrPhImpl): AccountResponseQrPh {
        return impl
    }

}