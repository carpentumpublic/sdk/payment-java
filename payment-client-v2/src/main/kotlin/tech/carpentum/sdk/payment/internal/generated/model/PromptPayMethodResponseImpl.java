//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** PROMPTPAY
 *
 * 
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public class PromptPayMethodResponseImpl implements PromptPayMethodResponse {
    private final IdPayin idPayin;

    @Override
    public IdPayin getIdPayin() {
        return idPayin;
    }


    private final IdPayment idPayment;

    @Override
    public IdPayment getIdPayment() {
        return idPayment;
    }


    private final AccountCustomerResponsePromptPay accountCustomer;

    @Override
    public AccountCustomerResponsePromptPay getAccountCustomer() {
        return accountCustomer;
    }


    private final MoneyPaymentResponse money;

    @Override
    public MoneyPaymentResponse getMoney() {
        return money;
    }


    private final Optional<MoneyVat> vat;

    @Override
    public Optional<MoneyVat> getVat() {
        return vat;
    }


    private final Optional<MoneyRequired> moneyRequired;

    @Override
    public Optional<MoneyRequired> getMoneyRequired() {
        return moneyRequired;
    }


    private final String merchantName;

    @Override
    public String getMerchantName() {
        return merchantName;
    }


    /** Reference number of transaction. */
    private final String reference;

    @Override
    public String getReference() {
        return reference;
    }


    /** The name of the QR code image to be scanned by a wallet or payment service compatible with this payment method. The QR code of the image can be labeled by qrName to increase the clarity of the payment instruction.
If this parameter contains any value, include it in the payment instructions for your customer. */
    private final String qrName;

    @Override
    public String getQrName() {
        return qrName;
    }


    /** The URL of the QR code image to be scanned by a wallet or payment service compatible with this payment method. The QR code encodes the instructions how make a payment.
If this parameter contains any value, include it in the payment instructions for your customer. */
    private final String qrCode;

    @Override
    public String getQrCode() {
        return qrCode;
    }


    /** This is the URL where the customers will be redirected after completing a payment.

The URL must be either IP or domain-based. */
    private final String returnUrl;

    @Override
    public String getReturnUrl() {
        return returnUrl;
    }


    /** Date and time when payment was accepted. */
    private final java.time.OffsetDateTime acceptedAt;

    @Override
    public java.time.OffsetDateTime getAcceptedAt() {
        return acceptedAt;
    }


    /** Date and time of payment expiration. If no money has been transferred to this time, payment is considered failed and callback with status change event will shortly follow. */
    private final java.time.OffsetDateTime expireAt;

    @Override
    public java.time.OffsetDateTime getExpireAt() {
        return expireAt;
    }


    @Override public PaymentMethodCode getPaymentMethodCode() { return PAYMENT_METHOD_CODE; }

    private final int hashCode;
    private final String toString;

    private PromptPayMethodResponseImpl(BuilderImpl builder) {
        this.idPayin = Objects.requireNonNull(builder.idPayin, "Property 'idPayin' is required.");
        this.idPayment = Objects.requireNonNull(builder.idPayment, "Property 'idPayment' is required.");
        this.accountCustomer = Objects.requireNonNull(builder.accountCustomer, "Property 'accountCustomer' is required.");
        this.money = Objects.requireNonNull(builder.money, "Property 'money' is required.");
        this.vat = Optional.ofNullable(builder.vat);
        this.moneyRequired = Optional.ofNullable(builder.moneyRequired);
        this.merchantName = Objects.requireNonNull(builder.merchantName, "Property 'merchantName' is required.");
        this.reference = Objects.requireNonNull(builder.reference, "Property 'reference' is required.");
        this.qrName = Objects.requireNonNull(builder.qrName, "Property 'qrName' is required.");
        this.qrCode = Objects.requireNonNull(builder.qrCode, "Property 'qrCode' is required.");
        this.returnUrl = Objects.requireNonNull(builder.returnUrl, "Property 'returnUrl' is required.");
        this.acceptedAt = Objects.requireNonNull(builder.acceptedAt, "Property 'acceptedAt' is required.");
        this.expireAt = Objects.requireNonNull(builder.expireAt, "Property 'expireAt' is required.");

        this.hashCode = Objects.hash(idPayin, idPayment, accountCustomer, money, vat, moneyRequired, merchantName, reference, qrName, qrCode, returnUrl, acceptedAt, expireAt);
        this.toString = builder.type + "(" +
                "idPayin=" + idPayin +
                ", idPayment=" + idPayment +
                ", accountCustomer=" + accountCustomer +
                ", money=" + money +
                ", vat=" + vat +
                ", moneyRequired=" + moneyRequired +
                ", merchantName=" + merchantName +
                ", reference=" + reference +
                ", qrName=" + qrName +
                ", qrCode=" + qrCode +
                ", returnUrl=" + returnUrl +
                ", acceptedAt=" + acceptedAt +
                ", expireAt=" + expireAt +
                ')';
    }

    @Override
    public int hashCode() {
        return hashCode;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }

        if (!(obj instanceof PromptPayMethodResponseImpl)) {
            return false;
        }

        PromptPayMethodResponseImpl that = (PromptPayMethodResponseImpl) obj;
        if (!Objects.equals(this.idPayin, that.idPayin)) return false;
        if (!Objects.equals(this.idPayment, that.idPayment)) return false;
        if (!Objects.equals(this.accountCustomer, that.accountCustomer)) return false;
        if (!Objects.equals(this.money, that.money)) return false;
        if (!Objects.equals(this.vat, that.vat)) return false;
        if (!Objects.equals(this.moneyRequired, that.moneyRequired)) return false;
        if (!Objects.equals(this.merchantName, that.merchantName)) return false;
        if (!Objects.equals(this.reference, that.reference)) return false;
        if (!Objects.equals(this.qrName, that.qrName)) return false;
        if (!Objects.equals(this.qrCode, that.qrCode)) return false;
        if (!Objects.equals(this.returnUrl, that.returnUrl)) return false;
        if (!Objects.equals(this.acceptedAt, that.acceptedAt)) return false;
        if (!Objects.equals(this.expireAt, that.expireAt)) return false;

        return true;
    }

    @Override
    public String toString() {
        return toString;
    }

    /** Builder for {@link PromptPayMethodResponse} model class. */
    public static class BuilderImpl implements PromptPayMethodResponse.Builder {
        private IdPayin idPayin = null;
        private IdPayment idPayment = null;
        private AccountCustomerResponsePromptPay accountCustomer = null;
        private MoneyPaymentResponse money = null;
        private MoneyVat vat = null;
        private MoneyRequired moneyRequired = null;
        private String merchantName = null;
        private String reference = null;
        private String qrName = null;
        private String qrCode = null;
        private String returnUrl = null;
        private java.time.OffsetDateTime acceptedAt = null;
        private java.time.OffsetDateTime expireAt = null;

        private final String type;

        public BuilderImpl(String type) {
            this.type = type;
        }

        public BuilderImpl() {
            this("PromptPayMethodResponse");
        }

        /**
          * Set {@link PromptPayMethodResponse#getIdPayin} property.
          *
          * 
          */
        @Override
        public BuilderImpl idPayin(IdPayin idPayin) {
            this.idPayin = idPayin;
            return this;
        }

        @Override
        public boolean isIdPayinDefined() {
            return this.idPayin != null;
        }

        /**
          * Set {@link PromptPayMethodResponse#getIdPayment} property.
          *
          * 
          */
        @Override
        public BuilderImpl idPayment(IdPayment idPayment) {
            this.idPayment = idPayment;
            return this;
        }

        @Override
        public boolean isIdPaymentDefined() {
            return this.idPayment != null;
        }

        /**
          * Set {@link PromptPayMethodResponse#getAccountCustomer} property.
          *
          * 
          */
        @Override
        public BuilderImpl accountCustomer(AccountCustomerResponsePromptPay accountCustomer) {
            this.accountCustomer = accountCustomer;
            return this;
        }

        @Override
        public boolean isAccountCustomerDefined() {
            return this.accountCustomer != null;
        }

        /**
          * Set {@link PromptPayMethodResponse#getMoney} property.
          *
          * 
          */
        @Override
        public BuilderImpl money(MoneyPaymentResponse money) {
            this.money = money;
            return this;
        }

        @Override
        public boolean isMoneyDefined() {
            return this.money != null;
        }

        /**
          * Set {@link PromptPayMethodResponse#getVat} property.
          *
          * 
          */
        @Override
        public BuilderImpl vat(MoneyVat vat) {
            this.vat = vat;
            return this;
        }

        @Override
        public boolean isVatDefined() {
            return this.vat != null;
        }

        /**
          * Set {@link PromptPayMethodResponse#getMoneyRequired} property.
          *
          * 
          */
        @Override
        public BuilderImpl moneyRequired(MoneyRequired moneyRequired) {
            this.moneyRequired = moneyRequired;
            return this;
        }

        @Override
        public boolean isMoneyRequiredDefined() {
            return this.moneyRequired != null;
        }

        /**
          * Set {@link PromptPayMethodResponse#getMerchantName} property.
          *
          * 
          */
        @Override
        public BuilderImpl merchantName(String merchantName) {
            this.merchantName = merchantName;
            return this;
        }

        @Override
        public boolean isMerchantNameDefined() {
            return this.merchantName != null;
        }

        /**
          * Set {@link PromptPayMethodResponse#getReference} property.
          *
          * Reference number of transaction.
          */
        @Override
        public BuilderImpl reference(String reference) {
            this.reference = reference;
            return this;
        }

        @Override
        public boolean isReferenceDefined() {
            return this.reference != null;
        }

        /**
          * Set {@link PromptPayMethodResponse#getQrName} property.
          *
          * The name of the QR code image to be scanned by a wallet or payment service compatible with this payment method. The QR code of the image can be labeled by qrName to increase the clarity of the payment instruction.
If this parameter contains any value, include it in the payment instructions for your customer.
          */
        @Override
        public BuilderImpl qrName(String qrName) {
            this.qrName = qrName;
            return this;
        }

        @Override
        public boolean isQrNameDefined() {
            return this.qrName != null;
        }

        /**
          * Set {@link PromptPayMethodResponse#getQrCode} property.
          *
          * The URL of the QR code image to be scanned by a wallet or payment service compatible with this payment method. The QR code encodes the instructions how make a payment.
If this parameter contains any value, include it in the payment instructions for your customer.
          */
        @Override
        public BuilderImpl qrCode(String qrCode) {
            this.qrCode = qrCode;
            return this;
        }

        @Override
        public boolean isQrCodeDefined() {
            return this.qrCode != null;
        }

        /**
          * Set {@link PromptPayMethodResponse#getReturnUrl} property.
          *
          * This is the URL where the customers will be redirected after completing a payment.

The URL must be either IP or domain-based.
          */
        @Override
        public BuilderImpl returnUrl(String returnUrl) {
            this.returnUrl = returnUrl;
            return this;
        }

        @Override
        public boolean isReturnUrlDefined() {
            return this.returnUrl != null;
        }

        /**
          * Set {@link PromptPayMethodResponse#getAcceptedAt} property.
          *
          * Date and time when payment was accepted.
          */
        @Override
        public BuilderImpl acceptedAt(java.time.OffsetDateTime acceptedAt) {
            this.acceptedAt = acceptedAt;
            return this;
        }

        @Override
        public boolean isAcceptedAtDefined() {
            return this.acceptedAt != null;
        }

        /**
          * Set {@link PromptPayMethodResponse#getExpireAt} property.
          *
          * Date and time of payment expiration. If no money has been transferred to this time, payment is considered failed and callback with status change event will shortly follow.
          */
        @Override
        public BuilderImpl expireAt(java.time.OffsetDateTime expireAt) {
            this.expireAt = expireAt;
            return this;
        }

        @Override
        public boolean isExpireAtDefined() {
            return this.expireAt != null;
        }

        /**
         * Create new instance of {@link PromptPayMethodResponse} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @Override
        public PromptPayMethodResponseImpl build() {
            return new PromptPayMethodResponseImpl(this);
        }

    }
}