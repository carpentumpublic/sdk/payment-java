//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.TopUp

class TopUpJsonAdapter {
    @FromJson
    fun fromJson(json: TopUpJson): TopUp {
        val builder = TopUp.builder()
        builder.topUpRequested(json.topUpRequested)
        builder.paymentMethod(json.paymentMethod)
        builder.callbackUrl(json.callbackUrl)
        builder.returnUrl(json.returnUrl)
        builder.customerIp(json.customerIp)
        return builder.build()
    }

    @ToJson
    fun toJson(model: TopUp): TopUpJson {
        val json = TopUpJson()
        json.topUpRequested = model.topUpRequested
        json.paymentMethod = model.paymentMethod
        json.callbackUrl = model.callbackUrl.orElse(null)
        json.returnUrl = model.returnUrl
        json.customerIp = model.customerIp.orElse(null)
        return json
    }

    @FromJson
    fun fromJsonImpl(model: TopUp): TopUpImpl {
        return model as TopUpImpl
    }

    @ToJson
    fun toJsonImpl(impl: TopUpImpl): TopUp {
        return impl
    }

}