//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.Balance

class BalanceJsonAdapter {
    @FromJson
    fun fromJson(json: BalanceJson): Balance {
        val builder = Balance.builder()
        builder.currencyCode(json.currencyCode)
        builder.balance(json.balance)
        builder.availableBalance(json.availableBalance)
        builder.pendingSettlementAmount(json.pendingSettlementAmount)
        builder.pendingPayoutAmount(json.pendingPayoutAmount)
        builder.lastBalanceMovement(json.lastBalanceMovement)
        return builder.build()
    }

    @ToJson
    fun toJson(model: Balance): BalanceJson {
        val json = BalanceJson()
        json.currencyCode = model.currencyCode
        json.balance = model.balance
        json.availableBalance = model.availableBalance
        json.pendingSettlementAmount = model.pendingSettlementAmount
        json.pendingPayoutAmount = model.pendingPayoutAmount
        json.lastBalanceMovement = model.lastBalanceMovement
        return json
    }

    @FromJson
    fun fromJsonImpl(model: Balance): BalanceImpl {
        return model as BalanceImpl
    }

    @ToJson
    fun toJsonImpl(impl: BalanceImpl): Balance {
        return impl
    }

}