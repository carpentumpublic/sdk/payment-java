//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.P2AV2MethodResponse

class P2AV2MethodResponseJsonAdapter {
    @FromJson
    fun fromJson(json: P2AV2MethodResponseJson): P2AV2MethodResponse {
        val builder = P2AV2MethodResponse.builder()
        builder.idPayin(json.idPayin)
        builder.idPayment(json.idPayment)
        builder.account(json.account)
        builder.money(json.money)
        builder.vat(json.vat)
        builder.merchantName(json.merchantName)
        builder.paymentAddress(json.paymentAddress)
        builder.qrName(json.qrName)
        builder.qrCode(json.qrCode)
        builder.reference(json.reference)
        builder.externalReference(json.externalReference)
        builder.returnUrl(json.returnUrl)
        builder.paymentOperator(json.paymentOperator)
        builder.acceptedAt(json.acceptedAt)
        builder.expireAt(json.expireAt)
        return builder.build()
    }

    @ToJson
    fun toJson(model: P2AV2MethodResponse): P2AV2MethodResponseJson {
        val json = P2AV2MethodResponseJson()
        json.idPayin = model.idPayin
        json.idPayment = model.idPayment
        json.account = model.account
        json.money = model.money
        json.vat = model.vat.orElse(null)
        json.merchantName = model.merchantName
        json.paymentAddress = model.paymentAddress
        json.qrName = model.qrName
        json.qrCode = model.qrCode
        json.reference = model.reference
        json.externalReference = model.externalReference.orElse(null)
        json.returnUrl = model.returnUrl
        json.paymentOperator = model.paymentOperator
        json.acceptedAt = model.acceptedAt
        json.expireAt = model.expireAt
        json.paymentMethodCode = model.paymentMethodCode.name
        return json
    }

    @FromJson
    fun fromJsonImpl(model: P2AV2MethodResponse): P2AV2MethodResponseImpl {
        return model as P2AV2MethodResponseImpl
    }

    @ToJson
    fun toJsonImpl(impl: P2AV2MethodResponseImpl): P2AV2MethodResponse {
        return impl
    }

}