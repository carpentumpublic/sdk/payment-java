//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** MerchantInfo
 *
 * Merchant information.
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public interface MerchantInfo {

    @NotNull ChannelInfo getChannelInfo();

    @NotNull static MerchantInfo ofChannelInfo(ChannelInfo channelInfo) { return builder().channelInfo(channelInfo).build(); }

    @NotNull static Builder builder() {
        return new MerchantInfoImpl.BuilderImpl();
    }

    /** Builder for {@link MerchantInfo} model class. */
    interface Builder {

        /**
          * Set {@link MerchantInfo#getChannelInfo} property.
          *
          * 
          */
        @NotNull Builder channelInfo(ChannelInfo channelInfo);

        boolean isChannelInfoDefined();


        /**
         * Create new instance of {@link MerchantInfo} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @NotNull MerchantInfo build();

    }
}