@file:JvmName("SettlementsApiUtils")

package tech.carpentum.sdk.payment

import tech.carpentum.sdk.payment.EndpointDefinition.Method.GET
import tech.carpentum.sdk.payment.SettlementsApi.Factory
import tech.carpentum.sdk.payment.internal.api.EnhancedSettlementsApi
import tech.carpentum.sdk.payment.internal.api.GetPaymentOptionsErrorExceptionFactory
import tech.carpentum.sdk.payment.internal.api.ResponseExceptionUtils
import tech.carpentum.sdk.payment.model.GetPaymentOptionsError
import tech.carpentum.sdk.payment.model.SettlementPaymentOptionsList
import java.io.InterruptedIOException
import java.time.Duration

/**
 *  RESTful API, Common methods that can be used for settlements.
 *
 * Use [Factory] to create new instance of the class.
 */
class SettlementsApi private constructor(
    private val apiVersion: Int,
    private val api: EnhancedSettlementsApi
) {

    /**
     * Throws [GetPaymentOptionsErrorException] ("406" response) with one of defined
     * [GetPaymentOptionsError] business validation error code.
     * Throws [InterruptedIOException] in case of timeout.
     */
    @Throws(ResponseException::class, InterruptedIOException::class)
    // tag::userGuidePublicApi[]
    fun settlementPaymentOptions(
        currencyCodes: String? = null,
        paymentOperatorCodes: String? = null
    ): SettlementPaymentOptionsList
    // end::userGuidePublicApi[]
    {
        return ResponseExceptionUtils.wrap(GetPaymentOptionsErrorExceptionFactory.instance) {
            api.getSettlementPaymentOptions(
                xAPIVersion = apiVersion,
                currencyCodes = currencyCodes,
                paymentOperatorCodes = paymentOperatorCodes
            )
        }
    }

    /**
     * Factory to create a new instance of [SettlementsApi].
     */
    companion object Factory {
        /**
         * Endpoint definition for [SettlementsApi.settlementPaymentOptions] method.
         */
        @JvmStatic
        fun defineSettlementPaymentOptionsEndpoint(): EndpointDefinition =
            EndpointDefinition(GET, "/settlement-payment-options")

        @JvmStatic
        @JvmOverloads
        fun create(context: PaymentContext, accessToken: String, callTimeout: Duration? = null): SettlementsApi {
            return SettlementsApi(
                apiVersion = context.apiVersion,
                api = EnhancedSettlementsApi(
                    basePath = context.apiBaseUrl,
                    accessToken = accessToken,
                    brand = context.brand,
                    callTimeout = callTimeout ?: context.defaultCallTimeout
                )
            )
        }
    }

}

/**
 * Grants [SettlementsApi.settlementPaymentOptions] endpoint, see [SettlementsApi.defineSettlementPaymentOptionsEndpoint] definition.
 */
@JvmName("grantSettlementPaymentOptionsEndpoint")
fun AuthTokenOperations.grantSettlementsApiSettlementPaymentOptionsEndpoint(): AuthTokenOperations =
    this.grant(SettlementsApi.defineSettlementPaymentOptionsEndpoint())
