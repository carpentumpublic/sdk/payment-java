//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.AbsaPayMethod

class AbsaPayMethodJsonAdapter {
    @FromJson
    fun fromJson(json: AbsaPayMethodJson): AbsaPayMethod {
        val builder = AbsaPayMethod.builder()
        builder.account(json.account)
        builder.emailAddress(json.emailAddress)
        return builder.build()
    }

    @ToJson
    fun toJson(model: AbsaPayMethod): AbsaPayMethodJson {
        val json = AbsaPayMethodJson()
        json.account = model.account
        json.emailAddress = model.emailAddress.orElse(null)
        json.paymentMethodCode = model.paymentMethodCode.name
        return json
    }

    @FromJson
    fun fromJsonImpl(model: AbsaPayMethod): AbsaPayMethodImpl {
        return model as AbsaPayMethodImpl
    }

    @ToJson
    fun toJsonImpl(impl: AbsaPayMethodImpl): AbsaPayMethod {
        return impl
    }

}