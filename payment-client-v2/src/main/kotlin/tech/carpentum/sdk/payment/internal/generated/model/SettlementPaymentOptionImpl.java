//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** SettlementPaymentOption
 *
 * 
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public class SettlementPaymentOptionImpl implements SettlementPaymentOption {
    private final SettlementMethodCode paymentMethodCode;

    @Override
    public SettlementMethodCode getPaymentMethodCode() {
        return paymentMethodCode;
    }


    private final CurrencyCode currencyCode;

    @Override
    public CurrencyCode getCurrencyCode() {
        return currencyCode;
    }


    private final IntervalNumberTo transactionAmountLimit;

    @Override
    public IntervalNumberTo getTransactionAmountLimit() {
        return transactionAmountLimit;
    }


    /** If set to false, the option is not currently available and must be activated in administration. */
    private final Boolean isAvailable;

    @Override
    public Boolean getIsAvailable() {
        return isAvailable;
    }


    /** Payment operators that can be used for paying via this settlement payment option. */
    private final java.util.List<@NotNull PaymentOperatorOption> paymentOperators;

    @Override
    public java.util.List<@NotNull PaymentOperatorOption> getPaymentOperators() {
        return paymentOperators;
    }




    private final int hashCode;
    private final String toString;

    private SettlementPaymentOptionImpl(BuilderImpl builder) {
        this.paymentMethodCode = Objects.requireNonNull(builder.paymentMethodCode, "Property 'paymentMethodCode' is required.");
        this.currencyCode = Objects.requireNonNull(builder.currencyCode, "Property 'currencyCode' is required.");
        this.transactionAmountLimit = Objects.requireNonNull(builder.transactionAmountLimit, "Property 'transactionAmountLimit' is required.");
        this.isAvailable = Objects.requireNonNull(builder.isAvailable, "Property 'isAvailable' is required.");
        this.paymentOperators = java.util.Collections.unmodifiableList(builder.paymentOperators);

        this.hashCode = Objects.hash(paymentMethodCode, currencyCode, transactionAmountLimit, isAvailable, paymentOperators);
        this.toString = builder.type + "(" +
                "paymentMethodCode=" + paymentMethodCode +
                ", currencyCode=" + currencyCode +
                ", transactionAmountLimit=" + transactionAmountLimit +
                ", isAvailable=" + isAvailable +
                ", paymentOperators=" + paymentOperators +
                ')';
    }

    @Override
    public int hashCode() {
        return hashCode;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }

        if (!(obj instanceof SettlementPaymentOptionImpl)) {
            return false;
        }

        SettlementPaymentOptionImpl that = (SettlementPaymentOptionImpl) obj;
        if (!Objects.equals(this.paymentMethodCode, that.paymentMethodCode)) return false;
        if (!Objects.equals(this.currencyCode, that.currencyCode)) return false;
        if (!Objects.equals(this.transactionAmountLimit, that.transactionAmountLimit)) return false;
        if (!Objects.equals(this.isAvailable, that.isAvailable)) return false;
        if (!Objects.equals(this.paymentOperators, that.paymentOperators)) return false;

        return true;
    }

    @Override
    public String toString() {
        return toString;
    }

    /** Builder for {@link SettlementPaymentOption} model class. */
    public static class BuilderImpl implements SettlementPaymentOption.Builder {
        private SettlementMethodCode paymentMethodCode = null;
        private CurrencyCode currencyCode = null;
        private IntervalNumberTo transactionAmountLimit = null;
        private Boolean isAvailable = null;
        private java.util.List<@NotNull PaymentOperatorOption> paymentOperators = new java.util.ArrayList<>();

        private final String type;

        public BuilderImpl(String type) {
            this.type = type;
        }

        public BuilderImpl() {
            this("SettlementPaymentOption");
        }

        /**
          * Set {@link SettlementPaymentOption#getPaymentMethodCode} property.
          *
          * 
          */
        @Override
        public BuilderImpl paymentMethodCode(SettlementMethodCode paymentMethodCode) {
            this.paymentMethodCode = paymentMethodCode;
            return this;
        }

        @Override
        public boolean isPaymentMethodCodeDefined() {
            return this.paymentMethodCode != null;
        }

        /**
          * Set {@link SettlementPaymentOption#getCurrencyCode} property.
          *
          * 
          */
        @Override
        public BuilderImpl currencyCode(CurrencyCode currencyCode) {
            this.currencyCode = currencyCode;
            return this;
        }

        @Override
        public boolean isCurrencyCodeDefined() {
            return this.currencyCode != null;
        }

        /**
          * Set {@link SettlementPaymentOption#getTransactionAmountLimit} property.
          *
          * 
          */
        @Override
        public BuilderImpl transactionAmountLimit(IntervalNumberTo transactionAmountLimit) {
            this.transactionAmountLimit = transactionAmountLimit;
            return this;
        }

        @Override
        public boolean isTransactionAmountLimitDefined() {
            return this.transactionAmountLimit != null;
        }

        /**
          * Set {@link SettlementPaymentOption#getIsAvailable} property.
          *
          * If set to false, the option is not currently available and must be activated in administration.
          */
        @Override
        public BuilderImpl isAvailable(Boolean isAvailable) {
            this.isAvailable = isAvailable;
            return this;
        }

        @Override
        public boolean isIsAvailableDefined() {
            return this.isAvailable != null;
        }

        /**
          * Replace all items in {@link SettlementPaymentOption#getPaymentOperators} list property.
          *
          * Payment operators that can be used for paying via this settlement payment option.
          */
        @Override
        public BuilderImpl paymentOperators(java.util.List<@NotNull PaymentOperatorOption> paymentOperators) {
            this.paymentOperators.clear();
            if (paymentOperators != null) {
                this.paymentOperators.addAll(paymentOperators);
            }
            return this;
        }
        /**
          * Add single item to {@link SettlementPaymentOption#getPaymentOperators} list property.
          *
          * Payment operators that can be used for paying via this settlement payment option.
          */
        @Override
        public BuilderImpl paymentOperatorsAdd(PaymentOperatorOption item) {
            if (item != null) {
                this.paymentOperators.add(item);
            }
            return this;
        }
        /**
          * Add all items to {@link SettlementPaymentOption#getPaymentOperators} list property.
          *
          * Payment operators that can be used for paying via this settlement payment option.
          */
        @Override
        public BuilderImpl paymentOperatorsAddAll(java.util.List<@NotNull PaymentOperatorOption> paymentOperators) {
            if (paymentOperators != null) {
                this.paymentOperators.addAll(paymentOperators);
            }
            return this;
        }


        /**
         * Create new instance of {@link SettlementPaymentOption} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @Override
        public SettlementPaymentOptionImpl build() {
            return new SettlementPaymentOptionImpl(this);
        }

    }
}