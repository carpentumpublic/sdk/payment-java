package tech.carpentum.sdk.payment.sample;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tech.carpentum.sdk.payment.AuthTokenOperations;
import tech.carpentum.sdk.payment.PaymentContext;
import tech.carpentum.sdk.payment.PaymentsApi;
import tech.carpentum.sdk.payment.ResponseException;
import tech.carpentum.sdk.payment.model.AuthTokenResponse;
import tech.carpentum.sdk.payment.model.CurrencyList;
import tech.carpentum.sdk.payment.model.PaymentMethodsList;
import tech.carpentum.sdk.payment.model.PaymentOperatorList;
import tech.carpentum.sdk.payment.model.PaymentOptionsList;
import tech.carpentum.sdk.payment.model.SegmentList;

import java.io.InterruptedIOException;
import java.time.Duration;
import java.util.function.Supplier;

/**
 * Wraps all Payments API operations.
 */
public class CodelistsScenario extends AbstractScenario {
    private final PaymentsApi paymentsApi;

    private CodelistsScenario(@NotNull PaymentsApi paymentsApi, @NotNull String authToken) {
        super(authToken);
        this.paymentsApi = paymentsApi;
    }

    /**
     * Creates new instance of the scenario with newly created Authorization token with specified validity.
     */
    public static CodelistsScenario create(@NotNull PaymentContext context, @Nullable Duration validity) throws ResponseException, InterruptedIOException {
        AuthTokenResponse authToken = context.createAuthToken(
                new AuthTokenOperations()
                        .grant(PaymentsApi.defineListCurrenciesEndpoint())
                        .grant(PaymentsApi.defineListPaymentMethodsEndpoint())
                        .grant(PaymentsApi.defineListPaymentOperatorsEndpoint())
                        .grant(PaymentsApi.defineListPaymentOptionsEndpoint())
                        .grant(PaymentsApi.defineListSegmentsEndpoint()),
                validity
        );
        return new CodelistsScenario(PaymentsApi.create(context, authToken.getToken()), authToken.getToken());
    }

    /**
     * Creates new instance of the scenario with newly created Authorization token with default {@code payment} validity.
     */
    public static CodelistsScenario create(@NotNull PaymentContext context) throws ResponseException, InterruptedIOException {
        return create(context, (Duration) null);
    }

    /**
     * Creates new instance of the scenario with existing Authorization token.
     */
    public static CodelistsScenario create(@NotNull PaymentContext context, @NotNull String authToken) throws ResponseException {
        return new CodelistsScenario(PaymentsApi.create(context, authToken), authToken);
    }

    public CurrencyList listCurrencies() throws ResponseException, InterruptedIOException {
        return paymentsApi.listCurrencies();
    }

    public SegmentList listSegments() throws ResponseException, InterruptedIOException {
        return paymentsApi.listSegments();
    }

    public PaymentOperatorList listPaymentOperators() throws ResponseException, InterruptedIOException {
        return paymentsApi.listPaymentOperators();
    }

    public PaymentMethodsList listPaymentMethods() throws ResponseException, InterruptedIOException {
        return paymentsApi.listPaymentMethods();
    }

    public PaymentOptionsList listPaymentOptions() throws ResponseException, InterruptedIOException {
        return paymentsApi.listPaymentOptions();
    }

    public PaymentOptionsList listPaymentOptions(@NotNull PaymentsApi.ListPaymentOptionsQuery query) throws ResponseException, InterruptedIOException {
        return paymentsApi.listPaymentOptions(query);
    }

    public PaymentOptionsList listPaymentOptions(@NotNull Supplier<PaymentsApi.ListPaymentOptionsQuery> querySupplier) throws ResponseException, InterruptedIOException {
        return paymentsApi.listPaymentOptions(querySupplier);
    }
}
