//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** AccountPayinRequestCodePayment
 *
 * Parameters of your customer's bank or wallet account which your customer sends funds from. These account parameters are used for the sender's account verification in processing of the payment.
Which parameters are mandatory depends on the payment method and the currency your customer choose to pay.
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public interface AccountPayinRequestCodePayment {

    /** Account Name is the name of the person who holds the account which your customer sends funds from to make his payment.
The name should be in the same format as the account holder name of the account. Allows numbers, some special characters and UNICODE symbols, see validation pattern. */
    @NotNull String getAccountName();

    @NotNull static AccountPayinRequestCodePayment ofAccountName(String accountName) { return builder().accountName(accountName).build(); }

    @NotNull static Builder builder() {
        return new AccountPayinRequestCodePaymentImpl.BuilderImpl();
    }

    /** Builder for {@link AccountPayinRequestCodePayment} model class. */
    interface Builder {

        /**
          * Set {@link AccountPayinRequestCodePayment#getAccountName} property.
          *
          * Account Name is the name of the person who holds the account which your customer sends funds from to make his payment.
The name should be in the same format as the account holder name of the account. Allows numbers, some special characters and UNICODE symbols, see validation pattern.
          */
        @NotNull Builder accountName(String accountName);

        boolean isAccountNameDefined();


        /**
         * Create new instance of {@link AccountPayinRequestCodePayment} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @NotNull AccountPayinRequestCodePayment build();

    }
}