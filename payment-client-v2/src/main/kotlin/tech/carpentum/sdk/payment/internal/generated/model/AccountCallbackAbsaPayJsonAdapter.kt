//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.AccountCallbackAbsaPay

class AccountCallbackAbsaPayJsonAdapter {
    @FromJson
    fun fromJson(json: AccountCallbackAbsaPayJson): AccountCallbackAbsaPay {
        val builder = AccountCallbackAbsaPay.builder()
        builder.accountName(json.accountName)
        builder.accountNumber(json.accountNumber)
        builder.accountIndex(json.accountIndex)
        return builder.build()
    }

    @ToJson
    fun toJson(model: AccountCallbackAbsaPay): AccountCallbackAbsaPayJson {
        val json = AccountCallbackAbsaPayJson()
        json.accountName = model.accountName
        json.accountNumber = model.accountNumber
        json.accountIndex = model.accountIndex
        return json
    }

    @FromJson
    fun fromJsonImpl(model: AccountCallbackAbsaPay): AccountCallbackAbsaPayImpl {
        return model as AccountCallbackAbsaPayImpl
    }

    @ToJson
    fun toJsonImpl(impl: AccountCallbackAbsaPayImpl): AccountCallbackAbsaPay {
        return impl
    }

}