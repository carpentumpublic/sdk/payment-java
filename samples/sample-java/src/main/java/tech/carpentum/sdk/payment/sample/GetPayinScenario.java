package tech.carpentum.sdk.payment.sample;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tech.carpentum.sdk.payment.AuthTokenOperations;
import tech.carpentum.sdk.payment.IncomingPaymentsApi;
import tech.carpentum.sdk.payment.PaymentContext;
import tech.carpentum.sdk.payment.ResponseException;
import tech.carpentum.sdk.payment.model.AuthTokenResponse;
import tech.carpentum.sdk.payment.model.PayinDetail;

import java.io.InterruptedIOException;
import java.time.Duration;

/**
 * Wraps just GET operation of Incoming Payments API for single `idPayin`.
 */
public class GetPayinScenario extends AbstractScenario {
    private final IncomingPaymentsApi incomingPaymentsApi;
    private final String idPayin;

    private GetPayinScenario(@NotNull IncomingPaymentsApi incomingPaymentsApi, @NotNull String idPayin, @NotNull String authToken) throws ResponseException {
        super(authToken);
        this.incomingPaymentsApi = incomingPaymentsApi;
        this.idPayin = idPayin;
    }

    /**
     * Creates new instance of the scenario with newly created Authorization token with specified validity.
     */
    public static GetPayinScenario create(@NotNull PaymentContext context, @NotNull String idPayin, @Nullable Duration validity) throws ResponseException, InterruptedIOException {
        AuthTokenResponse authToken = context.createAuthToken(
                new AuthTokenOperations(IncomingPaymentsApi.defineGetPayinEndpoint().forId(idPayin)),
                validity
        );
        return new GetPayinScenario(IncomingPaymentsApi.create(context, authToken.getToken()), idPayin, authToken.getToken());
    }

    /**
     * Creates new instance of the scenario with newly created Authorization token with default {@code payment} validity.
     */
    public static GetPayinScenario create(@NotNull PaymentContext context, @NotNull String idPayin) throws ResponseException, InterruptedIOException {
        return create(context, idPayin, (Duration) null);
    }

    /**
     * Creates new instance of the scenario with existing Authorization token.
     */
    public static GetPayinScenario create(@NotNull PaymentContext context, @NotNull String idPayin, @NotNull String authToken) throws ResponseException {
        return new GetPayinScenario(IncomingPaymentsApi.create(context, authToken), idPayin, authToken);
    }

    public PayinDetail getDetail() throws ResponseException, InterruptedIOException {
        return incomingPaymentsApi.getPayin(idPayin);
    }
}
