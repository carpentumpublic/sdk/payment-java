//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.AccountPayinRequestQrPh

class AccountPayinRequestQrPhJsonAdapter {
    @FromJson
    fun fromJson(json: AccountPayinRequestQrPhJson): AccountPayinRequestQrPh {
        val builder = AccountPayinRequestQrPh.builder()
        builder.accountName(json.accountName)
        builder.accountNumber(json.accountNumber)
        return builder.build()
    }

    @ToJson
    fun toJson(model: AccountPayinRequestQrPh): AccountPayinRequestQrPhJson {
        val json = AccountPayinRequestQrPhJson()
        json.accountName = model.accountName.orElse(null)
        json.accountNumber = model.accountNumber.orElse(null)
        return json
    }

    @FromJson
    fun fromJsonImpl(model: AccountPayinRequestQrPh): AccountPayinRequestQrPhImpl {
        return model as AccountPayinRequestQrPhImpl
    }

    @ToJson
    fun toJsonImpl(impl: AccountPayinRequestQrPhImpl): AccountPayinRequestQrPh {
        return impl
    }

}