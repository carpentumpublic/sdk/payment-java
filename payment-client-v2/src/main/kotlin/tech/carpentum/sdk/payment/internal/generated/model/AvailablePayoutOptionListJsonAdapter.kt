//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.AvailablePayoutOptionList

class AvailablePayoutOptionListJsonAdapter {
    @FromJson
    fun fromJson(json: AvailablePayoutOptionListJson): AvailablePayoutOptionList {
        val builder = AvailablePayoutOptionList.builder()
        builder.data(json.data?.toList())
        return builder.build()
    }

    @ToJson
    fun toJson(model: AvailablePayoutOptionList): AvailablePayoutOptionListJson {
        val json = AvailablePayoutOptionListJson()
        json.data = model.data.ifEmpty { null }
        return json
    }

    @FromJson
    fun fromJsonImpl(model: AvailablePayoutOptionList): AvailablePayoutOptionListImpl {
        return model as AvailablePayoutOptionListImpl
    }

    @ToJson
    fun toJsonImpl(impl: AvailablePayoutOptionListImpl): AvailablePayoutOptionList {
        return impl
    }

}