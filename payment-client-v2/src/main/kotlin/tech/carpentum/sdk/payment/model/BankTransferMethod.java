//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** BANK_TRANSFER
 *
 * Payment method for sending funds from your account to external bank account using a country specific payment system to process bank transfers.
It required customer to provide a receiving bank account details which are required by local payment system to process bank transfers.
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public interface BankTransferMethod extends PayoutMethod {
    /** A discriminator value of property {@link #getPaymentMethodCode}. The model class extends {@link PayoutMethod}. */
    PayoutMethod.PaymentMethodCode PAYMENT_METHOD_CODE = PayoutMethod.PaymentMethodCode.BANK_TRANSFER;

    @NotNull AccountPayoutRequestBankTransfer getAccount();

    /** One of following can serve as Payment Operator:

 * Financial or other institution (such as bank, card payment processor, ...) that manages transactions for your customers
 * Mobile wallet
 * Blockchain protocol for crypto currency payments

Customer is informed with the payment instructions where funds have to be transferred efficiently based on the selected Payment operator.
For getting list of the available payment options for payins use [POST /payins/!availablePaymentOptions](#operations-Incoming_payments-availablePaymentOptions) API, for payouts use [POST /payouts/!availablePaymentOptions](#operations-Outgoing_payments-availablePaymentOptions) API. */
    @NotNull String getPaymentOperatorCode();

    /** Your customer e-mail address in RFC 5322 format that is used for identification of the customer's payins. */
    @NotNull Optional<String> getEmailAddress();

    @NotNull Optional<String> getRemark();

    @NotNull static Builder builder(BankTransferMethod copyOf) {
        Builder builder = builder();
        builder.account(copyOf.getAccount());
        builder.paymentOperatorCode(copyOf.getPaymentOperatorCode());
        builder.emailAddress(copyOf.getEmailAddress().orElse(null));
        builder.remark(copyOf.getRemark().orElse(null));
        return builder;
    }

    @NotNull static Builder builder() {
        return new BankTransferMethodImpl.BuilderImpl();
    }

    /** Builder for {@link BankTransferMethod} model class. */
    interface Builder {

        /**
          * Set {@link BankTransferMethod#getAccount} property.
          *
          * 
          */
        @NotNull Builder account(AccountPayoutRequestBankTransfer account);

        boolean isAccountDefined();


        /**
          * Set {@link BankTransferMethod#getPaymentOperatorCode} property.
          *
          * One of following can serve as Payment Operator:

 * Financial or other institution (such as bank, card payment processor, ...) that manages transactions for your customers
 * Mobile wallet
 * Blockchain protocol for crypto currency payments

Customer is informed with the payment instructions where funds have to be transferred efficiently based on the selected Payment operator.
For getting list of the available payment options for payins use [POST /payins/!availablePaymentOptions](#operations-Incoming_payments-availablePaymentOptions) API, for payouts use [POST /payouts/!availablePaymentOptions](#operations-Outgoing_payments-availablePaymentOptions) API.
          */
        @NotNull Builder paymentOperatorCode(String paymentOperatorCode);

        boolean isPaymentOperatorCodeDefined();


        /**
          * Set {@link BankTransferMethod#getEmailAddress} property.
          *
          * Your customer e-mail address in RFC 5322 format that is used for identification of the customer's payins.
          */
        @NotNull Builder emailAddress(String emailAddress);

        boolean isEmailAddressDefined();


        /**
          * Set {@link BankTransferMethod#getRemark} property.
          *
          * 
          */
        @NotNull Builder remark(String remark);

        boolean isRemarkDefined();


        /**
         * Create new instance of {@link BankTransferMethod} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @NotNull BankTransferMethod build();

    }
}