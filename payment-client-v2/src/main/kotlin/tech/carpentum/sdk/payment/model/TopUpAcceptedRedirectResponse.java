//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** TopUpRedirectResponse
 *
 * Customer should be redirected to specified URL for the next payment process step.
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public interface TopUpAcceptedRedirectResponse extends TopUpAcceptedResponse {

    @NotNull TopUpRequested getTopUpRequested();

    @NotNull Redirection getRedirectTo();

    @NotNull static Builder builder(TopUpAcceptedRedirectResponse copyOf) {
        Builder builder = builder();
        builder.topUpRequested(copyOf.getTopUpRequested());
        builder.redirectTo(copyOf.getRedirectTo());
        return builder;
    }

    @NotNull static Builder builder() {
        return new TopUpAcceptedRedirectResponseImpl.BuilderImpl();
    }

    /** Builder for {@link TopUpAcceptedRedirectResponse} model class. */
    interface Builder {

        /**
          * Set {@link TopUpAcceptedRedirectResponse#getTopUpRequested} property.
          *
          * 
          */
        @NotNull Builder topUpRequested(TopUpRequested topUpRequested);

        boolean isTopUpRequestedDefined();


        /**
          * Set {@link TopUpAcceptedRedirectResponse#getRedirectTo} property.
          *
          * 
          */
        @NotNull Builder redirectTo(Redirection redirectTo);

        boolean isRedirectToDefined();


        /**
         * Create new instance of {@link TopUpAcceptedRedirectResponse} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @NotNull TopUpAcceptedRedirectResponse build();

    }
}