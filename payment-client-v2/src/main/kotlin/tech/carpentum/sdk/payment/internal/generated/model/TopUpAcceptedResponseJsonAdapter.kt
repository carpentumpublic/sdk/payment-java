//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.JsonDataException
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.TopUpAcceptedResponse
import tech.carpentum.sdk.payment.model.TopUpAcceptedRedirectResponse
import tech.carpentum.sdk.payment.model.TopUpAcceptedMethodResponse

class TopUpAcceptedResponseJsonAdapter {

    @FromJson
    fun fromJson(json: TopUpAcceptedResponseJson): TopUpAcceptedResponse {
        return if (json.topUpRequested != null && json.redirectTo != null) {
            val builder = TopUpAcceptedRedirectResponse.builder()
            builder.topUpRequested(json.topUpRequested)
            builder.redirectTo(json.redirectTo)
            builder.build()
        } else if (json.topUpRequested != null && json.moneyReceive != null && json.paymentMethodResponse != null) {
            val builder = TopUpAcceptedMethodResponse.builder()
            builder.topUpRequested(json.topUpRequested)
            builder.moneyReceive(json.moneyReceive)
            builder.paymentMethodResponse(json.paymentMethodResponse)
            builder.build()
        } else {
          throw JsonDataException("Missing required attributes: [topUpRequested, redirectTo] or [topUpRequested, moneyReceive, paymentMethodResponse].")
        }
    }

    @ToJson
    fun toJson(model: TopUpAcceptedResponse): TopUpAcceptedResponseJson {
        val json = TopUpAcceptedResponseJson()
        when (model) {
            is TopUpAcceptedRedirectResponse -> {
                json.topUpRequested = model.topUpRequested
                json.redirectTo = model.redirectTo
            }
            is TopUpAcceptedMethodResponse -> {
                json.topUpRequested = model.topUpRequested
                json.moneyReceive = model.moneyReceive
                json.paymentMethodResponse = model.paymentMethodResponse
            }
            else -> {
                throw JsonDataException("Unsupported type [${model.javaClass.name}] of model.")
            }
        }
        return json
    }

}