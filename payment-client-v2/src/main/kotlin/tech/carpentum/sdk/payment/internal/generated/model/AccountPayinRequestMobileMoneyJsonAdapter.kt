//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.AccountPayinRequestMobileMoney

class AccountPayinRequestMobileMoneyJsonAdapter {
    @FromJson
    fun fromJson(json: AccountPayinRequestMobileMoneyJson): AccountPayinRequestMobileMoney {
        val builder = AccountPayinRequestMobileMoney.builder()
        builder.accountName(json.accountName)
        return builder.build()
    }

    @ToJson
    fun toJson(model: AccountPayinRequestMobileMoney): AccountPayinRequestMobileMoneyJson {
        val json = AccountPayinRequestMobileMoneyJson()
        json.accountName = model.accountName
        return json
    }

    @FromJson
    fun fromJsonImpl(model: AccountPayinRequestMobileMoney): AccountPayinRequestMobileMoneyImpl {
        return model as AccountPayinRequestMobileMoneyImpl
    }

    @ToJson
    fun toJsonImpl(impl: AccountPayinRequestMobileMoneyImpl): AccountPayinRequestMobileMoney {
        return impl
    }

}