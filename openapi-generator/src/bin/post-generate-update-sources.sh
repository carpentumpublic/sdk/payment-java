#!/usr/bin/env bash

SOURCE_DIR="$(dirname $BASH_SOURCE)"

source $SOURCE_DIR/common.sh

#
# API
#

# open Api classes:
sed -i "" -e "s/^class /internal open class /g" $SDK_DIR/payment-client-v2/src/main/kotlin/tech/carpentum/sdk/payment/internal/generated/api/*.kt

#
# INFRASTRUCTURE
#

# ApiClient.kt
API_CLIENT_KT=$SDK_DIR/payment-client-v2/src/main/kotlin/tech/carpentum/sdk/payment/internal/generated/infrastructure/ApiClient.kt
sed -i "" -e "s/protected const val Authorization/const val Authorization/g" $API_CLIENT_KT
sed -i "" -e "s/protected fun <T> updateAuthParams/protected open fun <T> updateAuthParams/g" $API_CLIENT_KT
sed -i "" -e "s/updateAuthParams/updateRequestHeaders/g" $API_CLIENT_KT

# open selected methods in infrastructure:
sed -i "" -e "s/interface Response/sealed interface Response/g" $SDK_DIR/payment-client-v2/src/main/kotlin/tech/carpentum/sdk/payment/internal/generated/infrastructure/ApiResponse.kt
sed -i "" -e "s/^class /data class /g" $SDK_DIR/payment-client-v2/src/main/kotlin/tech/carpentum/sdk/payment/internal/generated/infrastructure/ApiResponse.kt

sed -i "" -e "s/^}/init { AdaptersForModelClasses.addAdapters(moshiBuilder) } }/g" $SDK_DIR/payment-client-v2/src/main/kotlin/tech/carpentum/sdk/payment/internal/generated/infrastructure/Serializer.kt

# remove files generated by alternative openapi generator
rm -rf $SDK_DIR/payment-client-v2/src/main/kotlin/tech/carpentum/sdk/payment/model
rm -rf $SDK_DIR/payment-client-v2/src/main/kotlin/tech/carpentum/sdk/payment/internal/generated/model
rm -rf $SDK_DIR/payment-client-v2/src/main/kotlin/tech/carpentum/sdk/payment/internal/api/BusinessValidationErrorExceptionFactories.kt
rm -rf $SDK_DIR/payment-client-v2/src/main/kotlin/tech/carpentum/sdk/payment/internal/generated/infrastructure/AdaptersForModelClasses.java
rm -rf $SDK_DIR/payment-client-v2/src/main/kotlin/tech/carpentum/sdk/payment/BusinessValidationErrorExceptions.kt
