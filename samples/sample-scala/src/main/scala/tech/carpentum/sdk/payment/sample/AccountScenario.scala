package tech.carpentum.sdk.payment.sample

import tech.carpentum.sdk.payment.model.BalanceList
import tech.carpentum.sdk.payment.{AccountsApi, AuthTokenOperations, PaymentContext, ResponseException}

import java.time.Duration
import scala.jdk.CollectionConverters.SetHasAsJava

/**
 * Wraps all Accounts API operations.
 */
object AccountScenario {
  /**
   * Creates new instance of the scenario with newly created Authorization token with specified validity.
   */
  @throws[ResponseException]
  def create(context: PaymentContext, validity: Duration = null): AccountScenario = {
    val authToken = context.createAuthToken(new AuthTokenOperations(AccountsApi.defineListBalancesEndpoint), validity).getToken
    new AccountScenario(AccountsApi.create(context, authToken), authToken)
  }

  /**
   * Creates new instance of the scenario with existing Authorization token.
   */
  @throws[ResponseException]
  def create(context: PaymentContext, authToken: String) =
    new AccountScenario(AccountsApi.create(context, authToken), authToken)
}

class AccountScenario private(val accountsApi: AccountsApi, override val authToken: String) extends AbstractScenario(authToken) {
  @throws[ResponseException]
  def listBalances(currencyCodes: String*): BalanceList = accountsApi.listBalances(currencyCodes.toSet.asJava)
}
