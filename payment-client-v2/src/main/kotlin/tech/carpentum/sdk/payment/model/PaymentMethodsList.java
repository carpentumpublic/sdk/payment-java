//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** PaymentMethodsList
 *
 * List of available payment methods.
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public interface PaymentMethodsList {

    @NotNull java.util.List<@NotNull PaymentMethod> getData();

    @NotNull static PaymentMethodsList ofData(java.util.List<@NotNull PaymentMethod> data) { return builder().data(data).build(); }

    @NotNull static Builder builder() {
        return new PaymentMethodsListImpl.BuilderImpl();
    }

    /** Builder for {@link PaymentMethodsList} model class. */
    interface Builder {

        /**
          * Replace all items in {@link PaymentMethodsList#getData} list property.
          *
          * 
          */
        @NotNull Builder data(java.util.List<@NotNull PaymentMethod> data);
        /**
          * Add single item to {@link PaymentMethodsList#getData} list property.
          *
          * 
          */
        @NotNull Builder dataAdd(PaymentMethod item);
        /**
          * Add all items to {@link PaymentMethodsList#getData} list property.
          *
          * 
          */
        @NotNull Builder dataAddAll(java.util.List<@NotNull PaymentMethod> data);


        /**
         * Create new instance of {@link PaymentMethodsList} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @NotNull PaymentMethodsList build();

    }
}