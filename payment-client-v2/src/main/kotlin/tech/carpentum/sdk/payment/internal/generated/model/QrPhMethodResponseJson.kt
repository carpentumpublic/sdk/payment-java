//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.JsonClass;
import tech.carpentum.sdk.payment.model.*;

@JsonClass(generateAdapter = true)
class QrPhMethodResponseJson {
    var idPayin: IdPayin? = null
    var idPayment: IdPayment? = null
    var account: AccountResponseQrPh? = null
    var accountCustomer: AccountCustomerResponseQrPh? = null
    var money: MoneyPaymentResponse? = null
    var moneyRequired: MoneyRequired? = null
    var vat: MoneyVat? = null
    var merchantName: String? = null
    var reference: String? = null
    var qrName: String? = null
    var qrCode: String? = null
    var paymentOperator: SelectedPaymentOperatorIncoming? = null
    var returnUrl: String? = null
    var acceptedAt: java.time.OffsetDateTime? = null
    var expireAt: java.time.OffsetDateTime? = null
    var paymentMethodCode: String? = null
}