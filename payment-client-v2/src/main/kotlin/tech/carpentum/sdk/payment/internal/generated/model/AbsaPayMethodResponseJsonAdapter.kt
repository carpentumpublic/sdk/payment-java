//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.AbsaPayMethodResponse

class AbsaPayMethodResponseJsonAdapter {
    @FromJson
    fun fromJson(json: AbsaPayMethodResponseJson): AbsaPayMethodResponse {
        val builder = AbsaPayMethodResponse.builder()
        builder.idPayin(json.idPayin)
        builder.idPayment(json.idPayment)
        builder.accounts(json.accounts?.toList())
        builder.accountCustomer(json.accountCustomer)
        builder.money(json.money)
        builder.vat(json.vat)
        builder.reference(json.reference)
        builder.returnUrl(json.returnUrl)
        builder.acceptedAt(json.acceptedAt)
        builder.expireAt(json.expireAt)
        return builder.build()
    }

    @ToJson
    fun toJson(model: AbsaPayMethodResponse): AbsaPayMethodResponseJson {
        val json = AbsaPayMethodResponseJson()
        json.idPayin = model.idPayin
        json.idPayment = model.idPayment
        json.accounts = model.accounts.ifEmpty { null }
        json.accountCustomer = model.accountCustomer
        json.money = model.money
        json.vat = model.vat.orElse(null)
        json.reference = model.reference
        json.returnUrl = model.returnUrl
        json.acceptedAt = model.acceptedAt
        json.expireAt = model.expireAt
        json.paymentMethodCode = model.paymentMethodCode.name
        return json
    }

    @FromJson
    fun fromJsonImpl(model: AbsaPayMethodResponse): AbsaPayMethodResponseImpl {
        return model as AbsaPayMethodResponseImpl
    }

    @ToJson
    fun toJsonImpl(impl: AbsaPayMethodResponseImpl): AbsaPayMethodResponse {
        return impl
    }

}