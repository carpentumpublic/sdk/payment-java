//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** AvailablePayoutOptionList
 *
 * The list of possible payment options.
The list always contains at least one payment option.
If no available payment options are found, an HTTP 406 response is returned with the error code.
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public interface AvailablePayoutOptionList {

    @NotNull java.util.List<@NotNull AvailablePayoutOption> getData();

    @NotNull static AvailablePayoutOptionList ofData(java.util.List<@NotNull AvailablePayoutOption> data) { return builder().data(data).build(); }

    @NotNull static Builder builder() {
        return new AvailablePayoutOptionListImpl.BuilderImpl();
    }

    /** Builder for {@link AvailablePayoutOptionList} model class. */
    interface Builder {

        /**
          * Replace all items in {@link AvailablePayoutOptionList#getData} list property.
          *
          * 
          */
        @NotNull Builder data(java.util.List<@NotNull AvailablePayoutOption> data);
        /**
          * Add single item to {@link AvailablePayoutOptionList#getData} list property.
          *
          * 
          */
        @NotNull Builder dataAdd(AvailablePayoutOption item);
        /**
          * Add all items to {@link AvailablePayoutOptionList#getData} list property.
          *
          * 
          */
        @NotNull Builder dataAddAll(java.util.List<@NotNull AvailablePayoutOption> data);


        /**
         * Create new instance of {@link AvailablePayoutOptionList} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @NotNull AvailablePayoutOptionList build();

    }
}