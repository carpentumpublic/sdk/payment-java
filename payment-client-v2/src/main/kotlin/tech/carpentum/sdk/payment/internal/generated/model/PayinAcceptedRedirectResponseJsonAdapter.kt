//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.PayinAcceptedRedirectResponse

class PayinAcceptedRedirectResponseJsonAdapter {
    @FromJson
    fun fromJson(json: PayinAcceptedRedirectResponseJson): PayinAcceptedRedirectResponse {
        val builder = PayinAcceptedRedirectResponse.builder()
        builder.paymentRequested(json.paymentRequested)
        builder.redirectTo(json.redirectTo)
        return builder.build()
    }

    @ToJson
    fun toJson(model: PayinAcceptedRedirectResponse): PayinAcceptedRedirectResponseJson {
        val json = PayinAcceptedRedirectResponseJson()
        json.paymentRequested = model.paymentRequested
        json.redirectTo = model.redirectTo
        return json
    }

    @FromJson
    fun fromJsonImpl(model: PayinAcceptedRedirectResponse): PayinAcceptedRedirectResponseImpl {
        return model as PayinAcceptedRedirectResponseImpl
    }

    @ToJson
    fun toJsonImpl(impl: PayinAcceptedRedirectResponseImpl): PayinAcceptedRedirectResponse {
        return impl
    }

}