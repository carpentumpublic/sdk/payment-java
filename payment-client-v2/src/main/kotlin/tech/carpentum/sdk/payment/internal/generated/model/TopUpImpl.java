//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** TopUp
 *
 * For every payment method there is appropriate payment specific request object in `paymentMethod` attribute.

If you have used the [`POST /topups/!availablePaymentOptions`](#operations-Incoming_payments-availablePaymentOptions) API to get the list of the available payment options, then it is expected that you use the same input data here to make sure that the payment will be accepted.
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public class TopUpImpl implements TopUp {
    private final TopUpRequested topUpRequested;

    @Override
    public TopUpRequested getTopUpRequested() {
        return topUpRequested;
    }


    private final PayinMethod paymentMethod;

    @Override
    public PayinMethod getPaymentMethod() {
        return paymentMethod;
    }


    /** This is the URL where the system will send the transaction final status after payment has been completed.

The URL must be either IP or domain-based. */
    private final Optional<String> callbackUrl;

    @Override
    public Optional<String> getCallbackUrl() {
        return callbackUrl;
    }


    /** This is the URL where the customers will be redirected after completing a payment.

The URL must be either IP or domain-based. */
    private final String returnUrl;

    @Override
    public String getReturnUrl() {
        return returnUrl;
    }


    /** The IP address of the customer making the payment in either the IPv4 or IPv6 format.

The IP address is used for validating against the IP address whitelists and blacklists from the merchant settings. */
    private final Optional<String> customerIp;

    @Override
    public Optional<String> getCustomerIp() {
        return customerIp;
    }




    private final int hashCode;
    private final String toString;

    private TopUpImpl(BuilderImpl builder) {
        this.topUpRequested = Objects.requireNonNull(builder.topUpRequested, "Property 'topUpRequested' is required.");
        this.paymentMethod = Objects.requireNonNull(builder.paymentMethod, "Property 'paymentMethod' is required.");
        this.callbackUrl = Optional.ofNullable(builder.callbackUrl);
        this.returnUrl = Objects.requireNonNull(builder.returnUrl, "Property 'returnUrl' is required.");
        this.customerIp = Optional.ofNullable(builder.customerIp);

        this.hashCode = Objects.hash(topUpRequested, paymentMethod, callbackUrl, returnUrl, customerIp);
        this.toString = builder.type + "(" +
                "topUpRequested=" + topUpRequested +
                ", paymentMethod=" + paymentMethod +
                ", callbackUrl=" + callbackUrl +
                ", returnUrl=" + returnUrl +
                ", customerIp=" + customerIp +
                ')';
    }

    @Override
    public int hashCode() {
        return hashCode;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }

        if (!(obj instanceof TopUpImpl)) {
            return false;
        }

        TopUpImpl that = (TopUpImpl) obj;
        if (!Objects.equals(this.topUpRequested, that.topUpRequested)) return false;
        if (!Objects.equals(this.paymentMethod, that.paymentMethod)) return false;
        if (!Objects.equals(this.callbackUrl, that.callbackUrl)) return false;
        if (!Objects.equals(this.returnUrl, that.returnUrl)) return false;
        if (!Objects.equals(this.customerIp, that.customerIp)) return false;

        return true;
    }

    @Override
    public String toString() {
        return toString;
    }

    /** Builder for {@link TopUp} model class. */
    public static class BuilderImpl implements TopUp.Builder {
        private TopUpRequested topUpRequested = null;
        private PayinMethod paymentMethod = null;
        private String callbackUrl = null;
        private String returnUrl = null;
        private String customerIp = null;

        private final String type;

        public BuilderImpl(String type) {
            this.type = type;
        }

        public BuilderImpl() {
            this("TopUp");
        }

        /**
          * Set {@link TopUp#getTopUpRequested} property.
          *
          * 
          */
        @Override
        public BuilderImpl topUpRequested(TopUpRequested topUpRequested) {
            this.topUpRequested = topUpRequested;
            return this;
        }

        @Override
        public boolean isTopUpRequestedDefined() {
            return this.topUpRequested != null;
        }

        /**
          * Set {@link TopUp#getPaymentMethod} property.
          *
          * 
          */
        @Override
        public BuilderImpl paymentMethod(PayinMethod paymentMethod) {
            this.paymentMethod = paymentMethod;
            return this;
        }

        @Override
        public boolean isPaymentMethodDefined() {
            return this.paymentMethod != null;
        }

        /**
          * Set {@link TopUp#getCallbackUrl} property.
          *
          * This is the URL where the system will send the transaction final status after payment has been completed.

The URL must be either IP or domain-based.
          */
        @Override
        public BuilderImpl callbackUrl(String callbackUrl) {
            this.callbackUrl = callbackUrl;
            return this;
        }

        @Override
        public boolean isCallbackUrlDefined() {
            return this.callbackUrl != null;
        }

        /**
          * Set {@link TopUp#getReturnUrl} property.
          *
          * This is the URL where the customers will be redirected after completing a payment.

The URL must be either IP or domain-based.
          */
        @Override
        public BuilderImpl returnUrl(String returnUrl) {
            this.returnUrl = returnUrl;
            return this;
        }

        @Override
        public boolean isReturnUrlDefined() {
            return this.returnUrl != null;
        }

        /**
          * Set {@link TopUp#getCustomerIp} property.
          *
          * The IP address of the customer making the payment in either the IPv4 or IPv6 format.

The IP address is used for validating against the IP address whitelists and blacklists from the merchant settings.
          */
        @Override
        public BuilderImpl customerIp(String customerIp) {
            this.customerIp = customerIp;
            return this;
        }

        @Override
        public boolean isCustomerIpDefined() {
            return this.customerIp != null;
        }

        /**
         * Create new instance of {@link TopUp} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @Override
        public TopUpImpl build() {
            return new TopUpImpl(this);
        }

    }
}