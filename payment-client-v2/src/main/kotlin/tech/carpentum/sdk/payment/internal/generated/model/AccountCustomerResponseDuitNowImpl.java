//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** AccountCustomerResponseDuitNow
 *
 * Parameters of your customer's bank or wallet account which your customer sends funds from. These account parameters are used for the sender's account verification in processing of the payment.
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public class AccountCustomerResponseDuitNowImpl implements AccountCustomerResponseDuitNow {
    /** Account Name is the name of the person who holds the bank or wallet account which your customer sends funds from to make his payment. */
    private final String accountName;

    @Override
    public String getAccountName() {
        return accountName;
    }


    /** Account Number is the number of your customer's bank account which your customer sends funds from to make his payment. */
    private final Optional<String> accountNumber;

    @Override
    public Optional<String> getAccountNumber() {
        return accountNumber;
    }




    private final int hashCode;
    private final String toString;

    private AccountCustomerResponseDuitNowImpl(BuilderImpl builder) {
        this.accountName = Objects.requireNonNull(builder.accountName, "Property 'accountName' is required.");
        this.accountNumber = Optional.ofNullable(builder.accountNumber);

        this.hashCode = Objects.hash(accountName, accountNumber);
        this.toString = builder.type + "(" +
                "accountName=" + accountName +
                ", accountNumber=" + accountNumber +
                ')';
    }

    @Override
    public int hashCode() {
        return hashCode;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }

        if (!(obj instanceof AccountCustomerResponseDuitNowImpl)) {
            return false;
        }

        AccountCustomerResponseDuitNowImpl that = (AccountCustomerResponseDuitNowImpl) obj;
        if (!Objects.equals(this.accountName, that.accountName)) return false;
        if (!Objects.equals(this.accountNumber, that.accountNumber)) return false;

        return true;
    }

    @Override
    public String toString() {
        return toString;
    }

    /** Builder for {@link AccountCustomerResponseDuitNow} model class. */
    public static class BuilderImpl implements AccountCustomerResponseDuitNow.Builder {
        private String accountName = null;
        private String accountNumber = null;

        private final String type;

        public BuilderImpl(String type) {
            this.type = type;
        }

        public BuilderImpl() {
            this("AccountCustomerResponseDuitNow");
        }

        /**
          * Set {@link AccountCustomerResponseDuitNow#getAccountName} property.
          *
          * Account Name is the name of the person who holds the bank or wallet account which your customer sends funds from to make his payment.
          */
        @Override
        public BuilderImpl accountName(String accountName) {
            this.accountName = accountName;
            return this;
        }

        @Override
        public boolean isAccountNameDefined() {
            return this.accountName != null;
        }

        /**
          * Set {@link AccountCustomerResponseDuitNow#getAccountNumber} property.
          *
          * Account Number is the number of your customer's bank account which your customer sends funds from to make his payment.
          */
        @Override
        public BuilderImpl accountNumber(String accountNumber) {
            this.accountNumber = accountNumber;
            return this;
        }

        @Override
        public boolean isAccountNumberDefined() {
            return this.accountNumber != null;
        }

        /**
         * Create new instance of {@link AccountCustomerResponseDuitNow} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @Override
        public AccountCustomerResponseDuitNowImpl build() {
            return new AccountCustomerResponseDuitNowImpl(this);
        }

    }
}