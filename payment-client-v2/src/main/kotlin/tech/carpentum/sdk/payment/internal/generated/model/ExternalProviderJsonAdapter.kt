//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.ExternalProvider

class ExternalProviderJsonAdapter {
    @FromJson
    fun fromJson(json: ExternalProviderJson): ExternalProvider {
        val builder = ExternalProvider.builder()
        builder.code(json.code)
        builder.name(json.name)
        return builder.build()
    }

    @ToJson
    fun toJson(model: ExternalProvider): ExternalProviderJson {
        val json = ExternalProviderJson()
        json.code = model.code
        json.name = model.name
        return json
    }

    @FromJson
    fun fromJsonImpl(model: ExternalProvider): ExternalProviderImpl {
        return model as ExternalProviderImpl
    }

    @ToJson
    fun toJsonImpl(impl: ExternalProviderImpl): ExternalProvider {
        return impl
    }

}