//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** AvailablePayinOptionList
 *
 * The list of possible payment options.
The list always contains at least one payment option.
If no available payment options are found, an HTTP 406 response is returned with the error code.
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public interface AvailablePayinOptionList {

    @NotNull java.util.List<@NotNull AvailablePayinOption> getData();

    @NotNull java.util.List<@NotNull AvailablePayinOptionVariantCurrency> getDataVariantCurrencies();

    @NotNull static Builder builder(AvailablePayinOptionList copyOf) {
        Builder builder = builder();
        builder.data(copyOf.getData());
        builder.dataVariantCurrencies(copyOf.getDataVariantCurrencies());
        return builder;
    }

    @NotNull static Builder builder() {
        return new AvailablePayinOptionListImpl.BuilderImpl();
    }

    /** Builder for {@link AvailablePayinOptionList} model class. */
    interface Builder {

        /**
          * Replace all items in {@link AvailablePayinOptionList#getData} list property.
          *
          * 
          */
        @NotNull Builder data(java.util.List<@NotNull AvailablePayinOption> data);
        /**
          * Add single item to {@link AvailablePayinOptionList#getData} list property.
          *
          * 
          */
        @NotNull Builder dataAdd(AvailablePayinOption item);
        /**
          * Add all items to {@link AvailablePayinOptionList#getData} list property.
          *
          * 
          */
        @NotNull Builder dataAddAll(java.util.List<@NotNull AvailablePayinOption> data);


        /**
          * Replace all items in {@link AvailablePayinOptionList#getDataVariantCurrencies} list property.
          *
          * 
          */
        @NotNull Builder dataVariantCurrencies(java.util.List<@NotNull AvailablePayinOptionVariantCurrency> dataVariantCurrencies);
        /**
          * Add single item to {@link AvailablePayinOptionList#getDataVariantCurrencies} list property.
          *
          * 
          */
        @NotNull Builder dataVariantCurrenciesAdd(AvailablePayinOptionVariantCurrency item);
        /**
          * Add all items to {@link AvailablePayinOptionList#getDataVariantCurrencies} list property.
          *
          * 
          */
        @NotNull Builder dataVariantCurrenciesAddAll(java.util.List<@NotNull AvailablePayinOptionVariantCurrency> dataVariantCurrencies);


        /**
         * Create new instance of {@link AvailablePayinOptionList} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @NotNull AvailablePayinOptionList build();

    }
}