//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** CRYPTO_TRANSFER
 *
 * Payment method for sending funds from your crypto wallet to external crypto wallet.
It requires customer to provide a receiving crypto wallet details in format related to chosen blockchain protocol.
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public class SettlementCryptoTransferMethodImpl implements SettlementCryptoTransferMethod {
    private final AccountSettlementRequestCryptoTransfer account;

    @Override
    public AccountSettlementRequestCryptoTransfer getAccount() {
        return account;
    }


    /** One of following can serve as Payment Operator:

 * Financial or other institution (such as bank, card payment processor, ...) that manages transactions for your customers
 * Mobile wallet
 * Blockchain protocol for crypto currency payments

Customer is informed with the payment instructions where funds have to be transferred efficiently based on the selected Payment operator.
For getting list of the available payment options for payins use [POST /payins/!availablePaymentOptions](#operations-Incoming_payments-availablePaymentOptions) API, for payouts use [POST /payouts/!availablePaymentOptions](#operations-Outgoing_payments-availablePaymentOptions) API. */
    private final String paymentOperatorCode;

    @Override
    public String getPaymentOperatorCode() {
        return paymentOperatorCode;
    }


    /** Reference number of transaction. */
    private final Optional<String> transactionReference;

    @Override
    public Optional<String> getTransactionReference() {
        return transactionReference;
    }


    @Override public PaymentMethodCode getPaymentMethodCode() { return PAYMENT_METHOD_CODE; }

    private final int hashCode;
    private final String toString;

    private SettlementCryptoTransferMethodImpl(BuilderImpl builder) {
        this.account = Objects.requireNonNull(builder.account, "Property 'account' is required.");
        this.paymentOperatorCode = Objects.requireNonNull(builder.paymentOperatorCode, "Property 'paymentOperatorCode' is required.");
        this.transactionReference = Optional.ofNullable(builder.transactionReference);

        this.hashCode = Objects.hash(account, paymentOperatorCode, transactionReference);
        this.toString = builder.type + "(" +
                "account=" + account +
                ", paymentOperatorCode=" + paymentOperatorCode +
                ", transactionReference=" + transactionReference +
                ')';
    }

    @Override
    public int hashCode() {
        return hashCode;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }

        if (!(obj instanceof SettlementCryptoTransferMethodImpl)) {
            return false;
        }

        SettlementCryptoTransferMethodImpl that = (SettlementCryptoTransferMethodImpl) obj;
        if (!Objects.equals(this.account, that.account)) return false;
        if (!Objects.equals(this.paymentOperatorCode, that.paymentOperatorCode)) return false;
        if (!Objects.equals(this.transactionReference, that.transactionReference)) return false;

        return true;
    }

    @Override
    public String toString() {
        return toString;
    }

    /** Builder for {@link SettlementCryptoTransferMethod} model class. */
    public static class BuilderImpl implements SettlementCryptoTransferMethod.Builder {
        private AccountSettlementRequestCryptoTransfer account = null;
        private String paymentOperatorCode = null;
        private String transactionReference = null;

        private final String type;

        public BuilderImpl(String type) {
            this.type = type;
        }

        public BuilderImpl() {
            this("SettlementCryptoTransferMethod");
        }

        /**
          * Set {@link SettlementCryptoTransferMethod#getAccount} property.
          *
          * 
          */
        @Override
        public BuilderImpl account(AccountSettlementRequestCryptoTransfer account) {
            this.account = account;
            return this;
        }

        @Override
        public boolean isAccountDefined() {
            return this.account != null;
        }

        /**
          * Set {@link SettlementCryptoTransferMethod#getPaymentOperatorCode} property.
          *
          * One of following can serve as Payment Operator:

 * Financial or other institution (such as bank, card payment processor, ...) that manages transactions for your customers
 * Mobile wallet
 * Blockchain protocol for crypto currency payments

Customer is informed with the payment instructions where funds have to be transferred efficiently based on the selected Payment operator.
For getting list of the available payment options for payins use [POST /payins/!availablePaymentOptions](#operations-Incoming_payments-availablePaymentOptions) API, for payouts use [POST /payouts/!availablePaymentOptions](#operations-Outgoing_payments-availablePaymentOptions) API.
          */
        @Override
        public BuilderImpl paymentOperatorCode(String paymentOperatorCode) {
            this.paymentOperatorCode = paymentOperatorCode;
            return this;
        }

        @Override
        public boolean isPaymentOperatorCodeDefined() {
            return this.paymentOperatorCode != null;
        }

        /**
          * Set {@link SettlementCryptoTransferMethod#getTransactionReference} property.
          *
          * Reference number of transaction.
          */
        @Override
        public BuilderImpl transactionReference(String transactionReference) {
            this.transactionReference = transactionReference;
            return this;
        }

        @Override
        public boolean isTransactionReferenceDefined() {
            return this.transactionReference != null;
        }

        /**
         * Create new instance of {@link SettlementCryptoTransferMethod} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @Override
        public SettlementCryptoTransferMethodImpl build() {
            return new SettlementCryptoTransferMethodImpl(this);
        }

    }
}