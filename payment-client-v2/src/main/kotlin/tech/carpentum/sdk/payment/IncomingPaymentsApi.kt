@file:JvmName("IncomingPaymentsApiUtils")

package tech.carpentum.sdk.payment

import tech.carpentum.sdk.payment.EndpointDefinition.Method.GET
import tech.carpentum.sdk.payment.EndpointDefinition.Method.POST
import tech.carpentum.sdk.payment.IncomingPaymentsApi.Factory
import tech.carpentum.sdk.payment.internal.api.ClientErrorExceptionFactory
import tech.carpentum.sdk.payment.internal.api.EnhancedIncomingPaymentsApi
import tech.carpentum.sdk.payment.internal.api.GetPayinErrorExceptionFactory
import tech.carpentum.sdk.payment.internal.api.GetTopupErrorExceptionFactory
import tech.carpentum.sdk.payment.internal.api.PostAvailablePayinOptionsErrorExceptionFactory
import tech.carpentum.sdk.payment.internal.api.PostAvailableTopUpOptionsErrorExceptionFactory
import tech.carpentum.sdk.payment.internal.api.PostPayinsErrorExceptionFactory
import tech.carpentum.sdk.payment.internal.api.ResponseExceptionUtils
import tech.carpentum.sdk.payment.model.AvailablePayinOptionList
import tech.carpentum.sdk.payment.model.AvailableTopUpOptions
import tech.carpentum.sdk.payment.model.ExternalReference
import tech.carpentum.sdk.payment.model.GetPayinError
import tech.carpentum.sdk.payment.model.Payin
import tech.carpentum.sdk.payment.model.PayinAcceptedResponse
import tech.carpentum.sdk.payment.model.PayinDetail
import tech.carpentum.sdk.payment.model.PaymentAccountDetail
import tech.carpentum.sdk.payment.model.PaymentRequested
import tech.carpentum.sdk.payment.model.PostAvailablePayinOptionsError
import tech.carpentum.sdk.payment.model.PostAvailableTopUpOptionsError
import tech.carpentum.sdk.payment.model.PostPayinsError
import tech.carpentum.sdk.payment.model.TopUp
import tech.carpentum.sdk.payment.model.TopUpAcceptedResponse
import tech.carpentum.sdk.payment.model.TopUpDetail
import tech.carpentum.sdk.payment.model.TopUpRequested
import java.io.InterruptedIOException
import java.time.Duration

/**
 * The Incoming payment (payin, top-up) is an operation where money is sent from the customer account to the merchant account RESTful API.
 *
 * Use [Factory] to create new instance of the class.
 */
class IncomingPaymentsApi private constructor(
    private val apiVersion: Int,
    private val api: EnhancedIncomingPaymentsApi
) {

    /**
     * Throws [PostAvailablePayinOptionsErrorException] ("406" response) with one of defined
     * [PostAvailablePayinOptionsError] business validation error code.
     * Throws [InterruptedIOException] in case of timeout.
     */
    @Throws(ResponseException::class, InterruptedIOException::class)
    // tag::userGuidePublicApi[]
    fun payinAvailablePaymentOptions(paymentRequested: PaymentRequested): AvailablePayinOptionList
    // end::userGuidePublicApi[]
    {
        return ResponseExceptionUtils.wrap(PostAvailablePayinOptionsErrorExceptionFactory.instance) {
            api.payinAvailablePaymentOptions(xAPIVersion = apiVersion, paymentRequested = paymentRequested)
        }
    }

    /**
     * Throws [PostPayinsErrorException] ("406" response) with one of defined
     * [PostPayinsError] business validation error code.
     * Throws [InterruptedIOException] in case of timeout.
     */
    @Throws(ResponseException::class, InterruptedIOException::class)
    // tag::userGuidePublicApi[]
    fun createPayin(idPayin: String, payin: Payin): PayinAcceptedResponse
    // end::userGuidePublicApi[]
    {
        return ResponseExceptionUtils.wrap(PostPayinsErrorExceptionFactory.instance) {
            api.createPayin(xAPIVersion = apiVersion, idPayin = idPayin, payin = payin)
        }
    }

    /**
     * Throws [InterruptedIOException] in case of timeout.
     */
    @Throws(ResponseException::class, InterruptedIOException::class)
    // tag::userGuidePublicApi[]
    fun setPayinExternalReference(idPayin: String, reference: String)
    // end::userGuidePublicApi[]
    {
        return ResponseExceptionUtils.wrap(ClientErrorExceptionFactory.instance) {
            api.payinSetExternalReference(
                xAPIVersion = apiVersion,
                idPayin = idPayin,
                externalReference = ExternalReference.ofReference(reference)
            )
        }
    }

    /**
     * Throws [GetPayinErrorException] ("406" response) with one of defined
     * [GetPayinError] business validation error code.
     * Throws [InterruptedIOException] in case of timeout.
     */
    @Throws(ResponseException::class, InterruptedIOException::class)
    // tag::userGuidePublicApi[]
    fun getPayin(idPayin: String): PayinDetail
    // end::userGuidePublicApi[]
    {
        return ResponseExceptionUtils.wrap(GetPayinErrorExceptionFactory.instance) {
            api.getPayin(xAPIVersion = apiVersion, idPayin = idPayin)
        }
    }

    /**
     * Throws [GetPayinErrorException] ("406" response) with one of defined
     * [GetPayinError] business validation error code.
     * Throws [InterruptedIOException] in case of timeout.
     */
    @Throws(ResponseException::class, InterruptedIOException::class)
    // tag::userGuidePublicApi[]
    fun getPayinAccounts(idPayin: String): PaymentAccountDetail
    // end::userGuidePublicApi[]
    {
        return ResponseExceptionUtils.wrap(GetPayinErrorExceptionFactory.instance) {
            api.payinAccounts(xAPIVersion = apiVersion, idPayin = idPayin)
        }
    }

    /**
     * Throws [PostAvailableTopUpOptionsErrorException] ("406" response) with one of defined
     * [PostAvailableTopUpOptionsError] business validation error code.
     * Throws [InterruptedIOException] in case of timeout.
     */
    @Throws(ResponseException::class, InterruptedIOException::class)
    // tag::userGuidePublicApi[]
    fun topupAvailablePaymentOptions(topUpRequested: TopUpRequested): AvailableTopUpOptions
    // end::userGuidePublicApi[]
    {
        return ResponseExceptionUtils.wrap(PostAvailableTopUpOptionsErrorExceptionFactory.instance) {
            api.topupAvailablePaymentOptions(xAPIVersion = apiVersion, topUpRequested = topUpRequested)
        }
    }

    /**
     * Throws [PostPayinsErrorException] ("406" response) with one of defined
     * [PostPayinsError] business validation error code.
     * Throws [InterruptedIOException] in case of timeout.
     */
    @Throws(ResponseException::class, InterruptedIOException::class)
    // tag::userGuidePublicApi[]
    fun createTopup(idTopUp: String, topUp: TopUp): TopUpAcceptedResponse
    // end::userGuidePublicApi[]
    {
        return ResponseExceptionUtils.wrap(PostPayinsErrorExceptionFactory.instance) {
            api.createTopup(xAPIVersion = apiVersion, idTopUp = idTopUp, topUp = topUp)
        }
    }

    /**
     * Throws [GetTopupErrorException] ("406" response) with one of defined
     * [GetTopupError] business validation error code.
     * Throws [InterruptedIOException] in case of timeout.
     */
    @Throws(ResponseException::class, InterruptedIOException::class)
    // tag::userGuidePublicApi[]
    fun getTopup(idTopUp: String): TopUpDetail
    // end::userGuidePublicApi[]
    {
        return ResponseExceptionUtils.wrap(GetTopupErrorExceptionFactory.instance) {
            api.getTopup(xAPIVersion = apiVersion, idTopUp = idTopUp)
        }
    }

    /**
     * Factory to create a new instance of [IncomingPaymentsApi].
     */
    companion object Factory {
        /**
         * Endpoint definition for [IncomingPaymentsApi.payinAvailablePaymentOptions] method.
         */
        @JvmStatic
        fun definePayinAvailablePaymentOptionsEndpoint(): EndpointDefinition =
            EndpointDefinition(POST, "/payins/!availablePaymentOptions")

        /**
         * Endpoint definition for [IncomingPaymentsApi.createPayin] method.
         */
        @JvmStatic
        fun defineCreatePayinEndpoint(): EndpointWithIdDefinition = EndpointWithIdDefinition(POST, "/payins/{id}")

        /**
         * Endpoint definition for [IncomingPaymentsApi.setPayinExternalReference] method.
         */
        @JvmStatic
        fun defineSetPayinExternalReferenceEndpoint(): EndpointWithIdDefinition =
            EndpointWithIdDefinition(POST, "/payins/{id}/!setExternalReference")

        /**
         * Endpoint definition for [IncomingPaymentsApi.getPayin] method.
         */
        @JvmStatic
        fun defineGetPayinEndpoint(): EndpointWithIdDefinition = EndpointWithIdDefinition(GET, "/payins/{id}")

        /**
         * Endpoint definition for [IncomingPaymentsApi.getPayinAccounts] method.
         */
        @JvmStatic
        fun defineGetPayinAccountsEndpoint(): EndpointWithIdDefinition =
            EndpointWithIdDefinition(GET, "/payins/{id}/accounts")

        /**
         * Endpoint definition for [IncomingPaymentsApi.topupAvailablePaymentOptions] method.
         */
        @JvmStatic
        fun defineTopUpAvailablePaymentOptionsEndpoint(): EndpointDefinition =
            EndpointDefinition(POST, "/topups/!availablePaymentOptions")

        /**
         * Endpoint definition for [IncomingPaymentsApi.createTopup] method.
         */
        @JvmStatic
        fun defineCreateTopUpEndpoint(): EndpointWithIdDefinition = EndpointWithIdDefinition(POST, "/topups/{id}")

        /**
         * Endpoint definition for [IncomingPaymentsApi.getTopup] method.
         */
        @JvmStatic
        fun defineGetTopUpEndpoint(): EndpointWithIdDefinition = EndpointWithIdDefinition(GET, "/topups/{id}")

        @JvmStatic
        @JvmOverloads
        fun create(context: PaymentContext, accessToken: String, callTimeout: Duration? = null): IncomingPaymentsApi {
            return IncomingPaymentsApi(
                apiVersion = context.apiVersion,
                api = EnhancedIncomingPaymentsApi(
                    basePath = context.apiBaseUrl,
                    accessToken = accessToken,
                    brand = context.brand,
                    callTimeout = callTimeout ?: context.defaultCallTimeout
                )
            )
        }
    }

}

/**
 * Grants [IncomingPaymentsApi.payinAvailablePaymentOptions] endpoint, see [IncomingPaymentsApi.definePayinAvailablePaymentOptionsEndpoint] definition.
 */
@JvmName("grantPayinAvailablePaymentOptionsEndpoint")
fun AuthTokenOperations.grantIncomingPaymentsApiPayinAvailablePaymentOptionsEndpoint(): AuthTokenOperations =
    this.grant(IncomingPaymentsApi.definePayinAvailablePaymentOptionsEndpoint())

/**
 * Grants [IncomingPaymentsApi.createPayin] endpoint, see [IncomingPaymentsApi.defineCreatePayinEndpoint] definition.
 */
@JvmName("grantCreatePayinEndpointForId")
fun AuthTokenOperations.grantIncomingPaymentsApiCreatePayinEndpointForId(id: String): AuthTokenOperations =
    this.grant(IncomingPaymentsApi.defineCreatePayinEndpoint().forId(id))

/**
 * Grants [IncomingPaymentsApi.createPayin] endpoint, see [IncomingPaymentsApi.defineCreatePayinEndpoint] definition.
 */
@JvmName("grantCreatePayinEndpointAnyId")
fun AuthTokenOperations.grantIncomingPaymentsApiCreatePayinEndpointAnyId(): AuthTokenOperations =
    this.grant(IncomingPaymentsApi.defineCreatePayinEndpoint().anyId())

/**
 * Grants [IncomingPaymentsApi.setPayinExternalReference] endpoint, see [IncomingPaymentsApi.defineSetPayinExternalReferenceEndpoint] definition.
 */
@JvmName("grantSetPayinExternalReferenceEndpointForId")
fun AuthTokenOperations.grantIncomingPaymentsApiSetPayinExternalReferenceEndpointForId(id: String): AuthTokenOperations =
    this.grant(IncomingPaymentsApi.defineSetPayinExternalReferenceEndpoint().forId(id))

/**
 * Grants [IncomingPaymentsApi.setPayinExternalReference] endpoint, see [IncomingPaymentsApi.defineSetPayinExternalReferenceEndpoint] definition.
 */
@JvmName("grantSetPayinExternalReferenceEndpointAnyId")
fun AuthTokenOperations.grantIncomingPaymentsApiSetPayinExternalReferenceEndpointAnyId(): AuthTokenOperations =
    this.grant(IncomingPaymentsApi.defineSetPayinExternalReferenceEndpoint().anyId())

/**
 * Grants [IncomingPaymentsApi.getPayin] endpoint, see [IncomingPaymentsApi.defineGetPayinEndpoint] definition.
 */
@JvmName("grantGetPayinEndpointForId")
fun AuthTokenOperations.grantIncomingPaymentsApiGetPayinEndpointForId(id: String): AuthTokenOperations =
    this.grant(IncomingPaymentsApi.defineGetPayinEndpoint().forId(id))

/**
 * Grants [IncomingPaymentsApi.getPayin] endpoint, see [IncomingPaymentsApi.defineGetPayinEndpoint] definition.
 */
@JvmName("grantGetPayinEndpointAnyId")
fun AuthTokenOperations.grantIncomingPaymentsApiGetPayinEndpointAnyId(): AuthTokenOperations =
    this.grant(IncomingPaymentsApi.defineGetPayinEndpoint().anyId())

/**
 * Grants [IncomingPaymentsApi.getPayinAccounts] endpoint, see [IncomingPaymentsApi.defineGetPayinAccountsEndpoint] definition.
 */
@JvmName("grantGetPayinAccountsEndpointForId")
fun AuthTokenOperations.grantIncomingPaymentsApiGetPayinAccountsEndpointForId(id: String): AuthTokenOperations =
    this.grant(IncomingPaymentsApi.defineGetPayinAccountsEndpoint().forId(id))

/**
 * Grants [IncomingPaymentsApi.getPayinAccounts] endpoint, see [IncomingPaymentsApi.defineGetPayinAccountsEndpoint] definition.
 */
@JvmName("grantGetPayinAccountsEndpointAnyId")
fun AuthTokenOperations.grantIncomingPaymentsApiGetPayinAccountsEndpointAnyId(): AuthTokenOperations =
    this.grant(IncomingPaymentsApi.defineGetPayinAccountsEndpoint().anyId())

/**
 * Grants [IncomingPaymentsApi.topupAvailablePaymentOptions] endpoint, see [IncomingPaymentsApi.defineTopUpAvailablePaymentOptionsEndpoint] definition.
 */
@JvmName("grantTopUpAvailablePaymentOptionsEndpoint")
fun AuthTokenOperations.grantIncomingPaymentsApiTopUpAvailablePaymentOptionsEndpoint(): AuthTokenOperations =
    this.grant(IncomingPaymentsApi.defineTopUpAvailablePaymentOptionsEndpoint())

/**
 * Grants [IncomingPaymentsApi.createTopup] endpoint, see [IncomingPaymentsApi.defineCreateTopUpEndpoint] definition.
 */
@JvmName("grantCreateTopUpEndpointForId")
fun AuthTokenOperations.grantIncomingPaymentsApiCreateTopUpEndpointForId(id: String): AuthTokenOperations =
    this.grant(IncomingPaymentsApi.defineCreateTopUpEndpoint().forId(id))

/**
 * Grants [IncomingPaymentsApi.createTopup] endpoint, see [IncomingPaymentsApi.defineCreateTopUpEndpoint] definition.
 */
@JvmName("grantCreateTopUpEndpointAnyId")
fun AuthTokenOperations.grantIncomingPaymentsApiCreateTopUpEndpointAnyId(): AuthTokenOperations =
    this.grant(IncomingPaymentsApi.defineCreateTopUpEndpoint().anyId())

/**
 * Grants [IncomingPaymentsApi.getTopup] endpoint, see [IncomingPaymentsApi.defineGetTopUpEndpoint] definition.
 */
@JvmName("grantGetTopUpEndpointForId")
fun AuthTokenOperations.grantIncomingPaymentsApiGetTopUpEndpointForId(id: String): AuthTokenOperations =
    this.grant(IncomingPaymentsApi.defineGetTopUpEndpoint().forId(id))

/**
 * Grants [IncomingPaymentsApi.getTopup] endpoint, see [IncomingPaymentsApi.defineGetTopUpEndpoint] definition.
 */
@JvmName("grantGetTopUpEndpointAnyId")
fun AuthTokenOperations.grantIncomingPaymentsApiGetTopUpEndpointAnyId(): AuthTokenOperations =
    this.grant(IncomingPaymentsApi.defineGetTopUpEndpoint().anyId())
