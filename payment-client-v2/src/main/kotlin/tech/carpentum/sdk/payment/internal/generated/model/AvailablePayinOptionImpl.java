//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** AvailablePayinOption
 *
 * 
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public class AvailablePayinOptionImpl implements AvailablePayinOption {
    private final PayinMethodCode paymentMethodCode;

    @Override
    public PayinMethodCode getPaymentMethodCode() {
        return paymentMethodCode;
    }


    private final java.util.List<@NotNull PaymentOperatorIncoming> paymentOperators;

    @Override
    public java.util.List<@NotNull PaymentOperatorIncoming> getPaymentOperators() {
        return paymentOperators;
    }


    private final Optional<SegmentCode> segmentCode;

    @Override
    public Optional<SegmentCode> getSegmentCode() {
        return segmentCode;
    }




    private final int hashCode;
    private final String toString;

    private AvailablePayinOptionImpl(BuilderImpl builder) {
        this.paymentMethodCode = Objects.requireNonNull(builder.paymentMethodCode, "Property 'paymentMethodCode' is required.");
        this.paymentOperators = java.util.Collections.unmodifiableList(builder.paymentOperators);
        this.segmentCode = Optional.ofNullable(builder.segmentCode);

        this.hashCode = Objects.hash(paymentMethodCode, paymentOperators, segmentCode);
        this.toString = builder.type + "(" +
                "paymentMethodCode=" + paymentMethodCode +
                ", paymentOperators=" + paymentOperators +
                ", segmentCode=" + segmentCode +
                ')';
    }

    @Override
    public int hashCode() {
        return hashCode;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }

        if (!(obj instanceof AvailablePayinOptionImpl)) {
            return false;
        }

        AvailablePayinOptionImpl that = (AvailablePayinOptionImpl) obj;
        if (!Objects.equals(this.paymentMethodCode, that.paymentMethodCode)) return false;
        if (!Objects.equals(this.paymentOperators, that.paymentOperators)) return false;
        if (!Objects.equals(this.segmentCode, that.segmentCode)) return false;

        return true;
    }

    @Override
    public String toString() {
        return toString;
    }

    /** Builder for {@link AvailablePayinOption} model class. */
    public static class BuilderImpl implements AvailablePayinOption.Builder {
        private PayinMethodCode paymentMethodCode = null;
        private java.util.List<@NotNull PaymentOperatorIncoming> paymentOperators = new java.util.ArrayList<>();
        private SegmentCode segmentCode = null;

        private final String type;

        public BuilderImpl(String type) {
            this.type = type;
        }

        public BuilderImpl() {
            this("AvailablePayinOption");
        }

        /**
          * Set {@link AvailablePayinOption#getPaymentMethodCode} property.
          *
          * 
          */
        @Override
        public BuilderImpl paymentMethodCode(PayinMethodCode paymentMethodCode) {
            this.paymentMethodCode = paymentMethodCode;
            return this;
        }

        @Override
        public boolean isPaymentMethodCodeDefined() {
            return this.paymentMethodCode != null;
        }

        /**
          * Replace all items in {@link AvailablePayinOption#getPaymentOperators} list property.
          *
          * 
          */
        @Override
        public BuilderImpl paymentOperators(java.util.List<@NotNull PaymentOperatorIncoming> paymentOperators) {
            this.paymentOperators.clear();
            if (paymentOperators != null) {
                this.paymentOperators.addAll(paymentOperators);
            }
            return this;
        }
        /**
          * Add single item to {@link AvailablePayinOption#getPaymentOperators} list property.
          *
          * 
          */
        @Override
        public BuilderImpl paymentOperatorsAdd(PaymentOperatorIncoming item) {
            if (item != null) {
                this.paymentOperators.add(item);
            }
            return this;
        }
        /**
          * Add all items to {@link AvailablePayinOption#getPaymentOperators} list property.
          *
          * 
          */
        @Override
        public BuilderImpl paymentOperatorsAddAll(java.util.List<@NotNull PaymentOperatorIncoming> paymentOperators) {
            if (paymentOperators != null) {
                this.paymentOperators.addAll(paymentOperators);
            }
            return this;
        }


        /**
          * Set {@link AvailablePayinOption#getSegmentCode} property.
          *
          * 
          */
        @Override
        public BuilderImpl segmentCode(SegmentCode segmentCode) {
            this.segmentCode = segmentCode;
            return this;
        }

        @Override
        public boolean isSegmentCodeDefined() {
            return this.segmentCode != null;
        }

        /**
         * Create new instance of {@link AvailablePayinOption} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @Override
        public AvailablePayinOptionImpl build() {
            return new AvailablePayinOptionImpl(this);
        }

    }
}