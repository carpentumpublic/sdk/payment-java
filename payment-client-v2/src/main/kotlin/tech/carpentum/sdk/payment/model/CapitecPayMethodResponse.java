//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** CAPITEC_PAY
 *
 * 
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public interface CapitecPayMethodResponse extends PayinMethodResponse {
    /** A discriminator value of property {@link #getPaymentMethodCode}. The model class extends {@link PayinMethodResponse}. */
    PayinMethodResponse.PaymentMethodCode PAYMENT_METHOD_CODE = PayinMethodResponse.PaymentMethodCode.CAPITEC_PAY;

    @NotNull IdPayin getIdPayin();

    @NotNull IdPayment getIdPayment();

    @NotNull MoneyPaymentResponse getMoney();

    @NotNull Optional<MoneyVat> getVat();

    @NotNull String getMerchantName();

    @NotNull Optional<AccountCustomerResponseCapitecPay> getAccountCustomer();

    /** Your customer mobile phone number. */
    @NotNull Optional<String> getPhoneNumber();

    @NotNull Optional<String> getSaid();

    /** Reference number of transaction. */
    @NotNull String getReference();

    /** This is the URL where the customers will be redirected after completing a payment.

The URL must be either IP or domain-based. */
    @NotNull String getReturnUrl();

    /** Date and time when payment was accepted. */
    @NotNull java.time.OffsetDateTime getAcceptedAt();

    /** Date and time of payment expiration. If no money has been transferred to this time, payment is considered failed and callback with status change event will shortly follow. */
    @NotNull java.time.OffsetDateTime getExpireAt();

    @NotNull static Builder builder(CapitecPayMethodResponse copyOf) {
        Builder builder = builder();
        builder.idPayin(copyOf.getIdPayin());
        builder.idPayment(copyOf.getIdPayment());
        builder.money(copyOf.getMoney());
        builder.vat(copyOf.getVat().orElse(null));
        builder.merchantName(copyOf.getMerchantName());
        builder.accountCustomer(copyOf.getAccountCustomer().orElse(null));
        builder.phoneNumber(copyOf.getPhoneNumber().orElse(null));
        builder.said(copyOf.getSaid().orElse(null));
        builder.reference(copyOf.getReference());
        builder.returnUrl(copyOf.getReturnUrl());
        builder.acceptedAt(copyOf.getAcceptedAt());
        builder.expireAt(copyOf.getExpireAt());
        return builder;
    }

    @NotNull static Builder builder() {
        return new CapitecPayMethodResponseImpl.BuilderImpl();
    }

    /** Builder for {@link CapitecPayMethodResponse} model class. */
    interface Builder {

        /**
          * Set {@link CapitecPayMethodResponse#getIdPayin} property.
          *
          * 
          */
        @NotNull Builder idPayin(IdPayin idPayin);

        boolean isIdPayinDefined();


        /**
          * Set {@link CapitecPayMethodResponse#getIdPayment} property.
          *
          * 
          */
        @NotNull Builder idPayment(IdPayment idPayment);

        boolean isIdPaymentDefined();


        /**
          * Set {@link CapitecPayMethodResponse#getMoney} property.
          *
          * 
          */
        @NotNull Builder money(MoneyPaymentResponse money);

        boolean isMoneyDefined();


        /**
          * Set {@link CapitecPayMethodResponse#getVat} property.
          *
          * 
          */
        @NotNull Builder vat(MoneyVat vat);

        boolean isVatDefined();


        /**
          * Set {@link CapitecPayMethodResponse#getMerchantName} property.
          *
          * 
          */
        @NotNull Builder merchantName(String merchantName);

        boolean isMerchantNameDefined();


        /**
          * Set {@link CapitecPayMethodResponse#getAccountCustomer} property.
          *
          * 
          */
        @NotNull Builder accountCustomer(AccountCustomerResponseCapitecPay accountCustomer);

        boolean isAccountCustomerDefined();


        /**
          * Set {@link CapitecPayMethodResponse#getPhoneNumber} property.
          *
          * Your customer mobile phone number.
          */
        @NotNull Builder phoneNumber(String phoneNumber);

        boolean isPhoneNumberDefined();


        /**
          * Set {@link CapitecPayMethodResponse#getSaid} property.
          *
          * 
          */
        @NotNull Builder said(String said);

        boolean isSaidDefined();


        /**
          * Set {@link CapitecPayMethodResponse#getReference} property.
          *
          * Reference number of transaction.
          */
        @NotNull Builder reference(String reference);

        boolean isReferenceDefined();


        /**
          * Set {@link CapitecPayMethodResponse#getReturnUrl} property.
          *
          * This is the URL where the customers will be redirected after completing a payment.

The URL must be either IP or domain-based.
          */
        @NotNull Builder returnUrl(String returnUrl);

        boolean isReturnUrlDefined();


        /**
          * Set {@link CapitecPayMethodResponse#getAcceptedAt} property.
          *
          * Date and time when payment was accepted.
          */
        @NotNull Builder acceptedAt(java.time.OffsetDateTime acceptedAt);

        boolean isAcceptedAtDefined();


        /**
          * Set {@link CapitecPayMethodResponse#getExpireAt} property.
          *
          * Date and time of payment expiration. If no money has been transferred to this time, payment is considered failed and callback with status change event will shortly follow.
          */
        @NotNull Builder expireAt(java.time.OffsetDateTime expireAt);

        boolean isExpireAtDefined();


        /**
         * Create new instance of {@link CapitecPayMethodResponse} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @NotNull CapitecPayMethodResponse build();

    }
}