//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** GetPayoutError
 *
 * 
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public class GetPayoutError extends BusinessValidationError {
    // tag::codeEnum[]
    /** @see #getCode */
    public static final String CODE_MERCHANT_INACTIVE = "MERCHANT_INACTIVE";
    // end::codeEnum[]








    private GetPayoutError(GetPayoutError.Builder builder) {
        super(builder);
    }

    @NotNull public static Builder builder() {
        return new Builder();
    }

    /** Builder for {@link GetPayoutError} model class. */
    public static class Builder extends BusinessValidationError.Builder<GetPayoutError, Builder> {
        private Builder() {}

        /**
         * Create new instance of {@link GetPayoutError} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @NotNull public GetPayoutError build() {
            return new GetPayoutError(this);
        }
    }
}