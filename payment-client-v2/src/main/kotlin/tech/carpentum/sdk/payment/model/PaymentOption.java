//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** PaymentOption
 *
 * 
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public interface PaymentOption {

    // tag::paymentTypeCodeEnum[]
    /** @see #getPaymentTypeCode */
    public static final String PAYMENT_TYPE_CODE_PAYIN = "PAYIN";
    /** @see #getPaymentTypeCode */
    public static final String PAYMENT_TYPE_CODE_PAYOUT = "PAYOUT";
    // end::paymentTypeCodeEnum[]
    /** Payment direction from the merchant's point of view. */
    @NotNull String getPaymentTypeCode();

    @NotNull PayinMethodCode getPaymentMethodCode();

    @NotNull CurrencyCode getCurrencyCode();

    @NotNull Optional<SegmentCode> getSegmentCode();

    @NotNull IntervalNumberTo getTransactionAmountLimit();

    /** If set to false, the option is not currently available and must be activated in administration. */
    @NotNull Boolean getIsAvailable();

    /** Payment operators that can be used for paying via this payment option. */
    @NotNull java.util.List<@NotNull PaymentOperatorOption> getPaymentOperators();

    @NotNull static Builder builder(PaymentOption copyOf) {
        Builder builder = builder();
        builder.paymentTypeCode(copyOf.getPaymentTypeCode());
        builder.paymentMethodCode(copyOf.getPaymentMethodCode());
        builder.currencyCode(copyOf.getCurrencyCode());
        builder.segmentCode(copyOf.getSegmentCode().orElse(null));
        builder.transactionAmountLimit(copyOf.getTransactionAmountLimit());
        builder.isAvailable(copyOf.getIsAvailable());
        builder.paymentOperators(copyOf.getPaymentOperators());
        return builder;
    }

    @NotNull static Builder builder() {
        return new PaymentOptionImpl.BuilderImpl();
    }

    /** Builder for {@link PaymentOption} model class. */
    interface Builder {

        /**
          * Set {@link PaymentOption#getPaymentTypeCode} property.
          *
          * Payment direction from the merchant's point of view.
          */
        @NotNull Builder paymentTypeCode(String paymentTypeCode);

        boolean isPaymentTypeCodeDefined();


        /**
          * Set {@link PaymentOption#getPaymentMethodCode} property.
          *
          * 
          */
        @NotNull Builder paymentMethodCode(PayinMethodCode paymentMethodCode);

        boolean isPaymentMethodCodeDefined();


        /**
          * Set {@link PaymentOption#getCurrencyCode} property.
          *
          * 
          */
        @NotNull Builder currencyCode(CurrencyCode currencyCode);

        boolean isCurrencyCodeDefined();


        /**
          * Set {@link PaymentOption#getSegmentCode} property.
          *
          * 
          */
        @NotNull Builder segmentCode(SegmentCode segmentCode);

        boolean isSegmentCodeDefined();


        /**
          * Set {@link PaymentOption#getTransactionAmountLimit} property.
          *
          * 
          */
        @NotNull Builder transactionAmountLimit(IntervalNumberTo transactionAmountLimit);

        boolean isTransactionAmountLimitDefined();


        /**
          * Set {@link PaymentOption#getIsAvailable} property.
          *
          * If set to false, the option is not currently available and must be activated in administration.
          */
        @NotNull Builder isAvailable(Boolean isAvailable);

        boolean isIsAvailableDefined();


        /**
          * Replace all items in {@link PaymentOption#getPaymentOperators} list property.
          *
          * Payment operators that can be used for paying via this payment option.
          */
        @NotNull Builder paymentOperators(java.util.List<@NotNull PaymentOperatorOption> paymentOperators);
        /**
          * Add single item to {@link PaymentOption#getPaymentOperators} list property.
          *
          * Payment operators that can be used for paying via this payment option.
          */
        @NotNull Builder paymentOperatorsAdd(PaymentOperatorOption item);
        /**
          * Add all items to {@link PaymentOption#getPaymentOperators} list property.
          *
          * Payment operators that can be used for paying via this payment option.
          */
        @NotNull Builder paymentOperatorsAddAll(java.util.List<@NotNull PaymentOperatorOption> paymentOperators);


        /**
         * Create new instance of {@link PaymentOption} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @NotNull PaymentOption build();

    }
}