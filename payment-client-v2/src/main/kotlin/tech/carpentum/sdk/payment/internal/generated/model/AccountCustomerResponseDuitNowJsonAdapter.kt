//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.AccountCustomerResponseDuitNow

class AccountCustomerResponseDuitNowJsonAdapter {
    @FromJson
    fun fromJson(json: AccountCustomerResponseDuitNowJson): AccountCustomerResponseDuitNow {
        val builder = AccountCustomerResponseDuitNow.builder()
        builder.accountName(json.accountName)
        builder.accountNumber(json.accountNumber)
        return builder.build()
    }

    @ToJson
    fun toJson(model: AccountCustomerResponseDuitNow): AccountCustomerResponseDuitNowJson {
        val json = AccountCustomerResponseDuitNowJson()
        json.accountName = model.accountName
        json.accountNumber = model.accountNumber.orElse(null)
        return json
    }

    @FromJson
    fun fromJsonImpl(model: AccountCustomerResponseDuitNow): AccountCustomerResponseDuitNowImpl {
        return model as AccountCustomerResponseDuitNowImpl
    }

    @ToJson
    fun toJsonImpl(impl: AccountCustomerResponseDuitNowImpl): AccountCustomerResponseDuitNow {
        return impl
    }

}