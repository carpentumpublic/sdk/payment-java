//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.JsonClass;
import tech.carpentum.sdk.payment.model.*;

@JsonClass(generateAdapter = true)
class WalletTransferMethodResponseJson {
    var idPayout: IdPayout? = null
    var idPayment: IdPayment? = null
    var paymentMethodCode: String? = null
    var reference: String? = null
}