//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** PaymentOptionsList
 *
 * List of (possibly) available payment options.
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public interface PaymentOptionsList {

    @NotNull java.util.List<@NotNull PaymentOption> getData();

    @NotNull static PaymentOptionsList ofData(java.util.List<@NotNull PaymentOption> data) { return builder().data(data).build(); }

    @NotNull static Builder builder() {
        return new PaymentOptionsListImpl.BuilderImpl();
    }

    /** Builder for {@link PaymentOptionsList} model class. */
    interface Builder {

        /**
          * Replace all items in {@link PaymentOptionsList#getData} list property.
          *
          * 
          */
        @NotNull Builder data(java.util.List<@NotNull PaymentOption> data);
        /**
          * Add single item to {@link PaymentOptionsList#getData} list property.
          *
          * 
          */
        @NotNull Builder dataAdd(PaymentOption item);
        /**
          * Add all items to {@link PaymentOptionsList#getData} list property.
          *
          * 
          */
        @NotNull Builder dataAddAll(java.util.List<@NotNull PaymentOption> data);


        /**
         * Create new instance of {@link PaymentOptionsList} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @NotNull PaymentOptionsList build();

    }
}