//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.TopUpRequestedMoneyRequired

class TopUpRequestedMoneyRequiredJsonAdapter {
    @FromJson
    fun fromJson(json: TopUpRequestedMoneyRequiredJson): TopUpRequestedMoneyRequired {
        val builder = TopUpRequestedMoneyRequired.builder()
        builder.moneyRequired(json.moneyRequired)
        builder.currencyProvided(json.currencyProvided)
        return builder.build()
    }

    @ToJson
    fun toJson(model: TopUpRequestedMoneyRequired): TopUpRequestedMoneyRequiredJson {
        val json = TopUpRequestedMoneyRequiredJson()
        json.moneyRequired = model.moneyRequired
        json.currencyProvided = model.currencyProvided.orElse(null)
        return json
    }

    @FromJson
    fun fromJsonImpl(model: TopUpRequestedMoneyRequired): TopUpRequestedMoneyRequiredImpl {
        return model as TopUpRequestedMoneyRequiredImpl
    }

    @ToJson
    fun toJsonImpl(impl: TopUpRequestedMoneyRequiredImpl): TopUpRequestedMoneyRequired {
        return impl
    }

}