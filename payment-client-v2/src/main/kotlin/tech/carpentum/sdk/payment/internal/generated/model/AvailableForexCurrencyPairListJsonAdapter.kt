//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.AvailableForexCurrencyPairList

class AvailableForexCurrencyPairListJsonAdapter {
    @FromJson
    fun fromJson(json: AvailableForexCurrencyPairListJson): AvailableForexCurrencyPairList {
        val builder = AvailableForexCurrencyPairList.builder()
        builder.data(json.data?.toList())
        return builder.build()
    }

    @ToJson
    fun toJson(model: AvailableForexCurrencyPairList): AvailableForexCurrencyPairListJson {
        val json = AvailableForexCurrencyPairListJson()
        json.data = model.data.ifEmpty { null }
        return json
    }

    @FromJson
    fun fromJsonImpl(model: AvailableForexCurrencyPairList): AvailableForexCurrencyPairListImpl {
        return model as AvailableForexCurrencyPairListImpl
    }

    @ToJson
    fun toJsonImpl(impl: AvailableForexCurrencyPairListImpl): AvailableForexCurrencyPairList {
        return impl
    }

}