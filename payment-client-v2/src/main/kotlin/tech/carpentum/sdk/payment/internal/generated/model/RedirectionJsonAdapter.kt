//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.Redirection

class RedirectionJsonAdapter {
    @FromJson
    fun fromJson(json: RedirectionJson): Redirection {
        val builder = Redirection.builder()
        builder.method(json.method)
        builder.url(json.url)
        builder.data(json.data)
        return builder.build()
    }

    @ToJson
    fun toJson(model: Redirection): RedirectionJson {
        val json = RedirectionJson()
        json.method = model.method
        json.url = model.url
        json.data = model.data.orElse(null)
        return json
    }

    @FromJson
    fun fromJsonImpl(model: Redirection): RedirectionImpl {
        return model as RedirectionImpl
    }

    @ToJson
    fun toJsonImpl(impl: RedirectionImpl): Redirection {
        return impl
    }

}