//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.TopUpAcceptedRedirectResponse

class TopUpAcceptedRedirectResponseJsonAdapter {
    @FromJson
    fun fromJson(json: TopUpAcceptedRedirectResponseJson): TopUpAcceptedRedirectResponse {
        val builder = TopUpAcceptedRedirectResponse.builder()
        builder.topUpRequested(json.topUpRequested)
        builder.redirectTo(json.redirectTo)
        return builder.build()
    }

    @ToJson
    fun toJson(model: TopUpAcceptedRedirectResponse): TopUpAcceptedRedirectResponseJson {
        val json = TopUpAcceptedRedirectResponseJson()
        json.topUpRequested = model.topUpRequested
        json.redirectTo = model.redirectTo
        return json
    }

    @FromJson
    fun fromJsonImpl(model: TopUpAcceptedRedirectResponse): TopUpAcceptedRedirectResponseImpl {
        return model as TopUpAcceptedRedirectResponseImpl
    }

    @ToJson
    fun toJsonImpl(impl: TopUpAcceptedRedirectResponseImpl): TopUpAcceptedRedirectResponse {
        return impl
    }

}