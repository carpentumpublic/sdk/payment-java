//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** AccountCustomerResponseOffline
 *
 * Parameters of your customer's bank or wallet account which your customer sends funds from. These account parameters are used for the sender's account verification in processing of the payment.
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public interface AccountCustomerResponseOffline {

    /** Account Number is the number of your customer's bank account which your customer sends funds from to make his payment. */
    @NotNull Optional<String> getAccountNumber();

    @NotNull static AccountCustomerResponseOffline ofAccountNumber(String accountNumber) { return builder().accountNumber(accountNumber).build(); }

    @NotNull static Builder builder() {
        return new AccountCustomerResponseOfflineImpl.BuilderImpl();
    }

    /** Builder for {@link AccountCustomerResponseOffline} model class. */
    interface Builder {

        /**
          * Set {@link AccountCustomerResponseOffline#getAccountNumber} property.
          *
          * Account Number is the number of your customer's bank account which your customer sends funds from to make his payment.
          */
        @NotNull Builder accountNumber(String accountNumber);

        boolean isAccountNumberDefined();


        /**
         * Create new instance of {@link AccountCustomerResponseOffline} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @NotNull AccountCustomerResponseOffline build();

    }
}