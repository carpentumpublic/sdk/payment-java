//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.AccountCustomerResponseAbsaPay

class AccountCustomerResponseAbsaPayJsonAdapter {
    @FromJson
    fun fromJson(json: AccountCustomerResponseAbsaPayJson): AccountCustomerResponseAbsaPay {
        val builder = AccountCustomerResponseAbsaPay.builder()
        builder.accountNumber(json.accountNumber)
        return builder.build()
    }

    @ToJson
    fun toJson(model: AccountCustomerResponseAbsaPay): AccountCustomerResponseAbsaPayJson {
        val json = AccountCustomerResponseAbsaPayJson()
        json.accountNumber = model.accountNumber
        return json
    }

    @FromJson
    fun fromJsonImpl(model: AccountCustomerResponseAbsaPay): AccountCustomerResponseAbsaPayImpl {
        return model as AccountCustomerResponseAbsaPayImpl
    }

    @ToJson
    fun toJsonImpl(impl: AccountCustomerResponseAbsaPayImpl): AccountCustomerResponseAbsaPay {
        return impl
    }

}