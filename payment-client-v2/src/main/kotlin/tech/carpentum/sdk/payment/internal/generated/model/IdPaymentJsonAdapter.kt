//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.IdPayment

class IdPaymentJsonAdapter {

    @FromJson
    fun fromJson(json: String): IdPayment {
        return IdPayment.of(json)
    }

    @ToJson
    fun toJson(model: IdPayment): String {
        return model.value
    }

    @FromJson
    fun fromJsonImpl(model: IdPayment): IdPaymentImpl {
        return model as IdPaymentImpl
    }

    @ToJson
    fun toJsonImpl(impl: IdPaymentImpl): IdPayment {
        return impl
    }

}