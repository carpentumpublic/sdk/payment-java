//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** AccountResponseCryptoOffline
 *
 * Parameters of a crypto wallet where we expect that your customer send funds to make a payment. These parameters has to be provided to your customer in form of an payment instructions.
The returned parameters are depended on the payment method and currency your customer choose to pay.
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public class AccountResponseCryptoOfflineImpl implements AccountResponseCryptoOffline {
    /** Address of the crypto wallet where we expect that your customer sends funds to make a payment.
This parameter is to be shown to your customer in the payment instructions. */
    private final String accountNumber;

    @Override
    public String getAccountNumber() {
        return accountNumber;
    }




    private final int hashCode;
    private final String toString;

    private AccountResponseCryptoOfflineImpl(BuilderImpl builder) {
        this.accountNumber = Objects.requireNonNull(builder.accountNumber, "Property 'accountNumber' is required.");

        this.hashCode = Objects.hash(accountNumber);
        this.toString = builder.type + "(" +
                "accountNumber=" + accountNumber +
                ')';
    }

    @Override
    public int hashCode() {
        return hashCode;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }

        if (!(obj instanceof AccountResponseCryptoOfflineImpl)) {
            return false;
        }

        AccountResponseCryptoOfflineImpl that = (AccountResponseCryptoOfflineImpl) obj;
        if (!Objects.equals(this.accountNumber, that.accountNumber)) return false;

        return true;
    }

    @Override
    public String toString() {
        return toString;
    }

    /** Builder for {@link AccountResponseCryptoOffline} model class. */
    public static class BuilderImpl implements AccountResponseCryptoOffline.Builder {
        private String accountNumber = null;

        private final String type;

        public BuilderImpl(String type) {
            this.type = type;
        }

        public BuilderImpl() {
            this("AccountResponseCryptoOffline");
        }

        /**
          * Set {@link AccountResponseCryptoOffline#getAccountNumber} property.
          *
          * Address of the crypto wallet where we expect that your customer sends funds to make a payment.
This parameter is to be shown to your customer in the payment instructions.
          */
        @Override
        public BuilderImpl accountNumber(String accountNumber) {
            this.accountNumber = accountNumber;
            return this;
        }

        @Override
        public boolean isAccountNumberDefined() {
            return this.accountNumber != null;
        }

        /**
         * Create new instance of {@link AccountResponseCryptoOffline} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @Override
        public AccountResponseCryptoOfflineImpl build() {
            return new AccountResponseCryptoOfflineImpl(this);
        }

    }
}