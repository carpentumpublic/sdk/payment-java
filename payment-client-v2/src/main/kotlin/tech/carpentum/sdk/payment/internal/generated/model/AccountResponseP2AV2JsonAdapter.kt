//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.AccountResponseP2AV2

class AccountResponseP2AV2JsonAdapter {
    @FromJson
    fun fromJson(json: AccountResponseP2AV2Json): AccountResponseP2AV2 {
        val builder = AccountResponseP2AV2.builder()
        builder.accountName(json.accountName)
        builder.accountNumber(json.accountNumber)
        builder.bankCode(json.bankCode)
        builder.bankName(json.bankName)
        builder.bankBranch(json.bankBranch)
        builder.bankCity(json.bankCity)
        builder.bankProvince(json.bankProvince)
        return builder.build()
    }

    @ToJson
    fun toJson(model: AccountResponseP2AV2): AccountResponseP2AV2Json {
        val json = AccountResponseP2AV2Json()
        json.accountName = model.accountName.orElse(null)
        json.accountNumber = model.accountNumber.orElse(null)
        json.bankCode = model.bankCode.orElse(null)
        json.bankName = model.bankName.orElse(null)
        json.bankBranch = model.bankBranch.orElse(null)
        json.bankCity = model.bankCity.orElse(null)
        json.bankProvince = model.bankProvince.orElse(null)
        return json
    }

    @FromJson
    fun fromJsonImpl(model: AccountResponseP2AV2): AccountResponseP2AV2Impl {
        return model as AccountResponseP2AV2Impl
    }

    @ToJson
    fun toJsonImpl(impl: AccountResponseP2AV2Impl): AccountResponseP2AV2 {
        return impl
    }

}