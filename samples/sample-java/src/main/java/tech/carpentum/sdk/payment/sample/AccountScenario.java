package tech.carpentum.sdk.payment.sample;

import kotlin.collections.SetsKt;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tech.carpentum.sdk.payment.AccountsApi;
import tech.carpentum.sdk.payment.AuthTokenOperations;
import tech.carpentum.sdk.payment.PaymentContext;
import tech.carpentum.sdk.payment.ResponseException;
import tech.carpentum.sdk.payment.model.AuthTokenResponse;
import tech.carpentum.sdk.payment.model.BalanceList;

import java.io.InterruptedIOException;
import java.time.Duration;

/**
 * Wraps all Accounts API operations.
 */
public class AccountScenario extends AbstractScenario {
    private final AccountsApi accountsApi;

    private AccountScenario(@NotNull AccountsApi accountsApi, @NotNull String authToken) {
        super(authToken);
        this.accountsApi = accountsApi;
    }

    /**
     * Creates new instance of the scenario with newly created Authorization token with specified validity.
     */
    public static AccountScenario create(@NotNull PaymentContext context, @Nullable Duration validity) throws ResponseException, InterruptedIOException {
        AuthTokenResponse authToken = context.createAuthToken(
                new AuthTokenOperations(AccountsApi.defineListBalancesEndpoint()),
                validity
        );
        return new AccountScenario(AccountsApi.create(context, authToken.getToken()), authToken.getToken());
    }

    /**
     * Creates new instance of the scenario with newly created Authorization token with default {@code payment} validity.
     */
    public static AccountScenario create(@NotNull PaymentContext context) throws ResponseException, InterruptedIOException {
        return create(context, (Duration) null);
    }

    /**
     * Creates new instance of the scenario with existing Authorization token.
     */
    public static AccountScenario create(@NotNull PaymentContext context, @NotNull String authToken) throws ResponseException {
        return new AccountScenario(AccountsApi.create(context, authToken), authToken);
    }

    public BalanceList listBalances(String... currencyCodes) throws ResponseException, InterruptedIOException {
        return accountsApi.listBalances(SetsKt.setOf(currencyCodes));
    }

}
