//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** PayoutDetail
 *
 * 
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public class PayoutDetailImpl implements PayoutDetail {
    private final PaymentRequested paymentRequested;

    @Override
    public PaymentRequested getPaymentRequested() {
        return paymentRequested;
    }


    private final PaymentProcess process;

    @Override
    public PaymentProcess getProcess() {
        return process;
    }


    private final MoneyFee fee;

    @Override
    public MoneyFee getFee() {
        return fee;
    }


    private final PayoutMethodResponse paymentMethodResponse;

    @Override
    public PayoutMethodResponse getPaymentMethodResponse() {
        return paymentMethodResponse;
    }




    private final int hashCode;
    private final String toString;

    private PayoutDetailImpl(BuilderImpl builder) {
        this.paymentRequested = Objects.requireNonNull(builder.paymentRequested, "Property 'paymentRequested' is required.");
        this.process = Objects.requireNonNull(builder.process, "Property 'process' is required.");
        this.fee = Objects.requireNonNull(builder.fee, "Property 'fee' is required.");
        this.paymentMethodResponse = Objects.requireNonNull(builder.paymentMethodResponse, "Property 'paymentMethodResponse' is required.");

        this.hashCode = Objects.hash(paymentRequested, process, fee, paymentMethodResponse);
        this.toString = builder.type + "(" +
                "paymentRequested=" + paymentRequested +
                ", process=" + process +
                ", fee=" + fee +
                ", paymentMethodResponse=" + paymentMethodResponse +
                ')';
    }

    @Override
    public int hashCode() {
        return hashCode;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }

        if (!(obj instanceof PayoutDetailImpl)) {
            return false;
        }

        PayoutDetailImpl that = (PayoutDetailImpl) obj;
        if (!Objects.equals(this.paymentRequested, that.paymentRequested)) return false;
        if (!Objects.equals(this.process, that.process)) return false;
        if (!Objects.equals(this.fee, that.fee)) return false;
        if (!Objects.equals(this.paymentMethodResponse, that.paymentMethodResponse)) return false;

        return true;
    }

    @Override
    public String toString() {
        return toString;
    }

    /** Builder for {@link PayoutDetail} model class. */
    public static class BuilderImpl implements PayoutDetail.Builder {
        private PaymentRequested paymentRequested = null;
        private PaymentProcess process = null;
        private MoneyFee fee = null;
        private PayoutMethodResponse paymentMethodResponse = null;

        private final String type;

        public BuilderImpl(String type) {
            this.type = type;
        }

        public BuilderImpl() {
            this("PayoutDetail");
        }

        /**
          * Set {@link PayoutDetail#getPaymentRequested} property.
          *
          * 
          */
        @Override
        public BuilderImpl paymentRequested(PaymentRequested paymentRequested) {
            this.paymentRequested = paymentRequested;
            return this;
        }

        @Override
        public boolean isPaymentRequestedDefined() {
            return this.paymentRequested != null;
        }

        /**
          * Set {@link PayoutDetail#getProcess} property.
          *
          * 
          */
        @Override
        public BuilderImpl process(PaymentProcess process) {
            this.process = process;
            return this;
        }

        @Override
        public boolean isProcessDefined() {
            return this.process != null;
        }

        /**
          * Set {@link PayoutDetail#getFee} property.
          *
          * 
          */
        @Override
        public BuilderImpl fee(MoneyFee fee) {
            this.fee = fee;
            return this;
        }

        @Override
        public boolean isFeeDefined() {
            return this.fee != null;
        }

        /**
          * Set {@link PayoutDetail#getPaymentMethodResponse} property.
          *
          * 
          */
        @Override
        public BuilderImpl paymentMethodResponse(PayoutMethodResponse paymentMethodResponse) {
            this.paymentMethodResponse = paymentMethodResponse;
            return this;
        }

        @Override
        public boolean isPaymentMethodResponseDefined() {
            return this.paymentMethodResponse != null;
        }

        /**
         * Create new instance of {@link PayoutDetail} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @Override
        public PayoutDetailImpl build() {
            return new PayoutDetailImpl(this);
        }

    }
}