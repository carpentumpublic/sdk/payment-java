//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** PAY_ME
 *
 * Pay Me is payment method intended for South Africa market which allows customers to send payments by scanning QR codes from supported banks and e-wallets.
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public interface PayMeMethod extends PayinMethod {
    /** A discriminator value of property {@link #getPaymentMethodCode}. The model class extends {@link PayinMethod}. */
    PayinMethod.PaymentMethodCode PAYMENT_METHOD_CODE = PayinMethod.PaymentMethodCode.PAY_ME;

    @NotNull AccountPayinRequestPayMe getAccount();

    /** Your customer e-mail address in RFC 5322 format that is used for identification of the customer's payins. */
    @NotNull Optional<String> getEmailAddress();

    /** Your customer mobile phone number in full international telephone number format, including country code. */
    @NotNull Optional<String> getPhoneNumber();

    @NotNull static Builder builder(PayMeMethod copyOf) {
        Builder builder = builder();
        builder.account(copyOf.getAccount());
        builder.emailAddress(copyOf.getEmailAddress().orElse(null));
        builder.phoneNumber(copyOf.getPhoneNumber().orElse(null));
        return builder;
    }

    @NotNull static Builder builder() {
        return new PayMeMethodImpl.BuilderImpl();
    }

    /** Builder for {@link PayMeMethod} model class. */
    interface Builder {

        /**
          * Set {@link PayMeMethod#getAccount} property.
          *
          * 
          */
        @NotNull Builder account(AccountPayinRequestPayMe account);

        boolean isAccountDefined();


        /**
          * Set {@link PayMeMethod#getEmailAddress} property.
          *
          * Your customer e-mail address in RFC 5322 format that is used for identification of the customer's payins.
          */
        @NotNull Builder emailAddress(String emailAddress);

        boolean isEmailAddressDefined();


        /**
          * Set {@link PayMeMethod#getPhoneNumber} property.
          *
          * Your customer mobile phone number in full international telephone number format, including country code.
          */
        @NotNull Builder phoneNumber(String phoneNumber);

        boolean isPhoneNumberDefined();


        /**
         * Create new instance of {@link PayMeMethod} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @NotNull PayMeMethod build();

    }
}