//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** WALLET_TRANSFER
 *
 * 
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public interface WalletTransferMethodResponse extends PayoutMethodResponse {
    /** A discriminator value of property {@link #getPaymentMethodCode}. The model class extends {@link PayoutMethodResponse}. */
    PayoutMethodResponse.PaymentMethodCode PAYMENT_METHOD_CODE = PayoutMethodResponse.PaymentMethodCode.WALLET_TRANSFER;

    @NotNull IdPayout getIdPayout();

    @NotNull IdPayment getIdPayment();

    /** Reference number of transaction. */
    @NotNull String getReference();

    @NotNull static Builder builder(WalletTransferMethodResponse copyOf) {
        Builder builder = builder();
        builder.idPayout(copyOf.getIdPayout());
        builder.idPayment(copyOf.getIdPayment());
        builder.reference(copyOf.getReference());
        return builder;
    }

    @NotNull static Builder builder() {
        return new WalletTransferMethodResponseImpl.BuilderImpl();
    }

    /** Builder for {@link WalletTransferMethodResponse} model class. */
    interface Builder {

        /**
          * Set {@link WalletTransferMethodResponse#getIdPayout} property.
          *
          * 
          */
        @NotNull Builder idPayout(IdPayout idPayout);

        boolean isIdPayoutDefined();


        /**
          * Set {@link WalletTransferMethodResponse#getIdPayment} property.
          *
          * 
          */
        @NotNull Builder idPayment(IdPayment idPayment);

        boolean isIdPaymentDefined();


        /**
          * Set {@link WalletTransferMethodResponse#getReference} property.
          *
          * Reference number of transaction.
          */
        @NotNull Builder reference(String reference);

        boolean isReferenceDefined();


        /**
         * Create new instance of {@link WalletTransferMethodResponse} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @NotNull WalletTransferMethodResponse build();

    }
}