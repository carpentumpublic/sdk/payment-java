//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.VaPayMethodResponse

class VaPayMethodResponseJsonAdapter {
    @FromJson
    fun fromJson(json: VaPayMethodResponseJson): VaPayMethodResponse {
        val builder = VaPayMethodResponse.builder()
        builder.idPayin(json.idPayin)
        builder.idPayment(json.idPayment)
        builder.account(json.account)
        builder.accountCustomer(json.accountCustomer)
        builder.money(json.money)
        builder.vat(json.vat)
        builder.reference(json.reference)
        builder.returnUrl(json.returnUrl)
        builder.paymentOperator(json.paymentOperator)
        builder.acceptedAt(json.acceptedAt)
        builder.expireAt(json.expireAt)
        builder.paymentInstructionList(json.paymentInstructionList?.toList())
        return builder.build()
    }

    @ToJson
    fun toJson(model: VaPayMethodResponse): VaPayMethodResponseJson {
        val json = VaPayMethodResponseJson()
        json.idPayin = model.idPayin
        json.idPayment = model.idPayment
        json.account = model.account
        json.accountCustomer = model.accountCustomer
        json.money = model.money
        json.vat = model.vat.orElse(null)
        json.reference = model.reference
        json.returnUrl = model.returnUrl
        json.paymentOperator = model.paymentOperator.orElse(null)
        json.acceptedAt = model.acceptedAt
        json.expireAt = model.expireAt
        json.paymentInstructionList = model.paymentInstructionList.ifEmpty { null }
        json.paymentMethodCode = model.paymentMethodCode.name
        return json
    }

    @FromJson
    fun fromJsonImpl(model: VaPayMethodResponse): VaPayMethodResponseImpl {
        return model as VaPayMethodResponseImpl
    }

    @ToJson
    fun toJsonImpl(impl: VaPayMethodResponseImpl): VaPayMethodResponse {
        return impl
    }

}