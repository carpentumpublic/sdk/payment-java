//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** PostPayinsError
 *
 * Input does not meet business validations. The payment has not been accepted. See [406 Not Acceptable](responseCodes.html#term-406-Not-Acceptable)
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public class PostPayinsError extends BusinessValidationError {
    // tag::codeEnum[]
    /** @see #getCode */
    public static final String CODE_ACCOUNT_TYPE_INELIGIBLE = "ACCOUNT_TYPE_INELIGIBLE";
    /** @see #getCode */
    public static final String CODE_CLIENT_IP_INVALID = "CLIENT_IP_INVALID";
    /** @see #getCode */
    public static final String CODE_CURRENCY_NOT_SUPPORTED = "CURRENCY_NOT_SUPPORTED";
    /** @see #getCode */
    public static final String CODE_CURRENCY_PRECISION_EXCEEDED = "CURRENCY_PRECISION_EXCEEDED";
    /** @see #getCode */
    public static final String CODE_EMAIL_INVALID = "EMAIL_INVALID";
    /** @see #getCode */
    public static final String CODE_FEATURE_UNAVAILABLE = "FEATURE_UNAVAILABLE";
    /** @see #getCode */
    public static final String CODE_INVALID_ACCOUNT_NUMBER = "INVALID_ACCOUNT_NUMBER";
    /** @see #getCode */
    public static final String CODE_INVALID_ID_NUMBER = "INVALID_ID_NUMBER";
    /** @see #getCode */
    public static final String CODE_INVALID_PHONE_NUMBER = "INVALID_PHONE_NUMBER";
    /** @see #getCode */
    public static final String CODE_IP_DENIED = "IP_DENIED";
    /** @see #getCode */
    public static final String CODE_NAME_SIMILARITY_CHECK_FAILED = "NAME_SIMILARITY_CHECK_FAILED";
    /** @see #getCode */
    public static final String CODE_PAYMENT_CHANNEL_AMOUNT_LIMITS = "PAYMENT_CHANNEL_AMOUNT_LIMITS";
    /** @see #getCode */
    public static final String CODE_PAYMENT_CHANNEL_DAILY_LIMITS = "PAYMENT_CHANNEL_DAILY_LIMITS";
    /** @see #getCode */
    public static final String CODE_PAYMENT_CHANNEL_NO_ACTIVE_FOUND = "PAYMENT_CHANNEL_NO_ACTIVE_FOUND";
    /** @see #getCode */
    public static final String CODE_PAYMENT_CHANNEL_NO_OPENED_FOUND = "PAYMENT_CHANNEL_NO_OPENED_FOUND";
    /** @see #getCode */
    public static final String CODE_PAYMENT_CHANNEL_NO_SEGMENT_FOUND = "PAYMENT_CHANNEL_NO_SEGMENT_FOUND";
    /** @see #getCode */
    public static final String CODE_PAYMENT_METHOD_CODE_INVALID = "PAYMENT_METHOD_CODE_INVALID";
    /** @see #getCode */
    public static final String CODE_PAYMENT_METHOD_ERROR = "PAYMENT_METHOD_ERROR";
    /** @see #getCode */
    public static final String CODE_PAYMENT_METHOD_NOT_FOUND = "PAYMENT_METHOD_NOT_FOUND";
    /** @see #getCode */
    public static final String CODE_PAYMENT_OPERATOR_INVALID = "PAYMENT_OPERATOR_INVALID";
    /** @see #getCode */
    public static final String CODE_PAYMENT_OPERATOR_NOT_FOUND = "PAYMENT_OPERATOR_NOT_FOUND";
    /** @see #getCode */
    public static final String CODE_PAYMENT_OPERATOR_REQUIRED = "PAYMENT_OPERATOR_REQUIRED";
    /** @see #getCode */
    public static final String CODE_PAYMENT_OPERATOR_UNAVAILABLE = "PAYMENT_OPERATOR_UNAVAILABLE";
    /** @see #getCode */
    public static final String CODE_SEGMENT_CODE_INVALID = "SEGMENT_CODE_INVALID";
    // end::codeEnum[]








    private PostPayinsError(PostPayinsError.Builder builder) {
        super(builder);
    }

    @NotNull public static Builder builder() {
        return new Builder();
    }

    /** Builder for {@link PostPayinsError} model class. */
    public static class Builder extends BusinessValidationError.Builder<PostPayinsError, Builder> {
        private Builder() {}

        /**
         * Create new instance of {@link PostPayinsError} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @NotNull public PostPayinsError build() {
            return new PostPayinsError(this);
        }
    }
}