package tech.carpentum.sdk.payment.internal.api

internal object CommaSeparatedCodes {

    fun of(vararg codes: String): Set<String>? {
        return when (codes.isNotEmpty()) {
            true -> codes.toSet()
            false -> null
        }
    }

    fun format(codes: Set<String>?): String? {
        return codes?.joinToString(separator = ",")
    }

}