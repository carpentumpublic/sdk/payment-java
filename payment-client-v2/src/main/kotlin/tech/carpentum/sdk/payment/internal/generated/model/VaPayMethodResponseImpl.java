//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** VAPAY
 *
 * The account parameters for this payment method are used to show payment instructions to the customer.
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public class VaPayMethodResponseImpl implements VaPayMethodResponse {
    private final IdPayin idPayin;

    @Override
    public IdPayin getIdPayin() {
        return idPayin;
    }


    private final IdPayment idPayment;

    @Override
    public IdPayment getIdPayment() {
        return idPayment;
    }


    private final AccountResponseWithBank account;

    @Override
    public AccountResponseWithBank getAccount() {
        return account;
    }


    private final AccountCustomerResponseVaPay accountCustomer;

    @Override
    public AccountCustomerResponseVaPay getAccountCustomer() {
        return accountCustomer;
    }


    private final MoneyPaymentResponse money;

    @Override
    public MoneyPaymentResponse getMoney() {
        return money;
    }


    private final Optional<MoneyVat> vat;

    @Override
    public Optional<MoneyVat> getVat() {
        return vat;
    }


    /** Reference number of transaction. */
    private final String reference;

    @Override
    public String getReference() {
        return reference;
    }


    /** This is the URL where the customers will be redirected after completing a payment.

The URL must be either IP or domain-based. */
    private final String returnUrl;

    @Override
    public String getReturnUrl() {
        return returnUrl;
    }


    private final Optional<SelectedPaymentOperatorIncoming> paymentOperator;

    @Override
    public Optional<SelectedPaymentOperatorIncoming> getPaymentOperator() {
        return paymentOperator;
    }


    /** Date and time when payment was accepted. */
    private final java.time.OffsetDateTime acceptedAt;

    @Override
    public java.time.OffsetDateTime getAcceptedAt() {
        return acceptedAt;
    }


    /** Date and time of payment expiration. If no money has been transferred to this time, payment is considered failed and callback with status change event will shortly follow. */
    private final java.time.OffsetDateTime expireAt;

    @Override
    public java.time.OffsetDateTime getExpireAt() {
        return expireAt;
    }


    private final java.util.List<@NotNull PaymentInstruction> paymentInstructionList;

    @Override
    public java.util.List<@NotNull PaymentInstruction> getPaymentInstructionList() {
        return paymentInstructionList;
    }


    @Override public PaymentMethodCode getPaymentMethodCode() { return PAYMENT_METHOD_CODE; }

    private final int hashCode;
    private final String toString;

    private VaPayMethodResponseImpl(BuilderImpl builder) {
        this.idPayin = Objects.requireNonNull(builder.idPayin, "Property 'idPayin' is required.");
        this.idPayment = Objects.requireNonNull(builder.idPayment, "Property 'idPayment' is required.");
        this.account = Objects.requireNonNull(builder.account, "Property 'account' is required.");
        this.accountCustomer = Objects.requireNonNull(builder.accountCustomer, "Property 'accountCustomer' is required.");
        this.money = Objects.requireNonNull(builder.money, "Property 'money' is required.");
        this.vat = Optional.ofNullable(builder.vat);
        this.reference = Objects.requireNonNull(builder.reference, "Property 'reference' is required.");
        this.returnUrl = Objects.requireNonNull(builder.returnUrl, "Property 'returnUrl' is required.");
        this.paymentOperator = Optional.ofNullable(builder.paymentOperator);
        this.acceptedAt = Objects.requireNonNull(builder.acceptedAt, "Property 'acceptedAt' is required.");
        this.expireAt = Objects.requireNonNull(builder.expireAt, "Property 'expireAt' is required.");
        this.paymentInstructionList = java.util.Collections.unmodifiableList(builder.paymentInstructionList);

        this.hashCode = Objects.hash(idPayin, idPayment, account, accountCustomer, money, vat, reference, returnUrl, paymentOperator, acceptedAt, expireAt, paymentInstructionList);
        this.toString = builder.type + "(" +
                "idPayin=" + idPayin +
                ", idPayment=" + idPayment +
                ", account=" + account +
                ", accountCustomer=" + accountCustomer +
                ", money=" + money +
                ", vat=" + vat +
                ", reference=" + reference +
                ", returnUrl=" + returnUrl +
                ", paymentOperator=" + paymentOperator +
                ", acceptedAt=" + acceptedAt +
                ", expireAt=" + expireAt +
                ", paymentInstructionList=" + paymentInstructionList +
                ')';
    }

    @Override
    public int hashCode() {
        return hashCode;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }

        if (!(obj instanceof VaPayMethodResponseImpl)) {
            return false;
        }

        VaPayMethodResponseImpl that = (VaPayMethodResponseImpl) obj;
        if (!Objects.equals(this.idPayin, that.idPayin)) return false;
        if (!Objects.equals(this.idPayment, that.idPayment)) return false;
        if (!Objects.equals(this.account, that.account)) return false;
        if (!Objects.equals(this.accountCustomer, that.accountCustomer)) return false;
        if (!Objects.equals(this.money, that.money)) return false;
        if (!Objects.equals(this.vat, that.vat)) return false;
        if (!Objects.equals(this.reference, that.reference)) return false;
        if (!Objects.equals(this.returnUrl, that.returnUrl)) return false;
        if (!Objects.equals(this.paymentOperator, that.paymentOperator)) return false;
        if (!Objects.equals(this.acceptedAt, that.acceptedAt)) return false;
        if (!Objects.equals(this.expireAt, that.expireAt)) return false;
        if (!Objects.equals(this.paymentInstructionList, that.paymentInstructionList)) return false;

        return true;
    }

    @Override
    public String toString() {
        return toString;
    }

    /** Builder for {@link VaPayMethodResponse} model class. */
    public static class BuilderImpl implements VaPayMethodResponse.Builder {
        private IdPayin idPayin = null;
        private IdPayment idPayment = null;
        private AccountResponseWithBank account = null;
        private AccountCustomerResponseVaPay accountCustomer = null;
        private MoneyPaymentResponse money = null;
        private MoneyVat vat = null;
        private String reference = null;
        private String returnUrl = null;
        private SelectedPaymentOperatorIncoming paymentOperator = null;
        private java.time.OffsetDateTime acceptedAt = null;
        private java.time.OffsetDateTime expireAt = null;
        private java.util.List<@NotNull PaymentInstruction> paymentInstructionList = new java.util.ArrayList<>();

        private final String type;

        public BuilderImpl(String type) {
            this.type = type;
        }

        public BuilderImpl() {
            this("VaPayMethodResponse");
        }

        /**
          * Set {@link VaPayMethodResponse#getIdPayin} property.
          *
          * 
          */
        @Override
        public BuilderImpl idPayin(IdPayin idPayin) {
            this.idPayin = idPayin;
            return this;
        }

        @Override
        public boolean isIdPayinDefined() {
            return this.idPayin != null;
        }

        /**
          * Set {@link VaPayMethodResponse#getIdPayment} property.
          *
          * 
          */
        @Override
        public BuilderImpl idPayment(IdPayment idPayment) {
            this.idPayment = idPayment;
            return this;
        }

        @Override
        public boolean isIdPaymentDefined() {
            return this.idPayment != null;
        }

        /**
          * Set {@link VaPayMethodResponse#getAccount} property.
          *
          * 
          */
        @Override
        public BuilderImpl account(AccountResponseWithBank account) {
            this.account = account;
            return this;
        }

        @Override
        public boolean isAccountDefined() {
            return this.account != null;
        }

        /**
          * Set {@link VaPayMethodResponse#getAccountCustomer} property.
          *
          * 
          */
        @Override
        public BuilderImpl accountCustomer(AccountCustomerResponseVaPay accountCustomer) {
            this.accountCustomer = accountCustomer;
            return this;
        }

        @Override
        public boolean isAccountCustomerDefined() {
            return this.accountCustomer != null;
        }

        /**
          * Set {@link VaPayMethodResponse#getMoney} property.
          *
          * 
          */
        @Override
        public BuilderImpl money(MoneyPaymentResponse money) {
            this.money = money;
            return this;
        }

        @Override
        public boolean isMoneyDefined() {
            return this.money != null;
        }

        /**
          * Set {@link VaPayMethodResponse#getVat} property.
          *
          * 
          */
        @Override
        public BuilderImpl vat(MoneyVat vat) {
            this.vat = vat;
            return this;
        }

        @Override
        public boolean isVatDefined() {
            return this.vat != null;
        }

        /**
          * Set {@link VaPayMethodResponse#getReference} property.
          *
          * Reference number of transaction.
          */
        @Override
        public BuilderImpl reference(String reference) {
            this.reference = reference;
            return this;
        }

        @Override
        public boolean isReferenceDefined() {
            return this.reference != null;
        }

        /**
          * Set {@link VaPayMethodResponse#getReturnUrl} property.
          *
          * This is the URL where the customers will be redirected after completing a payment.

The URL must be either IP or domain-based.
          */
        @Override
        public BuilderImpl returnUrl(String returnUrl) {
            this.returnUrl = returnUrl;
            return this;
        }

        @Override
        public boolean isReturnUrlDefined() {
            return this.returnUrl != null;
        }

        /**
          * Set {@link VaPayMethodResponse#getPaymentOperator} property.
          *
          * 
          */
        @Override
        public BuilderImpl paymentOperator(SelectedPaymentOperatorIncoming paymentOperator) {
            this.paymentOperator = paymentOperator;
            return this;
        }

        @Override
        public boolean isPaymentOperatorDefined() {
            return this.paymentOperator != null;
        }

        /**
          * Set {@link VaPayMethodResponse#getAcceptedAt} property.
          *
          * Date and time when payment was accepted.
          */
        @Override
        public BuilderImpl acceptedAt(java.time.OffsetDateTime acceptedAt) {
            this.acceptedAt = acceptedAt;
            return this;
        }

        @Override
        public boolean isAcceptedAtDefined() {
            return this.acceptedAt != null;
        }

        /**
          * Set {@link VaPayMethodResponse#getExpireAt} property.
          *
          * Date and time of payment expiration. If no money has been transferred to this time, payment is considered failed and callback with status change event will shortly follow.
          */
        @Override
        public BuilderImpl expireAt(java.time.OffsetDateTime expireAt) {
            this.expireAt = expireAt;
            return this;
        }

        @Override
        public boolean isExpireAtDefined() {
            return this.expireAt != null;
        }

        /**
          * Replace all items in {@link VaPayMethodResponse#getPaymentInstructionList} list property.
          *
          * 
          */
        @Override
        public BuilderImpl paymentInstructionList(java.util.List<@NotNull PaymentInstruction> paymentInstructionList) {
            this.paymentInstructionList.clear();
            if (paymentInstructionList != null) {
                this.paymentInstructionList.addAll(paymentInstructionList);
            }
            return this;
        }
        /**
          * Add single item to {@link VaPayMethodResponse#getPaymentInstructionList} list property.
          *
          * 
          */
        @Override
        public BuilderImpl paymentInstructionListAdd(PaymentInstruction item) {
            if (item != null) {
                this.paymentInstructionList.add(item);
            }
            return this;
        }
        /**
          * Add all items to {@link VaPayMethodResponse#getPaymentInstructionList} list property.
          *
          * 
          */
        @Override
        public BuilderImpl paymentInstructionListAddAll(java.util.List<@NotNull PaymentInstruction> paymentInstructionList) {
            if (paymentInstructionList != null) {
                this.paymentInstructionList.addAll(paymentInstructionList);
            }
            return this;
        }


        /**
         * Create new instance of {@link VaPayMethodResponse} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @Override
        public VaPayMethodResponseImpl build() {
            return new VaPayMethodResponseImpl(this);
        }

    }
}