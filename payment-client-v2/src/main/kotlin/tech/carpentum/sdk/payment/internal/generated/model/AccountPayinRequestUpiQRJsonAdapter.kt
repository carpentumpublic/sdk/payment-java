//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.AccountPayinRequestUpiQR

class AccountPayinRequestUpiQRJsonAdapter {
    @FromJson
    fun fromJson(json: AccountPayinRequestUpiQRJson): AccountPayinRequestUpiQR {
        val builder = AccountPayinRequestUpiQR.builder()
        builder.accountName(json.accountName)
        return builder.build()
    }

    @ToJson
    fun toJson(model: AccountPayinRequestUpiQR): AccountPayinRequestUpiQRJson {
        val json = AccountPayinRequestUpiQRJson()
        json.accountName = model.accountName.orElse(null)
        return json
    }

    @FromJson
    fun fromJsonImpl(model: AccountPayinRequestUpiQR): AccountPayinRequestUpiQRImpl {
        return model as AccountPayinRequestUpiQRImpl
    }

    @ToJson
    fun toJsonImpl(impl: AccountPayinRequestUpiQRImpl): AccountPayinRequestUpiQR {
        return impl
    }

}