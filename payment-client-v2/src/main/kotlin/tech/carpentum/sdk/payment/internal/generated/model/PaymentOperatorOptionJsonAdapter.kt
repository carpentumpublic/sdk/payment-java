//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.PaymentOperatorOption

class PaymentOperatorOptionJsonAdapter {
    @FromJson
    fun fromJson(json: PaymentOperatorOptionJson): PaymentOperatorOption {
        val builder = PaymentOperatorOption.builder()
        builder.code(json.code)
        builder.name(json.name)
        builder.isAvailable(json.isAvailable)
        builder.transactionAmountLimit(json.transactionAmountLimit)
        return builder.build()
    }

    @ToJson
    fun toJson(model: PaymentOperatorOption): PaymentOperatorOptionJson {
        val json = PaymentOperatorOptionJson()
        json.code = model.code
        json.name = model.name.orElse(null)
        json.isAvailable = model.isAvailable
        json.transactionAmountLimit = model.transactionAmountLimit
        return json
    }

    @FromJson
    fun fromJsonImpl(model: PaymentOperatorOption): PaymentOperatorOptionImpl {
        return model as PaymentOperatorOptionImpl
    }

    @ToJson
    fun toJsonImpl(impl: PaymentOperatorOptionImpl): PaymentOperatorOption {
        return impl
    }

}