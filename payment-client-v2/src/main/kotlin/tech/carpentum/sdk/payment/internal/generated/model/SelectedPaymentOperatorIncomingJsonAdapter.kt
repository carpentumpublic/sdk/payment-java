//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.SelectedPaymentOperatorIncoming

class SelectedPaymentOperatorIncomingJsonAdapter {
    @FromJson
    fun fromJson(json: SelectedPaymentOperatorIncomingJson): SelectedPaymentOperatorIncoming {
        val builder = SelectedPaymentOperatorIncoming.builder()
        builder.code(json.code)
        builder.name(json.name)
        return builder.build()
    }

    @ToJson
    fun toJson(model: SelectedPaymentOperatorIncoming): SelectedPaymentOperatorIncomingJson {
        val json = SelectedPaymentOperatorIncomingJson()
        json.code = model.code
        json.name = model.name
        return json
    }

    @FromJson
    fun fromJsonImpl(model: SelectedPaymentOperatorIncoming): SelectedPaymentOperatorIncomingImpl {
        return model as SelectedPaymentOperatorIncomingImpl
    }

    @ToJson
    fun toJsonImpl(impl: SelectedPaymentOperatorIncomingImpl): SelectedPaymentOperatorIncoming {
        return impl
    }

}