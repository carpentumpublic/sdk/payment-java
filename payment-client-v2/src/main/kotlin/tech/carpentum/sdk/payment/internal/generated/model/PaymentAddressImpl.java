//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** PaymentAddress
 *
 * Specifies the payment address (e.g. in UPI payment system) where customer can send the payment using theirs wallets or others banking services.
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public class PaymentAddressImpl implements PaymentAddress {
    /** The name of the payment address ID e.g. UPI ID/VPA or GPAY ID. */
    private final String name;

    @Override
    public String getName() {
        return name;
    }


    /** The payment address ID where customer can send the payment. */
    private final String idAddress;

    @Override
    public String getIdAddress() {
        return idAddress;
    }




    private final int hashCode;
    private final String toString;

    private PaymentAddressImpl(BuilderImpl builder) {
        this.name = Objects.requireNonNull(builder.name, "Property 'name' is required.");
        this.idAddress = Objects.requireNonNull(builder.idAddress, "Property 'idAddress' is required.");

        this.hashCode = Objects.hash(name, idAddress);
        this.toString = builder.type + "(" +
                "name=" + name +
                ", idAddress=" + idAddress +
                ')';
    }

    @Override
    public int hashCode() {
        return hashCode;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }

        if (!(obj instanceof PaymentAddressImpl)) {
            return false;
        }

        PaymentAddressImpl that = (PaymentAddressImpl) obj;
        if (!Objects.equals(this.name, that.name)) return false;
        if (!Objects.equals(this.idAddress, that.idAddress)) return false;

        return true;
    }

    @Override
    public String toString() {
        return toString;
    }

    /** Builder for {@link PaymentAddress} model class. */
    public static class BuilderImpl implements PaymentAddress.Builder {
        private String name = null;
        private String idAddress = null;

        private final String type;

        public BuilderImpl(String type) {
            this.type = type;
        }

        public BuilderImpl() {
            this("PaymentAddress");
        }

        /**
          * Set {@link PaymentAddress#getName} property.
          *
          * The name of the payment address ID e.g. UPI ID/VPA or GPAY ID.
          */
        @Override
        public BuilderImpl name(String name) {
            this.name = name;
            return this;
        }

        @Override
        public boolean isNameDefined() {
            return this.name != null;
        }

        /**
          * Set {@link PaymentAddress#getIdAddress} property.
          *
          * The payment address ID where customer can send the payment.
          */
        @Override
        public BuilderImpl idAddress(String idAddress) {
            this.idAddress = idAddress;
            return this;
        }

        @Override
        public boolean isIdAddressDefined() {
            return this.idAddress != null;
        }

        /**
         * Create new instance of {@link PaymentAddress} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @Override
        public PaymentAddressImpl build() {
            return new PaymentAddressImpl(this);
        }

    }
}