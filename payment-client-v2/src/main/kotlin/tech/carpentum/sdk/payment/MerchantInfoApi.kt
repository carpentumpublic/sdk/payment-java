@file:JvmName("MerchantInfoApiUtils")

package tech.carpentum.sdk.payment

import tech.carpentum.sdk.payment.EndpointDefinition.Method.GET
import tech.carpentum.sdk.payment.MerchantInfoApi.Factory
import tech.carpentum.sdk.payment.internal.api.ClientErrorExceptionFactory
import tech.carpentum.sdk.payment.internal.api.EnhancedMerchantInfoApi
import tech.carpentum.sdk.payment.internal.api.ResponseExceptionUtils
import tech.carpentum.sdk.payment.model.MerchantInfo
import java.io.InterruptedIOException
import java.time.Duration

/**
 * The MerchantInfo RESTful API.
 *
 * Use [Factory] to create new instance of the class.
 */
class MerchantInfoApi private constructor(
    private val apiVersion: Int,
    private val api: EnhancedMerchantInfoApi
) {

    /**
     * Throws [InterruptedIOException] in case of timeout.
     */
    @Throws(ResponseException::class, InterruptedIOException::class)
    // tag::userGuidePublicApi[]
    fun getMerchantInfo(): MerchantInfo
    // end::userGuidePublicApi[]
    {
        return ResponseExceptionUtils.wrap(ClientErrorExceptionFactory.instance) {
            api.getMerchantInfo(xAPIVersion = apiVersion)
        }
    }

    /**
     * Factory to create a new instance of [MerchantInfoApi].
     */
    companion object Factory {
        /**
         * Endpoint definition for [MerchantInfoApi.getMerchantInfo] method.
         */
        @JvmStatic
        fun defineGetMerchantInfoEndpoint(): EndpointDefinition = EndpointDefinition(GET, "/merchant-info")

        @JvmStatic
        @JvmOverloads
        fun create(context: PaymentContext, accessToken: String, callTimeout: Duration? = null): MerchantInfoApi {
            return MerchantInfoApi(
                context.apiVersion,
                EnhancedMerchantInfoApi(basePath = context.apiBaseUrl, accessToken = accessToken, brand = context.brand, callTimeout = callTimeout ?: context.defaultCallTimeout)
            )
        }
    }

}

/**
 * Grants [MerchantInfoApi.getMerchantInfo] endpoint, see [MerchantInfoApi.defineGetMerchantInfoEndpoint] definition.
 */
@JvmName("grantGetMerchantInfoEndpoint")
fun AuthTokenOperations.grantMerchantInfoApiGetMerchantInfoEndpoint(): AuthTokenOperations =
    this.grant(MerchantInfoApi.defineGetMerchantInfoEndpoint())
