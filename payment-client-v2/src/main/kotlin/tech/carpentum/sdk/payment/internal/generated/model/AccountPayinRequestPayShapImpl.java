//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** AccountPayinRequestPayShap
 *
 * Parameters of your customer's bank or wallet account which your customer sends funds from. This account parameters are used for the sender's account verification in processing of the payment.
Which parameters are mandatory depends on the payment method and the currency your customer choose to pay.
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public class AccountPayinRequestPayShapImpl implements AccountPayinRequestPayShap {
    /** Account Number is the number of your customer's bank account which your customer sends funds from to make his payment. Must contain only numbers. */
    private final Optional<String> accountNumber;

    @Override
    public Optional<String> getAccountNumber() {
        return accountNumber;
    }




    private final int hashCode;
    private final String toString;

    private AccountPayinRequestPayShapImpl(BuilderImpl builder) {
        this.accountNumber = Optional.ofNullable(builder.accountNumber);

        this.hashCode = Objects.hash(accountNumber);
        this.toString = builder.type + "(" +
                "accountNumber=" + accountNumber +
                ')';
    }

    @Override
    public int hashCode() {
        return hashCode;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }

        if (!(obj instanceof AccountPayinRequestPayShapImpl)) {
            return false;
        }

        AccountPayinRequestPayShapImpl that = (AccountPayinRequestPayShapImpl) obj;
        if (!Objects.equals(this.accountNumber, that.accountNumber)) return false;

        return true;
    }

    @Override
    public String toString() {
        return toString;
    }

    /** Builder for {@link AccountPayinRequestPayShap} model class. */
    public static class BuilderImpl implements AccountPayinRequestPayShap.Builder {
        private String accountNumber = null;

        private final String type;

        public BuilderImpl(String type) {
            this.type = type;
        }

        public BuilderImpl() {
            this("AccountPayinRequestPayShap");
        }

        /**
          * Set {@link AccountPayinRequestPayShap#getAccountNumber} property.
          *
          * Account Number is the number of your customer's bank account which your customer sends funds from to make his payment. Must contain only numbers.
          */
        @Override
        public BuilderImpl accountNumber(String accountNumber) {
            this.accountNumber = accountNumber;
            return this;
        }

        @Override
        public boolean isAccountNumberDefined() {
            return this.accountNumber != null;
        }

        /**
         * Create new instance of {@link AccountPayinRequestPayShap} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @Override
        public AccountPayinRequestPayShapImpl build() {
            return new AccountPayinRequestPayShapImpl(this);
        }

    }
}