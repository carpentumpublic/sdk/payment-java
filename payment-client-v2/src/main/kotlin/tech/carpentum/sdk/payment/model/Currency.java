//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** Currency
 *
 * 
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public interface Currency {

    @NotNull CurrencyCode getCode();

    @NotNull static Currency ofCode(CurrencyCode code) { return builder().code(code).build(); }

    @NotNull static Builder builder() {
        return new CurrencyImpl.BuilderImpl();
    }

    /** Builder for {@link Currency} model class. */
    interface Builder {

        /**
          * Set {@link Currency#getCode} property.
          *
          * 
          */
        @NotNull Builder code(CurrencyCode code);

        boolean isCodeDefined();


        /**
         * Create new instance of {@link Currency} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @NotNull Currency build();

    }
}