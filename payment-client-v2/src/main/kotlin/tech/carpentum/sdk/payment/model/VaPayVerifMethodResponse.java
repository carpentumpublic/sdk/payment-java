//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** VAPAY_VERIF
 *
 * 
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public interface VaPayVerifMethodResponse extends PayinMethodResponse {
    /** A discriminator value of property {@link #getPaymentMethodCode}. The model class extends {@link PayinMethodResponse}. */
    PayinMethodResponse.PaymentMethodCode PAYMENT_METHOD_CODE = PayinMethodResponse.PaymentMethodCode.VAPAY_VERIF;

    @NotNull IdPayin getIdPayin();

    @NotNull IdPayment getIdPayment();

    @NotNull AccountResponseWithBank getAccount();

    @NotNull AccountCustomerResponseVaPayVerif getAccountCustomer();

    @NotNull MoneyPaymentResponse getMoney();

    @NotNull Optional<MoneyVat> getVat();

    /** Reference number of transaction. */
    @NotNull String getReference();

    /** This is the URL where the customers will be redirected after completing a payment.

The URL must be either IP or domain-based. */
    @NotNull String getReturnUrl();

    @NotNull SelectedPaymentOperatorIncoming getPaymentOperator();

    /** Date and time when payment was accepted. */
    @NotNull java.time.OffsetDateTime getAcceptedAt();

    /** Date and time of payment expiration. If no money has been transferred to this time, payment is considered failed and callback with status change event will shortly follow. */
    @NotNull java.time.OffsetDateTime getExpireAt();

    @NotNull static Builder builder(VaPayVerifMethodResponse copyOf) {
        Builder builder = builder();
        builder.idPayin(copyOf.getIdPayin());
        builder.idPayment(copyOf.getIdPayment());
        builder.account(copyOf.getAccount());
        builder.accountCustomer(copyOf.getAccountCustomer());
        builder.money(copyOf.getMoney());
        builder.vat(copyOf.getVat().orElse(null));
        builder.reference(copyOf.getReference());
        builder.returnUrl(copyOf.getReturnUrl());
        builder.paymentOperator(copyOf.getPaymentOperator());
        builder.acceptedAt(copyOf.getAcceptedAt());
        builder.expireAt(copyOf.getExpireAt());
        return builder;
    }

    @NotNull static Builder builder() {
        return new VaPayVerifMethodResponseImpl.BuilderImpl();
    }

    /** Builder for {@link VaPayVerifMethodResponse} model class. */
    interface Builder {

        /**
          * Set {@link VaPayVerifMethodResponse#getIdPayin} property.
          *
          * 
          */
        @NotNull Builder idPayin(IdPayin idPayin);

        boolean isIdPayinDefined();


        /**
          * Set {@link VaPayVerifMethodResponse#getIdPayment} property.
          *
          * 
          */
        @NotNull Builder idPayment(IdPayment idPayment);

        boolean isIdPaymentDefined();


        /**
          * Set {@link VaPayVerifMethodResponse#getAccount} property.
          *
          * 
          */
        @NotNull Builder account(AccountResponseWithBank account);

        boolean isAccountDefined();


        /**
          * Set {@link VaPayVerifMethodResponse#getAccountCustomer} property.
          *
          * 
          */
        @NotNull Builder accountCustomer(AccountCustomerResponseVaPayVerif accountCustomer);

        boolean isAccountCustomerDefined();


        /**
          * Set {@link VaPayVerifMethodResponse#getMoney} property.
          *
          * 
          */
        @NotNull Builder money(MoneyPaymentResponse money);

        boolean isMoneyDefined();


        /**
          * Set {@link VaPayVerifMethodResponse#getVat} property.
          *
          * 
          */
        @NotNull Builder vat(MoneyVat vat);

        boolean isVatDefined();


        /**
          * Set {@link VaPayVerifMethodResponse#getReference} property.
          *
          * Reference number of transaction.
          */
        @NotNull Builder reference(String reference);

        boolean isReferenceDefined();


        /**
          * Set {@link VaPayVerifMethodResponse#getReturnUrl} property.
          *
          * This is the URL where the customers will be redirected after completing a payment.

The URL must be either IP or domain-based.
          */
        @NotNull Builder returnUrl(String returnUrl);

        boolean isReturnUrlDefined();


        /**
          * Set {@link VaPayVerifMethodResponse#getPaymentOperator} property.
          *
          * 
          */
        @NotNull Builder paymentOperator(SelectedPaymentOperatorIncoming paymentOperator);

        boolean isPaymentOperatorDefined();


        /**
          * Set {@link VaPayVerifMethodResponse#getAcceptedAt} property.
          *
          * Date and time when payment was accepted.
          */
        @NotNull Builder acceptedAt(java.time.OffsetDateTime acceptedAt);

        boolean isAcceptedAtDefined();


        /**
          * Set {@link VaPayVerifMethodResponse#getExpireAt} property.
          *
          * Date and time of payment expiration. If no money has been transferred to this time, payment is considered failed and callback with status change event will shortly follow.
          */
        @NotNull Builder expireAt(java.time.OffsetDateTime expireAt);

        boolean isExpireAtDefined();


        /**
         * Create new instance of {@link VaPayVerifMethodResponse} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @NotNull VaPayVerifMethodResponse build();

    }
}