//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.WalletTopUpOptionsList

class WalletTopUpOptionsListJsonAdapter {
    @FromJson
    fun fromJson(json: WalletTopUpOptionsListJson): WalletTopUpOptionsList {
        val builder = WalletTopUpOptionsList.builder()
        builder.data(json.data?.toList())
        return builder.build()
    }

    @ToJson
    fun toJson(model: WalletTopUpOptionsList): WalletTopUpOptionsListJson {
        val json = WalletTopUpOptionsListJson()
        json.data = model.data.ifEmpty { null }
        return json
    }

    @FromJson
    fun fromJsonImpl(model: WalletTopUpOptionsList): WalletTopUpOptionsListImpl {
        return model as WalletTopUpOptionsListImpl
    }

    @ToJson
    fun toJsonImpl(impl: WalletTopUpOptionsListImpl): WalletTopUpOptionsList {
        return impl
    }

}