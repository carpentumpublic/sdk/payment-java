//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** UPIID
 *
 * UPI (Unified Payment Method) is a method of fund transfers in India. The mechanism allows two parties to exchange funds using a UPI ID without having to share the banking information with the other party.
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public class UpiIdMethodImpl implements UpiIdMethod {
    /** Virtual payment address of the customer on UPI (Unified Payment Interface) */
    private final String upiId;

    @Override
    public String getUpiId() {
        return upiId;
    }


    /** Your customer e-mail address in RFC 5322 format that is used for identification of the customer's payins. */
    private final Optional<String> emailAddress;

    @Override
    public Optional<String> getEmailAddress() {
        return emailAddress;
    }


    @Override public PaymentMethodCode getPaymentMethodCode() { return PAYMENT_METHOD_CODE; }

    private final int hashCode;
    private final String toString;

    private UpiIdMethodImpl(BuilderImpl builder) {
        this.upiId = Objects.requireNonNull(builder.upiId, "Property 'upiId' is required.");
        this.emailAddress = Optional.ofNullable(builder.emailAddress);

        this.hashCode = Objects.hash(upiId, emailAddress);
        this.toString = builder.type + "(" +
                "upiId=" + upiId +
                ", emailAddress=" + emailAddress +
                ')';
    }

    @Override
    public int hashCode() {
        return hashCode;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }

        if (!(obj instanceof UpiIdMethodImpl)) {
            return false;
        }

        UpiIdMethodImpl that = (UpiIdMethodImpl) obj;
        if (!Objects.equals(this.upiId, that.upiId)) return false;
        if (!Objects.equals(this.emailAddress, that.emailAddress)) return false;

        return true;
    }

    @Override
    public String toString() {
        return toString;
    }

    /** Builder for {@link UpiIdMethod} model class. */
    public static class BuilderImpl implements UpiIdMethod.Builder {
        private String upiId = null;
        private String emailAddress = null;

        private final String type;

        public BuilderImpl(String type) {
            this.type = type;
        }

        public BuilderImpl() {
            this("UpiIdMethod");
        }

        /**
          * Set {@link UpiIdMethod#getUpiId} property.
          *
          * Virtual payment address of the customer on UPI (Unified Payment Interface)
          */
        @Override
        public BuilderImpl upiId(String upiId) {
            this.upiId = upiId;
            return this;
        }

        @Override
        public boolean isUpiIdDefined() {
            return this.upiId != null;
        }

        /**
          * Set {@link UpiIdMethod#getEmailAddress} property.
          *
          * Your customer e-mail address in RFC 5322 format that is used for identification of the customer's payins.
          */
        @Override
        public BuilderImpl emailAddress(String emailAddress) {
            this.emailAddress = emailAddress;
            return this;
        }

        @Override
        public boolean isEmailAddressDefined() {
            return this.emailAddress != null;
        }

        /**
         * Create new instance of {@link UpiIdMethod} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @Override
        public UpiIdMethodImpl build() {
            return new UpiIdMethodImpl(this);
        }

    }
}