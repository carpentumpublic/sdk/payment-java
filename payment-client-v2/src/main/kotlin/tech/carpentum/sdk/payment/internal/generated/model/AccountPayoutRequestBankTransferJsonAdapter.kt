//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.AccountPayoutRequestBankTransfer

class AccountPayoutRequestBankTransferJsonAdapter {
    @FromJson
    fun fromJson(json: AccountPayoutRequestBankTransferJson): AccountPayoutRequestBankTransfer {
        val builder = AccountPayoutRequestBankTransfer.builder()
        builder.accountName(json.accountName)
        builder.accountNumber(json.accountNumber)
        builder.accountType(json.accountType)
        builder.bankCode(json.bankCode)
        builder.bankName(json.bankName)
        builder.bankBranch(json.bankBranch)
        builder.bankCity(json.bankCity)
        builder.bankProvince(json.bankProvince)
        return builder.build()
    }

    @ToJson
    fun toJson(model: AccountPayoutRequestBankTransfer): AccountPayoutRequestBankTransferJson {
        val json = AccountPayoutRequestBankTransferJson()
        json.accountName = model.accountName
        json.accountNumber = model.accountNumber
        json.accountType = model.accountType.orElse(null)
        json.bankCode = model.bankCode.orElse(null)
        json.bankName = model.bankName.orElse(null)
        json.bankBranch = model.bankBranch.orElse(null)
        json.bankCity = model.bankCity.orElse(null)
        json.bankProvince = model.bankProvince.orElse(null)
        return json
    }

    @FromJson
    fun fromJsonImpl(model: AccountPayoutRequestBankTransfer): AccountPayoutRequestBankTransferImpl {
        return model as AccountPayoutRequestBankTransferImpl
    }

    @ToJson
    fun toJsonImpl(impl: AccountPayoutRequestBankTransferImpl): AccountPayoutRequestBankTransfer {
        return impl
    }

}