//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import tech.carpentum.sdk.payment.model.PayoutMethodResponse
import tech.carpentum.sdk.payment.model.PayoutMethodResponse.PaymentMethodCode

import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.Moshi
import com.squareup.moshi.adapters.PolymorphicJsonAdapterFactory
import java.lang.reflect.Type

class PayoutMethodResponseJsonAdapter : JsonAdapter.Factory {
    override fun create(type: Type, annotations: MutableSet<out Annotation>, moshi: Moshi): JsonAdapter<*>? {
        return PolymorphicJsonAdapterFactory
            .of(PayoutMethodResponse::class.java, PayoutMethodResponse.DISCRIMINATOR)
            .withSubtype(BankTransferMethodResponseImpl::class.java, PaymentMethodCode.BANK_TRANSFER.toString())
            .withSubtype(CryptoTransferMethodResponseImpl::class.java, PaymentMethodCode.CRYPTO_TRANSFER.toString())
            .withSubtype(WalletTransferMethodResponseImpl::class.java, PaymentMethodCode.WALLET_TRANSFER.toString())
            .create(type, annotations, moshi)
    }
}