//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** AvailableTopUpOption
 *
 * 
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public class AvailableTopUpOptionImpl implements AvailableTopUpOption {
    private final Money moneyPay;

    @Override
    public Money getMoneyPay() {
        return moneyPay;
    }


    private final Money moneyReceive;

    @Override
    public Money getMoneyReceive() {
        return moneyReceive;
    }


    private final PayinMethodCode paymentMethodCode;

    @Override
    public PayinMethodCode getPaymentMethodCode() {
        return paymentMethodCode;
    }


    private final java.util.List<@NotNull PaymentOperatorIncoming> paymentOperators;

    @Override
    public java.util.List<@NotNull PaymentOperatorIncoming> getPaymentOperators() {
        return paymentOperators;
    }




    private final int hashCode;
    private final String toString;

    private AvailableTopUpOptionImpl(BuilderImpl builder) {
        this.moneyPay = Objects.requireNonNull(builder.moneyPay, "Property 'moneyPay' is required.");
        this.moneyReceive = Objects.requireNonNull(builder.moneyReceive, "Property 'moneyReceive' is required.");
        this.paymentMethodCode = Objects.requireNonNull(builder.paymentMethodCode, "Property 'paymentMethodCode' is required.");
        this.paymentOperators = java.util.Collections.unmodifiableList(builder.paymentOperators);

        this.hashCode = Objects.hash(moneyPay, moneyReceive, paymentMethodCode, paymentOperators);
        this.toString = builder.type + "(" +
                "moneyPay=" + moneyPay +
                ", moneyReceive=" + moneyReceive +
                ", paymentMethodCode=" + paymentMethodCode +
                ", paymentOperators=" + paymentOperators +
                ')';
    }

    @Override
    public int hashCode() {
        return hashCode;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }

        if (!(obj instanceof AvailableTopUpOptionImpl)) {
            return false;
        }

        AvailableTopUpOptionImpl that = (AvailableTopUpOptionImpl) obj;
        if (!Objects.equals(this.moneyPay, that.moneyPay)) return false;
        if (!Objects.equals(this.moneyReceive, that.moneyReceive)) return false;
        if (!Objects.equals(this.paymentMethodCode, that.paymentMethodCode)) return false;
        if (!Objects.equals(this.paymentOperators, that.paymentOperators)) return false;

        return true;
    }

    @Override
    public String toString() {
        return toString;
    }

    /** Builder for {@link AvailableTopUpOption} model class. */
    public static class BuilderImpl implements AvailableTopUpOption.Builder {
        private Money moneyPay = null;
        private Money moneyReceive = null;
        private PayinMethodCode paymentMethodCode = null;
        private java.util.List<@NotNull PaymentOperatorIncoming> paymentOperators = new java.util.ArrayList<>();

        private final String type;

        public BuilderImpl(String type) {
            this.type = type;
        }

        public BuilderImpl() {
            this("AvailableTopUpOption");
        }

        /**
          * Set {@link AvailableTopUpOption#getMoneyPay} property.
          *
          * 
          */
        @Override
        public BuilderImpl moneyPay(Money moneyPay) {
            this.moneyPay = moneyPay;
            return this;
        }

        @Override
        public boolean isMoneyPayDefined() {
            return this.moneyPay != null;
        }

        /**
          * Set {@link AvailableTopUpOption#getMoneyReceive} property.
          *
          * 
          */
        @Override
        public BuilderImpl moneyReceive(Money moneyReceive) {
            this.moneyReceive = moneyReceive;
            return this;
        }

        @Override
        public boolean isMoneyReceiveDefined() {
            return this.moneyReceive != null;
        }

        /**
          * Set {@link AvailableTopUpOption#getPaymentMethodCode} property.
          *
          * 
          */
        @Override
        public BuilderImpl paymentMethodCode(PayinMethodCode paymentMethodCode) {
            this.paymentMethodCode = paymentMethodCode;
            return this;
        }

        @Override
        public boolean isPaymentMethodCodeDefined() {
            return this.paymentMethodCode != null;
        }

        /**
          * Replace all items in {@link AvailableTopUpOption#getPaymentOperators} list property.
          *
          * 
          */
        @Override
        public BuilderImpl paymentOperators(java.util.List<@NotNull PaymentOperatorIncoming> paymentOperators) {
            this.paymentOperators.clear();
            if (paymentOperators != null) {
                this.paymentOperators.addAll(paymentOperators);
            }
            return this;
        }
        /**
          * Add single item to {@link AvailableTopUpOption#getPaymentOperators} list property.
          *
          * 
          */
        @Override
        public BuilderImpl paymentOperatorsAdd(PaymentOperatorIncoming item) {
            if (item != null) {
                this.paymentOperators.add(item);
            }
            return this;
        }
        /**
          * Add all items to {@link AvailableTopUpOption#getPaymentOperators} list property.
          *
          * 
          */
        @Override
        public BuilderImpl paymentOperatorsAddAll(java.util.List<@NotNull PaymentOperatorIncoming> paymentOperators) {
            if (paymentOperators != null) {
                this.paymentOperators.addAll(paymentOperators);
            }
            return this;
        }


        /**
         * Create new instance of {@link AvailableTopUpOption} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @Override
        public AvailableTopUpOptionImpl build() {
            return new AvailableTopUpOptionImpl(this);
        }

    }
}