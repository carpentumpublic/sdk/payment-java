//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.DuitNowMethod

class DuitNowMethodJsonAdapter {
    @FromJson
    fun fromJson(json: DuitNowMethodJson): DuitNowMethod {
        val builder = DuitNowMethod.builder()
        builder.account(json.account)
        builder.paymentOperatorCode(json.paymentOperatorCode)
        builder.emailAddress(json.emailAddress)
        return builder.build()
    }

    @ToJson
    fun toJson(model: DuitNowMethod): DuitNowMethodJson {
        val json = DuitNowMethodJson()
        json.account = model.account
        json.paymentOperatorCode = model.paymentOperatorCode.orElse(null)
        json.emailAddress = model.emailAddress.orElse(null)
        json.paymentMethodCode = model.paymentMethodCode.name
        return json
    }

    @FromJson
    fun fromJsonImpl(model: DuitNowMethod): DuitNowMethodImpl {
        return model as DuitNowMethodImpl
    }

    @ToJson
    fun toJsonImpl(impl: DuitNowMethodImpl): DuitNowMethod {
        return impl
    }

}