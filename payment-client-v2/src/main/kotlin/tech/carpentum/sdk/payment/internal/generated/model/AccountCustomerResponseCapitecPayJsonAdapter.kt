//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.AccountCustomerResponseCapitecPay

class AccountCustomerResponseCapitecPayJsonAdapter {
    @FromJson
    fun fromJson(json: AccountCustomerResponseCapitecPayJson): AccountCustomerResponseCapitecPay {
        val builder = AccountCustomerResponseCapitecPay.builder()
        builder.accountName(json.accountName)
        builder.accountNumber(json.accountNumber)
        return builder.build()
    }

    @ToJson
    fun toJson(model: AccountCustomerResponseCapitecPay): AccountCustomerResponseCapitecPayJson {
        val json = AccountCustomerResponseCapitecPayJson()
        json.accountName = model.accountName.orElse(null)
        json.accountNumber = model.accountNumber
        return json
    }

    @FromJson
    fun fromJsonImpl(model: AccountCustomerResponseCapitecPay): AccountCustomerResponseCapitecPayImpl {
        return model as AccountCustomerResponseCapitecPayImpl
    }

    @ToJson
    fun toJsonImpl(impl: AccountCustomerResponseCapitecPayImpl): AccountCustomerResponseCapitecPay {
        return impl
    }

}