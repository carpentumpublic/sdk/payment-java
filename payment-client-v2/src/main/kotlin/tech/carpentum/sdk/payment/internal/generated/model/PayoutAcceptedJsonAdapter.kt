//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.PayoutAccepted

class PayoutAcceptedJsonAdapter {
    @FromJson
    fun fromJson(json: PayoutAcceptedJson): PayoutAccepted {
        val builder = PayoutAccepted.builder()
        builder.paymentRequested(json.paymentRequested)
        builder.paymentMethodResponse(json.paymentMethodResponse)
        return builder.build()
    }

    @ToJson
    fun toJson(model: PayoutAccepted): PayoutAcceptedJson {
        val json = PayoutAcceptedJson()
        json.paymentRequested = model.paymentRequested
        json.paymentMethodResponse = model.paymentMethodResponse
        return json
    }

    @FromJson
    fun fromJsonImpl(model: PayoutAccepted): PayoutAcceptedImpl {
        return model as PayoutAcceptedImpl
    }

    @ToJson
    fun toJsonImpl(impl: PayoutAcceptedImpl): PayoutAccepted {
        return impl
    }

}