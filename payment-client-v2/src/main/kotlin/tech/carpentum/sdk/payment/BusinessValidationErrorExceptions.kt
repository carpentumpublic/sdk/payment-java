//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment;

import tech.carpentum.sdk.payment.model.PostAuthTokensError
import tech.carpentum.sdk.payment.model.PostAvailablePayoutOptionsError
import tech.carpentum.sdk.payment.model.PostPayinsError
import tech.carpentum.sdk.payment.model.PostAvailableTopUpOptionsError
import tech.carpentum.sdk.payment.model.PostPayoutsError
import tech.carpentum.sdk.payment.model.GetFxRatesError
import tech.carpentum.sdk.payment.model.GetTopupError
import tech.carpentum.sdk.payment.model.GetPayinError
import tech.carpentum.sdk.payment.model.GetPaymentOptionsError
import tech.carpentum.sdk.payment.model.GetPayoutError
import tech.carpentum.sdk.payment.model.PostAvailablePayinOptionsError


/**
 * Exception for business validation error to wrap [PostAvailablePayinOptionsError] model class.
 */
class PostAvailablePayinOptionsErrorException(
    cause: Throwable,
    statusCode: Int,
    override val businessValidationErrors: Map<String, PostAvailablePayinOptionsError>
) : ClientBusinessValidationErrorException(cause, statusCode, businessValidationErrors)


/**
 * Exception for business validation error to wrap [GetTopupError] model class.
 */
class GetTopupErrorException(
    cause: Throwable,
    statusCode: Int,
    override val businessValidationErrors: Map<String, GetTopupError>
) : ClientBusinessValidationErrorException(cause, statusCode, businessValidationErrors)


/**
 * Exception for business validation error to wrap [GetPayoutError] model class.
 */
class GetPayoutErrorException(
    cause: Throwable,
    statusCode: Int,
    override val businessValidationErrors: Map<String, GetPayoutError>
) : ClientBusinessValidationErrorException(cause, statusCode, businessValidationErrors)


/**
 * Exception for business validation error to wrap [PostAvailablePayoutOptionsError] model class.
 */
class PostAvailablePayoutOptionsErrorException(
    cause: Throwable,
    statusCode: Int,
    override val businessValidationErrors: Map<String, PostAvailablePayoutOptionsError>
) : ClientBusinessValidationErrorException(cause, statusCode, businessValidationErrors)


/**
 * Exception for business validation error to wrap [PostAuthTokensError] model class.
 */
class PostAuthTokensErrorException(
    cause: Throwable,
    statusCode: Int,
    override val businessValidationErrors: Map<String, PostAuthTokensError>
) : ClientBusinessValidationErrorException(cause, statusCode, businessValidationErrors)


/**
 * Exception for business validation error to wrap [GetPaymentOptionsError] model class.
 */
class GetPaymentOptionsErrorException(
    cause: Throwable,
    statusCode: Int,
    override val businessValidationErrors: Map<String, GetPaymentOptionsError>
) : ClientBusinessValidationErrorException(cause, statusCode, businessValidationErrors)


/**
 * Exception for business validation error to wrap [GetPayinError] model class.
 */
class GetPayinErrorException(
    cause: Throwable,
    statusCode: Int,
    override val businessValidationErrors: Map<String, GetPayinError>
) : ClientBusinessValidationErrorException(cause, statusCode, businessValidationErrors)


/**
 * Exception for business validation error to wrap [PostPayinsError] model class.
 */
class PostPayinsErrorException(
    cause: Throwable,
    statusCode: Int,
    override val businessValidationErrors: Map<String, PostPayinsError>
) : ClientBusinessValidationErrorException(cause, statusCode, businessValidationErrors)


/**
 * Exception for business validation error to wrap [GetFxRatesError] model class.
 */
class GetFxRatesErrorException(
    cause: Throwable,
    statusCode: Int,
    override val businessValidationErrors: Map<String, GetFxRatesError>
) : ClientBusinessValidationErrorException(cause, statusCode, businessValidationErrors)


/**
 * Exception for business validation error to wrap [PostPayoutsError] model class.
 */
class PostPayoutsErrorException(
    cause: Throwable,
    statusCode: Int,
    override val businessValidationErrors: Map<String, PostPayoutsError>
) : ClientBusinessValidationErrorException(cause, statusCode, businessValidationErrors)


/**
 * Exception for business validation error to wrap [PostAvailableTopUpOptionsError] model class.
 */
class PostAvailableTopUpOptionsErrorException(
    cause: Throwable,
    statusCode: Int,
    override val businessValidationErrors: Map<String, PostAvailableTopUpOptionsError>
) : ClientBusinessValidationErrorException(cause, statusCode, businessValidationErrors)