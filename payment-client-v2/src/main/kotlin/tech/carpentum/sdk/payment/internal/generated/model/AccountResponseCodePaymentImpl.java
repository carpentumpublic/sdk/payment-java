//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** AccountResponseCodePayment
 *
 * Parameters of a Convenience Store Payment that your customer use to send funds to make a payment. These parameters has to be provided to your customer in form of an payment instructions.
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public class AccountResponseCodePaymentImpl implements AccountResponseCodePayment {
    /** Payment Code that we expect your customer to submit into kiosk in convenience store during the payment.
This parameter is to be shown to your customer in the payment instructions. */
    private final String accountNumber;

    @Override
    public String getAccountNumber() {
        return accountNumber;
    }




    private final int hashCode;
    private final String toString;

    private AccountResponseCodePaymentImpl(BuilderImpl builder) {
        this.accountNumber = Objects.requireNonNull(builder.accountNumber, "Property 'accountNumber' is required.");

        this.hashCode = Objects.hash(accountNumber);
        this.toString = builder.type + "(" +
                "accountNumber=" + accountNumber +
                ')';
    }

    @Override
    public int hashCode() {
        return hashCode;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }

        if (!(obj instanceof AccountResponseCodePaymentImpl)) {
            return false;
        }

        AccountResponseCodePaymentImpl that = (AccountResponseCodePaymentImpl) obj;
        if (!Objects.equals(this.accountNumber, that.accountNumber)) return false;

        return true;
    }

    @Override
    public String toString() {
        return toString;
    }

    /** Builder for {@link AccountResponseCodePayment} model class. */
    public static class BuilderImpl implements AccountResponseCodePayment.Builder {
        private String accountNumber = null;

        private final String type;

        public BuilderImpl(String type) {
            this.type = type;
        }

        public BuilderImpl() {
            this("AccountResponseCodePayment");
        }

        /**
          * Set {@link AccountResponseCodePayment#getAccountNumber} property.
          *
          * Payment Code that we expect your customer to submit into kiosk in convenience store during the payment.
This parameter is to be shown to your customer in the payment instructions.
          */
        @Override
        public BuilderImpl accountNumber(String accountNumber) {
            this.accountNumber = accountNumber;
            return this;
        }

        @Override
        public boolean isAccountNumberDefined() {
            return this.accountNumber != null;
        }

        /**
         * Create new instance of {@link AccountResponseCodePayment} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @Override
        public AccountResponseCodePaymentImpl build() {
            return new AccountResponseCodePaymentImpl(this);
        }

    }
}