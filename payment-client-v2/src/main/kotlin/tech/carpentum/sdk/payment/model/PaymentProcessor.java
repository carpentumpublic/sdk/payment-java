//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** PaymentProcessor
 *
 * The external processor of the payment. This field is returned only if an external processor is used.

 Possible values:
 * `POC` - PO Coins processor
 * `PAYSTAGE` - PayStage processor
 * `MTPX` - MetapayX processor
 * `TPAY` - Top Pay processor
 *
 * 
 *
 * The model class is immutable.
 *
 * Use static {@link #of} method to create a new model class instance.
 */
public interface PaymentProcessor {
    PaymentProcessor POC = PaymentProcessor.of("POC");
    PaymentProcessor PAYSTAGE = PaymentProcessor.of("PAYSTAGE");
    PaymentProcessor MTPX = PaymentProcessor.of("MTPX");
    PaymentProcessor TPAY = PaymentProcessor.of("TPAY");

    @NotNull String getValue();

    static PaymentProcessor of(@NotNull String value) {
        return new PaymentProcessorImpl(value);
    }
}