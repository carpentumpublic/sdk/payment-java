//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** MoneyVat
 *
 * 
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public interface MoneyVat {

    /** Amount of Value-Added Tax is returned in case it's levied on a payment order. */
    @NotNull java.math.BigDecimal getAmount();

    @NotNull CurrencyCode getCurrencyCode();

    @NotNull static Builder builder(MoneyVat copyOf) {
        Builder builder = builder();
        builder.amount(copyOf.getAmount());
        builder.currencyCode(copyOf.getCurrencyCode());
        return builder;
    }

    @NotNull static Builder builder() {
        return new MoneyVatImpl.BuilderImpl();
    }

    /** Builder for {@link MoneyVat} model class. */
    interface Builder {

        /**
          * Set {@link MoneyVat#getAmount} property.
          *
          * Amount of Value-Added Tax is returned in case it's levied on a payment order.
          */
        @NotNull Builder amount(java.math.BigDecimal amount);

        boolean isAmountDefined();


        /**
          * Set {@link MoneyVat#getCurrencyCode} property.
          *
          * 
          */
        @NotNull Builder currencyCode(CurrencyCode currencyCode);

        boolean isCurrencyCodeDefined();


        /**
         * Create new instance of {@link MoneyVat} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @NotNull MoneyVat build();

    }
}