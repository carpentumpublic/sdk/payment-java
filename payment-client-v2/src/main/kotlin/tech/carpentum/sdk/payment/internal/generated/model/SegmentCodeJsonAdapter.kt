//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.SegmentCode

class SegmentCodeJsonAdapter {

    @FromJson
    fun fromJson(json: String): SegmentCode {
        return SegmentCode.of(json)
    }

    @ToJson
    fun toJson(model: SegmentCode): String {
        return model.value
    }

    @FromJson
    fun fromJsonImpl(model: SegmentCode): SegmentCodeImpl {
        return model as SegmentCodeImpl
    }

    @ToJson
    fun toJsonImpl(impl: SegmentCodeImpl): SegmentCode {
        return impl
    }

}