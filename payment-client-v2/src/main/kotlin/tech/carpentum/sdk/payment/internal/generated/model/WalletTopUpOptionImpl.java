//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** WalletTopUpOption
 *
 * 
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public class WalletTopUpOptionImpl implements WalletTopUpOption {
    // tag::paymentTypeCodeEnum[]
    /** @see #getPaymentTypeCode */
    public static final String PAYMENT_TYPE_CODE_TOPUP_WALLET = "TOPUP_WALLET";
    // end::paymentTypeCodeEnum[]
    /** Wallet payment direction from the merchant's point of view. */
    private final String paymentTypeCode;

    @Override
    public String getPaymentTypeCode() {
        return paymentTypeCode;
    }


    private final PayinMethodCode paymentMethodCode;

    @Override
    public PayinMethodCode getPaymentMethodCode() {
        return paymentMethodCode;
    }


    private final CurrencyCode currencyCode;

    @Override
    public CurrencyCode getCurrencyCode() {
        return currencyCode;
    }


    private final Optional<SegmentCode> segmentCode;

    @Override
    public Optional<SegmentCode> getSegmentCode() {
        return segmentCode;
    }


    private final IntervalNumberTo transactionAmountLimit;

    @Override
    public IntervalNumberTo getTransactionAmountLimit() {
        return transactionAmountLimit;
    }


    /** If set to false, the option is not currently available and must be activated in administration. */
    private final Boolean isAvailable;

    @Override
    public Boolean getIsAvailable() {
        return isAvailable;
    }


    /** Payment operators that can be used for paying via this payment option. */
    private final java.util.List<@NotNull PaymentOperatorOption> paymentOperators;

    @Override
    public java.util.List<@NotNull PaymentOperatorOption> getPaymentOperators() {
        return paymentOperators;
    }




    private final int hashCode;
    private final String toString;

    private WalletTopUpOptionImpl(BuilderImpl builder) {
        this.paymentTypeCode = Objects.requireNonNull(builder.paymentTypeCode, "Property 'paymentTypeCode' is required.");
        this.paymentMethodCode = Objects.requireNonNull(builder.paymentMethodCode, "Property 'paymentMethodCode' is required.");
        this.currencyCode = Objects.requireNonNull(builder.currencyCode, "Property 'currencyCode' is required.");
        this.segmentCode = Optional.ofNullable(builder.segmentCode);
        this.transactionAmountLimit = Objects.requireNonNull(builder.transactionAmountLimit, "Property 'transactionAmountLimit' is required.");
        this.isAvailable = Objects.requireNonNull(builder.isAvailable, "Property 'isAvailable' is required.");
        this.paymentOperators = java.util.Collections.unmodifiableList(builder.paymentOperators);

        this.hashCode = Objects.hash(paymentTypeCode, paymentMethodCode, currencyCode, segmentCode, transactionAmountLimit, isAvailable, paymentOperators);
        this.toString = builder.type + "(" +
                "paymentTypeCode=" + paymentTypeCode +
                ", paymentMethodCode=" + paymentMethodCode +
                ", currencyCode=" + currencyCode +
                ", segmentCode=" + segmentCode +
                ", transactionAmountLimit=" + transactionAmountLimit +
                ", isAvailable=" + isAvailable +
                ", paymentOperators=" + paymentOperators +
                ')';
    }

    @Override
    public int hashCode() {
        return hashCode;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }

        if (!(obj instanceof WalletTopUpOptionImpl)) {
            return false;
        }

        WalletTopUpOptionImpl that = (WalletTopUpOptionImpl) obj;
        if (!Objects.equals(this.paymentTypeCode, that.paymentTypeCode)) return false;
        if (!Objects.equals(this.paymentMethodCode, that.paymentMethodCode)) return false;
        if (!Objects.equals(this.currencyCode, that.currencyCode)) return false;
        if (!Objects.equals(this.segmentCode, that.segmentCode)) return false;
        if (!Objects.equals(this.transactionAmountLimit, that.transactionAmountLimit)) return false;
        if (!Objects.equals(this.isAvailable, that.isAvailable)) return false;
        if (!Objects.equals(this.paymentOperators, that.paymentOperators)) return false;

        return true;
    }

    @Override
    public String toString() {
        return toString;
    }

    /** Builder for {@link WalletTopUpOption} model class. */
    public static class BuilderImpl implements WalletTopUpOption.Builder {
        private String paymentTypeCode = null;
        private PayinMethodCode paymentMethodCode = null;
        private CurrencyCode currencyCode = null;
        private SegmentCode segmentCode = null;
        private IntervalNumberTo transactionAmountLimit = null;
        private Boolean isAvailable = null;
        private java.util.List<@NotNull PaymentOperatorOption> paymentOperators = new java.util.ArrayList<>();

        private final String type;

        public BuilderImpl(String type) {
            this.type = type;
        }

        public BuilderImpl() {
            this("WalletTopUpOption");
        }

        /**
          * Set {@link WalletTopUpOption#getPaymentTypeCode} property.
          *
          * Wallet payment direction from the merchant's point of view.
          */
        @Override
        public BuilderImpl paymentTypeCode(String paymentTypeCode) {
            this.paymentTypeCode = paymentTypeCode;
            return this;
        }

        @Override
        public boolean isPaymentTypeCodeDefined() {
            return this.paymentTypeCode != null;
        }

        /**
          * Set {@link WalletTopUpOption#getPaymentMethodCode} property.
          *
          * 
          */
        @Override
        public BuilderImpl paymentMethodCode(PayinMethodCode paymentMethodCode) {
            this.paymentMethodCode = paymentMethodCode;
            return this;
        }

        @Override
        public boolean isPaymentMethodCodeDefined() {
            return this.paymentMethodCode != null;
        }

        /**
          * Set {@link WalletTopUpOption#getCurrencyCode} property.
          *
          * 
          */
        @Override
        public BuilderImpl currencyCode(CurrencyCode currencyCode) {
            this.currencyCode = currencyCode;
            return this;
        }

        @Override
        public boolean isCurrencyCodeDefined() {
            return this.currencyCode != null;
        }

        /**
          * Set {@link WalletTopUpOption#getSegmentCode} property.
          *
          * 
          */
        @Override
        public BuilderImpl segmentCode(SegmentCode segmentCode) {
            this.segmentCode = segmentCode;
            return this;
        }

        @Override
        public boolean isSegmentCodeDefined() {
            return this.segmentCode != null;
        }

        /**
          * Set {@link WalletTopUpOption#getTransactionAmountLimit} property.
          *
          * 
          */
        @Override
        public BuilderImpl transactionAmountLimit(IntervalNumberTo transactionAmountLimit) {
            this.transactionAmountLimit = transactionAmountLimit;
            return this;
        }

        @Override
        public boolean isTransactionAmountLimitDefined() {
            return this.transactionAmountLimit != null;
        }

        /**
          * Set {@link WalletTopUpOption#getIsAvailable} property.
          *
          * If set to false, the option is not currently available and must be activated in administration.
          */
        @Override
        public BuilderImpl isAvailable(Boolean isAvailable) {
            this.isAvailable = isAvailable;
            return this;
        }

        @Override
        public boolean isIsAvailableDefined() {
            return this.isAvailable != null;
        }

        /**
          * Replace all items in {@link WalletTopUpOption#getPaymentOperators} list property.
          *
          * Payment operators that can be used for paying via this payment option.
          */
        @Override
        public BuilderImpl paymentOperators(java.util.List<@NotNull PaymentOperatorOption> paymentOperators) {
            this.paymentOperators.clear();
            if (paymentOperators != null) {
                this.paymentOperators.addAll(paymentOperators);
            }
            return this;
        }
        /**
          * Add single item to {@link WalletTopUpOption#getPaymentOperators} list property.
          *
          * Payment operators that can be used for paying via this payment option.
          */
        @Override
        public BuilderImpl paymentOperatorsAdd(PaymentOperatorOption item) {
            if (item != null) {
                this.paymentOperators.add(item);
            }
            return this;
        }
        /**
          * Add all items to {@link WalletTopUpOption#getPaymentOperators} list property.
          *
          * Payment operators that can be used for paying via this payment option.
          */
        @Override
        public BuilderImpl paymentOperatorsAddAll(java.util.List<@NotNull PaymentOperatorOption> paymentOperators) {
            if (paymentOperators != null) {
                this.paymentOperators.addAll(paymentOperators);
            }
            return this;
        }


        /**
         * Create new instance of {@link WalletTopUpOption} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @Override
        public WalletTopUpOptionImpl build() {
            return new WalletTopUpOptionImpl(this);
        }

    }
}