//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** TopUpDetail
 *
 * 
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public interface TopUpDetail {

    @NotNull TopUpRequestedDetail getTopUpRequested();

    @NotNull PaymentProcess getProcess();

    @NotNull MoneyFee getFee();

    @NotNull PayinMethodResponse getPaymentMethodResponse();

    @NotNull Optional<SettlementMethod> getSettlement();

    @NotNull static Builder builder(TopUpDetail copyOf) {
        Builder builder = builder();
        builder.topUpRequested(copyOf.getTopUpRequested());
        builder.process(copyOf.getProcess());
        builder.fee(copyOf.getFee());
        builder.paymentMethodResponse(copyOf.getPaymentMethodResponse());
        builder.settlement(copyOf.getSettlement().orElse(null));
        return builder;
    }

    @NotNull static Builder builder() {
        return new TopUpDetailImpl.BuilderImpl();
    }

    /** Builder for {@link TopUpDetail} model class. */
    interface Builder {

        /**
          * Set {@link TopUpDetail#getTopUpRequested} property.
          *
          * 
          */
        @NotNull Builder topUpRequested(TopUpRequestedDetail topUpRequested);

        boolean isTopUpRequestedDefined();


        /**
          * Set {@link TopUpDetail#getProcess} property.
          *
          * 
          */
        @NotNull Builder process(PaymentProcess process);

        boolean isProcessDefined();


        /**
          * Set {@link TopUpDetail#getFee} property.
          *
          * 
          */
        @NotNull Builder fee(MoneyFee fee);

        boolean isFeeDefined();


        /**
          * Set {@link TopUpDetail#getPaymentMethodResponse} property.
          *
          * 
          */
        @NotNull Builder paymentMethodResponse(PayinMethodResponse paymentMethodResponse);

        boolean isPaymentMethodResponseDefined();


        /**
          * Set {@link TopUpDetail#getSettlement} property.
          *
          * 
          */
        @NotNull Builder settlement(SettlementMethod settlement);

        boolean isSettlementDefined();


        /**
         * Create new instance of {@link TopUpDetail} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @NotNull TopUpDetail build();

    }
}