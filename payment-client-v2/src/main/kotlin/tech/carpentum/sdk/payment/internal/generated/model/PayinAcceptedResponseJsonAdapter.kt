//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.JsonDataException
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.PayinAcceptedResponse
import tech.carpentum.sdk.payment.model.PayinAcceptedMethodResponse
import tech.carpentum.sdk.payment.model.PayinAcceptedRedirectResponse

class PayinAcceptedResponseJsonAdapter {

    @FromJson
    fun fromJson(json: PayinAcceptedResponseJson): PayinAcceptedResponse {
        return if (json.paymentRequested != null && json.paymentMethodResponse != null) {
            val builder = PayinAcceptedMethodResponse.builder()
            builder.paymentRequested(json.paymentRequested)
            builder.paymentMethodResponse(json.paymentMethodResponse)
            builder.build()
        } else if (json.paymentRequested != null && json.redirectTo != null) {
            val builder = PayinAcceptedRedirectResponse.builder()
            builder.paymentRequested(json.paymentRequested)
            builder.redirectTo(json.redirectTo)
            builder.build()
        } else {
          throw JsonDataException("Missing required attributes: [paymentRequested, paymentMethodResponse] or [paymentRequested, redirectTo].")
        }
    }

    @ToJson
    fun toJson(model: PayinAcceptedResponse): PayinAcceptedResponseJson {
        val json = PayinAcceptedResponseJson()
        when (model) {
            is PayinAcceptedMethodResponse -> {
                json.paymentRequested = model.paymentRequested
                json.paymentMethodResponse = model.paymentMethodResponse
            }
            is PayinAcceptedRedirectResponse -> {
                json.paymentRequested = model.paymentRequested
                json.redirectTo = model.redirectTo
            }
            else -> {
                throw JsonDataException("Unsupported type [${model.javaClass.name}] of model.")
            }
        }
        return json
    }

}