//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** AccountResponseP2AV2
 *
 * Parameters of a bank account where we expect that your customer send funds to make a payment. These account parameters has to be provided to your customer in form of an payment instructions.
The returned parameters are depended on the payment method and currency your customer choose to pay.
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@Deprecated
@JsonClass(generateAdapter = false)
public interface AccountResponseP2AV2 {

    /** Name of the bank account where we expect that your customer sends funds to make a payment.
If this parameter contains any value then show it to your customer in the payment instructions. */
    @NotNull Optional<String> getAccountName();

    /** Number of the bank account where we expect that your customer sends funds to make a payment.
If this parameter contains any value then show it to your customer in the payment instructions. */
    @NotNull Optional<String> getAccountNumber();

    /** Bank code of the bank where we expect that your customer sends funds to make a payment.
If this parameter contains any value then show it to your customer in the payment instructions. */
    @NotNull Optional<String> getBankCode();

    /** Name of the bank where we expect that your customer sends funds to make a payment.
If this parameter contains any value then show it to your customer in the payment instructions. */
    @NotNull Optional<String> getBankName();

    /** Branch name of the bank where we expect that your customer sends funds to make a payment.
If this parameter contains any value then show it to your customer in the payment instructions. */
    @NotNull Optional<String> getBankBranch();

    /** City of the bank where we expect that your customer sends funds to make a payment.
If this parameter contains any value then show it to your customer in the payment instructions. */
    @NotNull Optional<String> getBankCity();

    /** Province of the bank where we expect that your customer sends funds to make a payment.
If this parameter contains any value then show it to your customer in the payment instructions. */
    @NotNull Optional<String> getBankProvince();


    @Deprecated @NotNull static Builder builder() {
        return new AccountResponseP2AV2Impl.BuilderImpl();
    }

    /** Builder for {@link AccountResponseP2AV2} model class. */
    @Deprecated interface Builder {

        /**
          * Set {@link AccountResponseP2AV2#getAccountName} property.
          *
          * Name of the bank account where we expect that your customer sends funds to make a payment.
If this parameter contains any value then show it to your customer in the payment instructions.
          */
        @Deprecated @NotNull Builder accountName(String accountName);

        @Deprecated boolean isAccountNameDefined();


        /**
          * Set {@link AccountResponseP2AV2#getAccountNumber} property.
          *
          * Number of the bank account where we expect that your customer sends funds to make a payment.
If this parameter contains any value then show it to your customer in the payment instructions.
          */
        @Deprecated @NotNull Builder accountNumber(String accountNumber);

        @Deprecated boolean isAccountNumberDefined();


        /**
          * Set {@link AccountResponseP2AV2#getBankCode} property.
          *
          * Bank code of the bank where we expect that your customer sends funds to make a payment.
If this parameter contains any value then show it to your customer in the payment instructions.
          */
        @Deprecated @NotNull Builder bankCode(String bankCode);

        @Deprecated boolean isBankCodeDefined();


        /**
          * Set {@link AccountResponseP2AV2#getBankName} property.
          *
          * Name of the bank where we expect that your customer sends funds to make a payment.
If this parameter contains any value then show it to your customer in the payment instructions.
          */
        @Deprecated @NotNull Builder bankName(String bankName);

        @Deprecated boolean isBankNameDefined();


        /**
          * Set {@link AccountResponseP2AV2#getBankBranch} property.
          *
          * Branch name of the bank where we expect that your customer sends funds to make a payment.
If this parameter contains any value then show it to your customer in the payment instructions.
          */
        @Deprecated @NotNull Builder bankBranch(String bankBranch);

        @Deprecated boolean isBankBranchDefined();


        /**
          * Set {@link AccountResponseP2AV2#getBankCity} property.
          *
          * City of the bank where we expect that your customer sends funds to make a payment.
If this parameter contains any value then show it to your customer in the payment instructions.
          */
        @Deprecated @NotNull Builder bankCity(String bankCity);

        @Deprecated boolean isBankCityDefined();


        /**
          * Set {@link AccountResponseP2AV2#getBankProvince} property.
          *
          * Province of the bank where we expect that your customer sends funds to make a payment.
If this parameter contains any value then show it to your customer in the payment instructions.
          */
        @Deprecated @NotNull Builder bankProvince(String bankProvince);

        @Deprecated boolean isBankProvinceDefined();


        /**
         * Create new instance of {@link AccountResponseP2AV2} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @Deprecated @NotNull AccountResponseP2AV2 build();

    }
}