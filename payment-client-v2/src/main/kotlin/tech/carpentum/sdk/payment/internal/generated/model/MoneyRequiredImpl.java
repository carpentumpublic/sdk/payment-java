//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** MoneyRequired
 *
 * 
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public class MoneyRequiredImpl implements MoneyRequired {
    /** If returned, this field contains the actual amount that the end customer should pay. The amount provided in `paymentMethodResponse.money.amount` will be credited to your account.

Amount is accepted in the smallest currency unit. For fiat currencies the smallest currency unit are based on ISO 4217 (e.g. for USD two decimal places are available so amount can be accepted as 12.34). The exceptions are IDR and INR currencies: zero amount in decimal places are allowed for them (e.g. 42.05 is not allowed, while 42 or 42.00 or 43 or 43.00 are allowed).

If more decimal places than supported is provided request will fail on HTTP 406 error (e.g. for USD the amount 12.345 will not be accepted). */
    private final java.math.BigDecimal amount;

    @Override
    public java.math.BigDecimal getAmount() {
        return amount;
    }


    private final CurrencyCode currencyCode;

    @Override
    public CurrencyCode getCurrencyCode() {
        return currencyCode;
    }




    private final int hashCode;
    private final String toString;

    private MoneyRequiredImpl(BuilderImpl builder) {
        this.amount = Objects.requireNonNull(builder.amount, "Property 'amount' is required.");
        this.currencyCode = Objects.requireNonNull(builder.currencyCode, "Property 'currencyCode' is required.");

        this.hashCode = Objects.hash(amount, currencyCode);
        this.toString = builder.type + "(" +
                "amount=" + amount +
                ", currencyCode=" + currencyCode +
                ')';
    }

    @Override
    public int hashCode() {
        return hashCode;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }

        if (!(obj instanceof MoneyRequiredImpl)) {
            return false;
        }

        MoneyRequiredImpl that = (MoneyRequiredImpl) obj;
        if (!Objects.equals(this.amount, that.amount)) return false;
        if (!Objects.equals(this.currencyCode, that.currencyCode)) return false;

        return true;
    }

    @Override
    public String toString() {
        return toString;
    }

    /** Builder for {@link MoneyRequired} model class. */
    public static class BuilderImpl implements MoneyRequired.Builder {
        private java.math.BigDecimal amount = null;
        private CurrencyCode currencyCode = null;

        private final String type;

        public BuilderImpl(String type) {
            this.type = type;
        }

        public BuilderImpl() {
            this("MoneyRequired");
        }

        /**
          * Set {@link MoneyRequired#getAmount} property.
          *
          * If returned, this field contains the actual amount that the end customer should pay. The amount provided in `paymentMethodResponse.money.amount` will be credited to your account.

Amount is accepted in the smallest currency unit. For fiat currencies the smallest currency unit are based on ISO 4217 (e.g. for USD two decimal places are available so amount can be accepted as 12.34). The exceptions are IDR and INR currencies: zero amount in decimal places are allowed for them (e.g. 42.05 is not allowed, while 42 or 42.00 or 43 or 43.00 are allowed).

If more decimal places than supported is provided request will fail on HTTP 406 error (e.g. for USD the amount 12.345 will not be accepted).
          */
        @Override
        public BuilderImpl amount(java.math.BigDecimal amount) {
            this.amount = amount;
            return this;
        }

        @Override
        public boolean isAmountDefined() {
            return this.amount != null;
        }

        /**
          * Set {@link MoneyRequired#getCurrencyCode} property.
          *
          * 
          */
        @Override
        public BuilderImpl currencyCode(CurrencyCode currencyCode) {
            this.currencyCode = currencyCode;
            return this;
        }

        @Override
        public boolean isCurrencyCodeDefined() {
            return this.currencyCode != null;
        }

        /**
         * Create new instance of {@link MoneyRequired} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @Override
        public MoneyRequiredImpl build() {
            return new MoneyRequiredImpl(this);
        }

    }
}