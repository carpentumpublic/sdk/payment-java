//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.PayinMethodCode

class PayinMethodCodeJsonAdapter {

    @FromJson
    fun fromJson(json: String): PayinMethodCode {
        return PayinMethodCode.of(json)
    }

    @ToJson
    fun toJson(model: PayinMethodCode): String {
        return model.value
    }

    @FromJson
    fun fromJsonImpl(model: PayinMethodCode): PayinMethodCodeImpl {
        return model as PayinMethodCodeImpl
    }

    @ToJson
    fun toJsonImpl(impl: PayinMethodCodeImpl): PayinMethodCode {
        return impl
    }

}