//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** DUITNOW
 *
 * Payment method which requires customer to scan QR code from the Payment application by Customer's Payment service application right after the payment is submitted.
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public interface DuitNowMethod extends PayinMethod {
    /** A discriminator value of property {@link #getPaymentMethodCode}. The model class extends {@link PayinMethod}. */
    PayinMethod.PaymentMethodCode PAYMENT_METHOD_CODE = PayinMethod.PaymentMethodCode.DUITNOW;

    @NotNull AccountPayinRequestDuitNow getAccount();

    /** One of following can serve as Payment Operator:

 * Financial or other institution (such as bank, card payment processor, ...) that manages transactions for your customers
 * Mobile wallet
 * Blockchain protocol for crypto currency payments

Customer is informed with the payment instructions where funds have to be transferred efficiently based on the selected Payment operator.
For getting list of the available payment options for payins use [POST /payins/!availablePaymentOptions](#operations-Incoming_payments-availablePaymentOptions) API, for payouts use [POST /payouts/!availablePaymentOptions](#operations-Outgoing_payments-availablePaymentOptions) API. */
    @NotNull Optional<String> getPaymentOperatorCode();

    /** Your customer e-mail address in RFC 5322 format that is used for identification of the customer's payins. */
    @NotNull Optional<String> getEmailAddress();

    @NotNull static Builder builder(DuitNowMethod copyOf) {
        Builder builder = builder();
        builder.account(copyOf.getAccount());
        builder.paymentOperatorCode(copyOf.getPaymentOperatorCode().orElse(null));
        builder.emailAddress(copyOf.getEmailAddress().orElse(null));
        return builder;
    }

    @NotNull static Builder builder() {
        return new DuitNowMethodImpl.BuilderImpl();
    }

    /** Builder for {@link DuitNowMethod} model class. */
    interface Builder {

        /**
          * Set {@link DuitNowMethod#getAccount} property.
          *
          * 
          */
        @NotNull Builder account(AccountPayinRequestDuitNow account);

        boolean isAccountDefined();


        /**
          * Set {@link DuitNowMethod#getPaymentOperatorCode} property.
          *
          * One of following can serve as Payment Operator:

 * Financial or other institution (such as bank, card payment processor, ...) that manages transactions for your customers
 * Mobile wallet
 * Blockchain protocol for crypto currency payments

Customer is informed with the payment instructions where funds have to be transferred efficiently based on the selected Payment operator.
For getting list of the available payment options for payins use [POST /payins/!availablePaymentOptions](#operations-Incoming_payments-availablePaymentOptions) API, for payouts use [POST /payouts/!availablePaymentOptions](#operations-Outgoing_payments-availablePaymentOptions) API.
          */
        @NotNull Builder paymentOperatorCode(String paymentOperatorCode);

        boolean isPaymentOperatorCodeDefined();


        /**
          * Set {@link DuitNowMethod#getEmailAddress} property.
          *
          * Your customer e-mail address in RFC 5322 format that is used for identification of the customer's payins.
          */
        @NotNull Builder emailAddress(String emailAddress);

        boolean isEmailAddressDefined();


        /**
         * Create new instance of {@link DuitNowMethod} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @NotNull DuitNowMethod build();

    }
}