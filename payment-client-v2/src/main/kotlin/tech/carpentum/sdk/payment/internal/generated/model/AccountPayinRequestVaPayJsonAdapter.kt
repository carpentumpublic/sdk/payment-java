//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.AccountPayinRequestVaPay

class AccountPayinRequestVaPayJsonAdapter {
    @FromJson
    fun fromJson(json: AccountPayinRequestVaPayJson): AccountPayinRequestVaPay {
        val builder = AccountPayinRequestVaPay.builder()
        builder.accountName(json.accountName)
        builder.accountNumber(json.accountNumber)
        return builder.build()
    }

    @ToJson
    fun toJson(model: AccountPayinRequestVaPay): AccountPayinRequestVaPayJson {
        val json = AccountPayinRequestVaPayJson()
        json.accountName = model.accountName.orElse(null)
        json.accountNumber = model.accountNumber.orElse(null)
        return json
    }

    @FromJson
    fun fromJsonImpl(model: AccountPayinRequestVaPay): AccountPayinRequestVaPayImpl {
        return model as AccountPayinRequestVaPayImpl
    }

    @ToJson
    fun toJsonImpl(impl: AccountPayinRequestVaPayImpl): AccountPayinRequestVaPay {
        return impl
    }

}