//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** AccountResponsePayMe
 *
 * Parameters of a bank account where we expect that your customer send funds to make a payment. These account parameters has to be provided to your customer in form of an payment instructions.
The returned parameters are depended on the payment method and currency your customer choose to pay.
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public interface AccountResponsePayMe {

    /** Name of the bank account where we expect that your customer sends funds to make a payment.
This parameter is to be shown to your customer in the payment instructions. */
    @NotNull Optional<String> getAccountName();

    @NotNull static AccountResponsePayMe ofAccountName(String accountName) { return builder().accountName(accountName).build(); }

    @NotNull static Builder builder() {
        return new AccountResponsePayMeImpl.BuilderImpl();
    }

    /** Builder for {@link AccountResponsePayMe} model class. */
    interface Builder {

        /**
          * Set {@link AccountResponsePayMe#getAccountName} property.
          *
          * Name of the bank account where we expect that your customer sends funds to make a payment.
This parameter is to be shown to your customer in the payment instructions.
          */
        @NotNull Builder accountName(String accountName);

        boolean isAccountNameDefined();


        /**
         * Create new instance of {@link AccountResponsePayMe} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @NotNull AccountResponsePayMe build();

    }
}