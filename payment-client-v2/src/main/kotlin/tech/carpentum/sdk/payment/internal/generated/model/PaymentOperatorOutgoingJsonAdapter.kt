//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.PaymentOperatorOutgoing

class PaymentOperatorOutgoingJsonAdapter {
    @FromJson
    fun fromJson(json: PaymentOperatorOutgoingJson): PaymentOperatorOutgoing {
        val builder = PaymentOperatorOutgoing.builder()
        builder.code(json.code)
        builder.name(json.name)
        builder.customerTransactionFee(json.customerTransactionFee)
        return builder.build()
    }

    @ToJson
    fun toJson(model: PaymentOperatorOutgoing): PaymentOperatorOutgoingJson {
        val json = PaymentOperatorOutgoingJson()
        json.code = model.code
        json.name = model.name
        json.customerTransactionFee = model.customerTransactionFee.orElse(null)
        return json
    }

    @FromJson
    fun fromJsonImpl(model: PaymentOperatorOutgoing): PaymentOperatorOutgoingImpl {
        return model as PaymentOperatorOutgoingImpl
    }

    @ToJson
    fun toJsonImpl(impl: PaymentOperatorOutgoingImpl): PaymentOperatorOutgoing {
        return impl
    }

}