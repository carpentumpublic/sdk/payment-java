//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** AvailableTopUpOptions
 *
 * The list of possible top up options.
The list always contains at least one payment option.
If no available payment options are found, an HTTP 406 response is returned with the error code.
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public class AvailableTopUpOptionsImpl implements AvailableTopUpOptions {
    private final java.util.List<@NotNull AvailableTopUpOption> options;

    @Override
    public java.util.List<@NotNull AvailableTopUpOption> getOptions() {
        return options;
    }




    private final int hashCode;
    private final String toString;

    private AvailableTopUpOptionsImpl(BuilderImpl builder) {
        this.options = java.util.Collections.unmodifiableList(builder.options);

        this.hashCode = Objects.hash(options);
        this.toString = builder.type + "(" +
                "options=" + options +
                ')';
    }

    @Override
    public int hashCode() {
        return hashCode;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }

        if (!(obj instanceof AvailableTopUpOptionsImpl)) {
            return false;
        }

        AvailableTopUpOptionsImpl that = (AvailableTopUpOptionsImpl) obj;
        if (!Objects.equals(this.options, that.options)) return false;

        return true;
    }

    @Override
    public String toString() {
        return toString;
    }

    /** Builder for {@link AvailableTopUpOptions} model class. */
    public static class BuilderImpl implements AvailableTopUpOptions.Builder {
        private java.util.List<@NotNull AvailableTopUpOption> options = new java.util.ArrayList<>();

        private final String type;

        public BuilderImpl(String type) {
            this.type = type;
        }

        public BuilderImpl() {
            this("AvailableTopUpOptions");
        }

        /**
          * Replace all items in {@link AvailableTopUpOptions#getOptions} list property.
          *
          * 
          */
        @Override
        public BuilderImpl options(java.util.List<@NotNull AvailableTopUpOption> options) {
            this.options.clear();
            if (options != null) {
                this.options.addAll(options);
            }
            return this;
        }
        /**
          * Add single item to {@link AvailableTopUpOptions#getOptions} list property.
          *
          * 
          */
        @Override
        public BuilderImpl optionsAdd(AvailableTopUpOption item) {
            if (item != null) {
                this.options.add(item);
            }
            return this;
        }
        /**
          * Add all items to {@link AvailableTopUpOptions#getOptions} list property.
          *
          * 
          */
        @Override
        public BuilderImpl optionsAddAll(java.util.List<@NotNull AvailableTopUpOption> options) {
            if (options != null) {
                this.options.addAll(options);
            }
            return this;
        }


        /**
         * Create new instance of {@link AvailableTopUpOptions} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @Override
        public AvailableTopUpOptionsImpl build() {
            return new AvailableTopUpOptionsImpl(this);
        }

    }
}