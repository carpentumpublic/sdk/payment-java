//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import tech.carpentum.sdk.payment.model.PayoutMethod
import tech.carpentum.sdk.payment.model.PayoutMethod.PaymentMethodCode

import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.Moshi
import com.squareup.moshi.adapters.PolymorphicJsonAdapterFactory
import java.lang.reflect.Type

class PayoutMethodJsonAdapter : JsonAdapter.Factory {
    override fun create(type: Type, annotations: MutableSet<out Annotation>, moshi: Moshi): JsonAdapter<*>? {
        return PolymorphicJsonAdapterFactory
            .of(PayoutMethod::class.java, PayoutMethod.DISCRIMINATOR)
            .withSubtype(BankTransferMethodImpl::class.java, PaymentMethodCode.BANK_TRANSFER.toString())
            .withSubtype(CryptoTransferMethodImpl::class.java, PaymentMethodCode.CRYPTO_TRANSFER.toString())
            .withSubtype(WalletTransferMethodImpl::class.java, PaymentMethodCode.WALLET_TRANSFER.toString())
            .create(type, annotations, moshi)
    }
}