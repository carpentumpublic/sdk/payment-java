//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.ForexRate

class ForexRateJsonAdapter {
    @FromJson
    fun fromJson(json: ForexRateJson): ForexRate {
        val builder = ForexRate.builder()
        builder.baseCurrencyCode(json.baseCurrencyCode)
        builder.quoteCurrencyCode(json.quoteCurrencyCode)
        builder.buyRate(json.buyRate)
        builder.sellRate(json.sellRate)
        builder.validUpTo(json.validUpTo)
        return builder.build()
    }

    @ToJson
    fun toJson(model: ForexRate): ForexRateJson {
        val json = ForexRateJson()
        json.baseCurrencyCode = model.baseCurrencyCode
        json.quoteCurrencyCode = model.quoteCurrencyCode
        json.buyRate = model.buyRate.orElse(null)
        json.sellRate = model.sellRate.orElse(null)
        json.validUpTo = model.validUpTo.orElse(null)
        return json
    }

    @FromJson
    fun fromJsonImpl(model: ForexRate): ForexRateImpl {
        return model as ForexRateImpl
    }

    @ToJson
    fun toJsonImpl(impl: ForexRateImpl): ForexRate {
        return impl
    }

}