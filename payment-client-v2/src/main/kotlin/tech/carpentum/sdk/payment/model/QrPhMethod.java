//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** QRPH
 *
 * QR Ph is payment method intended for Philippine market which allows customers to send payments by scanning QR codes from supported banks and e-wallets.
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public interface QrPhMethod extends PayinMethod {
    /** A discriminator value of property {@link #getPaymentMethodCode}. The model class extends {@link PayinMethod}. */
    PayinMethod.PaymentMethodCode PAYMENT_METHOD_CODE = PayinMethod.PaymentMethodCode.QRPH;

    @NotNull Optional<AccountPayinRequestQrPh> getAccount();

    /** Your customer e-mail address in RFC 5322 format that is used for identification of the customer's payins. */
    @NotNull Optional<String> getEmailAddress();

    /** One of following can serve as Payment Operator:

 * Financial or other institution (such as bank, card payment processor, ...) that manages transactions for your customers
 * Mobile wallet
 * Blockchain protocol for crypto currency payments

Customer is informed with the payment instructions where funds have to be transferred efficiently based on the selected Payment operator.
For getting list of the available payment options for payins use [POST /payins/!availablePaymentOptions](#operations-Incoming_payments-availablePaymentOptions) API, for payouts use [POST /payouts/!availablePaymentOptions](#operations-Outgoing_payments-availablePaymentOptions) API. */
    @NotNull Optional<String> getPaymentOperatorCode();

    /** Your customer mobile phone number in full international telephone number format, including country code. */
    @NotNull Optional<String> getPhoneNumber();

    @NotNull static Builder builder(QrPhMethod copyOf) {
        Builder builder = builder();
        builder.account(copyOf.getAccount().orElse(null));
        builder.emailAddress(copyOf.getEmailAddress().orElse(null));
        builder.paymentOperatorCode(copyOf.getPaymentOperatorCode().orElse(null));
        builder.phoneNumber(copyOf.getPhoneNumber().orElse(null));
        return builder;
    }

    @NotNull static Builder builder() {
        return new QrPhMethodImpl.BuilderImpl();
    }

    /** Builder for {@link QrPhMethod} model class. */
    interface Builder {

        /**
          * Set {@link QrPhMethod#getAccount} property.
          *
          * 
          */
        @NotNull Builder account(AccountPayinRequestQrPh account);

        boolean isAccountDefined();


        /**
          * Set {@link QrPhMethod#getEmailAddress} property.
          *
          * Your customer e-mail address in RFC 5322 format that is used for identification of the customer's payins.
          */
        @NotNull Builder emailAddress(String emailAddress);

        boolean isEmailAddressDefined();


        /**
          * Set {@link QrPhMethod#getPaymentOperatorCode} property.
          *
          * One of following can serve as Payment Operator:

 * Financial or other institution (such as bank, card payment processor, ...) that manages transactions for your customers
 * Mobile wallet
 * Blockchain protocol for crypto currency payments

Customer is informed with the payment instructions where funds have to be transferred efficiently based on the selected Payment operator.
For getting list of the available payment options for payins use [POST /payins/!availablePaymentOptions](#operations-Incoming_payments-availablePaymentOptions) API, for payouts use [POST /payouts/!availablePaymentOptions](#operations-Outgoing_payments-availablePaymentOptions) API.
          */
        @NotNull Builder paymentOperatorCode(String paymentOperatorCode);

        boolean isPaymentOperatorCodeDefined();


        /**
          * Set {@link QrPhMethod#getPhoneNumber} property.
          *
          * Your customer mobile phone number in full international telephone number format, including country code.
          */
        @NotNull Builder phoneNumber(String phoneNumber);

        boolean isPhoneNumberDefined();


        /**
         * Create new instance of {@link QrPhMethod} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @NotNull QrPhMethod build();

    }
}