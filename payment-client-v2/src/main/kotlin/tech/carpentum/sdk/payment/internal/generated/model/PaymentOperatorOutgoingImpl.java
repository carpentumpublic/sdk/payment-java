//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** PaymentOperatorOutgoing
 *
 * PaymentOperator is a financial institution that manages transactions in your customer's account (such as banks, card payment processors, mobile wallets and so on).
The payment operators are assigned to the payment methods according to your account configuration.

For supported payment operators please refer to [`GET /payment-operators`](#operations-Payments-getPaymentOperators).
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public class PaymentOperatorOutgoingImpl implements PaymentOperatorOutgoing {
    /** One of following can serve as Payment Operator:

 * Financial or other institution (such as bank, card payment processor, ...) that manages transactions for your customers
 * Mobile wallet
 * Blockchain protocol for crypto currency payments

Customer is informed with the payment instructions where funds have to be transferred efficiently based on the selected Payment operator.
For getting list of the available payment options for payins use [POST /payins/!availablePaymentOptions](#operations-Incoming_payments-availablePaymentOptions) API, for payouts use [POST /payouts/!availablePaymentOptions](#operations-Outgoing_payments-availablePaymentOptions) API. */
    private final String code;

    @Override
    public String getCode() {
        return code;
    }


    private final String name;

    @Override
    public String getName() {
        return name;
    }


    private final Optional<CustomerTransactionFee> customerTransactionFee;

    @Override
    public Optional<CustomerTransactionFee> getCustomerTransactionFee() {
        return customerTransactionFee;
    }




    private final int hashCode;
    private final String toString;

    private PaymentOperatorOutgoingImpl(BuilderImpl builder) {
        this.code = Objects.requireNonNull(builder.code, "Property 'code' is required.");
        this.name = Objects.requireNonNull(builder.name, "Property 'name' is required.");
        this.customerTransactionFee = Optional.ofNullable(builder.customerTransactionFee);

        this.hashCode = Objects.hash(code, name, customerTransactionFee);
        this.toString = builder.type + "(" +
                "code=" + code +
                ", name=" + name +
                ", customerTransactionFee=" + customerTransactionFee +
                ')';
    }

    @Override
    public int hashCode() {
        return hashCode;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }

        if (!(obj instanceof PaymentOperatorOutgoingImpl)) {
            return false;
        }

        PaymentOperatorOutgoingImpl that = (PaymentOperatorOutgoingImpl) obj;
        if (!Objects.equals(this.code, that.code)) return false;
        if (!Objects.equals(this.name, that.name)) return false;
        if (!Objects.equals(this.customerTransactionFee, that.customerTransactionFee)) return false;

        return true;
    }

    @Override
    public String toString() {
        return toString;
    }

    /** Builder for {@link PaymentOperatorOutgoing} model class. */
    public static class BuilderImpl implements PaymentOperatorOutgoing.Builder {
        private String code = null;
        private String name = null;
        private CustomerTransactionFee customerTransactionFee = null;

        private final String type;

        public BuilderImpl(String type) {
            this.type = type;
        }

        public BuilderImpl() {
            this("PaymentOperatorOutgoing");
        }

        /**
          * Set {@link PaymentOperatorOutgoing#getCode} property.
          *
          * One of following can serve as Payment Operator:

 * Financial or other institution (such as bank, card payment processor, ...) that manages transactions for your customers
 * Mobile wallet
 * Blockchain protocol for crypto currency payments

Customer is informed with the payment instructions where funds have to be transferred efficiently based on the selected Payment operator.
For getting list of the available payment options for payins use [POST /payins/!availablePaymentOptions](#operations-Incoming_payments-availablePaymentOptions) API, for payouts use [POST /payouts/!availablePaymentOptions](#operations-Outgoing_payments-availablePaymentOptions) API.
          */
        @Override
        public BuilderImpl code(String code) {
            this.code = code;
            return this;
        }

        @Override
        public boolean isCodeDefined() {
            return this.code != null;
        }

        /**
          * Set {@link PaymentOperatorOutgoing#getName} property.
          *
          * 
          */
        @Override
        public BuilderImpl name(String name) {
            this.name = name;
            return this;
        }

        @Override
        public boolean isNameDefined() {
            return this.name != null;
        }

        /**
          * Set {@link PaymentOperatorOutgoing#getCustomerTransactionFee} property.
          *
          * 
          */
        @Override
        public BuilderImpl customerTransactionFee(CustomerTransactionFee customerTransactionFee) {
            this.customerTransactionFee = customerTransactionFee;
            return this;
        }

        @Override
        public boolean isCustomerTransactionFeeDefined() {
            return this.customerTransactionFee != null;
        }

        /**
         * Create new instance of {@link PaymentOperatorOutgoing} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @Override
        public PaymentOperatorOutgoingImpl build() {
            return new PaymentOperatorOutgoingImpl(this);
        }

    }
}