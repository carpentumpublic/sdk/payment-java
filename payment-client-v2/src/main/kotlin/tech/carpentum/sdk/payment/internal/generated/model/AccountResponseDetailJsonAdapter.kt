//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.AccountResponseDetail

class AccountResponseDetailJsonAdapter {
    @FromJson
    fun fromJson(json: AccountResponseDetailJson): AccountResponseDetail {
        val builder = AccountResponseDetail.builder()
        builder.accountName(json.accountName)
        builder.accountNumber(json.accountNumber)
        builder.accountType(json.accountType)
        builder.bankCode(json.bankCode)
        builder.paymentOperator(json.paymentOperator)
        builder.bankName(json.bankName)
        builder.bankBranch(json.bankBranch)
        builder.bankCity(json.bankCity)
        builder.bankProvince(json.bankProvince)
        return builder.build()
    }

    @ToJson
    fun toJson(model: AccountResponseDetail): AccountResponseDetailJson {
        val json = AccountResponseDetailJson()
        json.accountName = model.accountName.orElse(null)
        json.accountNumber = model.accountNumber.orElse(null)
        json.accountType = model.accountType.orElse(null)
        json.bankCode = model.bankCode.orElse(null)
        json.paymentOperator = model.paymentOperator.orElse(null)
        json.bankName = model.bankName.orElse(null)
        json.bankBranch = model.bankBranch.orElse(null)
        json.bankCity = model.bankCity.orElse(null)
        json.bankProvince = model.bankProvince.orElse(null)
        return json
    }

    @FromJson
    fun fromJsonImpl(model: AccountResponseDetail): AccountResponseDetailImpl {
        return model as AccountResponseDetailImpl
    }

    @ToJson
    fun toJsonImpl(impl: AccountResponseDetailImpl): AccountResponseDetail {
        return impl
    }

}