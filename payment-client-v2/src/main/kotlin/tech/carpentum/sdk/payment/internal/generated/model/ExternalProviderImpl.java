//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** ExternalProvider
 *
 * TODO: Add description
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public class ExternalProviderImpl implements ExternalProvider {
    private final String code;

    @Override
    public String getCode() {
        return code;
    }


    private final String name;

    @Override
    public String getName() {
        return name;
    }




    private final int hashCode;
    private final String toString;

    private ExternalProviderImpl(BuilderImpl builder) {
        this.code = Objects.requireNonNull(builder.code, "Property 'code' is required.");
        this.name = Objects.requireNonNull(builder.name, "Property 'name' is required.");

        this.hashCode = Objects.hash(code, name);
        this.toString = builder.type + "(" +
                "code=" + code +
                ", name=" + name +
                ')';
    }

    @Override
    public int hashCode() {
        return hashCode;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }

        if (!(obj instanceof ExternalProviderImpl)) {
            return false;
        }

        ExternalProviderImpl that = (ExternalProviderImpl) obj;
        if (!Objects.equals(this.code, that.code)) return false;
        if (!Objects.equals(this.name, that.name)) return false;

        return true;
    }

    @Override
    public String toString() {
        return toString;
    }

    /** Builder for {@link ExternalProvider} model class. */
    public static class BuilderImpl implements ExternalProvider.Builder {
        private String code = null;
        private String name = null;

        private final String type;

        public BuilderImpl(String type) {
            this.type = type;
        }

        public BuilderImpl() {
            this("ExternalProvider");
        }

        /**
          * Set {@link ExternalProvider#getCode} property.
          *
          * 
          */
        @Override
        public BuilderImpl code(String code) {
            this.code = code;
            return this;
        }

        @Override
        public boolean isCodeDefined() {
            return this.code != null;
        }

        /**
          * Set {@link ExternalProvider#getName} property.
          *
          * 
          */
        @Override
        public BuilderImpl name(String name) {
            this.name = name;
            return this;
        }

        @Override
        public boolean isNameDefined() {
            return this.name != null;
        }

        /**
         * Create new instance of {@link ExternalProvider} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @Override
        public ExternalProviderImpl build() {
            return new ExternalProviderImpl(this);
        }

    }
}