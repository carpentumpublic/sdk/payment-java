//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.AccountCustomerResponseQrPh

class AccountCustomerResponseQrPhJsonAdapter {
    @FromJson
    fun fromJson(json: AccountCustomerResponseQrPhJson): AccountCustomerResponseQrPh {
        val builder = AccountCustomerResponseQrPh.builder()
        builder.accountName(json.accountName)
        builder.accountNumber(json.accountNumber)
        return builder.build()
    }

    @ToJson
    fun toJson(model: AccountCustomerResponseQrPh): AccountCustomerResponseQrPhJson {
        val json = AccountCustomerResponseQrPhJson()
        json.accountName = model.accountName.orElse(null)
        json.accountNumber = model.accountNumber.orElse(null)
        return json
    }

    @FromJson
    fun fromJsonImpl(model: AccountCustomerResponseQrPh): AccountCustomerResponseQrPhImpl {
        return model as AccountCustomerResponseQrPhImpl
    }

    @ToJson
    fun toJsonImpl(impl: AccountCustomerResponseQrPhImpl): AccountCustomerResponseQrPh {
        return impl
    }

}