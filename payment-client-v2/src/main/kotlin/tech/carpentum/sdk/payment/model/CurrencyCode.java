//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** CurrencyCode
 *
 * For supported currencies please refer to [`GET /currencies`](#operations-Payments-getCurrencies).
 *
 * 
 *
 * The model class is immutable.
 *
 * Use static {@link #of} method to create a new model class instance.
 */
public interface CurrencyCode {


    @NotNull String getValue();

    static CurrencyCode of(@NotNull String value) {
        return new CurrencyCodeImpl(value);
    }
}