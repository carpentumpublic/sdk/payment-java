plugins {
    id("org.openapi.generator") version "7.6.0"
}

openApiGenerate {
    generatorName.set("kotlin")
    inputSpec.set("$projectDir/src/openapi/openapi.yaml")
    outputDir.set("$rootDir/payment-client-v2")
    packageName.set("tech.carpentum.sdk.payment.internal.generated")
    apiPackage.set("tech.carpentum.sdk.payment.internal.generated.api")
    modelPackage.set("tech.carpentum.sdk.payment.model")
    generateApiDocumentation.set(false)
    generateApiTests.set(false)
    generateModelTests.set(false)
    additionalProperties.set(
        mapOf(
            "enumPropertyNaming" to "UPPERCASE",
            "moshiCodeGen" to "true",
            "sortModelPropertiesByRequiredFlag" to "false",
            "sortParamsByRequiredFlag" to "false",
        )
    )
}
