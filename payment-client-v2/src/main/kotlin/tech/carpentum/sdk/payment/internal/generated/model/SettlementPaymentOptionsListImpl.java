//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** SettlementPaymentOptionsList
 *
 * List of (possibly) available settlement payment options.
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public class SettlementPaymentOptionsListImpl implements SettlementPaymentOptionsList {
    private final java.util.List<@NotNull SettlementPaymentOption> data;

    @Override
    public java.util.List<@NotNull SettlementPaymentOption> getData() {
        return data;
    }




    private final int hashCode;
    private final String toString;

    private SettlementPaymentOptionsListImpl(BuilderImpl builder) {
        this.data = java.util.Collections.unmodifiableList(builder.data);

        this.hashCode = Objects.hash(data);
        this.toString = builder.type + "(" +
                "data=" + data +
                ')';
    }

    @Override
    public int hashCode() {
        return hashCode;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }

        if (!(obj instanceof SettlementPaymentOptionsListImpl)) {
            return false;
        }

        SettlementPaymentOptionsListImpl that = (SettlementPaymentOptionsListImpl) obj;
        if (!Objects.equals(this.data, that.data)) return false;

        return true;
    }

    @Override
    public String toString() {
        return toString;
    }

    /** Builder for {@link SettlementPaymentOptionsList} model class. */
    public static class BuilderImpl implements SettlementPaymentOptionsList.Builder {
        private java.util.List<@NotNull SettlementPaymentOption> data = new java.util.ArrayList<>();

        private final String type;

        public BuilderImpl(String type) {
            this.type = type;
        }

        public BuilderImpl() {
            this("SettlementPaymentOptionsList");
        }

        /**
          * Replace all items in {@link SettlementPaymentOptionsList#getData} list property.
          *
          * 
          */
        @Override
        public BuilderImpl data(java.util.List<@NotNull SettlementPaymentOption> data) {
            this.data.clear();
            if (data != null) {
                this.data.addAll(data);
            }
            return this;
        }
        /**
          * Add single item to {@link SettlementPaymentOptionsList#getData} list property.
          *
          * 
          */
        @Override
        public BuilderImpl dataAdd(SettlementPaymentOption item) {
            if (item != null) {
                this.data.add(item);
            }
            return this;
        }
        /**
          * Add all items to {@link SettlementPaymentOptionsList#getData} list property.
          *
          * 
          */
        @Override
        public BuilderImpl dataAddAll(java.util.List<@NotNull SettlementPaymentOption> data) {
            if (data != null) {
                this.data.addAll(data);
            }
            return this;
        }


        /**
         * Create new instance of {@link SettlementPaymentOptionsList} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @Override
        public SettlementPaymentOptionsListImpl build() {
            return new SettlementPaymentOptionsListImpl(this);
        }

    }
}