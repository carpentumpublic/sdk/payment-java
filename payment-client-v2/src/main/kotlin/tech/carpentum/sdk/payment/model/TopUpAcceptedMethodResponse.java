//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** TopUpMethodResponse
 *
 * For every payment method there is appropriate payment specific response object in `paymentMethodResponse` attribute.

Use data from `paymentMethodResponse` for payment completion (for example show to the customer).
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public interface TopUpAcceptedMethodResponse extends TopUpAcceptedResponse {

    @NotNull TopUpRequested getTopUpRequested();

    @NotNull Money getMoneyReceive();

    @NotNull PayinMethodResponse getPaymentMethodResponse();

    @NotNull static Builder builder(TopUpAcceptedMethodResponse copyOf) {
        Builder builder = builder();
        builder.topUpRequested(copyOf.getTopUpRequested());
        builder.moneyReceive(copyOf.getMoneyReceive());
        builder.paymentMethodResponse(copyOf.getPaymentMethodResponse());
        return builder;
    }

    @NotNull static Builder builder() {
        return new TopUpAcceptedMethodResponseImpl.BuilderImpl();
    }

    /** Builder for {@link TopUpAcceptedMethodResponse} model class. */
    interface Builder {

        /**
          * Set {@link TopUpAcceptedMethodResponse#getTopUpRequested} property.
          *
          * 
          */
        @NotNull Builder topUpRequested(TopUpRequested topUpRequested);

        boolean isTopUpRequestedDefined();


        /**
          * Set {@link TopUpAcceptedMethodResponse#getMoneyReceive} property.
          *
          * 
          */
        @NotNull Builder moneyReceive(Money moneyReceive);

        boolean isMoneyReceiveDefined();


        /**
          * Set {@link TopUpAcceptedMethodResponse#getPaymentMethodResponse} property.
          *
          * 
          */
        @NotNull Builder paymentMethodResponse(PayinMethodResponse paymentMethodResponse);

        boolean isPaymentMethodResponseDefined();


        /**
         * Create new instance of {@link TopUpAcceptedMethodResponse} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @NotNull TopUpAcceptedMethodResponse build();

    }
}