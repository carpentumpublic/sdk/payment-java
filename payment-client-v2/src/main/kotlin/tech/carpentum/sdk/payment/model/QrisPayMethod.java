//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** QRISPAY
 *
 * QRIS is payment method intended for the Indonesian market which allows users to pay using scanning QR codes by theirs Payment application.
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public interface QrisPayMethod extends PayinMethod {
    /** A discriminator value of property {@link #getPaymentMethodCode}. The model class extends {@link PayinMethod}. */
    PayinMethod.PaymentMethodCode PAYMENT_METHOD_CODE = PayinMethod.PaymentMethodCode.QRISPAY;

    @NotNull Optional<AccountPayinRequestQrisPay> getAccount();

    /** Your customer e-mail address in RFC 5322 format that is used for identification of the customer's payins. */
    @NotNull Optional<String> getEmailAddress();

    @NotNull static Builder builder(QrisPayMethod copyOf) {
        Builder builder = builder();
        builder.account(copyOf.getAccount().orElse(null));
        builder.emailAddress(copyOf.getEmailAddress().orElse(null));
        return builder;
    }

    @NotNull static Builder builder() {
        return new QrisPayMethodImpl.BuilderImpl();
    }

    /** Builder for {@link QrisPayMethod} model class. */
    interface Builder {

        /**
          * Set {@link QrisPayMethod#getAccount} property.
          *
          * 
          */
        @NotNull Builder account(AccountPayinRequestQrisPay account);

        boolean isAccountDefined();


        /**
          * Set {@link QrisPayMethod#getEmailAddress} property.
          *
          * Your customer e-mail address in RFC 5322 format that is used for identification of the customer's payins.
          */
        @NotNull Builder emailAddress(String emailAddress);

        boolean isEmailAddressDefined();


        /**
         * Create new instance of {@link QrisPayMethod} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @NotNull QrisPayMethod build();

    }
}