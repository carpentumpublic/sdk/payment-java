//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.BankTransferMethod

class BankTransferMethodJsonAdapter {
    @FromJson
    fun fromJson(json: BankTransferMethodJson): BankTransferMethod {
        val builder = BankTransferMethod.builder()
        builder.account(json.account)
        builder.paymentOperatorCode(json.paymentOperatorCode)
        builder.emailAddress(json.emailAddress)
        builder.remark(json.remark)
        return builder.build()
    }

    @ToJson
    fun toJson(model: BankTransferMethod): BankTransferMethodJson {
        val json = BankTransferMethodJson()
        json.account = model.account
        json.paymentOperatorCode = model.paymentOperatorCode
        json.emailAddress = model.emailAddress.orElse(null)
        json.remark = model.remark.orElse(null)
        json.paymentMethodCode = model.paymentMethodCode.name
        return json
    }

    @FromJson
    fun fromJsonImpl(model: BankTransferMethod): BankTransferMethodImpl {
        return model as BankTransferMethodImpl
    }

    @ToJson
    fun toJsonImpl(impl: BankTransferMethodImpl): BankTransferMethod {
        return impl
    }

}