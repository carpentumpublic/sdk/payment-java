//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.JsonClass;
import tech.carpentum.sdk.payment.model.*;

@JsonClass(generateAdapter = true)
class PaymentRequestedJson {
    var money: MoneyPaymentRequested? = null
    var segmentCode: SegmentCode? = null
    var exchangedToCurrency: CurrencyCode? = null
}