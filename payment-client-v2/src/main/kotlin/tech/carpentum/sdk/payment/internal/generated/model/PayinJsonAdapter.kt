//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.Payin

class PayinJsonAdapter {
    @FromJson
    fun fromJson(json: PayinJson): Payin {
        val builder = Payin.builder()
        builder.paymentRequested(json.paymentRequested)
        builder.paymentMethod(json.paymentMethod)
        builder.callbackUrl(json.callbackUrl)
        builder.returnUrl(json.returnUrl)
        builder.customerIp(json.customerIp)
        return builder.build()
    }

    @ToJson
    fun toJson(model: Payin): PayinJson {
        val json = PayinJson()
        json.paymentRequested = model.paymentRequested
        json.paymentMethod = model.paymentMethod
        json.callbackUrl = model.callbackUrl.orElse(null)
        json.returnUrl = model.returnUrl
        json.customerIp = model.customerIp.orElse(null)
        return json
    }

    @FromJson
    fun fromJsonImpl(model: Payin): PayinImpl {
        return model as PayinImpl
    }

    @ToJson
    fun toJsonImpl(impl: PayinImpl): Payin {
        return impl
    }

}