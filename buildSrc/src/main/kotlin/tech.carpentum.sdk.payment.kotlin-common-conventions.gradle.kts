plugins {
    // Apply the common convention plugin for shared build configuration between library and application projects.
    id("tech.carpentum.sdk.payment.java-common-conventions")

    id("org.jetbrains.kotlin.jvm")
}

dependencies {
    constraints {
        testImplementation("io.kotest:kotest-runner-junit5:5.9.0")
    }

    // Use the Kotlin JDK 8 standard library.
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")

    // Align versions of all Kotlin components
    implementation(platform("org.jetbrains.kotlin:kotlin-bom"))
}

kotlin {
    jvmToolchain {
        languageVersion.set(JavaLanguageVersion.of(17))
    }
}
