//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** AvailablePayinOptionVariantCurrency
 *
 * 
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public class AvailablePayinOptionVariantCurrencyImpl implements AvailablePayinOptionVariantCurrency {
    private final PayinMethodCode paymentMethodCode;

    @Override
    public PayinMethodCode getPaymentMethodCode() {
        return paymentMethodCode;
    }


    private final java.util.List<@NotNull PaymentOperatorIncoming> paymentOperators;

    @Override
    public java.util.List<@NotNull PaymentOperatorIncoming> getPaymentOperators() {
        return paymentOperators;
    }


    private final Optional<SegmentCode> segmentCode;

    @Override
    public Optional<SegmentCode> getSegmentCode() {
        return segmentCode;
    }


    private final Money money;

    @Override
    public Money getMoney() {
        return money;
    }




    private final int hashCode;
    private final String toString;

    private AvailablePayinOptionVariantCurrencyImpl(BuilderImpl builder) {
        this.paymentMethodCode = Objects.requireNonNull(builder.paymentMethodCode, "Property 'paymentMethodCode' is required.");
        this.paymentOperators = java.util.Collections.unmodifiableList(builder.paymentOperators);
        this.segmentCode = Optional.ofNullable(builder.segmentCode);
        this.money = Objects.requireNonNull(builder.money, "Property 'money' is required.");

        this.hashCode = Objects.hash(paymentMethodCode, paymentOperators, segmentCode, money);
        this.toString = builder.type + "(" +
                "paymentMethodCode=" + paymentMethodCode +
                ", paymentOperators=" + paymentOperators +
                ", segmentCode=" + segmentCode +
                ", money=" + money +
                ')';
    }

    @Override
    public int hashCode() {
        return hashCode;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }

        if (!(obj instanceof AvailablePayinOptionVariantCurrencyImpl)) {
            return false;
        }

        AvailablePayinOptionVariantCurrencyImpl that = (AvailablePayinOptionVariantCurrencyImpl) obj;
        if (!Objects.equals(this.paymentMethodCode, that.paymentMethodCode)) return false;
        if (!Objects.equals(this.paymentOperators, that.paymentOperators)) return false;
        if (!Objects.equals(this.segmentCode, that.segmentCode)) return false;
        if (!Objects.equals(this.money, that.money)) return false;

        return true;
    }

    @Override
    public String toString() {
        return toString;
    }

    /** Builder for {@link AvailablePayinOptionVariantCurrency} model class. */
    public static class BuilderImpl implements AvailablePayinOptionVariantCurrency.Builder {
        private PayinMethodCode paymentMethodCode = null;
        private java.util.List<@NotNull PaymentOperatorIncoming> paymentOperators = new java.util.ArrayList<>();
        private SegmentCode segmentCode = null;
        private Money money = null;

        private final String type;

        public BuilderImpl(String type) {
            this.type = type;
        }

        public BuilderImpl() {
            this("AvailablePayinOptionVariantCurrency");
        }

        /**
          * Set {@link AvailablePayinOptionVariantCurrency#getPaymentMethodCode} property.
          *
          * 
          */
        @Override
        public BuilderImpl paymentMethodCode(PayinMethodCode paymentMethodCode) {
            this.paymentMethodCode = paymentMethodCode;
            return this;
        }

        @Override
        public boolean isPaymentMethodCodeDefined() {
            return this.paymentMethodCode != null;
        }

        /**
          * Replace all items in {@link AvailablePayinOptionVariantCurrency#getPaymentOperators} list property.
          *
          * 
          */
        @Override
        public BuilderImpl paymentOperators(java.util.List<@NotNull PaymentOperatorIncoming> paymentOperators) {
            this.paymentOperators.clear();
            if (paymentOperators != null) {
                this.paymentOperators.addAll(paymentOperators);
            }
            return this;
        }
        /**
          * Add single item to {@link AvailablePayinOptionVariantCurrency#getPaymentOperators} list property.
          *
          * 
          */
        @Override
        public BuilderImpl paymentOperatorsAdd(PaymentOperatorIncoming item) {
            if (item != null) {
                this.paymentOperators.add(item);
            }
            return this;
        }
        /**
          * Add all items to {@link AvailablePayinOptionVariantCurrency#getPaymentOperators} list property.
          *
          * 
          */
        @Override
        public BuilderImpl paymentOperatorsAddAll(java.util.List<@NotNull PaymentOperatorIncoming> paymentOperators) {
            if (paymentOperators != null) {
                this.paymentOperators.addAll(paymentOperators);
            }
            return this;
        }


        /**
          * Set {@link AvailablePayinOptionVariantCurrency#getSegmentCode} property.
          *
          * 
          */
        @Override
        public BuilderImpl segmentCode(SegmentCode segmentCode) {
            this.segmentCode = segmentCode;
            return this;
        }

        @Override
        public boolean isSegmentCodeDefined() {
            return this.segmentCode != null;
        }

        /**
          * Set {@link AvailablePayinOptionVariantCurrency#getMoney} property.
          *
          * 
          */
        @Override
        public BuilderImpl money(Money money) {
            this.money = money;
            return this;
        }

        @Override
        public boolean isMoneyDefined() {
            return this.money != null;
        }

        /**
         * Create new instance of {@link AvailablePayinOptionVariantCurrency} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @Override
        public AvailablePayinOptionVariantCurrencyImpl build() {
            return new AvailablePayinOptionVariantCurrencyImpl(this);
        }

    }
}