//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.ExternalReference

class ExternalReferenceJsonAdapter {
    @FromJson
    fun fromJson(json: ExternalReferenceJson): ExternalReference {
        val builder = ExternalReference.builder()
        builder.reference(json.reference)
        return builder.build()
    }

    @ToJson
    fun toJson(model: ExternalReference): ExternalReferenceJson {
        val json = ExternalReferenceJson()
        json.reference = model.reference
        return json
    }

    @FromJson
    fun fromJsonImpl(model: ExternalReference): ExternalReferenceImpl {
        return model as ExternalReferenceImpl
    }

    @ToJson
    fun toJsonImpl(impl: ExternalReferenceImpl): ExternalReference {
        return impl
    }

}