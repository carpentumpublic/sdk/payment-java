//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** AccountPayoutRequestCryptoTransfer
 *
 * Parameters of a customer's crypto wallet information where your customer would like his funds to be transferred.
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public class AccountPayoutRequestCryptoTransferImpl implements AccountPayoutRequestCryptoTransfer {
    /** Address of a customer's crypto wallet where your customer would like his funds to be transferred. */
    private final String accountNumber;

    @Override
    public String getAccountNumber() {
        return accountNumber;
    }




    private final int hashCode;
    private final String toString;

    private AccountPayoutRequestCryptoTransferImpl(BuilderImpl builder) {
        this.accountNumber = Objects.requireNonNull(builder.accountNumber, "Property 'accountNumber' is required.");

        this.hashCode = Objects.hash(accountNumber);
        this.toString = builder.type + "(" +
                "accountNumber=" + accountNumber +
                ')';
    }

    @Override
    public int hashCode() {
        return hashCode;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }

        if (!(obj instanceof AccountPayoutRequestCryptoTransferImpl)) {
            return false;
        }

        AccountPayoutRequestCryptoTransferImpl that = (AccountPayoutRequestCryptoTransferImpl) obj;
        if (!Objects.equals(this.accountNumber, that.accountNumber)) return false;

        return true;
    }

    @Override
    public String toString() {
        return toString;
    }

    /** Builder for {@link AccountPayoutRequestCryptoTransfer} model class. */
    public static class BuilderImpl implements AccountPayoutRequestCryptoTransfer.Builder {
        private String accountNumber = null;

        private final String type;

        public BuilderImpl(String type) {
            this.type = type;
        }

        public BuilderImpl() {
            this("AccountPayoutRequestCryptoTransfer");
        }

        /**
          * Set {@link AccountPayoutRequestCryptoTransfer#getAccountNumber} property.
          *
          * Address of a customer's crypto wallet where your customer would like his funds to be transferred.
          */
        @Override
        public BuilderImpl accountNumber(String accountNumber) {
            this.accountNumber = accountNumber;
            return this;
        }

        @Override
        public boolean isAccountNumberDefined() {
            return this.accountNumber != null;
        }

        /**
         * Create new instance of {@link AccountPayoutRequestCryptoTransfer} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @Override
        public AccountPayoutRequestCryptoTransferImpl build() {
            return new AccountPayoutRequestCryptoTransferImpl(this);
        }

    }
}