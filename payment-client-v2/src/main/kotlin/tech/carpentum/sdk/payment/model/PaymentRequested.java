//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** PaymentRequested
 *
 * The requested payment.
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public interface PaymentRequested {

    @NotNull MoneyPaymentRequested getMoney();

    @NotNull Optional<SegmentCode> getSegmentCode();

    @NotNull Optional<CurrencyCode> getExchangedToCurrency();

    @NotNull static Builder builder(PaymentRequested copyOf) {
        Builder builder = builder();
        builder.money(copyOf.getMoney());
        builder.segmentCode(copyOf.getSegmentCode().orElse(null));
        builder.exchangedToCurrency(copyOf.getExchangedToCurrency().orElse(null));
        return builder;
    }

    @NotNull static Builder builder() {
        return new PaymentRequestedImpl.BuilderImpl();
    }

    /** Builder for {@link PaymentRequested} model class. */
    interface Builder {

        /**
          * Set {@link PaymentRequested#getMoney} property.
          *
          * 
          */
        @NotNull Builder money(MoneyPaymentRequested money);

        boolean isMoneyDefined();


        /**
          * Set {@link PaymentRequested#getSegmentCode} property.
          *
          * 
          */
        @NotNull Builder segmentCode(SegmentCode segmentCode);

        boolean isSegmentCodeDefined();


        /**
          * Set {@link PaymentRequested#getExchangedToCurrency} property.
          *
          * 
          */
        @NotNull Builder exchangedToCurrency(CurrencyCode exchangedToCurrency);

        boolean isExchangedToCurrencyDefined();


        /**
         * Create new instance of {@link PaymentRequested} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @NotNull PaymentRequested build();

    }
}