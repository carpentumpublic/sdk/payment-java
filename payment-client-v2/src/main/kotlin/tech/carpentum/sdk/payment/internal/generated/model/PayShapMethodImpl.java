//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** PAYSHAP
 *
 * PayShap offers a quick, easy, and secure way to send money without having to input banking login details.

Customers can make payments using just a phone number or account number, with transactions processed instantly. 
It is a convenient solution for everyday payments, designed to simplify and speed up the payment experience.
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public class PayShapMethodImpl implements PayShapMethod {
    private final Optional<AccountPayinRequestPayShap> account;

    @Override
    public Optional<AccountPayinRequestPayShap> getAccount() {
        return account;
    }


    /** Your customer mobile phone number. */
    private final Optional<String> phoneNumber;

    @Override
    public Optional<String> getPhoneNumber() {
        return phoneNumber;
    }


    /** One of following can serve as Payment Operator:

 * Financial or other institution (such as bank, card payment processor, ...) that manages transactions for your customers
 * Mobile wallet
 * Blockchain protocol for crypto currency payments

Customer is informed with the payment instructions where funds have to be transferred efficiently based on the selected Payment operator.
For getting list of the available payment options for payins use [POST /payins/!availablePaymentOptions](#operations-Incoming_payments-availablePaymentOptions) API, for payouts use [POST /payouts/!availablePaymentOptions](#operations-Outgoing_payments-availablePaymentOptions) API. */
    private final String paymentOperatorCode;

    @Override
    public String getPaymentOperatorCode() {
        return paymentOperatorCode;
    }


    private final Optional<String> externalProviderCode;

    @Override
    public Optional<String> getExternalProviderCode() {
        return externalProviderCode;
    }


    @Override public PaymentMethodCode getPaymentMethodCode() { return PAYMENT_METHOD_CODE; }

    private final int hashCode;
    private final String toString;

    private PayShapMethodImpl(BuilderImpl builder) {
        this.account = Optional.ofNullable(builder.account);
        this.phoneNumber = Optional.ofNullable(builder.phoneNumber);
        this.paymentOperatorCode = Objects.requireNonNull(builder.paymentOperatorCode, "Property 'paymentOperatorCode' is required.");
        this.externalProviderCode = Optional.ofNullable(builder.externalProviderCode);

        this.hashCode = Objects.hash(account, phoneNumber, paymentOperatorCode, externalProviderCode);
        this.toString = builder.type + "(" +
                "account=" + account +
                ", phoneNumber=" + phoneNumber +
                ", paymentOperatorCode=" + paymentOperatorCode +
                ", externalProviderCode=" + externalProviderCode +
                ')';
    }

    @Override
    public int hashCode() {
        return hashCode;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }

        if (!(obj instanceof PayShapMethodImpl)) {
            return false;
        }

        PayShapMethodImpl that = (PayShapMethodImpl) obj;
        if (!Objects.equals(this.account, that.account)) return false;
        if (!Objects.equals(this.phoneNumber, that.phoneNumber)) return false;
        if (!Objects.equals(this.paymentOperatorCode, that.paymentOperatorCode)) return false;
        if (!Objects.equals(this.externalProviderCode, that.externalProviderCode)) return false;

        return true;
    }

    @Override
    public String toString() {
        return toString;
    }

    /** Builder for {@link PayShapMethod} model class. */
    public static class BuilderImpl implements PayShapMethod.Builder {
        private AccountPayinRequestPayShap account = null;
        private String phoneNumber = null;
        private String paymentOperatorCode = null;
        private String externalProviderCode = null;

        private final String type;

        public BuilderImpl(String type) {
            this.type = type;
        }

        public BuilderImpl() {
            this("PayShapMethod");
        }

        /**
          * Set {@link PayShapMethod#getAccount} property.
          *
          * 
          */
        @Override
        public BuilderImpl account(AccountPayinRequestPayShap account) {
            this.account = account;
            return this;
        }

        @Override
        public boolean isAccountDefined() {
            return this.account != null;
        }

        /**
          * Set {@link PayShapMethod#getPhoneNumber} property.
          *
          * Your customer mobile phone number.
          */
        @Override
        public BuilderImpl phoneNumber(String phoneNumber) {
            this.phoneNumber = phoneNumber;
            return this;
        }

        @Override
        public boolean isPhoneNumberDefined() {
            return this.phoneNumber != null;
        }

        /**
          * Set {@link PayShapMethod#getPaymentOperatorCode} property.
          *
          * One of following can serve as Payment Operator:

 * Financial or other institution (such as bank, card payment processor, ...) that manages transactions for your customers
 * Mobile wallet
 * Blockchain protocol for crypto currency payments

Customer is informed with the payment instructions where funds have to be transferred efficiently based on the selected Payment operator.
For getting list of the available payment options for payins use [POST /payins/!availablePaymentOptions](#operations-Incoming_payments-availablePaymentOptions) API, for payouts use [POST /payouts/!availablePaymentOptions](#operations-Outgoing_payments-availablePaymentOptions) API.
          */
        @Override
        public BuilderImpl paymentOperatorCode(String paymentOperatorCode) {
            this.paymentOperatorCode = paymentOperatorCode;
            return this;
        }

        @Override
        public boolean isPaymentOperatorCodeDefined() {
            return this.paymentOperatorCode != null;
        }

        /**
          * Set {@link PayShapMethod#getExternalProviderCode} property.
          *
          * 
          */
        @Override
        public BuilderImpl externalProviderCode(String externalProviderCode) {
            this.externalProviderCode = externalProviderCode;
            return this;
        }

        @Override
        public boolean isExternalProviderCodeDefined() {
            return this.externalProviderCode != null;
        }

        /**
         * Create new instance of {@link PayShapMethod} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @Override
        public PayShapMethodImpl build() {
            return new PayShapMethodImpl(this);
        }

    }
}