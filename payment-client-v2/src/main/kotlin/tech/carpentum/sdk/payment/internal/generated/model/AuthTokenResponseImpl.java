//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** AuthTokenResponse
 *
 * Auth token for required operations. The Auth token payload can be decoded at [JWT official webiste](https://jwt.io/).
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public class AuthTokenResponseImpl implements AuthTokenResponse {
    /** The JWT, its content can be [decoded at jwt.io](https://jwt.io/). See also [token content explanation](https://jwt.io/introduction).

Example content

```
{
  "operations": [
    "POST /payins/123"
  ],
  "exp": 1619518389,
  "jti": "53ee5f14-5ca5-4034-815e-815de85b96c5",
  "iat": 1619514789,
  "sub": "YOUR_MERCHANT_ID"
}
```

The token contains the following data
 - operations - allowed endpoints, see [Fine-grained privileges](general.html#fine-grained-privileges) for details.
 - exp - expiration time as number of seconds from 1970-01-01T00:00:00Z UTC
 - jti - JWT id
 - iat - issued at, time when the token was created as number of seconds from 1970-01-01T00:00:00Z UTC
 - sub - subject, the [merchant code](terminology.html#term-Merchant-code) */
    private final String token;

    @Override
    public String getToken() {
        return token;
    }




    private final int hashCode;
    private final String toString;

    private AuthTokenResponseImpl(BuilderImpl builder) {
        this.token = Objects.requireNonNull(builder.token, "Property 'token' is required.");

        this.hashCode = Objects.hash(token);
        this.toString = builder.type + "(" +
                "token=" + token +
                ')';
    }

    @Override
    public int hashCode() {
        return hashCode;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }

        if (!(obj instanceof AuthTokenResponseImpl)) {
            return false;
        }

        AuthTokenResponseImpl that = (AuthTokenResponseImpl) obj;
        if (!Objects.equals(this.token, that.token)) return false;

        return true;
    }

    @Override
    public String toString() {
        return toString;
    }

    /** Builder for {@link AuthTokenResponse} model class. */
    public static class BuilderImpl implements AuthTokenResponse.Builder {
        private String token = null;

        private final String type;

        public BuilderImpl(String type) {
            this.type = type;
        }

        public BuilderImpl() {
            this("AuthTokenResponse");
        }

        /**
          * Set {@link AuthTokenResponse#getToken} property.
          *
          * The JWT, its content can be [decoded at jwt.io](https://jwt.io/). See also [token content explanation](https://jwt.io/introduction).

Example content

```
{
  "operations": [
    "POST /payins/123"
  ],
  "exp": 1619518389,
  "jti": "53ee5f14-5ca5-4034-815e-815de85b96c5",
  "iat": 1619514789,
  "sub": "YOUR_MERCHANT_ID"
}
```

The token contains the following data
 - operations - allowed endpoints, see [Fine-grained privileges](general.html#fine-grained-privileges) for details.
 - exp - expiration time as number of seconds from 1970-01-01T00:00:00Z UTC
 - jti - JWT id
 - iat - issued at, time when the token was created as number of seconds from 1970-01-01T00:00:00Z UTC
 - sub - subject, the [merchant code](terminology.html#term-Merchant-code)
          */
        @Override
        public BuilderImpl token(String token) {
            this.token = token;
            return this;
        }

        @Override
        public boolean isTokenDefined() {
            return this.token != null;
        }

        /**
         * Create new instance of {@link AuthTokenResponse} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @Override
        public AuthTokenResponseImpl build() {
            return new AuthTokenResponseImpl(this);
        }

    }
}