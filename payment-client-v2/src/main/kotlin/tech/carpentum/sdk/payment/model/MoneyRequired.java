//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** MoneyRequired
 *
 * 
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public interface MoneyRequired {

    /** If returned, this field contains the actual amount that the end customer should pay. The amount provided in `paymentMethodResponse.money.amount` will be credited to your account.

Amount is accepted in the smallest currency unit. For fiat currencies the smallest currency unit are based on ISO 4217 (e.g. for USD two decimal places are available so amount can be accepted as 12.34). The exceptions are IDR and INR currencies: zero amount in decimal places are allowed for them (e.g. 42.05 is not allowed, while 42 or 42.00 or 43 or 43.00 are allowed).

If more decimal places than supported is provided request will fail on HTTP 406 error (e.g. for USD the amount 12.345 will not be accepted). */
    @NotNull java.math.BigDecimal getAmount();

    @NotNull CurrencyCode getCurrencyCode();

    @NotNull static Builder builder(MoneyRequired copyOf) {
        Builder builder = builder();
        builder.amount(copyOf.getAmount());
        builder.currencyCode(copyOf.getCurrencyCode());
        return builder;
    }

    @NotNull static Builder builder() {
        return new MoneyRequiredImpl.BuilderImpl();
    }

    /** Builder for {@link MoneyRequired} model class. */
    interface Builder {

        /**
          * Set {@link MoneyRequired#getAmount} property.
          *
          * If returned, this field contains the actual amount that the end customer should pay. The amount provided in `paymentMethodResponse.money.amount` will be credited to your account.

Amount is accepted in the smallest currency unit. For fiat currencies the smallest currency unit are based on ISO 4217 (e.g. for USD two decimal places are available so amount can be accepted as 12.34). The exceptions are IDR and INR currencies: zero amount in decimal places are allowed for them (e.g. 42.05 is not allowed, while 42 or 42.00 or 43 or 43.00 are allowed).

If more decimal places than supported is provided request will fail on HTTP 406 error (e.g. for USD the amount 12.345 will not be accepted).
          */
        @NotNull Builder amount(java.math.BigDecimal amount);

        boolean isAmountDefined();


        /**
          * Set {@link MoneyRequired#getCurrencyCode} property.
          *
          * 
          */
        @NotNull Builder currencyCode(CurrencyCode currencyCode);

        boolean isCurrencyCodeDefined();


        /**
         * Create new instance of {@link MoneyRequired} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @NotNull MoneyRequired build();

    }
}