//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** IntervalNumberTo
 *
 * 
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public interface IntervalNumberTo {

    @NotNull Optional<java.math.BigDecimal> getFrom();

    @NotNull Optional<java.math.BigDecimal> getTo();

    @NotNull static Builder builder(IntervalNumberTo copyOf) {
        Builder builder = builder();
        builder.from(copyOf.getFrom().orElse(null));
        builder.to(copyOf.getTo().orElse(null));
        return builder;
    }

    @NotNull static Builder builder() {
        return new IntervalNumberToImpl.BuilderImpl();
    }

    /** Builder for {@link IntervalNumberTo} model class. */
    interface Builder {

        /**
          * Set {@link IntervalNumberTo#getFrom} property.
          *
          * 
          */
        @NotNull Builder from(java.math.BigDecimal from);

        boolean isFromDefined();


        /**
          * Set {@link IntervalNumberTo#getTo} property.
          *
          * 
          */
        @NotNull Builder to(java.math.BigDecimal to);

        boolean isToDefined();


        /**
         * Create new instance of {@link IntervalNumberTo} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @NotNull IntervalNumberTo build();

    }
}