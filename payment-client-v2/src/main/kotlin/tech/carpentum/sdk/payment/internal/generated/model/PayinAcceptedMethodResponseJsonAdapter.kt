//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.PayinAcceptedMethodResponse

class PayinAcceptedMethodResponseJsonAdapter {
    @FromJson
    fun fromJson(json: PayinAcceptedMethodResponseJson): PayinAcceptedMethodResponse {
        val builder = PayinAcceptedMethodResponse.builder()
        builder.paymentRequested(json.paymentRequested)
        builder.paymentMethodResponse(json.paymentMethodResponse)
        return builder.build()
    }

    @ToJson
    fun toJson(model: PayinAcceptedMethodResponse): PayinAcceptedMethodResponseJson {
        val json = PayinAcceptedMethodResponseJson()
        json.paymentRequested = model.paymentRequested
        json.paymentMethodResponse = model.paymentMethodResponse
        return json
    }

    @FromJson
    fun fromJsonImpl(model: PayinAcceptedMethodResponse): PayinAcceptedMethodResponseImpl {
        return model as PayinAcceptedMethodResponseImpl
    }

    @ToJson
    fun toJsonImpl(impl: PayinAcceptedMethodResponseImpl): PayinAcceptedMethodResponse {
        return impl
    }

}