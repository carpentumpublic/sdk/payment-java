package tech.carpentum.sdk.payment

/**
 * Definition of simple endpoint.
 *
 * Used by individual API classes:
 *
 * - [AccountsApi]
 * - [IncomingPaymentsApi]
 * - [MerchantInfoApi]
 * - [OutgoingPaymentsApi]
 * - [PaymentsApi]
 * - [RateListsApi]
 * - [SettlementsApi]
 *
 * See also [EndpointWithIdDefinition].
 */
data class EndpointDefinition(
    val method: Method,
    val resource: String
) {

    /**
     * Supported HTTP method of RESTful API used by [EndpointDefinition].
     */
    enum class Method {
        GET, POST
    }

}
