//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** AccountResponseCodePayment
 *
 * Parameters of a Convenience Store Payment that your customer use to send funds to make a payment. These parameters has to be provided to your customer in form of an payment instructions.
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public interface AccountResponseCodePayment {

    /** Payment Code that we expect your customer to submit into kiosk in convenience store during the payment.
This parameter is to be shown to your customer in the payment instructions. */
    @NotNull String getAccountNumber();

    @NotNull static AccountResponseCodePayment ofAccountNumber(String accountNumber) { return builder().accountNumber(accountNumber).build(); }

    @NotNull static Builder builder() {
        return new AccountResponseCodePaymentImpl.BuilderImpl();
    }

    /** Builder for {@link AccountResponseCodePayment} model class. */
    interface Builder {

        /**
          * Set {@link AccountResponseCodePayment#getAccountNumber} property.
          *
          * Payment Code that we expect your customer to submit into kiosk in convenience store during the payment.
This parameter is to be shown to your customer in the payment instructions.
          */
        @NotNull Builder accountNumber(String accountNumber);

        boolean isAccountNumberDefined();


        /**
         * Create new instance of {@link AccountResponseCodePayment} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @NotNull AccountResponseCodePayment build();

    }
}