//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** CRYPTO_OFFLINE
 *
 * Crypto Offline is a payment method that allows customers to pay by crypto currency from they crypto wallet to an address that they are provided.
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public class CryptoOfflineMethodImpl implements CryptoOfflineMethod {
    /** One of following can serve as Payment Operator:

 * Financial or other institution (such as bank, card payment processor, ...) that manages transactions for your customers
 * Mobile wallet
 * Blockchain protocol for crypto currency payments

Customer is informed with the payment instructions where funds have to be transferred efficiently based on the selected Payment operator.
For getting list of the available payment options for payins use [POST /payins/!availablePaymentOptions](#operations-Incoming_payments-availablePaymentOptions) API, for payouts use [POST /payouts/!availablePaymentOptions](#operations-Outgoing_payments-availablePaymentOptions) API. */
    private final String paymentOperatorCode;

    @Override
    public String getPaymentOperatorCode() {
        return paymentOperatorCode;
    }


    /** Your customer e-mail address in RFC 5322 format that is used for identification of the customer's payins. */
    private final Optional<String> emailAddress;

    @Override
    public Optional<String> getEmailAddress() {
        return emailAddress;
    }


    @Override public PaymentMethodCode getPaymentMethodCode() { return PAYMENT_METHOD_CODE; }

    private final int hashCode;
    private final String toString;

    private CryptoOfflineMethodImpl(BuilderImpl builder) {
        this.paymentOperatorCode = Objects.requireNonNull(builder.paymentOperatorCode, "Property 'paymentOperatorCode' is required.");
        this.emailAddress = Optional.ofNullable(builder.emailAddress);

        this.hashCode = Objects.hash(paymentOperatorCode, emailAddress);
        this.toString = builder.type + "(" +
                "paymentOperatorCode=" + paymentOperatorCode +
                ", emailAddress=" + emailAddress +
                ')';
    }

    @Override
    public int hashCode() {
        return hashCode;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }

        if (!(obj instanceof CryptoOfflineMethodImpl)) {
            return false;
        }

        CryptoOfflineMethodImpl that = (CryptoOfflineMethodImpl) obj;
        if (!Objects.equals(this.paymentOperatorCode, that.paymentOperatorCode)) return false;
        if (!Objects.equals(this.emailAddress, that.emailAddress)) return false;

        return true;
    }

    @Override
    public String toString() {
        return toString;
    }

    /** Builder for {@link CryptoOfflineMethod} model class. */
    public static class BuilderImpl implements CryptoOfflineMethod.Builder {
        private String paymentOperatorCode = null;
        private String emailAddress = null;

        private final String type;

        public BuilderImpl(String type) {
            this.type = type;
        }

        public BuilderImpl() {
            this("CryptoOfflineMethod");
        }

        /**
          * Set {@link CryptoOfflineMethod#getPaymentOperatorCode} property.
          *
          * One of following can serve as Payment Operator:

 * Financial or other institution (such as bank, card payment processor, ...) that manages transactions for your customers
 * Mobile wallet
 * Blockchain protocol for crypto currency payments

Customer is informed with the payment instructions where funds have to be transferred efficiently based on the selected Payment operator.
For getting list of the available payment options for payins use [POST /payins/!availablePaymentOptions](#operations-Incoming_payments-availablePaymentOptions) API, for payouts use [POST /payouts/!availablePaymentOptions](#operations-Outgoing_payments-availablePaymentOptions) API.
          */
        @Override
        public BuilderImpl paymentOperatorCode(String paymentOperatorCode) {
            this.paymentOperatorCode = paymentOperatorCode;
            return this;
        }

        @Override
        public boolean isPaymentOperatorCodeDefined() {
            return this.paymentOperatorCode != null;
        }

        /**
          * Set {@link CryptoOfflineMethod#getEmailAddress} property.
          *
          * Your customer e-mail address in RFC 5322 format that is used for identification of the customer's payins.
          */
        @Override
        public BuilderImpl emailAddress(String emailAddress) {
            this.emailAddress = emailAddress;
            return this;
        }

        @Override
        public boolean isEmailAddressDefined() {
            return this.emailAddress != null;
        }

        /**
         * Create new instance of {@link CryptoOfflineMethod} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @Override
        public CryptoOfflineMethodImpl build() {
            return new CryptoOfflineMethodImpl(this);
        }

    }
}