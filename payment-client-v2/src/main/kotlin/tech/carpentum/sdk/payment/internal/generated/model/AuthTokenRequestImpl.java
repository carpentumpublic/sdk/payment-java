//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** AuthTokenRequest
 *
 * 
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public class AuthTokenRequestImpl implements AuthTokenRequest {
    /** Your Merchant ID. This Merchant ID is obtained upon [Merchant enrollment process](sandbox.html#sandbox-account-registration). */
    private final String merchantCode;

    @Override
    public String getMerchantCode() {
        return merchantCode;
    }


    /** See [Merchant Secret Key](terminology.html#term-Merchant-secret). */
    private final String secret;

    @Override
    public String getSecret() {
        return secret;
    }


    /** Validity of the token in seconds. Maximum value 3600, default value 1800 (30 minutes). */
    private final Optional<Integer> validitySecs;

    @Override
    public Optional<Integer> getValiditySecs() {
        return validitySecs;
    }


    /** Endpoints that can be called with the issued token. See [Fine-grained privileges](general.html#fine-grained-privileges) description. */
    private final java.util.List<@NotNull String> operations;

    @Override
    public java.util.List<@NotNull String> getOperations() {
        return operations;
    }


    private final Optional<Money> money;

    @Override
    public Optional<Money> getMoney() {
        return money;
    }


    private final Optional<Money> moneyProvided;

    @Override
    public Optional<Money> getMoneyProvided() {
        return moneyProvided;
    }


    private final Optional<CurrencyCode> currencyCodeRequired;

    @Override
    public Optional<CurrencyCode> getCurrencyCodeRequired() {
        return currencyCodeRequired;
    }


    private final Optional<Money> moneyRequired;

    @Override
    public Optional<Money> getMoneyRequired() {
        return moneyRequired;
    }


    private final Optional<CurrencyCode> currencyCodeProvided;

    @Override
    public Optional<CurrencyCode> getCurrencyCodeProvided() {
        return currencyCodeProvided;
    }


    private final Optional<SettlementMethod> settlementMethod;

    @Override
    public Optional<SettlementMethod> getSettlementMethod() {
        return settlementMethod;
    }




    private final int hashCode;
    private final String toString;

    private AuthTokenRequestImpl(BuilderImpl builder) {
        this.merchantCode = Objects.requireNonNull(builder.merchantCode, "Property 'merchantCode' is required.");
        this.secret = Objects.requireNonNull(builder.secret, "Property 'secret' is required.");
        this.validitySecs = Optional.ofNullable(builder.validitySecs);
        this.operations = java.util.Collections.unmodifiableList(builder.operations);
        this.money = Optional.ofNullable(builder.money);
        this.moneyProvided = Optional.ofNullable(builder.moneyProvided);
        this.currencyCodeRequired = Optional.ofNullable(builder.currencyCodeRequired);
        this.moneyRequired = Optional.ofNullable(builder.moneyRequired);
        this.currencyCodeProvided = Optional.ofNullable(builder.currencyCodeProvided);
        this.settlementMethod = Optional.ofNullable(builder.settlementMethod);

        this.hashCode = Objects.hash(merchantCode, secret, validitySecs, operations, money, moneyProvided, currencyCodeRequired, moneyRequired, currencyCodeProvided, settlementMethod);
        this.toString = builder.type + "(" +
                "merchantCode=" + merchantCode +
                ", secret=" + secret +
                ", validitySecs=" + validitySecs +
                ", operations=" + operations +
                ", money=" + money +
                ", moneyProvided=" + moneyProvided +
                ", currencyCodeRequired=" + currencyCodeRequired +
                ", moneyRequired=" + moneyRequired +
                ", currencyCodeProvided=" + currencyCodeProvided +
                ", settlementMethod=" + settlementMethod +
                ')';
    }

    @Override
    public int hashCode() {
        return hashCode;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }

        if (!(obj instanceof AuthTokenRequestImpl)) {
            return false;
        }

        AuthTokenRequestImpl that = (AuthTokenRequestImpl) obj;
        if (!Objects.equals(this.merchantCode, that.merchantCode)) return false;
        if (!Objects.equals(this.secret, that.secret)) return false;
        if (!Objects.equals(this.validitySecs, that.validitySecs)) return false;
        if (!Objects.equals(this.operations, that.operations)) return false;
        if (!Objects.equals(this.money, that.money)) return false;
        if (!Objects.equals(this.moneyProvided, that.moneyProvided)) return false;
        if (!Objects.equals(this.currencyCodeRequired, that.currencyCodeRequired)) return false;
        if (!Objects.equals(this.moneyRequired, that.moneyRequired)) return false;
        if (!Objects.equals(this.currencyCodeProvided, that.currencyCodeProvided)) return false;
        if (!Objects.equals(this.settlementMethod, that.settlementMethod)) return false;

        return true;
    }

    @Override
    public String toString() {
        return toString;
    }

    /** Builder for {@link AuthTokenRequest} model class. */
    public static class BuilderImpl implements AuthTokenRequest.Builder {
        private String merchantCode = null;
        private String secret = null;
        private Integer validitySecs = null;
        private java.util.List<@NotNull String> operations = new java.util.ArrayList<>();
        private Money money = null;
        private Money moneyProvided = null;
        private CurrencyCode currencyCodeRequired = null;
        private Money moneyRequired = null;
        private CurrencyCode currencyCodeProvided = null;
        private SettlementMethod settlementMethod = null;

        private final String type;

        public BuilderImpl(String type) {
            this.type = type;
        }

        public BuilderImpl() {
            this("AuthTokenRequest");
        }

        /**
          * Set {@link AuthTokenRequest#getMerchantCode} property.
          *
          * Your Merchant ID. This Merchant ID is obtained upon [Merchant enrollment process](sandbox.html#sandbox-account-registration).
          */
        @Override
        public BuilderImpl merchantCode(String merchantCode) {
            this.merchantCode = merchantCode;
            return this;
        }

        @Override
        public boolean isMerchantCodeDefined() {
            return this.merchantCode != null;
        }

        /**
          * Set {@link AuthTokenRequest#getSecret} property.
          *
          * See [Merchant Secret Key](terminology.html#term-Merchant-secret).
          */
        @Override
        public BuilderImpl secret(String secret) {
            this.secret = secret;
            return this;
        }

        @Override
        public boolean isSecretDefined() {
            return this.secret != null;
        }

        /**
          * Set {@link AuthTokenRequest#getValiditySecs} property.
          *
          * Validity of the token in seconds. Maximum value 3600, default value 1800 (30 minutes).
          */
        @Override
        public BuilderImpl validitySecs(Integer validitySecs) {
            this.validitySecs = validitySecs;
            return this;
        }

        @Override
        public boolean isValiditySecsDefined() {
            return this.validitySecs != null;
        }

        /**
          * Replace all items in {@link AuthTokenRequest#getOperations} list property.
          *
          * Endpoints that can be called with the issued token. See [Fine-grained privileges](general.html#fine-grained-privileges) description.
          */
        @Override
        public BuilderImpl operations(java.util.List<@NotNull String> operations) {
            this.operations.clear();
            if (operations != null) {
                this.operations.addAll(operations);
            }
            return this;
        }
        /**
          * Add single item to {@link AuthTokenRequest#getOperations} list property.
          *
          * Endpoints that can be called with the issued token. See [Fine-grained privileges](general.html#fine-grained-privileges) description.
          */
        @Override
        public BuilderImpl operationsAdd(String item) {
            if (item != null) {
                this.operations.add(item);
            }
            return this;
        }
        /**
          * Add all items to {@link AuthTokenRequest#getOperations} list property.
          *
          * Endpoints that can be called with the issued token. See [Fine-grained privileges](general.html#fine-grained-privileges) description.
          */
        @Override
        public BuilderImpl operationsAddAll(java.util.List<@NotNull String> operations) {
            if (operations != null) {
                this.operations.addAll(operations);
            }
            return this;
        }


        /**
          * Set {@link AuthTokenRequest#getMoney} property.
          *
          * 
          */
        @Override
        public BuilderImpl money(Money money) {
            this.money = money;
            return this;
        }

        @Override
        public boolean isMoneyDefined() {
            return this.money != null;
        }

        /**
          * Set {@link AuthTokenRequest#getMoneyProvided} property.
          *
          * 
          */
        @Override
        public BuilderImpl moneyProvided(Money moneyProvided) {
            this.moneyProvided = moneyProvided;
            return this;
        }

        @Override
        public boolean isMoneyProvidedDefined() {
            return this.moneyProvided != null;
        }

        /**
          * Set {@link AuthTokenRequest#getCurrencyCodeRequired} property.
          *
          * 
          */
        @Override
        public BuilderImpl currencyCodeRequired(CurrencyCode currencyCodeRequired) {
            this.currencyCodeRequired = currencyCodeRequired;
            return this;
        }

        @Override
        public boolean isCurrencyCodeRequiredDefined() {
            return this.currencyCodeRequired != null;
        }

        /**
          * Set {@link AuthTokenRequest#getMoneyRequired} property.
          *
          * 
          */
        @Override
        public BuilderImpl moneyRequired(Money moneyRequired) {
            this.moneyRequired = moneyRequired;
            return this;
        }

        @Override
        public boolean isMoneyRequiredDefined() {
            return this.moneyRequired != null;
        }

        /**
          * Set {@link AuthTokenRequest#getCurrencyCodeProvided} property.
          *
          * 
          */
        @Override
        public BuilderImpl currencyCodeProvided(CurrencyCode currencyCodeProvided) {
            this.currencyCodeProvided = currencyCodeProvided;
            return this;
        }

        @Override
        public boolean isCurrencyCodeProvidedDefined() {
            return this.currencyCodeProvided != null;
        }

        /**
          * Set {@link AuthTokenRequest#getSettlementMethod} property.
          *
          * 
          */
        @Override
        public BuilderImpl settlementMethod(SettlementMethod settlementMethod) {
            this.settlementMethod = settlementMethod;
            return this;
        }

        @Override
        public boolean isSettlementMethodDefined() {
            return this.settlementMethod != null;
        }

        /**
         * Create new instance of {@link AuthTokenRequest} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @Override
        public AuthTokenRequestImpl build() {
            return new AuthTokenRequestImpl(this);
        }

    }
}