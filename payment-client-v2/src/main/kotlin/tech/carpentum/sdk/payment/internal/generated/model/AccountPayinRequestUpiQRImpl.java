//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** AccountPayinRequestUpiQR
 *
 * Parameters of your customer's bank or wallet account which your customer sends funds from. These account parameters are used for the sender's account verification in processing of the payment.
Which parameters are mandatory depends on the payment method and the currency your customer choose to pay.
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public class AccountPayinRequestUpiQRImpl implements AccountPayinRequestUpiQR {
    /** Account Name is the name of the person who holds the bank or wallet account which your customer sends funds from to make his payment.
The name should be in the same format as the account holder name of the account. Allows numbers, some special characters and UNICODE symbols, see validation pattern. */
    private final Optional<String> accountName;

    @Override
    public Optional<String> getAccountName() {
        return accountName;
    }




    private final int hashCode;
    private final String toString;

    private AccountPayinRequestUpiQRImpl(BuilderImpl builder) {
        this.accountName = Optional.ofNullable(builder.accountName);

        this.hashCode = Objects.hash(accountName);
        this.toString = builder.type + "(" +
                "accountName=" + accountName +
                ')';
    }

    @Override
    public int hashCode() {
        return hashCode;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }

        if (!(obj instanceof AccountPayinRequestUpiQRImpl)) {
            return false;
        }

        AccountPayinRequestUpiQRImpl that = (AccountPayinRequestUpiQRImpl) obj;
        if (!Objects.equals(this.accountName, that.accountName)) return false;

        return true;
    }

    @Override
    public String toString() {
        return toString;
    }

    /** Builder for {@link AccountPayinRequestUpiQR} model class. */
    public static class BuilderImpl implements AccountPayinRequestUpiQR.Builder {
        private String accountName = null;

        private final String type;

        public BuilderImpl(String type) {
            this.type = type;
        }

        public BuilderImpl() {
            this("AccountPayinRequestUpiQR");
        }

        /**
          * Set {@link AccountPayinRequestUpiQR#getAccountName} property.
          *
          * Account Name is the name of the person who holds the bank or wallet account which your customer sends funds from to make his payment.
The name should be in the same format as the account holder name of the account. Allows numbers, some special characters and UNICODE symbols, see validation pattern.
          */
        @Override
        public BuilderImpl accountName(String accountName) {
            this.accountName = accountName;
            return this;
        }

        @Override
        public boolean isAccountNameDefined() {
            return this.accountName != null;
        }

        /**
         * Create new instance of {@link AccountPayinRequestUpiQR} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @Override
        public AccountPayinRequestUpiQRImpl build() {
            return new AccountPayinRequestUpiQRImpl(this);
        }

    }
}