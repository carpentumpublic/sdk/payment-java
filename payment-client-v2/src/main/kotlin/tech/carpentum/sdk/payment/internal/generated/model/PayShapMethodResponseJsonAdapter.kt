//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.PayShapMethodResponse

class PayShapMethodResponseJsonAdapter {
    @FromJson
    fun fromJson(json: PayShapMethodResponseJson): PayShapMethodResponse {
        val builder = PayShapMethodResponse.builder()
        builder.idPayin(json.idPayin)
        builder.idPayment(json.idPayment)
        builder.money(json.money)
        builder.vat(json.vat)
        builder.merchantName(json.merchantName)
        builder.accountCustomer(json.accountCustomer)
        builder.phoneNumber(json.phoneNumber)
        builder.reference(json.reference)
        builder.returnUrl(json.returnUrl)
        builder.paymentOperator(json.paymentOperator)
        builder.externalProvider(json.externalProvider)
        builder.acceptedAt(json.acceptedAt)
        builder.expireAt(json.expireAt)
        return builder.build()
    }

    @ToJson
    fun toJson(model: PayShapMethodResponse): PayShapMethodResponseJson {
        val json = PayShapMethodResponseJson()
        json.idPayin = model.idPayin
        json.idPayment = model.idPayment
        json.money = model.money
        json.vat = model.vat.orElse(null)
        json.merchantName = model.merchantName
        json.accountCustomer = model.accountCustomer.orElse(null)
        json.phoneNumber = model.phoneNumber.orElse(null)
        json.reference = model.reference
        json.returnUrl = model.returnUrl
        json.paymentOperator = model.paymentOperator
        json.externalProvider = model.externalProvider.orElse(null)
        json.acceptedAt = model.acceptedAt
        json.expireAt = model.expireAt
        json.paymentMethodCode = model.paymentMethodCode.name
        return json
    }

    @FromJson
    fun fromJsonImpl(model: PayShapMethodResponse): PayShapMethodResponseImpl {
        return model as PayShapMethodResponseImpl
    }

    @ToJson
    fun toJsonImpl(impl: PayShapMethodResponseImpl): PayShapMethodResponse {
        return impl
    }

}