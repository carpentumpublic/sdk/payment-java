//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** QRISPAY
 *
 * QRIS is payment method intended for the Indonesian market which allows users to pay using scanning QR codes by theirs Payment application.
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public class QrisPayMethodImpl implements QrisPayMethod {
    private final Optional<AccountPayinRequestQrisPay> account;

    @Override
    public Optional<AccountPayinRequestQrisPay> getAccount() {
        return account;
    }


    /** Your customer e-mail address in RFC 5322 format that is used for identification of the customer's payins. */
    private final Optional<String> emailAddress;

    @Override
    public Optional<String> getEmailAddress() {
        return emailAddress;
    }


    @Override public PaymentMethodCode getPaymentMethodCode() { return PAYMENT_METHOD_CODE; }

    private final int hashCode;
    private final String toString;

    private QrisPayMethodImpl(BuilderImpl builder) {
        this.account = Optional.ofNullable(builder.account);
        this.emailAddress = Optional.ofNullable(builder.emailAddress);

        this.hashCode = Objects.hash(account, emailAddress);
        this.toString = builder.type + "(" +
                "account=" + account +
                ", emailAddress=" + emailAddress +
                ')';
    }

    @Override
    public int hashCode() {
        return hashCode;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }

        if (!(obj instanceof QrisPayMethodImpl)) {
            return false;
        }

        QrisPayMethodImpl that = (QrisPayMethodImpl) obj;
        if (!Objects.equals(this.account, that.account)) return false;
        if (!Objects.equals(this.emailAddress, that.emailAddress)) return false;

        return true;
    }

    @Override
    public String toString() {
        return toString;
    }

    /** Builder for {@link QrisPayMethod} model class. */
    public static class BuilderImpl implements QrisPayMethod.Builder {
        private AccountPayinRequestQrisPay account = null;
        private String emailAddress = null;

        private final String type;

        public BuilderImpl(String type) {
            this.type = type;
        }

        public BuilderImpl() {
            this("QrisPayMethod");
        }

        /**
          * Set {@link QrisPayMethod#getAccount} property.
          *
          * 
          */
        @Override
        public BuilderImpl account(AccountPayinRequestQrisPay account) {
            this.account = account;
            return this;
        }

        @Override
        public boolean isAccountDefined() {
            return this.account != null;
        }

        /**
          * Set {@link QrisPayMethod#getEmailAddress} property.
          *
          * Your customer e-mail address in RFC 5322 format that is used for identification of the customer's payins.
          */
        @Override
        public BuilderImpl emailAddress(String emailAddress) {
            this.emailAddress = emailAddress;
            return this;
        }

        @Override
        public boolean isEmailAddressDefined() {
            return this.emailAddress != null;
        }

        /**
         * Create new instance of {@link QrisPayMethod} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @Override
        public QrisPayMethodImpl build() {
            return new QrisPayMethodImpl(this);
        }

    }
}