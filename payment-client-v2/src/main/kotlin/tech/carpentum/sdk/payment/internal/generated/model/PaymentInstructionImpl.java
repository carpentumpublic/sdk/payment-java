//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** PaymentInstruction
 *
 * If not empty then it specifies special instructions for different banking services your customers has to follow when entering payment in their banking service.
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public class PaymentInstructionImpl implements PaymentInstruction {
    /** It specifies your customer banking service to which companyCode belongs. e.g. ATM-MANDIRI - Mandiri Bank ATM banking service. */
    private final String bankingService;

    @Override
    public String getBankingService() {
        return bankingService;
    }


    /** It informs your customers that companyCode has to be used in their banking service together with Virtual Account Number from accountNumber parameter e.g. 82017 + 12903437820 for particular bankingService  provided that it is specified. */
    private final String companyCode;

    @Override
    public String getCompanyCode() {
        return companyCode;
    }




    private final int hashCode;
    private final String toString;

    private PaymentInstructionImpl(BuilderImpl builder) {
        this.bankingService = Objects.requireNonNull(builder.bankingService, "Property 'bankingService' is required.");
        this.companyCode = Objects.requireNonNull(builder.companyCode, "Property 'companyCode' is required.");

        this.hashCode = Objects.hash(bankingService, companyCode);
        this.toString = builder.type + "(" +
                "bankingService=" + bankingService +
                ", companyCode=" + companyCode +
                ')';
    }

    @Override
    public int hashCode() {
        return hashCode;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }

        if (!(obj instanceof PaymentInstructionImpl)) {
            return false;
        }

        PaymentInstructionImpl that = (PaymentInstructionImpl) obj;
        if (!Objects.equals(this.bankingService, that.bankingService)) return false;
        if (!Objects.equals(this.companyCode, that.companyCode)) return false;

        return true;
    }

    @Override
    public String toString() {
        return toString;
    }

    /** Builder for {@link PaymentInstruction} model class. */
    public static class BuilderImpl implements PaymentInstruction.Builder {
        private String bankingService = null;
        private String companyCode = null;

        private final String type;

        public BuilderImpl(String type) {
            this.type = type;
        }

        public BuilderImpl() {
            this("PaymentInstruction");
        }

        /**
          * Set {@link PaymentInstruction#getBankingService} property.
          *
          * It specifies your customer banking service to which companyCode belongs. e.g. ATM-MANDIRI - Mandiri Bank ATM banking service.
          */
        @Override
        public BuilderImpl bankingService(String bankingService) {
            this.bankingService = bankingService;
            return this;
        }

        @Override
        public boolean isBankingServiceDefined() {
            return this.bankingService != null;
        }

        /**
          * Set {@link PaymentInstruction#getCompanyCode} property.
          *
          * It informs your customers that companyCode has to be used in their banking service together with Virtual Account Number from accountNumber parameter e.g. 82017 + 12903437820 for particular bankingService  provided that it is specified.
          */
        @Override
        public BuilderImpl companyCode(String companyCode) {
            this.companyCode = companyCode;
            return this;
        }

        @Override
        public boolean isCompanyCodeDefined() {
            return this.companyCode != null;
        }

        /**
         * Create new instance of {@link PaymentInstruction} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @Override
        public PaymentInstructionImpl build() {
            return new PaymentInstructionImpl(this);
        }

    }
}