plugins {
    id("tech.carpentum.sdk.payment.java-application-conventions")

    scala
}

dependencies {
    implementation(project(":payment-client-v2"))
    implementation("ch.qos.logback:logback-classic")
    implementation("org.scala-lang:scala-library:2.13.14")
}

application {
    // Define the main class for the application.
    mainClass.set("tech.carpentum.sdk.payment.sample.AdvancedScenariosApp")
}
