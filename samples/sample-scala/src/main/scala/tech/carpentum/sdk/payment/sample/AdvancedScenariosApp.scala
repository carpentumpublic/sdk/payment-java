package tech.carpentum.sdk.payment.sample

import tech.carpentum.sdk.payment.PaymentsApi.ListPaymentOptionsQuery
import tech.carpentum.sdk.payment.PaymentsApi.ListPaymentOptionsQuery.PaymentTypeCode
import tech.carpentum.sdk.payment.model.PayinMethod.PaymentMethodCode
import tech.carpentum.sdk.payment.model.{
  AccountPayinRequestVaPay,
  AccountPayoutRequestBankTransfer,
  BankTransferMethod,
  CurrencyCode,
  GetPaymentOptionsError,
  Money,
  MoneyPaymentRequested,
  Payin,
  PaymentRequested,
  Payout,
  VaPayMethod,
}
import tech.carpentum.sdk.payment.{ ClientErrorException, GetPaymentOptionsErrorException, PaymentContext }

import java.math.BigDecimal
import java.net.HttpURLConnection
import java.time.Duration
import java.util.{ Objects, UUID }

object AdvancedScenariosApp {
  /* Use for example https://pipedream.com/workflows service. */
  private val callbackUrl = Objects.requireNonNull(System.getenv("TECH_CARPENTUM_SDK_PAYMENT_SAMPLE_CALLBACK_URL"))

  def main(args: Array[String]): Unit = {
    val context = PaymentContext.builder
      .brand("brand007")
      .defaultCallTimeout(Duration.ofMinutes(1))
      .build

    scenarioMerchantInfo(context)
    println("---------------------------------------------------")

    scenarioCodelists(context)
    System.out.println("---------------------------------------------------")

    scenarioAccount(context)
    System.out.println("---------------------------------------------------")

    val idPayin1 = scenarioCreatePayin(context)
    System.out.println("---------------------------------------------------")

    scenarioGetPayin(context, idPayin1)
    System.out.println("---------------------------------------------------")

    val idPayout1 = scenarioCreatePayout(context)
    System.out.println("---------------------------------------------------")

    scenarioGetPayout(context, idPayout1)
    System.out.println("---------------------------------------------------")

    val idPayout2 = scenarioCreatePayoutWithBalanceCheck(context)
    System.out.println("---------------------------------------------------")
    scenarioGetPayout410(context, idPayout2)
    System.out.println("---------------------------------------------------")
  }

  private def scenarioCreatePayin(context: PaymentContext): String = {
    val idPayin = "TST-" + UUID.randomUUID
    val createPayinScenario = CreatePayinScenario.create(context, idPayin)
    val availablePaymentOptions = createPayinScenario.availablePaymentOptions(
      PaymentRequested
        .builder()
        .money(
          MoneyPaymentRequested
            .builder()
            .amount(new BigDecimal("10050"))
            .currencyCode(CurrencyCode.of("IDR"))
            .build(),
        )
        .build(),
    )
    System.out.println("availablePaymentOptions= " + availablePaymentOptions)
    val payin = createPayinScenario.createPayment(
      Payin
        .builder()
        .paymentRequested(
          PaymentRequested
            .builder()
            .money(
              MoneyPaymentRequested
                .builder()
                .amount(new BigDecimal("10050"))
                .currencyCode(CurrencyCode.of("IDR"))
                .build(),
            )
            .build(),
        )
        .paymentMethod(
          VaPayMethod
            .builder()
            .account(
              AccountPayinRequestVaPay
                .builder()
                .accountName("Test account")
                .accountNumber("11223344")
                .build(),
            )
            .paymentOperatorCode("IDR_002")
            .emailAddress("customer@test.com")
            .build(),
        )
        .returnUrl("https://example.com?id=123")
        .callbackUrl(callbackUrl)
        .build(),
    )
    System.out.println("payin= " + payin)
    createPayinScenario.setExternalReference("my-fererence-" + UUID.randomUUID)
    Thread.sleep(2000L)
    val detail = createPayinScenario.getDetail
    System.out.println("detail= " + detail)
    System.out.println("paymentMethodResponse.paymentMethodCode= " + detail.getPaymentMethodResponse.getPaymentMethodCode)
    idPayin
  }

  private def scenarioGetPayin(context: PaymentContext, idPayin: String): String = {
    val getPayinScenario = GetPayinScenario.create(context, idPayin)
    val detail = getPayinScenario.getDetail
    System.out.println("detail= " + detail)
    System.out.println("paymentMethodResponse.paymentMethodCode= " + detail.getPaymentMethodResponse.getPaymentMethodCode)
    idPayin
  }

  private def scenarioCreatePayout(context: PaymentContext): String = {
    val idPayout = "TST-" + UUID.randomUUID
    val createPayoutScenario = CreatePayoutScenario.create(context, idPayout)
    val payout = createPayoutScenario.createPayment(
      Payout
        .builder()
        .paymentRequested(
          PaymentRequested
            .builder()
            .money(
              MoneyPaymentRequested
                .builder()
                .amount(new BigDecimal("50"))
                .currencyCode(CurrencyCode.of("IDR"))
                .build(),
            )
            .build(),
        )
        .paymentMethod(
          BankTransferMethod
            .builder()
            .account(AccountPayoutRequestBankTransfer.builder().accountName("Test account").accountNumber("11223344").build())
            .paymentOperatorCode("IDR_002")
            .emailAddress("customer@test.com")
            .build(),
        )
        .callbackUrl(callbackUrl)
        .build(),
    )
    System.out.println("payout= " + payout)
    val detail = createPayoutScenario.getDetail
    System.out.println("detail= " + detail)
    System.out.println("paymentMethodResponse.paymentMethodCode= " + detail.getPaymentMethodResponse.getPaymentMethodCode)
    idPayout
  }

  private def scenarioCreatePayoutWithBalanceCheck(context: PaymentContext): String = {
    val idPayout = "TST-" + UUID.randomUUID
    val createPayoutWithBalanceCheckScenario = CreatePayoutWithBalanceCheckScenario.create(context, idPayout)
    val payout = createPayoutWithBalanceCheckScenario.createPaymentWithBalanceCheck(
      Payout
        .builder()
        .paymentRequested(
          PaymentRequested
            .builder()
            .money(
              MoneyPaymentRequested
                .builder()
                .amount(new BigDecimal("5000000"))
                .currencyCode(CurrencyCode.of("IDR"))
                .build(),
            )
            .build(),
        )
        .paymentMethod(
          BankTransferMethod
            .builder()
            .account(AccountPayoutRequestBankTransfer.builder().accountName("Test account").accountNumber("11223344").build())
            .paymentOperatorCode("IDR_002")
            .emailAddress("customer@test.com")
            .build(),
        )
        .callbackUrl(callbackUrl)
        .build(),
    )
    System.out.println("payout= " + payout)
    if (payout.isPresent) {
      val detail = createPayoutWithBalanceCheckScenario.getDetail
      System.out.println("detail= " + detail)
      System.out.println("paymentMethodResponse.paymentMethodCode= " + detail.getPaymentMethodResponse.getPaymentMethodCode)
    } else System.out.println("Payout NOT created! EXPECTED")
    idPayout
  }

  private def scenarioGetPayout(context: PaymentContext, idPayout: String): Unit = {
    val getPayoutScenario = GetPayoutScenario.create(context, idPayout)
    val detail = getPayoutScenario.getDetail
    System.out.println("detail= " + detail)
    System.out.println("paymentMethodResponse.paymentMethodCode= " + detail.getPaymentMethodResponse.getPaymentMethodCode)
  }

  private def scenarioGetPayout410(context: PaymentContext, idPayout: String): Unit = {
    val getPayoutScenario = GetPayoutScenario.create(context, idPayout)
    try getPayoutScenario.getDetail
    catch {
      case ex: ClientErrorException =>
        // expected exception
        System.out.println(
          "[" + ex.getStatusCode + "] " + "(" + (ex.getStatusCode == HttpURLConnection.HTTP_GONE) + ") " + ex,
        ) // must be 410 Gone
    }
  }

  private def scenarioAccount(context: PaymentContext): Unit = {
    val accountScenario = AccountScenario.create(context)
    System.out.println("Current Authorization token: " + accountScenario.authToken)

    val balances1 = accountScenario.listBalances()
    System.out.println("balances= " + balances1)

    val balances2 = accountScenario.listBalances("IDR", "CZK")
    System.out.println("balances= " + balances2)
  }

  private def scenarioCodelists(context: PaymentContext): Unit = {
    val codelistsScenario = CodelistsScenario.create(context, Duration.ofHours(1))
    val currencies = codelistsScenario.listCurrencies
    System.out.println("currencies= " + currencies)
    val segments = codelistsScenario.listSegments
    System.out.println("segments= " + segments)
    val data = segments.getData
    System.out.println("data= " + data)
    val paymentOperators = codelistsScenario.listPaymentOperators
    System.out.println("paymentOperators= " + paymentOperators)
    val paymentMethods = codelistsScenario.listPaymentMethods
    System.out.println("paymentMethods= " + paymentMethods)

    val paymentOptions1 = codelistsScenario.listPaymentOptions
    System.out.println("paymentOptions= " + paymentOptions1)

    val paymentOptions2 = codelistsScenario.listPaymentOptions(ListPaymentOptionsQuery.paymentTypeCode(PaymentTypeCode.PAYIN))
    System.out.println("paymentOptions= " + paymentOptions2)

    val paymentOptions3 =
      codelistsScenario.listPaymentOptions(ListPaymentOptionsQuery.paymentMethodCodes(PaymentMethodCode.OFFLINE, PaymentMethodCode.ONLINE))
    System.out.println("paymentOptions= " + paymentOptions3)

    val paymentOptions4 = codelistsScenario.listPaymentOptions(ListPaymentOptionsQuery.currencyCodes("INR", "IDR"))
    System.out.println("paymentOptions= " + paymentOptions4)

    val paymentOptions5 = codelistsScenario.listPaymentOptions(ListPaymentOptionsQuery.segmentCodes("MEDIUM", "NEWBIE"))
    System.out.println("paymentOptions= " + paymentOptions5)

    try {
      val paymentOptions6 = codelistsScenario.listPaymentOptions(
        ListPaymentOptionsQuery
          .currencyCodes("IDR")
          .paymentTypeCode(PaymentTypeCode.PAYIN)
          .paymentMethodCodes(PaymentMethodCode.IMPS)
          .segmentCodes("VIP")
          .paymentOperatorCodes("IDR_016")
          .get,
      )
      System.out.println("paymentOptions= " + paymentOptions6)
    } catch {
      case ex: GetPaymentOptionsErrorException =>
        ex.getBusinessValidationErrors.forEach {
          case (code, error) =>
            code match {
              case GetPaymentOptionsError.CODE_MERCHANT_INACTIVE =>
                throw new IllegalStateException("Fatal error: " + error)
            }
        }
    }
  }

  private def scenarioMerchantInfo(context: PaymentContext): Unit = {
    val merchantInfoScenario = MerchantInfoScenario.create(context)
    System.out.println("Current Authorization token: " + merchantInfoScenario.authToken)

    val merchantInfo = merchantInfoScenario.getMerchantInfo
    System.out.println("merchantInfo= " + merchantInfo)
  }

}
