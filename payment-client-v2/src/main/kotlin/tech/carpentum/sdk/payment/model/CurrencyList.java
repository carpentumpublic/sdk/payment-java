//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** CurrencyList
 *
 * List of available currencies.
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public interface CurrencyList {

    @NotNull java.util.List<@NotNull Currency> getData();

    @NotNull static CurrencyList ofData(java.util.List<@NotNull Currency> data) { return builder().data(data).build(); }

    @NotNull static Builder builder() {
        return new CurrencyListImpl.BuilderImpl();
    }

    /** Builder for {@link CurrencyList} model class. */
    interface Builder {

        /**
          * Replace all items in {@link CurrencyList#getData} list property.
          *
          * 
          */
        @NotNull Builder data(java.util.List<@NotNull Currency> data);
        /**
          * Add single item to {@link CurrencyList#getData} list property.
          *
          * 
          */
        @NotNull Builder dataAdd(Currency item);
        /**
          * Add all items to {@link CurrencyList#getData} list property.
          *
          * 
          */
        @NotNull Builder dataAddAll(java.util.List<@NotNull Currency> data);


        /**
         * Create new instance of {@link CurrencyList} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @NotNull CurrencyList build();

    }
}