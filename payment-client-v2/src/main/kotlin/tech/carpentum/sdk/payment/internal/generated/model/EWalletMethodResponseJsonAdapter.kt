//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.EWalletMethodResponse

class EWalletMethodResponseJsonAdapter {
    @FromJson
    fun fromJson(json: EWalletMethodResponseJson): EWalletMethodResponse {
        val builder = EWalletMethodResponse.builder()
        builder.idPayin(json.idPayin)
        builder.idPayment(json.idPayment)
        builder.money(json.money)
        builder.vat(json.vat)
        builder.merchantName(json.merchantName)
        builder.reference(json.reference)
        builder.paymentOperator(json.paymentOperator)
        builder.returnUrl(json.returnUrl)
        builder.acceptedAt(json.acceptedAt)
        builder.expireAt(json.expireAt)
        return builder.build()
    }

    @ToJson
    fun toJson(model: EWalletMethodResponse): EWalletMethodResponseJson {
        val json = EWalletMethodResponseJson()
        json.idPayin = model.idPayin
        json.idPayment = model.idPayment
        json.money = model.money
        json.vat = model.vat.orElse(null)
        json.merchantName = model.merchantName
        json.reference = model.reference
        json.paymentOperator = model.paymentOperator.orElse(null)
        json.returnUrl = model.returnUrl
        json.acceptedAt = model.acceptedAt
        json.expireAt = model.expireAt
        json.paymentMethodCode = model.paymentMethodCode.name
        return json
    }

    @FromJson
    fun fromJsonImpl(model: EWalletMethodResponse): EWalletMethodResponseImpl {
        return model as EWalletMethodResponseImpl
    }

    @ToJson
    fun toJsonImpl(impl: EWalletMethodResponseImpl): EWalletMethodResponse {
        return impl
    }

}