//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** SegmentCode
 *
 * A customer segment which allows you to divide your customers into groups that reflect similarity among customers in each group.

The goal of segmenting customers is to decide which payment options and limitation you would like to apply to customers in each segment in order to follow your internal business rules.

This parameter is applied only when you have configured the segmentation in your account setting.

For supported segments please refer to [`GET /segments`](#operations-Payments-getSegments) API.
 *
 * 
 *
 * The model class is immutable.
 *
 * Use static {@link #of} method to create a new model class instance.
 */
@JsonClass(generateAdapter = false)
public class SegmentCodeImpl implements SegmentCode {
    private final @NotNull String value;

    private final int hashCode;
    private final String toString;

    public SegmentCodeImpl(@NotNull String value) {
        this.value = Objects.requireNonNull(value, "Property 'SegmentCode' is required.");

        this.hashCode = value.hashCode();
        this.toString = String.valueOf(value);
    }

    @Override
    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return toString;
    }

    @Override
    public int hashCode() {
        return hashCode;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }

        if (!(obj instanceof SegmentCodeImpl)) {
            return false;
        }

        SegmentCodeImpl that = (SegmentCodeImpl) obj;
        if (!this.value.equals(that.value)) return false;

        return true;
    }
}