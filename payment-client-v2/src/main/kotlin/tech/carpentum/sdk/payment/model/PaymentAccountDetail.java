//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** PaymentAccountDetail
 *
 * 
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public interface PaymentAccountDetail {

    @NotNull AccountResponseDetail getCustomerAccountRequest();

    @NotNull AccountResponseDetail getCustomerAccountProcess();

    @NotNull static Builder builder(PaymentAccountDetail copyOf) {
        Builder builder = builder();
        builder.customerAccountRequest(copyOf.getCustomerAccountRequest());
        builder.customerAccountProcess(copyOf.getCustomerAccountProcess());
        return builder;
    }

    @NotNull static Builder builder() {
        return new PaymentAccountDetailImpl.BuilderImpl();
    }

    /** Builder for {@link PaymentAccountDetail} model class. */
    interface Builder {

        /**
          * Set {@link PaymentAccountDetail#getCustomerAccountRequest} property.
          *
          * 
          */
        @NotNull Builder customerAccountRequest(AccountResponseDetail customerAccountRequest);

        boolean isCustomerAccountRequestDefined();


        /**
          * Set {@link PaymentAccountDetail#getCustomerAccountProcess} property.
          *
          * 
          */
        @NotNull Builder customerAccountProcess(AccountResponseDetail customerAccountProcess);

        boolean isCustomerAccountProcessDefined();


        /**
         * Create new instance of {@link PaymentAccountDetail} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @NotNull PaymentAccountDetail build();

    }
}