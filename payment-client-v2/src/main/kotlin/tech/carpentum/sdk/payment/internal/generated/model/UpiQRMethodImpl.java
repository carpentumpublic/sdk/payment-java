//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** UPIQR
 *
 * UPI QR is payment method intended for the Indian market. This payment method requires customers to scan QR code by Customer's Payment service application compatible with UPI payment schema.
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public class UpiQRMethodImpl implements UpiQRMethod {
    private final Optional<AccountPayinRequestUpiQR> account;

    @Override
    public Optional<AccountPayinRequestUpiQR> getAccount() {
        return account;
    }


    /** Virtual payment address of the customer on UPI (Unified Payment Interface) */
    private final Optional<String> upiId;

    @Override
    public Optional<String> getUpiId() {
        return upiId;
    }


    /** Your customer e-mail address in RFC 5322 format that is used for identification of the customer's payins. */
    private final Optional<String> emailAddress;

    @Override
    public Optional<String> getEmailAddress() {
        return emailAddress;
    }


    /** Your customer mobile phone number in full international telephone number format, including country code. */
    private final Optional<String> phoneNumber;

    @Override
    public Optional<String> getPhoneNumber() {
        return phoneNumber;
    }


    /** One of following can serve as Payment Operator:

 * Financial or other institution (such as bank, card payment processor, ...) that manages transactions for your customers
 * Mobile wallet
 * Blockchain protocol for crypto currency payments

Customer is informed with the payment instructions where funds have to be transferred efficiently based on the selected Payment operator.
For getting list of the available payment options for payins use [POST /payins/!availablePaymentOptions](#operations-Incoming_payments-availablePaymentOptions) API, for payouts use [POST /payouts/!availablePaymentOptions](#operations-Outgoing_payments-availablePaymentOptions) API. */
    private final Optional<String> paymentOperatorCode;

    @Override
    public Optional<String> getPaymentOperatorCode() {
        return paymentOperatorCode;
    }


    @Override public PaymentMethodCode getPaymentMethodCode() { return PAYMENT_METHOD_CODE; }

    private final int hashCode;
    private final String toString;

    private UpiQRMethodImpl(BuilderImpl builder) {
        this.account = Optional.ofNullable(builder.account);
        this.upiId = Optional.ofNullable(builder.upiId);
        this.emailAddress = Optional.ofNullable(builder.emailAddress);
        this.phoneNumber = Optional.ofNullable(builder.phoneNumber);
        this.paymentOperatorCode = Optional.ofNullable(builder.paymentOperatorCode);

        this.hashCode = Objects.hash(account, upiId, emailAddress, phoneNumber, paymentOperatorCode);
        this.toString = builder.type + "(" +
                "account=" + account +
                ", upiId=" + upiId +
                ", emailAddress=" + emailAddress +
                ", phoneNumber=" + phoneNumber +
                ", paymentOperatorCode=" + paymentOperatorCode +
                ')';
    }

    @Override
    public int hashCode() {
        return hashCode;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }

        if (!(obj instanceof UpiQRMethodImpl)) {
            return false;
        }

        UpiQRMethodImpl that = (UpiQRMethodImpl) obj;
        if (!Objects.equals(this.account, that.account)) return false;
        if (!Objects.equals(this.upiId, that.upiId)) return false;
        if (!Objects.equals(this.emailAddress, that.emailAddress)) return false;
        if (!Objects.equals(this.phoneNumber, that.phoneNumber)) return false;
        if (!Objects.equals(this.paymentOperatorCode, that.paymentOperatorCode)) return false;

        return true;
    }

    @Override
    public String toString() {
        return toString;
    }

    /** Builder for {@link UpiQRMethod} model class. */
    public static class BuilderImpl implements UpiQRMethod.Builder {
        private AccountPayinRequestUpiQR account = null;
        private String upiId = null;
        private String emailAddress = null;
        private String phoneNumber = null;
        private String paymentOperatorCode = null;

        private final String type;

        public BuilderImpl(String type) {
            this.type = type;
        }

        public BuilderImpl() {
            this("UpiQRMethod");
        }

        /**
          * Set {@link UpiQRMethod#getAccount} property.
          *
          * 
          */
        @Override
        public BuilderImpl account(AccountPayinRequestUpiQR account) {
            this.account = account;
            return this;
        }

        @Override
        public boolean isAccountDefined() {
            return this.account != null;
        }

        /**
          * Set {@link UpiQRMethod#getUpiId} property.
          *
          * Virtual payment address of the customer on UPI (Unified Payment Interface)
          */
        @Override
        public BuilderImpl upiId(String upiId) {
            this.upiId = upiId;
            return this;
        }

        @Override
        public boolean isUpiIdDefined() {
            return this.upiId != null;
        }

        /**
          * Set {@link UpiQRMethod#getEmailAddress} property.
          *
          * Your customer e-mail address in RFC 5322 format that is used for identification of the customer's payins.
          */
        @Override
        public BuilderImpl emailAddress(String emailAddress) {
            this.emailAddress = emailAddress;
            return this;
        }

        @Override
        public boolean isEmailAddressDefined() {
            return this.emailAddress != null;
        }

        /**
          * Set {@link UpiQRMethod#getPhoneNumber} property.
          *
          * Your customer mobile phone number in full international telephone number format, including country code.
          */
        @Override
        public BuilderImpl phoneNumber(String phoneNumber) {
            this.phoneNumber = phoneNumber;
            return this;
        }

        @Override
        public boolean isPhoneNumberDefined() {
            return this.phoneNumber != null;
        }

        /**
          * Set {@link UpiQRMethod#getPaymentOperatorCode} property.
          *
          * One of following can serve as Payment Operator:

 * Financial or other institution (such as bank, card payment processor, ...) that manages transactions for your customers
 * Mobile wallet
 * Blockchain protocol for crypto currency payments

Customer is informed with the payment instructions where funds have to be transferred efficiently based on the selected Payment operator.
For getting list of the available payment options for payins use [POST /payins/!availablePaymentOptions](#operations-Incoming_payments-availablePaymentOptions) API, for payouts use [POST /payouts/!availablePaymentOptions](#operations-Outgoing_payments-availablePaymentOptions) API.
          */
        @Override
        public BuilderImpl paymentOperatorCode(String paymentOperatorCode) {
            this.paymentOperatorCode = paymentOperatorCode;
            return this;
        }

        @Override
        public boolean isPaymentOperatorCodeDefined() {
            return this.paymentOperatorCode != null;
        }

        /**
         * Create new instance of {@link UpiQRMethod} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @Override
        public UpiQRMethodImpl build() {
            return new UpiQRMethodImpl(this);
        }

    }
}