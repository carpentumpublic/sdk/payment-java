package tech.carpentum.sdk.payment.sample

import tech.carpentum.sdk.payment.AuthTokenOperations
import tech.carpentum.sdk.payment.IncomingPaymentsApi
import tech.carpentum.sdk.payment.IncomingPaymentsApi.Factory.defineGetPayinEndpoint
import tech.carpentum.sdk.payment.PaymentContext
import tech.carpentum.sdk.payment.ResponseException
import tech.carpentum.sdk.payment.model.PayinDetail
import java.io.InterruptedIOException
import java.time.Duration

/**
 * Wraps just GET operation of Incoming Payments API for single `idPayin`.
 */
class GetPayinScenario private constructor(
    private val incomingPaymentsApi: IncomingPaymentsApi,
    private val idPayin: String,
    authToken: String
) : AbstractScenario(authToken) {

    @Throws(ResponseException::class, InterruptedIOException::class)
    fun getDetail(): PayinDetail {
        return incomingPaymentsApi.getPayin(idPayin)
    }

    companion object {
        /**
         * Creates new instance of the scenario with newly created Authorization token with specified validity.
         */
        @Throws(ResponseException::class, InterruptedIOException::class)
        fun create(
            context: PaymentContext,
            idPayin: String,
            validity: Duration? = null
        ): GetPayinScenario {
            val token = context.createAuthToken(
                AuthTokenOperations(defineGetPayinEndpoint().forId(idPayin)),
                validity
            ).token
            return GetPayinScenario(IncomingPaymentsApi.create(context, token), idPayin, token)
        }

        /**
         * Creates new instance of the scenario with existing Authorization token.
         */
        @Throws(ResponseException::class, InterruptedIOException::class)
        fun create(context: PaymentContext, idPayin: String, authToken: String): GetPayinScenario {
            return GetPayinScenario(IncomingPaymentsApi.create(context, authToken), idPayin, authToken)
        }
    }
}