//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.UpiQRMethodResponse

class UpiQRMethodResponseJsonAdapter {
    @FromJson
    fun fromJson(json: UpiQRMethodResponseJson): UpiQRMethodResponse {
        val builder = UpiQRMethodResponse.builder()
        builder.idPayin(json.idPayin)
        builder.idPayment(json.idPayment)
        builder.accountCustomer(json.accountCustomer)
        builder.money(json.money)
        builder.vat(json.vat)
        builder.merchantName(json.merchantName)
        builder.reference(json.reference)
        builder.externalReference(json.externalReference)
        builder.processor(json.processor)
        builder.qrName(json.qrName)
        builder.qrCode(json.qrCode)
        builder.upiQrDeepLink(json.upiQrDeepLink)
        builder.paymentOperator(json.paymentOperator)
        builder.upiId(json.upiId)
        builder.upiIdCustomer(json.upiIdCustomer)
        builder.phoneNumber(json.phoneNumber)
        builder.returnUrl(json.returnUrl)
        builder.acceptedAt(json.acceptedAt)
        builder.expireAt(json.expireAt)
        return builder.build()
    }

    @ToJson
    fun toJson(model: UpiQRMethodResponse): UpiQRMethodResponseJson {
        val json = UpiQRMethodResponseJson()
        json.idPayin = model.idPayin
        json.idPayment = model.idPayment
        json.accountCustomer = model.accountCustomer.orElse(null)
        json.money = model.money
        json.vat = model.vat.orElse(null)
        json.merchantName = model.merchantName
        json.reference = model.reference
        json.externalReference = model.externalReference.orElse(null)
        json.processor = model.processor.orElse(null)
        json.qrName = model.qrName
        json.qrCode = model.qrCode
        json.upiQrDeepLink = model.upiQrDeepLink.orElse(null)
        json.paymentOperator = model.paymentOperator.orElse(null)
        json.upiId = model.upiId.orElse(null)
        json.upiIdCustomer = model.upiIdCustomer.orElse(null)
        json.phoneNumber = model.phoneNumber.orElse(null)
        json.returnUrl = model.returnUrl
        json.acceptedAt = model.acceptedAt
        json.expireAt = model.expireAt
        json.paymentMethodCode = model.paymentMethodCode.name
        return json
    }

    @FromJson
    fun fromJsonImpl(model: UpiQRMethodResponse): UpiQRMethodResponseImpl {
        return model as UpiQRMethodResponseImpl
    }

    @ToJson
    fun toJsonImpl(impl: UpiQRMethodResponseImpl): UpiQRMethodResponse {
        return impl
    }

}