//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.TopUpRequestedDetail

class TopUpRequestedDetailJsonAdapter {
    @FromJson
    fun fromJson(json: TopUpRequestedDetailJson): TopUpRequestedDetail {
        val builder = TopUpRequestedDetail.builder()
        builder.money(json.money)
        builder.exchangedFrom(json.exchangedFrom)
        return builder.build()
    }

    @ToJson
    fun toJson(model: TopUpRequestedDetail): TopUpRequestedDetailJson {
        val json = TopUpRequestedDetailJson()
        json.money = model.money
        json.exchangedFrom = model.exchangedFrom.orElse(null)
        return json
    }

    @FromJson
    fun fromJsonImpl(model: TopUpRequestedDetail): TopUpRequestedDetailImpl {
        return model as TopUpRequestedDetailImpl
    }

    @ToJson
    fun toJsonImpl(impl: TopUpRequestedDetailImpl): TopUpRequestedDetail {
        return impl
    }

}