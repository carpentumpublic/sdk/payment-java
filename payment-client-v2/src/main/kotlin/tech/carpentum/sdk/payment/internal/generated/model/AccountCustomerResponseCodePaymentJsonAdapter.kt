//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.AccountCustomerResponseCodePayment

class AccountCustomerResponseCodePaymentJsonAdapter {
    @FromJson
    fun fromJson(json: AccountCustomerResponseCodePaymentJson): AccountCustomerResponseCodePayment {
        val builder = AccountCustomerResponseCodePayment.builder()
        builder.accountName(json.accountName)
        return builder.build()
    }

    @ToJson
    fun toJson(model: AccountCustomerResponseCodePayment): AccountCustomerResponseCodePaymentJson {
        val json = AccountCustomerResponseCodePaymentJson()
        json.accountName = model.accountName
        return json
    }

    @FromJson
    fun fromJsonImpl(model: AccountCustomerResponseCodePayment): AccountCustomerResponseCodePaymentImpl {
        return model as AccountCustomerResponseCodePaymentImpl
    }

    @ToJson
    fun toJsonImpl(impl: AccountCustomerResponseCodePaymentImpl): AccountCustomerResponseCodePayment {
        return impl
    }

}