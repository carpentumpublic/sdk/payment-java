//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.TopUpAcceptedMethodResponse

class TopUpAcceptedMethodResponseJsonAdapter {
    @FromJson
    fun fromJson(json: TopUpAcceptedMethodResponseJson): TopUpAcceptedMethodResponse {
        val builder = TopUpAcceptedMethodResponse.builder()
        builder.topUpRequested(json.topUpRequested)
        builder.moneyReceive(json.moneyReceive)
        builder.paymentMethodResponse(json.paymentMethodResponse)
        return builder.build()
    }

    @ToJson
    fun toJson(model: TopUpAcceptedMethodResponse): TopUpAcceptedMethodResponseJson {
        val json = TopUpAcceptedMethodResponseJson()
        json.topUpRequested = model.topUpRequested
        json.moneyReceive = model.moneyReceive
        json.paymentMethodResponse = model.paymentMethodResponse
        return json
    }

    @FromJson
    fun fromJsonImpl(model: TopUpAcceptedMethodResponse): TopUpAcceptedMethodResponseImpl {
        return model as TopUpAcceptedMethodResponseImpl
    }

    @ToJson
    fun toJsonImpl(impl: TopUpAcceptedMethodResponseImpl): TopUpAcceptedMethodResponse {
        return impl
    }

}