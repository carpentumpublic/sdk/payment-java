//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** PaymentOperatorIncoming
 *
 * 
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public interface PaymentOperatorIncoming {

    /** One of following can serve as Payment Operator:

 * Financial or other institution (such as bank, card payment processor, ...) that manages transactions for your customers
 * Mobile wallet
 * Blockchain protocol for crypto currency payments

Customer is informed with the payment instructions where funds have to be transferred efficiently based on the selected Payment operator.
For getting list of the available payment options for payins use [POST /payins/!availablePaymentOptions](#operations-Incoming_payments-availablePaymentOptions) API, for payouts use [POST /payouts/!availablePaymentOptions](#operations-Outgoing_payments-availablePaymentOptions) API. */
    @NotNull String getCode();

    @NotNull String getName();

    @NotNull Optional<ExternalProviders> getExternalProviders();

    @NotNull static Builder builder(PaymentOperatorIncoming copyOf) {
        Builder builder = builder();
        builder.code(copyOf.getCode());
        builder.name(copyOf.getName());
        builder.externalProviders(copyOf.getExternalProviders().orElse(null));
        return builder;
    }

    @NotNull static Builder builder() {
        return new PaymentOperatorIncomingImpl.BuilderImpl();
    }

    /** Builder for {@link PaymentOperatorIncoming} model class. */
    interface Builder {

        /**
          * Set {@link PaymentOperatorIncoming#getCode} property.
          *
          * One of following can serve as Payment Operator:

 * Financial or other institution (such as bank, card payment processor, ...) that manages transactions for your customers
 * Mobile wallet
 * Blockchain protocol for crypto currency payments

Customer is informed with the payment instructions where funds have to be transferred efficiently based on the selected Payment operator.
For getting list of the available payment options for payins use [POST /payins/!availablePaymentOptions](#operations-Incoming_payments-availablePaymentOptions) API, for payouts use [POST /payouts/!availablePaymentOptions](#operations-Outgoing_payments-availablePaymentOptions) API.
          */
        @NotNull Builder code(String code);

        boolean isCodeDefined();


        /**
          * Set {@link PaymentOperatorIncoming#getName} property.
          *
          * 
          */
        @NotNull Builder name(String name);

        boolean isNameDefined();


        /**
          * Set {@link PaymentOperatorIncoming#getExternalProviders} property.
          *
          * 
          */
        @NotNull Builder externalProviders(ExternalProviders externalProviders);

        boolean isExternalProvidersDefined();


        /**
         * Create new instance of {@link PaymentOperatorIncoming} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @NotNull PaymentOperatorIncoming build();

    }
}