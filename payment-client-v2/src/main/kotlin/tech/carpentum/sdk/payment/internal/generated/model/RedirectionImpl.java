//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** Redirection
 *
 * 
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public class RedirectionImpl implements Redirection {
    // tag::methodEnum[]
    /** @see #getMethod */
    public static final String METHOD_GET = "GET";
    /** @see #getMethod */
    public static final String METHOD_POST = "POST";
    // end::methodEnum[]
    /** The HTTP method that should be used for redirection. */
    private final String method;

    @Override
    public String getMethod() {
        return method;
    }


    /** The destination URL for redirect to. */
    private final String url;

    @Override
    public String getUrl() {
        return url;
    }


    /** The optional data in the `application/x-www-form-urlencoded` format that should be used for redirection when POST method is used. */
    private final Optional<String> data;

    @Override
    public Optional<String> getData() {
        return data;
    }




    private final int hashCode;
    private final String toString;

    private RedirectionImpl(BuilderImpl builder) {
        this.method = Objects.requireNonNull(builder.method, "Property 'method' is required.");
        this.url = Objects.requireNonNull(builder.url, "Property 'url' is required.");
        this.data = Optional.ofNullable(builder.data);

        this.hashCode = Objects.hash(method, url, data);
        this.toString = builder.type + "(" +
                "method=" + method +
                ", url=" + url +
                ", data=" + data +
                ')';
    }

    @Override
    public int hashCode() {
        return hashCode;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }

        if (!(obj instanceof RedirectionImpl)) {
            return false;
        }

        RedirectionImpl that = (RedirectionImpl) obj;
        if (!Objects.equals(this.method, that.method)) return false;
        if (!Objects.equals(this.url, that.url)) return false;
        if (!Objects.equals(this.data, that.data)) return false;

        return true;
    }

    @Override
    public String toString() {
        return toString;
    }

    /** Builder for {@link Redirection} model class. */
    public static class BuilderImpl implements Redirection.Builder {
        private String method = null;
        private String url = null;
        private String data = null;

        private final String type;

        public BuilderImpl(String type) {
            this.type = type;
        }

        public BuilderImpl() {
            this("Redirection");
        }

        /**
          * Set {@link Redirection#getMethod} property.
          *
          * The HTTP method that should be used for redirection.
          */
        @Override
        public BuilderImpl method(String method) {
            this.method = method;
            return this;
        }

        @Override
        public boolean isMethodDefined() {
            return this.method != null;
        }

        /**
          * Set {@link Redirection#getUrl} property.
          *
          * The destination URL for redirect to.
          */
        @Override
        public BuilderImpl url(String url) {
            this.url = url;
            return this;
        }

        @Override
        public boolean isUrlDefined() {
            return this.url != null;
        }

        /**
          * Set {@link Redirection#getData} property.
          *
          * The optional data in the `application/x-www-form-urlencoded` format that should be used for redirection when POST method is used.
          */
        @Override
        public BuilderImpl data(String data) {
            this.data = data;
            return this;
        }

        @Override
        public boolean isDataDefined() {
            return this.data != null;
        }

        /**
         * Create new instance of {@link Redirection} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @Override
        public RedirectionImpl build() {
            return new RedirectionImpl(this);
        }

    }
}