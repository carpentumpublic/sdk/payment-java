//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** ChannelInfo
 *
 * Merchant information which is supposed to be displayed to a customer during payment process in the [Payment Web App](payin-webapp.html) or your UI supporting payment flow.
Merchant information setup is available in the Merchant BackOffice portal.
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public class ChannelInfoImpl implements ChannelInfo {
    private final String name;

    @Override
    public String getName() {
        return name;
    }




    private final int hashCode;
    private final String toString;

    private ChannelInfoImpl(BuilderImpl builder) {
        this.name = Objects.requireNonNull(builder.name, "Property 'name' is required.");

        this.hashCode = Objects.hash(name);
        this.toString = builder.type + "(" +
                "name=" + name +
                ')';
    }

    @Override
    public int hashCode() {
        return hashCode;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }

        if (!(obj instanceof ChannelInfoImpl)) {
            return false;
        }

        ChannelInfoImpl that = (ChannelInfoImpl) obj;
        if (!Objects.equals(this.name, that.name)) return false;

        return true;
    }

    @Override
    public String toString() {
        return toString;
    }

    /** Builder for {@link ChannelInfo} model class. */
    public static class BuilderImpl implements ChannelInfo.Builder {
        private String name = null;

        private final String type;

        public BuilderImpl(String type) {
            this.type = type;
        }

        public BuilderImpl() {
            this("ChannelInfo");
        }

        /**
          * Set {@link ChannelInfo#getName} property.
          *
          * 
          */
        @Override
        public BuilderImpl name(String name) {
            this.name = name;
            return this;
        }

        @Override
        public boolean isNameDefined() {
            return this.name != null;
        }

        /**
         * Create new instance of {@link ChannelInfo} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @Override
        public ChannelInfoImpl build() {
            return new ChannelInfoImpl(this);
        }

    }
}