//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** PayoutMethodCode
 *
 * Payment method is a payment instrument that your customers use (such as online bank transfers, VA payments, UPI and so on).
Select a payment method from the list of the methods that you have configured in your account.
The `PaymentMethodCode` represents payment method available for a payment.

 - BANK_TRANSFER - A payment method for sending funds from your account to external bank account using a country specific payment system to process bank transfers. It requires customer to provide a receiving bank account details which are required by local payment system to process bank transfers.
 - CRYPTO_TRANSFER - A payment method for sending funds from your crypto wallet to external crypto wallet. It requires customer to provide a receiving crypto wallet details in format related to a chosen blockchain protocol.
 - WALLET_TRANSFER - A payment method for sending funds from your account to external wallet. It requires customer to provide a receiving wallet details in format related to the provider of a wallet.
 *
 * 
 *
 * The model class is immutable.
 *
 * Use static {@link #of} method to create a new model class instance.
 */
public interface PayoutMethodCode {


    @NotNull String getValue();

    static PayoutMethodCode of(@NotNull String value) {
        return new PayoutMethodCodeImpl(value);
    }
}