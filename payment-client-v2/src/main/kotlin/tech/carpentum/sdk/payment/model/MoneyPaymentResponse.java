//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** MoneyPaymentResponse
 *
 * 
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public interface MoneyPaymentResponse {

    /** Actual amount that end-customer should pay if `paymentMethodResponse.moneyRequired.amount` is not returned.

Amount is accepted in the smallest currency unit. For fiat currencies the smallest currency unit are based on ISO 4217 (e.g. for USD two decimal places are available so amount can be accepted as 12.34). The exceptions are IDR and INR currencies: zero amount in decimal places are allowed for them (e.g. 42.05 is not allowed, while 42 or 42.00 or 43 or 43.00 are allowed).

If more decimal places than supported is provided request will fail on HTTP 406 error (e.g. for USD the amount 12.345 will not be accepted). */
    @NotNull java.math.BigDecimal getAmount();

    @NotNull CurrencyCode getCurrencyCode();

    @NotNull static Builder builder(MoneyPaymentResponse copyOf) {
        Builder builder = builder();
        builder.amount(copyOf.getAmount());
        builder.currencyCode(copyOf.getCurrencyCode());
        return builder;
    }

    @NotNull static Builder builder() {
        return new MoneyPaymentResponseImpl.BuilderImpl();
    }

    /** Builder for {@link MoneyPaymentResponse} model class. */
    interface Builder {

        /**
          * Set {@link MoneyPaymentResponse#getAmount} property.
          *
          * Actual amount that end-customer should pay if `paymentMethodResponse.moneyRequired.amount` is not returned.

Amount is accepted in the smallest currency unit. For fiat currencies the smallest currency unit are based on ISO 4217 (e.g. for USD two decimal places are available so amount can be accepted as 12.34). The exceptions are IDR and INR currencies: zero amount in decimal places are allowed for them (e.g. 42.05 is not allowed, while 42 or 42.00 or 43 or 43.00 are allowed).

If more decimal places than supported is provided request will fail on HTTP 406 error (e.g. for USD the amount 12.345 will not be accepted).
          */
        @NotNull Builder amount(java.math.BigDecimal amount);

        boolean isAmountDefined();


        /**
          * Set {@link MoneyPaymentResponse#getCurrencyCode} property.
          *
          * 
          */
        @NotNull Builder currencyCode(CurrencyCode currencyCode);

        boolean isCurrencyCodeDefined();


        /**
         * Create new instance of {@link MoneyPaymentResponse} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @NotNull MoneyPaymentResponse build();

    }
}