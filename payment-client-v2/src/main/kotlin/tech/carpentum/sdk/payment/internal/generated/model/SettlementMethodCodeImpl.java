//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** SettlementMethodCode
 *
 * The `SettlementMethodCode` represents settlement payment method available for a payment.

 - CRYPTO_TRANSFER - A payment method for sending funds from your crypto wallet to external crypto wallet.
 *
 * 
 *
 * The model class is immutable.
 *
 * Use static {@link #of} method to create a new model class instance.
 */
@JsonClass(generateAdapter = false)
public class SettlementMethodCodeImpl implements SettlementMethodCode {
    private final @NotNull String value;

    private final int hashCode;
    private final String toString;

    public SettlementMethodCodeImpl(@NotNull String value) {
        this.value = Objects.requireNonNull(value, "Property 'SettlementMethodCode' is required.");

        this.hashCode = value.hashCode();
        this.toString = String.valueOf(value);
    }

    @Override
    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return toString;
    }

    @Override
    public int hashCode() {
        return hashCode;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }

        if (!(obj instanceof SettlementMethodCodeImpl)) {
            return false;
        }

        SettlementMethodCodeImpl that = (SettlementMethodCodeImpl) obj;
        if (!this.value.equals(that.value)) return false;

        return true;
    }
}