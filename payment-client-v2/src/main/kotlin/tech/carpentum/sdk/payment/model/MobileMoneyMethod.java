//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** MOBILE_MONEY
 *
 * Mobile Money is a payment method which allows customers to simply complete a payment from their mobile money wallet.
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public interface MobileMoneyMethod extends PayinMethod {
    /** A discriminator value of property {@link #getPaymentMethodCode}. The model class extends {@link PayinMethod}. */
    PayinMethod.PaymentMethodCode PAYMENT_METHOD_CODE = PayinMethod.PaymentMethodCode.MOBILE_MONEY;

    @NotNull AccountPayinRequestMobileMoney getAccount();

    /** Your customer e-mail address in RFC 5322 format that is used for identification of the customer's payins. */
    @NotNull Optional<String> getEmailAddress();

    /** Your customer mobile phone number in full international telephone number format, including country code. */
    @NotNull String getPhoneNumber();

    @NotNull static Builder builder(MobileMoneyMethod copyOf) {
        Builder builder = builder();
        builder.account(copyOf.getAccount());
        builder.emailAddress(copyOf.getEmailAddress().orElse(null));
        builder.phoneNumber(copyOf.getPhoneNumber());
        return builder;
    }

    @NotNull static Builder builder() {
        return new MobileMoneyMethodImpl.BuilderImpl();
    }

    /** Builder for {@link MobileMoneyMethod} model class. */
    interface Builder {

        /**
          * Set {@link MobileMoneyMethod#getAccount} property.
          *
          * 
          */
        @NotNull Builder account(AccountPayinRequestMobileMoney account);

        boolean isAccountDefined();


        /**
          * Set {@link MobileMoneyMethod#getEmailAddress} property.
          *
          * Your customer e-mail address in RFC 5322 format that is used for identification of the customer's payins.
          */
        @NotNull Builder emailAddress(String emailAddress);

        boolean isEmailAddressDefined();


        /**
          * Set {@link MobileMoneyMethod#getPhoneNumber} property.
          *
          * Your customer mobile phone number in full international telephone number format, including country code.
          */
        @NotNull Builder phoneNumber(String phoneNumber);

        boolean isPhoneNumberDefined();


        /**
         * Create new instance of {@link MobileMoneyMethod} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @NotNull MobileMoneyMethod build();

    }
}