//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** ForexRate
 *
 * 
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public interface ForexRate {

    @NotNull CurrencyCode getBaseCurrencyCode();

    @NotNull CurrencyCode getQuoteCurrencyCode();

    @NotNull Optional<java.math.BigDecimal> getBuyRate();

    @NotNull Optional<java.math.BigDecimal> getSellRate();

    @NotNull Optional<java.time.OffsetDateTime> getValidUpTo();

    @NotNull static Builder builder(ForexRate copyOf) {
        Builder builder = builder();
        builder.baseCurrencyCode(copyOf.getBaseCurrencyCode());
        builder.quoteCurrencyCode(copyOf.getQuoteCurrencyCode());
        builder.buyRate(copyOf.getBuyRate().orElse(null));
        builder.sellRate(copyOf.getSellRate().orElse(null));
        builder.validUpTo(copyOf.getValidUpTo().orElse(null));
        return builder;
    }

    @NotNull static Builder builder() {
        return new ForexRateImpl.BuilderImpl();
    }

    /** Builder for {@link ForexRate} model class. */
    interface Builder {

        /**
          * Set {@link ForexRate#getBaseCurrencyCode} property.
          *
          * 
          */
        @NotNull Builder baseCurrencyCode(CurrencyCode baseCurrencyCode);

        boolean isBaseCurrencyCodeDefined();


        /**
          * Set {@link ForexRate#getQuoteCurrencyCode} property.
          *
          * 
          */
        @NotNull Builder quoteCurrencyCode(CurrencyCode quoteCurrencyCode);

        boolean isQuoteCurrencyCodeDefined();


        /**
          * Set {@link ForexRate#getBuyRate} property.
          *
          * 
          */
        @NotNull Builder buyRate(java.math.BigDecimal buyRate);

        boolean isBuyRateDefined();


        /**
          * Set {@link ForexRate#getSellRate} property.
          *
          * 
          */
        @NotNull Builder sellRate(java.math.BigDecimal sellRate);

        boolean isSellRateDefined();


        /**
          * Set {@link ForexRate#getValidUpTo} property.
          *
          * 
          */
        @NotNull Builder validUpTo(java.time.OffsetDateTime validUpTo);

        boolean isValidUpToDefined();


        /**
         * Create new instance of {@link ForexRate} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @NotNull ForexRate build();

    }
}