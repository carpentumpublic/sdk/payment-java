//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.AccountCustomerResponseVaPay

class AccountCustomerResponseVaPayJsonAdapter {
    @FromJson
    fun fromJson(json: AccountCustomerResponseVaPayJson): AccountCustomerResponseVaPay {
        val builder = AccountCustomerResponseVaPay.builder()
        builder.accountName(json.accountName)
        builder.accountNumber(json.accountNumber)
        return builder.build()
    }

    @ToJson
    fun toJson(model: AccountCustomerResponseVaPay): AccountCustomerResponseVaPayJson {
        val json = AccountCustomerResponseVaPayJson()
        json.accountName = model.accountName
        json.accountNumber = model.accountNumber.orElse(null)
        return json
    }

    @FromJson
    fun fromJsonImpl(model: AccountCustomerResponseVaPay): AccountCustomerResponseVaPayImpl {
        return model as AccountCustomerResponseVaPayImpl
    }

    @ToJson
    fun toJsonImpl(impl: AccountCustomerResponseVaPayImpl): AccountCustomerResponseVaPay {
        return impl
    }

}