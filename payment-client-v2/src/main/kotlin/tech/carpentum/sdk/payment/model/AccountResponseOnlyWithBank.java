//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** AccountResponseOnlyWithBank
 *
 * Parameters of a bank account where we expect that your customer send funds to make a payment. These account parameters has to be provided to your customer in form of an payment instructions.
The returned parameters are depended on the payment method and currency your customer choose to pay.
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@Deprecated
@JsonClass(generateAdapter = false)
public interface AccountResponseOnlyWithBank {

    @NotNull Optional<String> getAccountName();

    @NotNull Optional<String> getAccountNumber();

    /** Bank code of the bank where we expect that your customer sends funds to make a payment.
If this parameter contains any value then show it to your customer in the payment instructions. */
    @NotNull Optional<String> getBankCode();

    /** Name of the bank where we expect that your customer sends funds to make a payment.
If this parameter contains any value then show it to your customer in the payment instructions. */
    @NotNull Optional<String> getBankName();

    /** Branch name of the bank where we expect that your customer sends funds to make a payment.
If this parameter contains any value then show it to your customer in the payment instructions. */
    @NotNull Optional<String> getBankBranch();

    /** City of the bank where we expect that your customer sends funds to make a payment.
If this parameter contains any value then show it to your customer in the payment instructions. */
    @NotNull Optional<String> getBankCity();

    /** Province of the bank where we expect that your customer sends funds to make a payment.
If this parameter contains any value then show it to your customer in the payment instructions. */
    @NotNull Optional<String> getBankProvince();


    @Deprecated @NotNull static Builder builder() {
        return new AccountResponseOnlyWithBankImpl.BuilderImpl();
    }

    /** Builder for {@link AccountResponseOnlyWithBank} model class. */
    @Deprecated interface Builder {

        /**
          * Set {@link AccountResponseOnlyWithBank#getAccountName} property.
          *
          * 
          */
        @Deprecated @NotNull Builder accountName(String accountName);

        @Deprecated boolean isAccountNameDefined();


        /**
          * Set {@link AccountResponseOnlyWithBank#getAccountNumber} property.
          *
          * 
          */
        @Deprecated @NotNull Builder accountNumber(String accountNumber);

        @Deprecated boolean isAccountNumberDefined();


        /**
          * Set {@link AccountResponseOnlyWithBank#getBankCode} property.
          *
          * Bank code of the bank where we expect that your customer sends funds to make a payment.
If this parameter contains any value then show it to your customer in the payment instructions.
          */
        @Deprecated @NotNull Builder bankCode(String bankCode);

        @Deprecated boolean isBankCodeDefined();


        /**
          * Set {@link AccountResponseOnlyWithBank#getBankName} property.
          *
          * Name of the bank where we expect that your customer sends funds to make a payment.
If this parameter contains any value then show it to your customer in the payment instructions.
          */
        @Deprecated @NotNull Builder bankName(String bankName);

        @Deprecated boolean isBankNameDefined();


        /**
          * Set {@link AccountResponseOnlyWithBank#getBankBranch} property.
          *
          * Branch name of the bank where we expect that your customer sends funds to make a payment.
If this parameter contains any value then show it to your customer in the payment instructions.
          */
        @Deprecated @NotNull Builder bankBranch(String bankBranch);

        @Deprecated boolean isBankBranchDefined();


        /**
          * Set {@link AccountResponseOnlyWithBank#getBankCity} property.
          *
          * City of the bank where we expect that your customer sends funds to make a payment.
If this parameter contains any value then show it to your customer in the payment instructions.
          */
        @Deprecated @NotNull Builder bankCity(String bankCity);

        @Deprecated boolean isBankCityDefined();


        /**
          * Set {@link AccountResponseOnlyWithBank#getBankProvince} property.
          *
          * Province of the bank where we expect that your customer sends funds to make a payment.
If this parameter contains any value then show it to your customer in the payment instructions.
          */
        @Deprecated @NotNull Builder bankProvince(String bankProvince);

        @Deprecated boolean isBankProvinceDefined();


        /**
         * Create new instance of {@link AccountResponseOnlyWithBank} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @Deprecated @NotNull AccountResponseOnlyWithBank build();

    }
}