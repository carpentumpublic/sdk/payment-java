//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import tech.carpentum.sdk.payment.model.PayinMethodResponse
import tech.carpentum.sdk.payment.model.PayinMethodResponse.PaymentMethodCode

import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.Moshi
import com.squareup.moshi.adapters.PolymorphicJsonAdapterFactory
import java.lang.reflect.Type

class PayinMethodResponseJsonAdapter : JsonAdapter.Factory {
    override fun create(type: Type, annotations: MutableSet<out Annotation>, moshi: Moshi): JsonAdapter<*>? {
        return PolymorphicJsonAdapterFactory
            .of(PayinMethodResponse::class.java, PayinMethodResponse.DISCRIMINATOR)
            .withSubtype(AbsaPayMethodResponseImpl::class.java, PaymentMethodCode.ABSA_PAY.toString())
            .withSubtype(CapitecPayMethodResponseImpl::class.java, PaymentMethodCode.CAPITEC_PAY.toString())
            .withSubtype(CodePaymentMethodResponseImpl::class.java, PaymentMethodCode.CODE_PAYMENT.toString())
            .withSubtype(CryptoOfflineMethodResponseImpl::class.java, PaymentMethodCode.CRYPTO_OFFLINE.toString())
            .withSubtype(DuitNowPayMethodResponseImpl::class.java, PaymentMethodCode.DUITNOW.toString())
            .withSubtype(EWalletMethodResponseImpl::class.java, PaymentMethodCode.EWALLET.toString())
            .withSubtype(IMPSMethodResponseImpl::class.java, PaymentMethodCode.IMPS.toString())
            .withSubtype(MobileMoneyMethodResponseImpl::class.java, PaymentMethodCode.MOBILE_MONEY.toString())
            .withSubtype(NetBankingMethodResponseImpl::class.java, PaymentMethodCode.NETBANKING.toString())
            .withSubtype(OfflineMethodResponseImpl::class.java, PaymentMethodCode.OFFLINE.toString())
            .withSubtype(OnlineMethodResponseImpl::class.java, PaymentMethodCode.ONLINE.toString())
            .withSubtype(P2AV2MethodResponseImpl::class.java, PaymentMethodCode.P2A_V2.toString())
            .withSubtype(PayShapMethodResponseImpl::class.java, PaymentMethodCode.PAYSHAP.toString())
            .withSubtype(PayMeMethodResponseImpl::class.java, PaymentMethodCode.PAY_ME.toString())
            .withSubtype(PromptPayMethodResponseImpl::class.java, PaymentMethodCode.PROMPTPAY.toString())
            .withSubtype(QrisPayMethodResponseImpl::class.java, PaymentMethodCode.QRISPAY.toString())
            .withSubtype(QrPhMethodResponseImpl::class.java, PaymentMethodCode.QRPH.toString())
            .withSubtype(UpiIdMethodResponseImpl::class.java, PaymentMethodCode.UPIID.toString())
            .withSubtype(UpiQRMethodResponseImpl::class.java, PaymentMethodCode.UPIQR.toString())
            .withSubtype(VaPayMethodResponseImpl::class.java, PaymentMethodCode.VAPAY.toString())
            .withSubtype(VaPayVerifMethodResponseImpl::class.java, PaymentMethodCode.VAPAY_VERIF.toString())
            .withSubtype(VietQRMethodResponseImpl::class.java, PaymentMethodCode.VIETQR.toString())
            .create(type, annotations, moshi)
    }
}