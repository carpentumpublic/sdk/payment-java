//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.CapitecPayMethodResponse

class CapitecPayMethodResponseJsonAdapter {
    @FromJson
    fun fromJson(json: CapitecPayMethodResponseJson): CapitecPayMethodResponse {
        val builder = CapitecPayMethodResponse.builder()
        builder.idPayin(json.idPayin)
        builder.idPayment(json.idPayment)
        builder.money(json.money)
        builder.vat(json.vat)
        builder.merchantName(json.merchantName)
        builder.accountCustomer(json.accountCustomer)
        builder.phoneNumber(json.phoneNumber)
        builder.said(json.said)
        builder.reference(json.reference)
        builder.returnUrl(json.returnUrl)
        builder.acceptedAt(json.acceptedAt)
        builder.expireAt(json.expireAt)
        return builder.build()
    }

    @ToJson
    fun toJson(model: CapitecPayMethodResponse): CapitecPayMethodResponseJson {
        val json = CapitecPayMethodResponseJson()
        json.idPayin = model.idPayin
        json.idPayment = model.idPayment
        json.money = model.money
        json.vat = model.vat.orElse(null)
        json.merchantName = model.merchantName
        json.accountCustomer = model.accountCustomer.orElse(null)
        json.phoneNumber = model.phoneNumber.orElse(null)
        json.said = model.said.orElse(null)
        json.reference = model.reference
        json.returnUrl = model.returnUrl
        json.acceptedAt = model.acceptedAt
        json.expireAt = model.expireAt
        json.paymentMethodCode = model.paymentMethodCode.name
        return json
    }

    @FromJson
    fun fromJsonImpl(model: CapitecPayMethodResponse): CapitecPayMethodResponseImpl {
        return model as CapitecPayMethodResponseImpl
    }

    @ToJson
    fun toJsonImpl(impl: CapitecPayMethodResponseImpl): CapitecPayMethodResponse {
        return impl
    }

}