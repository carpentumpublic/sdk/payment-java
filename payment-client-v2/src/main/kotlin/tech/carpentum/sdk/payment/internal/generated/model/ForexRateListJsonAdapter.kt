//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.ForexRateList

class ForexRateListJsonAdapter {
    @FromJson
    fun fromJson(json: ForexRateListJson): ForexRateList {
        val builder = ForexRateList.builder()
        builder.data(json.data?.toList())
        return builder.build()
    }

    @ToJson
    fun toJson(model: ForexRateList): ForexRateListJson {
        val json = ForexRateListJson()
        json.data = model.data.ifEmpty { null }
        return json
    }

    @FromJson
    fun fromJsonImpl(model: ForexRateList): ForexRateListImpl {
        return model as ForexRateListImpl
    }

    @ToJson
    fun toJsonImpl(impl: ForexRateListImpl): ForexRateList {
        return impl
    }

}