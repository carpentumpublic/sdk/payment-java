//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** BasicError
 *
 * 
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public class BasicErrorImpl implements BasicError {
    private final Optional<String> description;

    @Override
    public Optional<String> getDescription() {
        return description;
    }




    private final int hashCode;
    private final String toString;

    private BasicErrorImpl(BuilderImpl builder) {
        this.description = Optional.ofNullable(builder.description);

        this.hashCode = Objects.hash(description);
        this.toString = builder.type + "(" +
                "description=" + description +
                ')';
    }

    @Override
    public int hashCode() {
        return hashCode;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }

        if (!(obj instanceof BasicErrorImpl)) {
            return false;
        }

        BasicErrorImpl that = (BasicErrorImpl) obj;
        if (!Objects.equals(this.description, that.description)) return false;

        return true;
    }

    @Override
    public String toString() {
        return toString;
    }

    /** Builder for {@link BasicError} model class. */
    public static class BuilderImpl implements BasicError.Builder {
        private String description = null;

        private final String type;

        public BuilderImpl(String type) {
            this.type = type;
        }

        public BuilderImpl() {
            this("BasicError");
        }

        /**
          * Set {@link BasicError#getDescription} property.
          *
          * 
          */
        @Override
        public BuilderImpl description(String description) {
            this.description = description;
            return this;
        }

        @Override
        public boolean isDescriptionDefined() {
            return this.description != null;
        }

        /**
         * Create new instance of {@link BasicError} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @Override
        public BasicErrorImpl build() {
            return new BasicErrorImpl(this);
        }

    }
}