//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** TopUpRequestedMoneyProvided
 *
 * Provide how much money (amount) and in what currency (currencyCode) you want to pay to have your account topped up in a different currency (currencyRequired). The API will return how much money will be added to your account.
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public interface TopUpRequestedMoneyProvided extends TopUpRequested {

    @NotNull Money getMoneyProvided();

    @NotNull Optional<CurrencyCode> getCurrencyRequired();

    @NotNull static Builder builder(TopUpRequestedMoneyProvided copyOf) {
        Builder builder = builder();
        builder.moneyProvided(copyOf.getMoneyProvided());
        builder.currencyRequired(copyOf.getCurrencyRequired().orElse(null));
        return builder;
    }

    @NotNull static Builder builder() {
        return new TopUpRequestedMoneyProvidedImpl.BuilderImpl();
    }

    /** Builder for {@link TopUpRequestedMoneyProvided} model class. */
    interface Builder {

        /**
          * Set {@link TopUpRequestedMoneyProvided#getMoneyProvided} property.
          *
          * 
          */
        @NotNull Builder moneyProvided(Money moneyProvided);

        boolean isMoneyProvidedDefined();


        /**
          * Set {@link TopUpRequestedMoneyProvided#getCurrencyRequired} property.
          *
          * 
          */
        @NotNull Builder currencyRequired(CurrencyCode currencyRequired);

        boolean isCurrencyRequiredDefined();


        /**
         * Create new instance of {@link TopUpRequestedMoneyProvided} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @NotNull TopUpRequestedMoneyProvided build();

    }
}