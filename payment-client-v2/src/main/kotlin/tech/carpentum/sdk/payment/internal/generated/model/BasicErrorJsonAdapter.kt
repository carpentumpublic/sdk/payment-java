//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.BasicError

class BasicErrorJsonAdapter {
    @FromJson
    fun fromJson(json: BasicErrorJson): BasicError {
        val builder = BasicError.builder()
        builder.description(json.description)
        return builder.build()
    }

    @ToJson
    fun toJson(model: BasicError): BasicErrorJson {
        val json = BasicErrorJson()
        json.description = model.description.orElse(null)
        return json
    }

    @FromJson
    fun fromJsonImpl(model: BasicError): BasicErrorImpl {
        return model as BasicErrorImpl
    }

    @ToJson
    fun toJsonImpl(impl: BasicErrorImpl): BasicError {
        return impl
    }

}