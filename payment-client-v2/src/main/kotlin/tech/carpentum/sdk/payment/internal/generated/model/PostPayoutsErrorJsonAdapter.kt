//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.PostPayoutsError

class PostPayoutsErrorJsonAdapter {
    @FromJson
    fun fromJson(json: PostPayoutsErrorJson): PostPayoutsError {
        val builder = PostPayoutsError.builder()
        builder.code(json.code)
        builder.description(json.description)
        builder.attrCode(json.attrCode)
        builder.attrValue(json.attrValue)
        return builder.build()
    }

    @ToJson
    fun toJson(model: PostPayoutsError): PostPayoutsErrorJson {
        val json = PostPayoutsErrorJson()
        json.code = model.code
        json.description = model.description.orElse(null)
        json.attrCode = model.attrCode.orElse(null)
        json.attrValue = model.attrValue.orElse(null)
        return json
    }

}