package tech.carpentum.sdk.payment.internal.api

import tech.carpentum.sdk.payment.internal.generated.api.OutgoingPaymentsApi
import tech.carpentum.sdk.payment.internal.generated.infrastructure.RequestConfig
import java.time.Duration

internal class EnhancedOutgoingPaymentsApi(
    basePath: String = defaultBasePath,
    private val accessToken: String,
    private val brand: String?,
    callTimeout: Duration
) : OutgoingPaymentsApi(basePath, ApiUtils.getClient(callTimeout)) {

    override fun <T> updateRequestHeaders(requestConfig: RequestConfig<T>) {
        requestConfig.addAuthorizationHeader(accessToken)
        brand?.apply { requestConfig.addBrandHeader(this) }
    }
}
