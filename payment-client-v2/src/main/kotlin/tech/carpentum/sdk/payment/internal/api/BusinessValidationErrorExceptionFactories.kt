//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.api;


import com.squareup.moshi.Types
import tech.carpentum.sdk.payment.ClientErrorException
import tech.carpentum.sdk.payment.internal.generated.infrastructure.Serializer

import tech.carpentum.sdk.payment.model.PostAuthTokensError
import tech.carpentum.sdk.payment.model.PostAvailablePayoutOptionsError
import tech.carpentum.sdk.payment.model.PostPayinsError
import tech.carpentum.sdk.payment.model.PostAvailableTopUpOptionsError
import tech.carpentum.sdk.payment.model.PostPayoutsError
import tech.carpentum.sdk.payment.model.GetFxRatesError
import tech.carpentum.sdk.payment.model.GetTopupError
import tech.carpentum.sdk.payment.model.GetPayinError
import tech.carpentum.sdk.payment.model.GetPaymentOptionsError
import tech.carpentum.sdk.payment.model.GetPayoutError
import tech.carpentum.sdk.payment.model.PostAvailablePayinOptionsError

import tech.carpentum.sdk.payment.PostAuthTokensErrorException
import tech.carpentum.sdk.payment.GetPaymentOptionsErrorException
import tech.carpentum.sdk.payment.GetPayoutErrorException
import tech.carpentum.sdk.payment.GetPayinErrorException
import tech.carpentum.sdk.payment.GetFxRatesErrorException
import tech.carpentum.sdk.payment.PostPayoutsErrorException
import tech.carpentum.sdk.payment.GetTopupErrorException
import tech.carpentum.sdk.payment.PostAvailablePayinOptionsErrorException
import tech.carpentum.sdk.payment.PostAvailableTopUpOptionsErrorException
import tech.carpentum.sdk.payment.PostPayinsErrorException
import tech.carpentum.sdk.payment.PostAvailablePayoutOptionsErrorException

interface BusinessValidationErrorExceptionFactory<T : ClientErrorException> {
    fun create(cause: Throwable, statusCode: Int, bodyContent: String): T
}

class ClientErrorExceptionFactory : BusinessValidationErrorExceptionFactory<ClientErrorException> {
    override fun create(
        cause: Throwable,
        statusCode: Int,
        bodyContent: String
    ): ClientErrorException {
        return ClientErrorException(cause, statusCode, bodyContent)
    }

    companion object {
        val instance = ClientErrorExceptionFactory()
    }
}


/**
 * See [GetPayoutError].
 */
class GetPayoutErrorExceptionFactory : BusinessValidationErrorExceptionFactory<GetPayoutErrorException> {
    override fun create(
        cause: Throwable,
        statusCode: Int,
        bodyContent: String
    ): GetPayoutErrorException {
        return GetPayoutErrorException(
            cause,
            statusCode,
            jsonAdapterBusinessValidationErrorList.fromJson(bodyContent)!!.associateBy { error -> error.code }
        )
    }

    companion object {
        val instance = GetPayoutErrorExceptionFactory()

        private val jsonAdapterBusinessValidationErrorList =
            Serializer.moshi.adapter<List<GetPayoutError>>(
                Types.newParameterizedType(
                    List::class.java,
                    GetPayoutError::class.java
                )
            )
    }
}


/**
 * See [PostAuthTokensError].
 */
class PostAuthTokensErrorExceptionFactory : BusinessValidationErrorExceptionFactory<PostAuthTokensErrorException> {
    override fun create(
        cause: Throwable,
        statusCode: Int,
        bodyContent: String
    ): PostAuthTokensErrorException {
        return PostAuthTokensErrorException(
            cause,
            statusCode,
            jsonAdapterBusinessValidationErrorList.fromJson(bodyContent)!!.associateBy { error -> error.code }
        )
    }

    companion object {
        val instance = PostAuthTokensErrorExceptionFactory()

        private val jsonAdapterBusinessValidationErrorList =
            Serializer.moshi.adapter<List<PostAuthTokensError>>(
                Types.newParameterizedType(
                    List::class.java,
                    PostAuthTokensError::class.java
                )
            )
    }
}


/**
 * See [GetFxRatesError].
 */
class GetFxRatesErrorExceptionFactory : BusinessValidationErrorExceptionFactory<GetFxRatesErrorException> {
    override fun create(
        cause: Throwable,
        statusCode: Int,
        bodyContent: String
    ): GetFxRatesErrorException {
        return GetFxRatesErrorException(
            cause,
            statusCode,
            jsonAdapterBusinessValidationErrorList.fromJson(bodyContent)!!.associateBy { error -> error.code }
        )
    }

    companion object {
        val instance = GetFxRatesErrorExceptionFactory()

        private val jsonAdapterBusinessValidationErrorList =
            Serializer.moshi.adapter<List<GetFxRatesError>>(
                Types.newParameterizedType(
                    List::class.java,
                    GetFxRatesError::class.java
                )
            )
    }
}


/**
 * See [PostPayinsError].
 */
class PostPayinsErrorExceptionFactory : BusinessValidationErrorExceptionFactory<PostPayinsErrorException> {
    override fun create(
        cause: Throwable,
        statusCode: Int,
        bodyContent: String
    ): PostPayinsErrorException {
        return PostPayinsErrorException(
            cause,
            statusCode,
            jsonAdapterBusinessValidationErrorList.fromJson(bodyContent)!!.associateBy { error -> error.code }
        )
    }

    companion object {
        val instance = PostPayinsErrorExceptionFactory()

        private val jsonAdapterBusinessValidationErrorList =
            Serializer.moshi.adapter<List<PostPayinsError>>(
                Types.newParameterizedType(
                    List::class.java,
                    PostPayinsError::class.java
                )
            )
    }
}


/**
 * See [PostAvailablePayinOptionsError].
 */
class PostAvailablePayinOptionsErrorExceptionFactory : BusinessValidationErrorExceptionFactory<PostAvailablePayinOptionsErrorException> {
    override fun create(
        cause: Throwable,
        statusCode: Int,
        bodyContent: String
    ): PostAvailablePayinOptionsErrorException {
        return PostAvailablePayinOptionsErrorException(
            cause,
            statusCode,
            jsonAdapterBusinessValidationErrorList.fromJson(bodyContent)!!.associateBy { error -> error.code }
        )
    }

    companion object {
        val instance = PostAvailablePayinOptionsErrorExceptionFactory()

        private val jsonAdapterBusinessValidationErrorList =
            Serializer.moshi.adapter<List<PostAvailablePayinOptionsError>>(
                Types.newParameterizedType(
                    List::class.java,
                    PostAvailablePayinOptionsError::class.java
                )
            )
    }
}


/**
 * See [GetPaymentOptionsError].
 */
class GetPaymentOptionsErrorExceptionFactory : BusinessValidationErrorExceptionFactory<GetPaymentOptionsErrorException> {
    override fun create(
        cause: Throwable,
        statusCode: Int,
        bodyContent: String
    ): GetPaymentOptionsErrorException {
        return GetPaymentOptionsErrorException(
            cause,
            statusCode,
            jsonAdapterBusinessValidationErrorList.fromJson(bodyContent)!!.associateBy { error -> error.code }
        )
    }

    companion object {
        val instance = GetPaymentOptionsErrorExceptionFactory()

        private val jsonAdapterBusinessValidationErrorList =
            Serializer.moshi.adapter<List<GetPaymentOptionsError>>(
                Types.newParameterizedType(
                    List::class.java,
                    GetPaymentOptionsError::class.java
                )
            )
    }
}


/**
 * See [PostPayoutsError].
 */
class PostPayoutsErrorExceptionFactory : BusinessValidationErrorExceptionFactory<PostPayoutsErrorException> {
    override fun create(
        cause: Throwable,
        statusCode: Int,
        bodyContent: String
    ): PostPayoutsErrorException {
        return PostPayoutsErrorException(
            cause,
            statusCode,
            jsonAdapterBusinessValidationErrorList.fromJson(bodyContent)!!.associateBy { error -> error.code }
        )
    }

    companion object {
        val instance = PostPayoutsErrorExceptionFactory()

        private val jsonAdapterBusinessValidationErrorList =
            Serializer.moshi.adapter<List<PostPayoutsError>>(
                Types.newParameterizedType(
                    List::class.java,
                    PostPayoutsError::class.java
                )
            )
    }
}


/**
 * See [GetPayinError].
 */
class GetPayinErrorExceptionFactory : BusinessValidationErrorExceptionFactory<GetPayinErrorException> {
    override fun create(
        cause: Throwable,
        statusCode: Int,
        bodyContent: String
    ): GetPayinErrorException {
        return GetPayinErrorException(
            cause,
            statusCode,
            jsonAdapterBusinessValidationErrorList.fromJson(bodyContent)!!.associateBy { error -> error.code }
        )
    }

    companion object {
        val instance = GetPayinErrorExceptionFactory()

        private val jsonAdapterBusinessValidationErrorList =
            Serializer.moshi.adapter<List<GetPayinError>>(
                Types.newParameterizedType(
                    List::class.java,
                    GetPayinError::class.java
                )
            )
    }
}


/**
 * See [PostAvailablePayoutOptionsError].
 */
class PostAvailablePayoutOptionsErrorExceptionFactory : BusinessValidationErrorExceptionFactory<PostAvailablePayoutOptionsErrorException> {
    override fun create(
        cause: Throwable,
        statusCode: Int,
        bodyContent: String
    ): PostAvailablePayoutOptionsErrorException {
        return PostAvailablePayoutOptionsErrorException(
            cause,
            statusCode,
            jsonAdapterBusinessValidationErrorList.fromJson(bodyContent)!!.associateBy { error -> error.code }
        )
    }

    companion object {
        val instance = PostAvailablePayoutOptionsErrorExceptionFactory()

        private val jsonAdapterBusinessValidationErrorList =
            Serializer.moshi.adapter<List<PostAvailablePayoutOptionsError>>(
                Types.newParameterizedType(
                    List::class.java,
                    PostAvailablePayoutOptionsError::class.java
                )
            )
    }
}


/**
 * See [GetTopupError].
 */
class GetTopupErrorExceptionFactory : BusinessValidationErrorExceptionFactory<GetTopupErrorException> {
    override fun create(
        cause: Throwable,
        statusCode: Int,
        bodyContent: String
    ): GetTopupErrorException {
        return GetTopupErrorException(
            cause,
            statusCode,
            jsonAdapterBusinessValidationErrorList.fromJson(bodyContent)!!.associateBy { error -> error.code }
        )
    }

    companion object {
        val instance = GetTopupErrorExceptionFactory()

        private val jsonAdapterBusinessValidationErrorList =
            Serializer.moshi.adapter<List<GetTopupError>>(
                Types.newParameterizedType(
                    List::class.java,
                    GetTopupError::class.java
                )
            )
    }
}


/**
 * See [PostAvailableTopUpOptionsError].
 */
class PostAvailableTopUpOptionsErrorExceptionFactory : BusinessValidationErrorExceptionFactory<PostAvailableTopUpOptionsErrorException> {
    override fun create(
        cause: Throwable,
        statusCode: Int,
        bodyContent: String
    ): PostAvailableTopUpOptionsErrorException {
        return PostAvailableTopUpOptionsErrorException(
            cause,
            statusCode,
            jsonAdapterBusinessValidationErrorList.fromJson(bodyContent)!!.associateBy { error -> error.code }
        )
    }

    companion object {
        val instance = PostAvailableTopUpOptionsErrorExceptionFactory()

        private val jsonAdapterBusinessValidationErrorList =
            Serializer.moshi.adapter<List<PostAvailableTopUpOptionsError>>(
                Types.newParameterizedType(
                    List::class.java,
                    PostAvailableTopUpOptionsError::class.java
                )
            )
    }
}