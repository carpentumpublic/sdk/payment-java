//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.OnlineMethod

class OnlineMethodJsonAdapter {
    @FromJson
    fun fromJson(json: OnlineMethodJson): OnlineMethod {
        val builder = OnlineMethod.builder()
        builder.account(json.account)
        builder.paymentOperatorCode(json.paymentOperatorCode)
        builder.emailAddress(json.emailAddress)
        return builder.build()
    }

    @ToJson
    fun toJson(model: OnlineMethod): OnlineMethodJson {
        val json = OnlineMethodJson()
        json.account = model.account.orElse(null)
        json.paymentOperatorCode = model.paymentOperatorCode.orElse(null)
        json.emailAddress = model.emailAddress.orElse(null)
        json.paymentMethodCode = model.paymentMethodCode.name
        return json
    }

    @FromJson
    fun fromJsonImpl(model: OnlineMethod): OnlineMethodImpl {
        return model as OnlineMethodImpl
    }

    @ToJson
    fun toJsonImpl(impl: OnlineMethodImpl): OnlineMethod {
        return impl
    }

}