//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.VaPayVerifMethod

class VaPayVerifMethodJsonAdapter {
    @FromJson
    fun fromJson(json: VaPayVerifMethodJson): VaPayVerifMethod {
        val builder = VaPayVerifMethod.builder()
        builder.account(json.account)
        builder.paymentOperatorCode(json.paymentOperatorCode)
        builder.emailAddress(json.emailAddress)
        return builder.build()
    }

    @ToJson
    fun toJson(model: VaPayVerifMethod): VaPayVerifMethodJson {
        val json = VaPayVerifMethodJson()
        json.account = model.account
        json.paymentOperatorCode = model.paymentOperatorCode
        json.emailAddress = model.emailAddress.orElse(null)
        json.paymentMethodCode = model.paymentMethodCode.name
        return json
    }

    @FromJson
    fun fromJsonImpl(model: VaPayVerifMethod): VaPayVerifMethodImpl {
        return model as VaPayVerifMethodImpl
    }

    @ToJson
    fun toJsonImpl(impl: VaPayVerifMethodImpl): VaPayVerifMethod {
        return impl
    }

}