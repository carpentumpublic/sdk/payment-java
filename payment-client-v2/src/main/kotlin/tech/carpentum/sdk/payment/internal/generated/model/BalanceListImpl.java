//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** BalanceList
 *
 * The list of account balances by currency.
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public class BalanceListImpl implements BalanceList {
    private final java.util.List<@NotNull Balance> data;

    @Override
    public java.util.List<@NotNull Balance> getData() {
        return data;
    }




    private final int hashCode;
    private final String toString;

    private BalanceListImpl(BuilderImpl builder) {
        this.data = java.util.Collections.unmodifiableList(builder.data);

        this.hashCode = Objects.hash(data);
        this.toString = builder.type + "(" +
                "data=" + data +
                ')';
    }

    @Override
    public int hashCode() {
        return hashCode;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }

        if (!(obj instanceof BalanceListImpl)) {
            return false;
        }

        BalanceListImpl that = (BalanceListImpl) obj;
        if (!Objects.equals(this.data, that.data)) return false;

        return true;
    }

    @Override
    public String toString() {
        return toString;
    }

    /** Builder for {@link BalanceList} model class. */
    public static class BuilderImpl implements BalanceList.Builder {
        private java.util.List<@NotNull Balance> data = new java.util.ArrayList<>();

        private final String type;

        public BuilderImpl(String type) {
            this.type = type;
        }

        public BuilderImpl() {
            this("BalanceList");
        }

        /**
          * Replace all items in {@link BalanceList#getData} list property.
          *
          * 
          */
        @Override
        public BuilderImpl data(java.util.List<@NotNull Balance> data) {
            this.data.clear();
            if (data != null) {
                this.data.addAll(data);
            }
            return this;
        }
        /**
          * Add single item to {@link BalanceList#getData} list property.
          *
          * 
          */
        @Override
        public BuilderImpl dataAdd(Balance item) {
            if (item != null) {
                this.data.add(item);
            }
            return this;
        }
        /**
          * Add all items to {@link BalanceList#getData} list property.
          *
          * 
          */
        @Override
        public BuilderImpl dataAddAll(java.util.List<@NotNull Balance> data) {
            if (data != null) {
                this.data.addAll(data);
            }
            return this;
        }


        /**
         * Create new instance of {@link BalanceList} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @Override
        public BalanceListImpl build() {
            return new BalanceListImpl(this);
        }

    }
}