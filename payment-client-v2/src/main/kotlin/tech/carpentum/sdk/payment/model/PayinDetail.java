//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** PayinDetail
 *
 * 
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public interface PayinDetail {

    @NotNull PaymentRequested getPaymentRequested();

    @NotNull PaymentProcess getProcess();

    @NotNull MoneyFee getFee();

    @NotNull PayinMethodResponse getPaymentMethodResponse();

    @NotNull Optional<SettlementMethod> getSettlement();

    @NotNull static Builder builder(PayinDetail copyOf) {
        Builder builder = builder();
        builder.paymentRequested(copyOf.getPaymentRequested());
        builder.process(copyOf.getProcess());
        builder.fee(copyOf.getFee());
        builder.paymentMethodResponse(copyOf.getPaymentMethodResponse());
        builder.settlement(copyOf.getSettlement().orElse(null));
        return builder;
    }

    @NotNull static Builder builder() {
        return new PayinDetailImpl.BuilderImpl();
    }

    /** Builder for {@link PayinDetail} model class. */
    interface Builder {

        /**
          * Set {@link PayinDetail#getPaymentRequested} property.
          *
          * 
          */
        @NotNull Builder paymentRequested(PaymentRequested paymentRequested);

        boolean isPaymentRequestedDefined();


        /**
          * Set {@link PayinDetail#getProcess} property.
          *
          * 
          */
        @NotNull Builder process(PaymentProcess process);

        boolean isProcessDefined();


        /**
          * Set {@link PayinDetail#getFee} property.
          *
          * 
          */
        @NotNull Builder fee(MoneyFee fee);

        boolean isFeeDefined();


        /**
          * Set {@link PayinDetail#getPaymentMethodResponse} property.
          *
          * 
          */
        @NotNull Builder paymentMethodResponse(PayinMethodResponse paymentMethodResponse);

        boolean isPaymentMethodResponseDefined();


        /**
          * Set {@link PayinDetail#getSettlement} property.
          *
          * 
          */
        @NotNull Builder settlement(SettlementMethod settlement);

        boolean isSettlementDefined();


        /**
         * Create new instance of {@link PayinDetail} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @NotNull PayinDetail build();

    }
}