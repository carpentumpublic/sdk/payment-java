//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** ForexRate
 *
 * 
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public class ForexRateImpl implements ForexRate {
    private final CurrencyCode baseCurrencyCode;

    @Override
    public CurrencyCode getBaseCurrencyCode() {
        return baseCurrencyCode;
    }


    private final CurrencyCode quoteCurrencyCode;

    @Override
    public CurrencyCode getQuoteCurrencyCode() {
        return quoteCurrencyCode;
    }


    private final Optional<java.math.BigDecimal> buyRate;

    @Override
    public Optional<java.math.BigDecimal> getBuyRate() {
        return buyRate;
    }


    private final Optional<java.math.BigDecimal> sellRate;

    @Override
    public Optional<java.math.BigDecimal> getSellRate() {
        return sellRate;
    }


    private final Optional<java.time.OffsetDateTime> validUpTo;

    @Override
    public Optional<java.time.OffsetDateTime> getValidUpTo() {
        return validUpTo;
    }




    private final int hashCode;
    private final String toString;

    private ForexRateImpl(BuilderImpl builder) {
        this.baseCurrencyCode = Objects.requireNonNull(builder.baseCurrencyCode, "Property 'baseCurrencyCode' is required.");
        this.quoteCurrencyCode = Objects.requireNonNull(builder.quoteCurrencyCode, "Property 'quoteCurrencyCode' is required.");
        this.buyRate = Optional.ofNullable(builder.buyRate);
        this.sellRate = Optional.ofNullable(builder.sellRate);
        this.validUpTo = Optional.ofNullable(builder.validUpTo);

        this.hashCode = Objects.hash(baseCurrencyCode, quoteCurrencyCode, buyRate, sellRate, validUpTo);
        this.toString = builder.type + "(" +
                "baseCurrencyCode=" + baseCurrencyCode +
                ", quoteCurrencyCode=" + quoteCurrencyCode +
                ", buyRate=" + buyRate +
                ", sellRate=" + sellRate +
                ", validUpTo=" + validUpTo +
                ')';
    }

    @Override
    public int hashCode() {
        return hashCode;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }

        if (!(obj instanceof ForexRateImpl)) {
            return false;
        }

        ForexRateImpl that = (ForexRateImpl) obj;
        if (!Objects.equals(this.baseCurrencyCode, that.baseCurrencyCode)) return false;
        if (!Objects.equals(this.quoteCurrencyCode, that.quoteCurrencyCode)) return false;
        if (!Objects.equals(this.buyRate, that.buyRate)) return false;
        if (!Objects.equals(this.sellRate, that.sellRate)) return false;
        if (!Objects.equals(this.validUpTo, that.validUpTo)) return false;

        return true;
    }

    @Override
    public String toString() {
        return toString;
    }

    /** Builder for {@link ForexRate} model class. */
    public static class BuilderImpl implements ForexRate.Builder {
        private CurrencyCode baseCurrencyCode = null;
        private CurrencyCode quoteCurrencyCode = null;
        private java.math.BigDecimal buyRate = null;
        private java.math.BigDecimal sellRate = null;
        private java.time.OffsetDateTime validUpTo = null;

        private final String type;

        public BuilderImpl(String type) {
            this.type = type;
        }

        public BuilderImpl() {
            this("ForexRate");
        }

        /**
          * Set {@link ForexRate#getBaseCurrencyCode} property.
          *
          * 
          */
        @Override
        public BuilderImpl baseCurrencyCode(CurrencyCode baseCurrencyCode) {
            this.baseCurrencyCode = baseCurrencyCode;
            return this;
        }

        @Override
        public boolean isBaseCurrencyCodeDefined() {
            return this.baseCurrencyCode != null;
        }

        /**
          * Set {@link ForexRate#getQuoteCurrencyCode} property.
          *
          * 
          */
        @Override
        public BuilderImpl quoteCurrencyCode(CurrencyCode quoteCurrencyCode) {
            this.quoteCurrencyCode = quoteCurrencyCode;
            return this;
        }

        @Override
        public boolean isQuoteCurrencyCodeDefined() {
            return this.quoteCurrencyCode != null;
        }

        /**
          * Set {@link ForexRate#getBuyRate} property.
          *
          * 
          */
        @Override
        public BuilderImpl buyRate(java.math.BigDecimal buyRate) {
            this.buyRate = buyRate;
            return this;
        }

        @Override
        public boolean isBuyRateDefined() {
            return this.buyRate != null;
        }

        /**
          * Set {@link ForexRate#getSellRate} property.
          *
          * 
          */
        @Override
        public BuilderImpl sellRate(java.math.BigDecimal sellRate) {
            this.sellRate = sellRate;
            return this;
        }

        @Override
        public boolean isSellRateDefined() {
            return this.sellRate != null;
        }

        /**
          * Set {@link ForexRate#getValidUpTo} property.
          *
          * 
          */
        @Override
        public BuilderImpl validUpTo(java.time.OffsetDateTime validUpTo) {
            this.validUpTo = validUpTo;
            return this;
        }

        @Override
        public boolean isValidUpToDefined() {
            return this.validUpTo != null;
        }

        /**
         * Create new instance of {@link ForexRate} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @Override
        public ForexRateImpl build() {
            return new ForexRateImpl(this);
        }

    }
}