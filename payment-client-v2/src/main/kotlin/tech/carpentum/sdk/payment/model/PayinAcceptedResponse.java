//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** PayinAcceptedResponse
 *
 * 
 *
 * @see PayinAcceptedMethodResponse
 * @see PayinAcceptedRedirectResponse
 *
 * The model class is immutable.
 *
 *
 */
public interface PayinAcceptedResponse {
}