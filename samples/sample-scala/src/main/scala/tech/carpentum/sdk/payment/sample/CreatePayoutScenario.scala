package tech.carpentum.sdk.payment.sample

import tech.carpentum.sdk.payment.model.{Payout, PayoutAccepted, PayoutDetail}
import tech.carpentum.sdk.payment.{AuthTokenOperations, OutgoingPaymentsApi, PaymentContext, ResponseException}

import java.time.Duration

/**
 * Wraps all Outgoing Payments API operations for single `idPayout`.
 */
object CreatePayoutScenario {
  /**
   * Creates new instance of the scenario with newly created Authorization token with specified validity.
   */
  @throws[ResponseException]
  def create(context: PaymentContext, idPayout: String, validity: Duration = null): CreatePayoutScenario = {
    val authToken = context.createAuthToken(
      new AuthTokenOperations()
        .grant(OutgoingPaymentsApi.defineCreatePayoutEndpoint.forId(idPayout))
        .grant(OutgoingPaymentsApi.defineGetPayoutEndpoint.forId(idPayout)),
      validity
    ).getToken
    new CreatePayoutScenario(OutgoingPaymentsApi.create(context, authToken), idPayout, authToken)
  }

  /**
   * Creates new instance of the scenario with existing Authorization token.
   */
  @throws[ResponseException]
  def create(context: PaymentContext, idPayout: String, authToken: String) =
    new CreatePayoutScenario(OutgoingPaymentsApi.create(context, authToken), idPayout, authToken)
}

class CreatePayoutScenario private(val outgoingPaymentsApi: OutgoingPaymentsApi, val idPayout: String, override val authToken: String) extends AbstractScenario(authToken) {
  @throws[ResponseException]
  def createPayment(payout: Payout): PayoutAccepted = outgoingPaymentsApi.createPayout(idPayout, payout)


  @throws[ResponseException]
  def getDetail: PayoutDetail = outgoingPaymentsApi.getPayout(idPayout)
}
