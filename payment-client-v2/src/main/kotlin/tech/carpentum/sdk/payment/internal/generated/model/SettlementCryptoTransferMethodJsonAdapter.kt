//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.SettlementCryptoTransferMethod

class SettlementCryptoTransferMethodJsonAdapter {
    @FromJson
    fun fromJson(json: SettlementCryptoTransferMethodJson): SettlementCryptoTransferMethod {
        val builder = SettlementCryptoTransferMethod.builder()
        builder.account(json.account)
        builder.paymentOperatorCode(json.paymentOperatorCode)
        builder.transactionReference(json.transactionReference)
        return builder.build()
    }

    @ToJson
    fun toJson(model: SettlementCryptoTransferMethod): SettlementCryptoTransferMethodJson {
        val json = SettlementCryptoTransferMethodJson()
        json.account = model.account
        json.paymentOperatorCode = model.paymentOperatorCode
        json.paymentMethodCode = model.paymentMethodCode.name
        json.transactionReference = model.transactionReference.orElse(null)
        return json
    }

    @FromJson
    fun fromJsonImpl(model: SettlementCryptoTransferMethod): SettlementCryptoTransferMethodImpl {
        return model as SettlementCryptoTransferMethodImpl
    }

    @ToJson
    fun toJsonImpl(impl: SettlementCryptoTransferMethodImpl): SettlementCryptoTransferMethod {
        return impl
    }

}