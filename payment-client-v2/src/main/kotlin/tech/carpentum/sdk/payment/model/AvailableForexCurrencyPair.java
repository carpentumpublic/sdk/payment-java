//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** AvailableForexCurrencyPair
 *
 * 
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public interface AvailableForexCurrencyPair {

    @NotNull CurrencyCode getBaseCurrencyCode();

    @NotNull CurrencyCode getQuoteCurrencyCode();

    @NotNull static Builder builder(AvailableForexCurrencyPair copyOf) {
        Builder builder = builder();
        builder.baseCurrencyCode(copyOf.getBaseCurrencyCode());
        builder.quoteCurrencyCode(copyOf.getQuoteCurrencyCode());
        return builder;
    }

    @NotNull static Builder builder() {
        return new AvailableForexCurrencyPairImpl.BuilderImpl();
    }

    /** Builder for {@link AvailableForexCurrencyPair} model class. */
    interface Builder {

        /**
          * Set {@link AvailableForexCurrencyPair#getBaseCurrencyCode} property.
          *
          * 
          */
        @NotNull Builder baseCurrencyCode(CurrencyCode baseCurrencyCode);

        boolean isBaseCurrencyCodeDefined();


        /**
          * Set {@link AvailableForexCurrencyPair#getQuoteCurrencyCode} property.
          *
          * 
          */
        @NotNull Builder quoteCurrencyCode(CurrencyCode quoteCurrencyCode);

        boolean isQuoteCurrencyCodeDefined();


        /**
         * Create new instance of {@link AvailableForexCurrencyPair} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @NotNull AvailableForexCurrencyPair build();

    }
}