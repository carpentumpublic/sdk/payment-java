//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** AvailableTopUpOption
 *
 * 
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public interface AvailableTopUpOption {

    @NotNull Money getMoneyPay();

    @NotNull Money getMoneyReceive();

    @NotNull PayinMethodCode getPaymentMethodCode();

    @NotNull java.util.List<@NotNull PaymentOperatorIncoming> getPaymentOperators();

    @NotNull static Builder builder(AvailableTopUpOption copyOf) {
        Builder builder = builder();
        builder.moneyPay(copyOf.getMoneyPay());
        builder.moneyReceive(copyOf.getMoneyReceive());
        builder.paymentMethodCode(copyOf.getPaymentMethodCode());
        builder.paymentOperators(copyOf.getPaymentOperators());
        return builder;
    }

    @NotNull static Builder builder() {
        return new AvailableTopUpOptionImpl.BuilderImpl();
    }

    /** Builder for {@link AvailableTopUpOption} model class. */
    interface Builder {

        /**
          * Set {@link AvailableTopUpOption#getMoneyPay} property.
          *
          * 
          */
        @NotNull Builder moneyPay(Money moneyPay);

        boolean isMoneyPayDefined();


        /**
          * Set {@link AvailableTopUpOption#getMoneyReceive} property.
          *
          * 
          */
        @NotNull Builder moneyReceive(Money moneyReceive);

        boolean isMoneyReceiveDefined();


        /**
          * Set {@link AvailableTopUpOption#getPaymentMethodCode} property.
          *
          * 
          */
        @NotNull Builder paymentMethodCode(PayinMethodCode paymentMethodCode);

        boolean isPaymentMethodCodeDefined();


        /**
          * Replace all items in {@link AvailableTopUpOption#getPaymentOperators} list property.
          *
          * 
          */
        @NotNull Builder paymentOperators(java.util.List<@NotNull PaymentOperatorIncoming> paymentOperators);
        /**
          * Add single item to {@link AvailableTopUpOption#getPaymentOperators} list property.
          *
          * 
          */
        @NotNull Builder paymentOperatorsAdd(PaymentOperatorIncoming item);
        /**
          * Add all items to {@link AvailableTopUpOption#getPaymentOperators} list property.
          *
          * 
          */
        @NotNull Builder paymentOperatorsAddAll(java.util.List<@NotNull PaymentOperatorIncoming> paymentOperators);


        /**
         * Create new instance of {@link AvailableTopUpOption} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @NotNull AvailableTopUpOption build();

    }
}