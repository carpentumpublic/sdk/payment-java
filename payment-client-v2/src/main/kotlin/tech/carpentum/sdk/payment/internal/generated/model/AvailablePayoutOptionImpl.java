//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** AvailablePayoutOption
 *
 * 
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public class AvailablePayoutOptionImpl implements AvailablePayoutOption {
    private final PayoutMethodCode paymentMethodCode;

    @Override
    public PayoutMethodCode getPaymentMethodCode() {
        return paymentMethodCode;
    }


    private final java.util.List<@NotNull PaymentOperatorOutgoing> paymentOperators;

    @Override
    public java.util.List<@NotNull PaymentOperatorOutgoing> getPaymentOperators() {
        return paymentOperators;
    }


    private final Optional<SegmentCode> segmentCode;

    @Override
    public Optional<SegmentCode> getSegmentCode() {
        return segmentCode;
    }




    private final int hashCode;
    private final String toString;

    private AvailablePayoutOptionImpl(BuilderImpl builder) {
        this.paymentMethodCode = Objects.requireNonNull(builder.paymentMethodCode, "Property 'paymentMethodCode' is required.");
        this.paymentOperators = java.util.Collections.unmodifiableList(builder.paymentOperators);
        this.segmentCode = Optional.ofNullable(builder.segmentCode);

        this.hashCode = Objects.hash(paymentMethodCode, paymentOperators, segmentCode);
        this.toString = builder.type + "(" +
                "paymentMethodCode=" + paymentMethodCode +
                ", paymentOperators=" + paymentOperators +
                ", segmentCode=" + segmentCode +
                ')';
    }

    @Override
    public int hashCode() {
        return hashCode;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }

        if (!(obj instanceof AvailablePayoutOptionImpl)) {
            return false;
        }

        AvailablePayoutOptionImpl that = (AvailablePayoutOptionImpl) obj;
        if (!Objects.equals(this.paymentMethodCode, that.paymentMethodCode)) return false;
        if (!Objects.equals(this.paymentOperators, that.paymentOperators)) return false;
        if (!Objects.equals(this.segmentCode, that.segmentCode)) return false;

        return true;
    }

    @Override
    public String toString() {
        return toString;
    }

    /** Builder for {@link AvailablePayoutOption} model class. */
    public static class BuilderImpl implements AvailablePayoutOption.Builder {
        private PayoutMethodCode paymentMethodCode = null;
        private java.util.List<@NotNull PaymentOperatorOutgoing> paymentOperators = new java.util.ArrayList<>();
        private SegmentCode segmentCode = null;

        private final String type;

        public BuilderImpl(String type) {
            this.type = type;
        }

        public BuilderImpl() {
            this("AvailablePayoutOption");
        }

        /**
          * Set {@link AvailablePayoutOption#getPaymentMethodCode} property.
          *
          * 
          */
        @Override
        public BuilderImpl paymentMethodCode(PayoutMethodCode paymentMethodCode) {
            this.paymentMethodCode = paymentMethodCode;
            return this;
        }

        @Override
        public boolean isPaymentMethodCodeDefined() {
            return this.paymentMethodCode != null;
        }

        /**
          * Replace all items in {@link AvailablePayoutOption#getPaymentOperators} list property.
          *
          * 
          */
        @Override
        public BuilderImpl paymentOperators(java.util.List<@NotNull PaymentOperatorOutgoing> paymentOperators) {
            this.paymentOperators.clear();
            if (paymentOperators != null) {
                this.paymentOperators.addAll(paymentOperators);
            }
            return this;
        }
        /**
          * Add single item to {@link AvailablePayoutOption#getPaymentOperators} list property.
          *
          * 
          */
        @Override
        public BuilderImpl paymentOperatorsAdd(PaymentOperatorOutgoing item) {
            if (item != null) {
                this.paymentOperators.add(item);
            }
            return this;
        }
        /**
          * Add all items to {@link AvailablePayoutOption#getPaymentOperators} list property.
          *
          * 
          */
        @Override
        public BuilderImpl paymentOperatorsAddAll(java.util.List<@NotNull PaymentOperatorOutgoing> paymentOperators) {
            if (paymentOperators != null) {
                this.paymentOperators.addAll(paymentOperators);
            }
            return this;
        }


        /**
          * Set {@link AvailablePayoutOption#getSegmentCode} property.
          *
          * 
          */
        @Override
        public BuilderImpl segmentCode(SegmentCode segmentCode) {
            this.segmentCode = segmentCode;
            return this;
        }

        @Override
        public boolean isSegmentCodeDefined() {
            return this.segmentCode != null;
        }

        /**
         * Create new instance of {@link AvailablePayoutOption} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @Override
        public AvailablePayoutOptionImpl build() {
            return new AvailablePayoutOptionImpl(this);
        }

    }
}