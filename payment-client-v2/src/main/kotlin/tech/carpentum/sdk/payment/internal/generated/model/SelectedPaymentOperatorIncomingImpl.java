//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** SelectedPaymentOperatorIncoming
 *
 * 
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public class SelectedPaymentOperatorIncomingImpl implements SelectedPaymentOperatorIncoming {
    /** One of following can serve as Payment Operator:

 * Financial or other institution (such as bank, card payment processor, ...) that manages transactions for your customers
 * Mobile wallet
 * Blockchain protocol for crypto currency payments

Customer is informed with the payment instructions where funds have to be transferred efficiently based on the selected Payment operator.
For getting list of the available payment options for payins use [POST /payins/!availablePaymentOptions](#operations-Incoming_payments-availablePaymentOptions) API, for payouts use [POST /payouts/!availablePaymentOptions](#operations-Outgoing_payments-availablePaymentOptions) API. */
    private final String code;

    @Override
    public String getCode() {
        return code;
    }


    private final String name;

    @Override
    public String getName() {
        return name;
    }




    private final int hashCode;
    private final String toString;

    private SelectedPaymentOperatorIncomingImpl(BuilderImpl builder) {
        this.code = Objects.requireNonNull(builder.code, "Property 'code' is required.");
        this.name = Objects.requireNonNull(builder.name, "Property 'name' is required.");

        this.hashCode = Objects.hash(code, name);
        this.toString = builder.type + "(" +
                "code=" + code +
                ", name=" + name +
                ')';
    }

    @Override
    public int hashCode() {
        return hashCode;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }

        if (!(obj instanceof SelectedPaymentOperatorIncomingImpl)) {
            return false;
        }

        SelectedPaymentOperatorIncomingImpl that = (SelectedPaymentOperatorIncomingImpl) obj;
        if (!Objects.equals(this.code, that.code)) return false;
        if (!Objects.equals(this.name, that.name)) return false;

        return true;
    }

    @Override
    public String toString() {
        return toString;
    }

    /** Builder for {@link SelectedPaymentOperatorIncoming} model class. */
    public static class BuilderImpl implements SelectedPaymentOperatorIncoming.Builder {
        private String code = null;
        private String name = null;

        private final String type;

        public BuilderImpl(String type) {
            this.type = type;
        }

        public BuilderImpl() {
            this("SelectedPaymentOperatorIncoming");
        }

        /**
          * Set {@link SelectedPaymentOperatorIncoming#getCode} property.
          *
          * One of following can serve as Payment Operator:

 * Financial or other institution (such as bank, card payment processor, ...) that manages transactions for your customers
 * Mobile wallet
 * Blockchain protocol for crypto currency payments

Customer is informed with the payment instructions where funds have to be transferred efficiently based on the selected Payment operator.
For getting list of the available payment options for payins use [POST /payins/!availablePaymentOptions](#operations-Incoming_payments-availablePaymentOptions) API, for payouts use [POST /payouts/!availablePaymentOptions](#operations-Outgoing_payments-availablePaymentOptions) API.
          */
        @Override
        public BuilderImpl code(String code) {
            this.code = code;
            return this;
        }

        @Override
        public boolean isCodeDefined() {
            return this.code != null;
        }

        /**
          * Set {@link SelectedPaymentOperatorIncoming#getName} property.
          *
          * 
          */
        @Override
        public BuilderImpl name(String name) {
            this.name = name;
            return this;
        }

        @Override
        public boolean isNameDefined() {
            return this.name != null;
        }

        /**
         * Create new instance of {@link SelectedPaymentOperatorIncoming} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @Override
        public SelectedPaymentOperatorIncomingImpl build() {
            return new SelectedPaymentOperatorIncomingImpl(this);
        }

    }
}