//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** PayinMethodResponse
 *
 * 
 *
 * @see VaPayMethodResponse
 * @see OfflineMethodResponse
 * @see CapitecPayMethodResponse
 * @see NetBankingMethodResponse
 * @see QrPhMethodResponse
 * @see VaPayVerifMethodResponse
 * @see OnlineMethodResponse
 * @see PayMeMethodResponse
 * @see QrisPayMethodResponse
 * @see UpiIdMethodResponse
 * @see IMPSMethodResponse
 * @see CodePaymentMethodResponse
 * @see DuitNowPayMethodResponse
 * @see EWalletMethodResponse
 * @see VietQRMethodResponse
 * @see MobileMoneyMethodResponse
 * @see UpiQRMethodResponse
 * @see PromptPayMethodResponse
 * @see AbsaPayMethodResponse
 * @see CryptoOfflineMethodResponse
 * @see P2AV2MethodResponse
 * @see PayShapMethodResponse
 *
 * The model class is immutable.
 *
 *
 */
public interface PayinMethodResponse {
    /** Name of discriminator property used to distinguish between "one-of" subclasses. */
    String DISCRIMINATOR = "paymentMethodCode";

    /**
     * @see PaymentMethodCode
     */
    @NotNull PaymentMethodCode getPaymentMethodCode();

    /**
     * Enumeration of "one-of" subclasses distinguished by {@code paymentMethodCode} discriminator value.
     */
    enum PaymentMethodCode {
        ABSA_PAY,
        CAPITEC_PAY,
        CODE_PAYMENT,
        CRYPTO_OFFLINE,
        DUITNOW,
        EWALLET,
        IMPS,
        MOBILE_MONEY,
        NETBANKING,
        OFFLINE,
        ONLINE,
        P2A_V2,
        PAYSHAP,
        PAY_ME,
        PROMPTPAY,
        QRISPAY,
        QRPH,
        UPIID,
        UPIQR,
        VAPAY,
        VAPAY_VERIF,
        VIETQR,
    }
}