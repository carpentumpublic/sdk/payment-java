//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** MoneyVat
 *
 * 
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public class MoneyVatImpl implements MoneyVat {
    /** Amount of Value-Added Tax is returned in case it's levied on a payment order. */
    private final java.math.BigDecimal amount;

    @Override
    public java.math.BigDecimal getAmount() {
        return amount;
    }


    private final CurrencyCode currencyCode;

    @Override
    public CurrencyCode getCurrencyCode() {
        return currencyCode;
    }




    private final int hashCode;
    private final String toString;

    private MoneyVatImpl(BuilderImpl builder) {
        this.amount = Objects.requireNonNull(builder.amount, "Property 'amount' is required.");
        this.currencyCode = Objects.requireNonNull(builder.currencyCode, "Property 'currencyCode' is required.");

        this.hashCode = Objects.hash(amount, currencyCode);
        this.toString = builder.type + "(" +
                "amount=" + amount +
                ", currencyCode=" + currencyCode +
                ')';
    }

    @Override
    public int hashCode() {
        return hashCode;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }

        if (!(obj instanceof MoneyVatImpl)) {
            return false;
        }

        MoneyVatImpl that = (MoneyVatImpl) obj;
        if (!Objects.equals(this.amount, that.amount)) return false;
        if (!Objects.equals(this.currencyCode, that.currencyCode)) return false;

        return true;
    }

    @Override
    public String toString() {
        return toString;
    }

    /** Builder for {@link MoneyVat} model class. */
    public static class BuilderImpl implements MoneyVat.Builder {
        private java.math.BigDecimal amount = null;
        private CurrencyCode currencyCode = null;

        private final String type;

        public BuilderImpl(String type) {
            this.type = type;
        }

        public BuilderImpl() {
            this("MoneyVat");
        }

        /**
          * Set {@link MoneyVat#getAmount} property.
          *
          * Amount of Value-Added Tax is returned in case it's levied on a payment order.
          */
        @Override
        public BuilderImpl amount(java.math.BigDecimal amount) {
            this.amount = amount;
            return this;
        }

        @Override
        public boolean isAmountDefined() {
            return this.amount != null;
        }

        /**
          * Set {@link MoneyVat#getCurrencyCode} property.
          *
          * 
          */
        @Override
        public BuilderImpl currencyCode(CurrencyCode currencyCode) {
            this.currencyCode = currencyCode;
            return this;
        }

        @Override
        public boolean isCurrencyCodeDefined() {
            return this.currencyCode != null;
        }

        /**
         * Create new instance of {@link MoneyVat} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @Override
        public MoneyVatImpl build() {
            return new MoneyVatImpl(this);
        }

    }
}