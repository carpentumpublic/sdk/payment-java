package tech.carpentum.sdk.payment;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Supplier;

/**
 * Builder to specify set of operations to be authorized via {@link PaymentContext#createAuthToken} methods.
 */
public class AuthTokenOperations implements Supplier<List<String>> {
    private final Set<String> grants;

    public AuthTokenOperations() {
        this.grants = new HashSet<>();
    }

    public AuthTokenOperations(@NotNull EndpointDefinition definition) {
        this();
        grant(definition);
    }

    private @NotNull AuthTokenOperations grant(@NotNull EndpointDefinition.Method method, @NotNull String resource) {
        grants.add(method + " " + resource);
        return this;
    }

    //
    // generic API
    //

    public @NotNull AuthTokenOperations grant(@NotNull EndpointDefinition definition) {
        return grant(definition.getMethod(), definition.getResource());
    }

    public @NotNull AuthTokenOperations grantGet(@NotNull String resource) {
        return grant(EndpointDefinition.Method.GET, resource);
    }

    public @NotNull AuthTokenOperations grantPost(@NotNull String resource) {
        return grant(EndpointDefinition.Method.POST, resource);
    }

    //
    // Supplier<List<String>>
    //

    /**
     * Returns new {@link List} instance of formatted operations attributes to restrict the privileges of the token holder.
     */
    @Override
    public @NotNull List<String> get() {
        return new ArrayList<>(grants);
    }

}
