//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** PaymentAccountDetail
 *
 * 
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public class PaymentAccountDetailImpl implements PaymentAccountDetail {
    private final AccountResponseDetail customerAccountRequest;

    @Override
    public AccountResponseDetail getCustomerAccountRequest() {
        return customerAccountRequest;
    }


    private final AccountResponseDetail customerAccountProcess;

    @Override
    public AccountResponseDetail getCustomerAccountProcess() {
        return customerAccountProcess;
    }




    private final int hashCode;
    private final String toString;

    private PaymentAccountDetailImpl(BuilderImpl builder) {
        this.customerAccountRequest = Objects.requireNonNull(builder.customerAccountRequest, "Property 'customerAccountRequest' is required.");
        this.customerAccountProcess = Objects.requireNonNull(builder.customerAccountProcess, "Property 'customerAccountProcess' is required.");

        this.hashCode = Objects.hash(customerAccountRequest, customerAccountProcess);
        this.toString = builder.type + "(" +
                "customerAccountRequest=" + customerAccountRequest +
                ", customerAccountProcess=" + customerAccountProcess +
                ')';
    }

    @Override
    public int hashCode() {
        return hashCode;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }

        if (!(obj instanceof PaymentAccountDetailImpl)) {
            return false;
        }

        PaymentAccountDetailImpl that = (PaymentAccountDetailImpl) obj;
        if (!Objects.equals(this.customerAccountRequest, that.customerAccountRequest)) return false;
        if (!Objects.equals(this.customerAccountProcess, that.customerAccountProcess)) return false;

        return true;
    }

    @Override
    public String toString() {
        return toString;
    }

    /** Builder for {@link PaymentAccountDetail} model class. */
    public static class BuilderImpl implements PaymentAccountDetail.Builder {
        private AccountResponseDetail customerAccountRequest = null;
        private AccountResponseDetail customerAccountProcess = null;

        private final String type;

        public BuilderImpl(String type) {
            this.type = type;
        }

        public BuilderImpl() {
            this("PaymentAccountDetail");
        }

        /**
          * Set {@link PaymentAccountDetail#getCustomerAccountRequest} property.
          *
          * 
          */
        @Override
        public BuilderImpl customerAccountRequest(AccountResponseDetail customerAccountRequest) {
            this.customerAccountRequest = customerAccountRequest;
            return this;
        }

        @Override
        public boolean isCustomerAccountRequestDefined() {
            return this.customerAccountRequest != null;
        }

        /**
          * Set {@link PaymentAccountDetail#getCustomerAccountProcess} property.
          *
          * 
          */
        @Override
        public BuilderImpl customerAccountProcess(AccountResponseDetail customerAccountProcess) {
            this.customerAccountProcess = customerAccountProcess;
            return this;
        }

        @Override
        public boolean isCustomerAccountProcessDefined() {
            return this.customerAccountProcess != null;
        }

        /**
         * Create new instance of {@link PaymentAccountDetail} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @Override
        public PaymentAccountDetailImpl build() {
            return new PaymentAccountDetailImpl(this);
        }

    }
}