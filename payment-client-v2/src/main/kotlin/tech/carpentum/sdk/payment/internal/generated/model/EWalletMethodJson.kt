//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.JsonClass;
import tech.carpentum.sdk.payment.model.*;

@JsonClass(generateAdapter = true)
class EWalletMethodJson {
    var account: AccountPayinRequestEWallet? = null
    var emailAddress: String? = null
    var phoneNumber: String? = null
    var paymentOperatorCode: String? = null
    var paymentMethodCode: String? = null
}