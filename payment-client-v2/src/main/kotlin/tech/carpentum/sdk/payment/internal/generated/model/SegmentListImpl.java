//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** SegmentList
 *
 * List of available segments.
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public class SegmentListImpl implements SegmentList {
    private final java.util.List<@NotNull Segment> data;

    @Override
    public java.util.List<@NotNull Segment> getData() {
        return data;
    }




    private final int hashCode;
    private final String toString;

    private SegmentListImpl(BuilderImpl builder) {
        this.data = java.util.Collections.unmodifiableList(builder.data);

        this.hashCode = Objects.hash(data);
        this.toString = builder.type + "(" +
                "data=" + data +
                ')';
    }

    @Override
    public int hashCode() {
        return hashCode;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }

        if (!(obj instanceof SegmentListImpl)) {
            return false;
        }

        SegmentListImpl that = (SegmentListImpl) obj;
        if (!Objects.equals(this.data, that.data)) return false;

        return true;
    }

    @Override
    public String toString() {
        return toString;
    }

    /** Builder for {@link SegmentList} model class. */
    public static class BuilderImpl implements SegmentList.Builder {
        private java.util.List<@NotNull Segment> data = new java.util.ArrayList<>();

        private final String type;

        public BuilderImpl(String type) {
            this.type = type;
        }

        public BuilderImpl() {
            this("SegmentList");
        }

        /**
          * Replace all items in {@link SegmentList#getData} list property.
          *
          * 
          */
        @Override
        public BuilderImpl data(java.util.List<@NotNull Segment> data) {
            this.data.clear();
            if (data != null) {
                this.data.addAll(data);
            }
            return this;
        }
        /**
          * Add single item to {@link SegmentList#getData} list property.
          *
          * 
          */
        @Override
        public BuilderImpl dataAdd(Segment item) {
            if (item != null) {
                this.data.add(item);
            }
            return this;
        }
        /**
          * Add all items to {@link SegmentList#getData} list property.
          *
          * 
          */
        @Override
        public BuilderImpl dataAddAll(java.util.List<@NotNull Segment> data) {
            if (data != null) {
                this.data.addAll(data);
            }
            return this;
        }


        /**
         * Create new instance of {@link SegmentList} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @Override
        public SegmentListImpl build() {
            return new SegmentListImpl(this);
        }

    }
}