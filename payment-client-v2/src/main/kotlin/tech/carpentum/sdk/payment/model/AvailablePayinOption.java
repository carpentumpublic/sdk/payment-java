//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** AvailablePayinOption
 *
 * 
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public interface AvailablePayinOption {

    @NotNull PayinMethodCode getPaymentMethodCode();

    @NotNull java.util.List<@NotNull PaymentOperatorIncoming> getPaymentOperators();

    @NotNull Optional<SegmentCode> getSegmentCode();

    @NotNull static Builder builder(AvailablePayinOption copyOf) {
        Builder builder = builder();
        builder.paymentMethodCode(copyOf.getPaymentMethodCode());
        builder.paymentOperators(copyOf.getPaymentOperators());
        builder.segmentCode(copyOf.getSegmentCode().orElse(null));
        return builder;
    }

    @NotNull static Builder builder() {
        return new AvailablePayinOptionImpl.BuilderImpl();
    }

    /** Builder for {@link AvailablePayinOption} model class. */
    interface Builder {

        /**
          * Set {@link AvailablePayinOption#getPaymentMethodCode} property.
          *
          * 
          */
        @NotNull Builder paymentMethodCode(PayinMethodCode paymentMethodCode);

        boolean isPaymentMethodCodeDefined();


        /**
          * Replace all items in {@link AvailablePayinOption#getPaymentOperators} list property.
          *
          * 
          */
        @NotNull Builder paymentOperators(java.util.List<@NotNull PaymentOperatorIncoming> paymentOperators);
        /**
          * Add single item to {@link AvailablePayinOption#getPaymentOperators} list property.
          *
          * 
          */
        @NotNull Builder paymentOperatorsAdd(PaymentOperatorIncoming item);
        /**
          * Add all items to {@link AvailablePayinOption#getPaymentOperators} list property.
          *
          * 
          */
        @NotNull Builder paymentOperatorsAddAll(java.util.List<@NotNull PaymentOperatorIncoming> paymentOperators);


        /**
          * Set {@link AvailablePayinOption#getSegmentCode} property.
          *
          * 
          */
        @NotNull Builder segmentCode(SegmentCode segmentCode);

        boolean isSegmentCodeDefined();


        /**
         * Create new instance of {@link AvailablePayinOption} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @NotNull AvailablePayinOption build();

    }
}