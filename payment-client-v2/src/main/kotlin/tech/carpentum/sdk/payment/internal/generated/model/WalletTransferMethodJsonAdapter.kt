//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.WalletTransferMethod

class WalletTransferMethodJsonAdapter {
    @FromJson
    fun fromJson(json: WalletTransferMethodJson): WalletTransferMethod {
        val builder = WalletTransferMethod.builder()
        builder.account(json.account)
        builder.paymentOperatorCode(json.paymentOperatorCode)
        builder.phoneNumber(json.phoneNumber)
        builder.remark(json.remark)
        return builder.build()
    }

    @ToJson
    fun toJson(model: WalletTransferMethod): WalletTransferMethodJson {
        val json = WalletTransferMethodJson()
        json.account = model.account
        json.paymentOperatorCode = model.paymentOperatorCode.orElse(null)
        json.phoneNumber = model.phoneNumber.orElse(null)
        json.remark = model.remark.orElse(null)
        json.paymentMethodCode = model.paymentMethodCode.name
        return json
    }

    @FromJson
    fun fromJsonImpl(model: WalletTransferMethod): WalletTransferMethodImpl {
        return model as WalletTransferMethodImpl
    }

    @ToJson
    fun toJsonImpl(impl: WalletTransferMethodImpl): WalletTransferMethod {
        return impl
    }

}