//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** PostAuthTokensError
 *
 * 
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public class PostAuthTokensError extends BusinessValidationError {
    // tag::codeEnum[]
    /** @see #getCode */
    public static final String CODE_ACTION_INVALID = "ACTION_INVALID";
    /** @see #getCode */
    public static final String CODE_ID_PAYMENT_MISSING = "ID_PAYMENT_MISSING";
    /** @see #getCode */
    public static final String CODE_MERCHANT_INACTIVE = "MERCHANT_INACTIVE";
    /** @see #getCode */
    public static final String CODE_MERCHANT_NOT_FOUND = "MERCHANT_NOT_FOUND";
    // end::codeEnum[]








    private PostAuthTokensError(PostAuthTokensError.Builder builder) {
        super(builder);
    }

    @NotNull public static Builder builder() {
        return new Builder();
    }

    /** Builder for {@link PostAuthTokensError} model class. */
    public static class Builder extends BusinessValidationError.Builder<PostAuthTokensError, Builder> {
        private Builder() {}

        /**
         * Create new instance of {@link PostAuthTokensError} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @NotNull public PostAuthTokensError build() {
            return new PostAuthTokensError(this);
        }
    }
}