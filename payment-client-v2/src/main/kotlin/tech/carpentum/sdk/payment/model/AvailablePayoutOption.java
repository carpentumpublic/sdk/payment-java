//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** AvailablePayoutOption
 *
 * 
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public interface AvailablePayoutOption {

    @NotNull PayoutMethodCode getPaymentMethodCode();

    @NotNull java.util.List<@NotNull PaymentOperatorOutgoing> getPaymentOperators();

    @NotNull Optional<SegmentCode> getSegmentCode();

    @NotNull static Builder builder(AvailablePayoutOption copyOf) {
        Builder builder = builder();
        builder.paymentMethodCode(copyOf.getPaymentMethodCode());
        builder.paymentOperators(copyOf.getPaymentOperators());
        builder.segmentCode(copyOf.getSegmentCode().orElse(null));
        return builder;
    }

    @NotNull static Builder builder() {
        return new AvailablePayoutOptionImpl.BuilderImpl();
    }

    /** Builder for {@link AvailablePayoutOption} model class. */
    interface Builder {

        /**
          * Set {@link AvailablePayoutOption#getPaymentMethodCode} property.
          *
          * 
          */
        @NotNull Builder paymentMethodCode(PayoutMethodCode paymentMethodCode);

        boolean isPaymentMethodCodeDefined();


        /**
          * Replace all items in {@link AvailablePayoutOption#getPaymentOperators} list property.
          *
          * 
          */
        @NotNull Builder paymentOperators(java.util.List<@NotNull PaymentOperatorOutgoing> paymentOperators);
        /**
          * Add single item to {@link AvailablePayoutOption#getPaymentOperators} list property.
          *
          * 
          */
        @NotNull Builder paymentOperatorsAdd(PaymentOperatorOutgoing item);
        /**
          * Add all items to {@link AvailablePayoutOption#getPaymentOperators} list property.
          *
          * 
          */
        @NotNull Builder paymentOperatorsAddAll(java.util.List<@NotNull PaymentOperatorOutgoing> paymentOperators);


        /**
          * Set {@link AvailablePayoutOption#getSegmentCode} property.
          *
          * 
          */
        @NotNull Builder segmentCode(SegmentCode segmentCode);

        boolean isSegmentCodeDefined();


        /**
         * Create new instance of {@link AvailablePayoutOption} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @NotNull AvailablePayoutOption build();

    }
}