//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** ProcessorStatus
 *
 * If present, this field specifies the status of the payment processing by an external processor.

Possible values:
 * `ACCEPTED` - The payment was accepted by the processor.
 * `PROCESSED` - The payment has been processed. For the result of the processing, check the "status" field in the response.
 *
 * 
 *
 * The model class is immutable.
 *
 * Use static {@link #of} method to create a new model class instance.
 */
public interface ProcessorStatus {
    ProcessorStatus ACCEPTED = ProcessorStatus.of("ACCEPTED");
    ProcessorStatus PROCESSED = ProcessorStatus.of("PROCESSED");

    @NotNull String getValue();

    static ProcessorStatus of(@NotNull String value) {
        return new ProcessorStatusImpl(value);
    }
}