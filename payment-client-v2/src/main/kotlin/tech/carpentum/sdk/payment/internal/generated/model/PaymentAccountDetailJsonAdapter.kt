//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.PaymentAccountDetail

class PaymentAccountDetailJsonAdapter {
    @FromJson
    fun fromJson(json: PaymentAccountDetailJson): PaymentAccountDetail {
        val builder = PaymentAccountDetail.builder()
        builder.customerAccountRequest(json.customerAccountRequest)
        builder.customerAccountProcess(json.customerAccountProcess)
        return builder.build()
    }

    @ToJson
    fun toJson(model: PaymentAccountDetail): PaymentAccountDetailJson {
        val json = PaymentAccountDetailJson()
        json.customerAccountRequest = model.customerAccountRequest
        json.customerAccountProcess = model.customerAccountProcess
        return json
    }

    @FromJson
    fun fromJsonImpl(model: PaymentAccountDetail): PaymentAccountDetailImpl {
        return model as PaymentAccountDetailImpl
    }

    @ToJson
    fun toJsonImpl(impl: PaymentAccountDetailImpl): PaymentAccountDetail {
        return impl
    }

}