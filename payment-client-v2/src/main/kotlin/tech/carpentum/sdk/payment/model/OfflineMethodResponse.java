//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** OFFLINE
 *
 * The account parameters for this payment method are used to show payment instructions to the customer.
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public interface OfflineMethodResponse extends PayinMethodResponse {
    /** A discriminator value of property {@link #getPaymentMethodCode}. The model class extends {@link PayinMethodResponse}. */
    PayinMethodResponse.PaymentMethodCode PAYMENT_METHOD_CODE = PayinMethodResponse.PaymentMethodCode.OFFLINE;

    @NotNull IdPayin getIdPayin();

    @NotNull IdPayment getIdPayment();

    @NotNull AccountResponseOffline getAccount();

    @NotNull AccountCustomerResponseOffline getAccountCustomer();

    @NotNull MoneyPaymentResponse getMoney();

    @NotNull Optional<MoneyRequired> getMoneyRequired();

    @NotNull Optional<MoneyVat> getVat();

    /** Reference number of transaction. */
    @NotNull String getReference();

    @NotNull Optional<PaymentProcessor> getProcessor();

    /** The name of the QR code image to be scanned by a wallet or payment service compatible with this payment method. The QR code of the image can be labeled by qrName to increase the clarity of the payment instruction.
If this parameter contains any value, include it in the payment instructions for your customer. */
    @NotNull Optional<String> getQrName();

    /** The URL of the QR code image to be scanned by a wallet or payment service compatible with this payment method. The QR code encodes the instructions how make a payment.
If this parameter contains any value, include it in the payment instructions for your customer.

This parameter is returned for the LAK and VND currency. */
    @NotNull Optional<String> getQrCode();

    /** This is the URL where the customers will be redirected after completing a payment.

The URL must be either IP or domain-based. */
    @NotNull String getReturnUrl();

    @NotNull Optional<SelectedPaymentOperatorIncoming> getPaymentOperator();

    /** Identification of a product related to a payment order. This field is returned only if it is provided in a payment order request. */
    @NotNull Optional<String> getProductId();

    /** Date and time when payment was accepted. */
    @NotNull java.time.OffsetDateTime getAcceptedAt();

    /** Date and time of payment expiration. If no money has been transferred to this time, payment is considered failed and callback with status change event will shortly follow. */
    @NotNull java.time.OffsetDateTime getExpireAt();

    @NotNull static Builder builder(OfflineMethodResponse copyOf) {
        Builder builder = builder();
        builder.idPayin(copyOf.getIdPayin());
        builder.idPayment(copyOf.getIdPayment());
        builder.account(copyOf.getAccount());
        builder.accountCustomer(copyOf.getAccountCustomer());
        builder.money(copyOf.getMoney());
        builder.moneyRequired(copyOf.getMoneyRequired().orElse(null));
        builder.vat(copyOf.getVat().orElse(null));
        builder.reference(copyOf.getReference());
        builder.processor(copyOf.getProcessor().orElse(null));
        builder.qrName(copyOf.getQrName().orElse(null));
        builder.qrCode(copyOf.getQrCode().orElse(null));
        builder.returnUrl(copyOf.getReturnUrl());
        builder.paymentOperator(copyOf.getPaymentOperator().orElse(null));
        builder.productId(copyOf.getProductId().orElse(null));
        builder.acceptedAt(copyOf.getAcceptedAt());
        builder.expireAt(copyOf.getExpireAt());
        return builder;
    }

    @NotNull static Builder builder() {
        return new OfflineMethodResponseImpl.BuilderImpl();
    }

    /** Builder for {@link OfflineMethodResponse} model class. */
    interface Builder {

        /**
          * Set {@link OfflineMethodResponse#getIdPayin} property.
          *
          * 
          */
        @NotNull Builder idPayin(IdPayin idPayin);

        boolean isIdPayinDefined();


        /**
          * Set {@link OfflineMethodResponse#getIdPayment} property.
          *
          * 
          */
        @NotNull Builder idPayment(IdPayment idPayment);

        boolean isIdPaymentDefined();


        /**
          * Set {@link OfflineMethodResponse#getAccount} property.
          *
          * 
          */
        @NotNull Builder account(AccountResponseOffline account);

        boolean isAccountDefined();


        /**
          * Set {@link OfflineMethodResponse#getAccountCustomer} property.
          *
          * 
          */
        @NotNull Builder accountCustomer(AccountCustomerResponseOffline accountCustomer);

        boolean isAccountCustomerDefined();


        /**
          * Set {@link OfflineMethodResponse#getMoney} property.
          *
          * 
          */
        @NotNull Builder money(MoneyPaymentResponse money);

        boolean isMoneyDefined();


        /**
          * Set {@link OfflineMethodResponse#getMoneyRequired} property.
          *
          * 
          */
        @NotNull Builder moneyRequired(MoneyRequired moneyRequired);

        boolean isMoneyRequiredDefined();


        /**
          * Set {@link OfflineMethodResponse#getVat} property.
          *
          * 
          */
        @NotNull Builder vat(MoneyVat vat);

        boolean isVatDefined();


        /**
          * Set {@link OfflineMethodResponse#getReference} property.
          *
          * Reference number of transaction.
          */
        @NotNull Builder reference(String reference);

        boolean isReferenceDefined();


        /**
          * Set {@link OfflineMethodResponse#getProcessor} property.
          *
          * 
          */
        @NotNull Builder processor(PaymentProcessor processor);

        boolean isProcessorDefined();


        /**
          * Set {@link OfflineMethodResponse#getQrName} property.
          *
          * The name of the QR code image to be scanned by a wallet or payment service compatible with this payment method. The QR code of the image can be labeled by qrName to increase the clarity of the payment instruction.
If this parameter contains any value, include it in the payment instructions for your customer.
          */
        @NotNull Builder qrName(String qrName);

        boolean isQrNameDefined();


        /**
          * Set {@link OfflineMethodResponse#getQrCode} property.
          *
          * The URL of the QR code image to be scanned by a wallet or payment service compatible with this payment method. The QR code encodes the instructions how make a payment.
If this parameter contains any value, include it in the payment instructions for your customer.

This parameter is returned for the LAK and VND currency.
          */
        @NotNull Builder qrCode(String qrCode);

        boolean isQrCodeDefined();


        /**
          * Set {@link OfflineMethodResponse#getReturnUrl} property.
          *
          * This is the URL where the customers will be redirected after completing a payment.

The URL must be either IP or domain-based.
          */
        @NotNull Builder returnUrl(String returnUrl);

        boolean isReturnUrlDefined();


        /**
          * Set {@link OfflineMethodResponse#getPaymentOperator} property.
          *
          * 
          */
        @NotNull Builder paymentOperator(SelectedPaymentOperatorIncoming paymentOperator);

        boolean isPaymentOperatorDefined();


        /**
          * Set {@link OfflineMethodResponse#getProductId} property.
          *
          * Identification of a product related to a payment order. This field is returned only if it is provided in a payment order request.
          */
        @NotNull Builder productId(String productId);

        boolean isProductIdDefined();


        /**
          * Set {@link OfflineMethodResponse#getAcceptedAt} property.
          *
          * Date and time when payment was accepted.
          */
        @NotNull Builder acceptedAt(java.time.OffsetDateTime acceptedAt);

        boolean isAcceptedAtDefined();


        /**
          * Set {@link OfflineMethodResponse#getExpireAt} property.
          *
          * Date and time of payment expiration. If no money has been transferred to this time, payment is considered failed and callback with status change event will shortly follow.
          */
        @NotNull Builder expireAt(java.time.OffsetDateTime expireAt);

        boolean isExpireAtDefined();


        /**
         * Create new instance of {@link OfflineMethodResponse} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @NotNull OfflineMethodResponse build();

    }
}