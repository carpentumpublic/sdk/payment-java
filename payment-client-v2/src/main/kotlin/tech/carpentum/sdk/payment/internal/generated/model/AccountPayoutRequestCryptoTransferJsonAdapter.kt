//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.AccountPayoutRequestCryptoTransfer

class AccountPayoutRequestCryptoTransferJsonAdapter {
    @FromJson
    fun fromJson(json: AccountPayoutRequestCryptoTransferJson): AccountPayoutRequestCryptoTransfer {
        val builder = AccountPayoutRequestCryptoTransfer.builder()
        builder.accountNumber(json.accountNumber)
        return builder.build()
    }

    @ToJson
    fun toJson(model: AccountPayoutRequestCryptoTransfer): AccountPayoutRequestCryptoTransferJson {
        val json = AccountPayoutRequestCryptoTransferJson()
        json.accountNumber = model.accountNumber
        return json
    }

    @FromJson
    fun fromJsonImpl(model: AccountPayoutRequestCryptoTransfer): AccountPayoutRequestCryptoTransferImpl {
        return model as AccountPayoutRequestCryptoTransferImpl
    }

    @ToJson
    fun toJsonImpl(impl: AccountPayoutRequestCryptoTransferImpl): AccountPayoutRequestCryptoTransfer {
        return impl
    }

}