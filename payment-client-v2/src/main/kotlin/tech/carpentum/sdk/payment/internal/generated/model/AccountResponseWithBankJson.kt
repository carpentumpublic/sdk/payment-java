//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.JsonClass;
import tech.carpentum.sdk.payment.model.*;

@JsonClass(generateAdapter = true)
class AccountResponseWithBankJson {
    var accountName: String? = null
    var accountNumber: String? = null
    var bankCode: String? = null
    var bankName: String? = null
    var bankBranch: String? = null
    var bankCity: String? = null
    var bankProvince: String? = null
}