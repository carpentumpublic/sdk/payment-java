//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** TopUpRequestedDetail
 *
 * The requested payment.
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public interface TopUpRequestedDetail {

    @NotNull Money getMoney();

    @NotNull Optional<Money> getExchangedFrom();

    @NotNull static Builder builder(TopUpRequestedDetail copyOf) {
        Builder builder = builder();
        builder.money(copyOf.getMoney());
        builder.exchangedFrom(copyOf.getExchangedFrom().orElse(null));
        return builder;
    }

    @NotNull static Builder builder() {
        return new TopUpRequestedDetailImpl.BuilderImpl();
    }

    /** Builder for {@link TopUpRequestedDetail} model class. */
    interface Builder {

        /**
          * Set {@link TopUpRequestedDetail#getMoney} property.
          *
          * 
          */
        @NotNull Builder money(Money money);

        boolean isMoneyDefined();


        /**
          * Set {@link TopUpRequestedDetail#getExchangedFrom} property.
          *
          * 
          */
        @NotNull Builder exchangedFrom(Money exchangedFrom);

        boolean isExchangedFromDefined();


        /**
         * Create new instance of {@link TopUpRequestedDetail} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @NotNull TopUpRequestedDetail build();

    }
}