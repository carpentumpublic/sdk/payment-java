//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** UPIID
 *
 * UPI (Unified Payment Method) is a method of fund transfers in India. The mechanism allows two parties to exchange funds using a UPI ID without having to share the banking information with the other party.
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public interface UpiIdMethod extends PayinMethod {
    /** A discriminator value of property {@link #getPaymentMethodCode}. The model class extends {@link PayinMethod}. */
    PayinMethod.PaymentMethodCode PAYMENT_METHOD_CODE = PayinMethod.PaymentMethodCode.UPIID;

    /** Virtual payment address of the customer on UPI (Unified Payment Interface) */
    @NotNull String getUpiId();

    /** Your customer e-mail address in RFC 5322 format that is used for identification of the customer's payins. */
    @NotNull Optional<String> getEmailAddress();

    @NotNull static Builder builder(UpiIdMethod copyOf) {
        Builder builder = builder();
        builder.upiId(copyOf.getUpiId());
        builder.emailAddress(copyOf.getEmailAddress().orElse(null));
        return builder;
    }

    @NotNull static Builder builder() {
        return new UpiIdMethodImpl.BuilderImpl();
    }

    /** Builder for {@link UpiIdMethod} model class. */
    interface Builder {

        /**
          * Set {@link UpiIdMethod#getUpiId} property.
          *
          * Virtual payment address of the customer on UPI (Unified Payment Interface)
          */
        @NotNull Builder upiId(String upiId);

        boolean isUpiIdDefined();


        /**
          * Set {@link UpiIdMethod#getEmailAddress} property.
          *
          * Your customer e-mail address in RFC 5322 format that is used for identification of the customer's payins.
          */
        @NotNull Builder emailAddress(String emailAddress);

        boolean isEmailAddressDefined();


        /**
         * Create new instance of {@link UpiIdMethod} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @NotNull UpiIdMethod build();

    }
}