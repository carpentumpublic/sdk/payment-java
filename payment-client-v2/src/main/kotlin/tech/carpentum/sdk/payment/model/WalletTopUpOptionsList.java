//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** WalletTopUpOptionsList
 *
 * List of (possibly) available wallet payment options.
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public interface WalletTopUpOptionsList {

    @NotNull java.util.List<@NotNull WalletTopUpOption> getData();

    @NotNull static WalletTopUpOptionsList ofData(java.util.List<@NotNull WalletTopUpOption> data) { return builder().data(data).build(); }

    @NotNull static Builder builder() {
        return new WalletTopUpOptionsListImpl.BuilderImpl();
    }

    /** Builder for {@link WalletTopUpOptionsList} model class. */
    interface Builder {

        /**
          * Replace all items in {@link WalletTopUpOptionsList#getData} list property.
          *
          * 
          */
        @NotNull Builder data(java.util.List<@NotNull WalletTopUpOption> data);
        /**
          * Add single item to {@link WalletTopUpOptionsList#getData} list property.
          *
          * 
          */
        @NotNull Builder dataAdd(WalletTopUpOption item);
        /**
          * Add all items to {@link WalletTopUpOptionsList#getData} list property.
          *
          * 
          */
        @NotNull Builder dataAddAll(java.util.List<@NotNull WalletTopUpOption> data);


        /**
         * Create new instance of {@link WalletTopUpOptionsList} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @NotNull WalletTopUpOptionsList build();

    }
}