//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.AccountCustomerResponseUpiQR

class AccountCustomerResponseUpiQRJsonAdapter {
    @FromJson
    fun fromJson(json: AccountCustomerResponseUpiQRJson): AccountCustomerResponseUpiQR {
        val builder = AccountCustomerResponseUpiQR.builder()
        builder.accountName(json.accountName)
        return builder.build()
    }

    @ToJson
    fun toJson(model: AccountCustomerResponseUpiQR): AccountCustomerResponseUpiQRJson {
        val json = AccountCustomerResponseUpiQRJson()
        json.accountName = model.accountName.orElse(null)
        return json
    }

    @FromJson
    fun fromJsonImpl(model: AccountCustomerResponseUpiQR): AccountCustomerResponseUpiQRImpl {
        return model as AccountCustomerResponseUpiQRImpl
    }

    @ToJson
    fun toJsonImpl(impl: AccountCustomerResponseUpiQRImpl): AccountCustomerResponseUpiQR {
        return impl
    }

}