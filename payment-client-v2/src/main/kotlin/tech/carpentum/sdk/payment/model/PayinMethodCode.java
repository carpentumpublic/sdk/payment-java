//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** PayinMethodCode
 *
 * Payment method is a payment instrument that your customers use (such as online bank transfers, VA payments, UPI and so on).
Select a payment method from the list of the methods that you have configured in your account.
The `PaymentMethodCode` represents payment method available for a payment.

 - OFFLINE - Offline Bank Transfer. Payment method which requires customer to copy Payment instruction from the Payment application right after the incoming payment is submitted and create the Payment transfer using the instructions within customer's own payment service such as Internet or mobile banking or wallet.

   As the Payment relies on the customer offline Payment transfer processing it can take minutes for Payment to be confirmed and also number of Payment expiration ratio can be higher.
 - ONLINE - Online Bank Transfer. Payment method where Customer is redirected to his own preferred Payment service application right after the payment is submitted and create the Payment transfer with pre-filled Payment instruction.
 - VAPAY - Virtual Accounts. Payment method which requires customer to copy Payment instruction with generated Virtual Account from the Payment application right after the payment is submitted and create the Payment transfer using the instructions within customer's own payment service such as Internet or mobile banking, wallet or ATM.

   One Virtual account cannot be used for sending funds repeatedly.
 - P2A_V2 - Phone to Account. Payment method which requires customer to scan QR code from the Payment application by Customer's Payment service application right after the payment is submitted.

   Supported Payment service applications are UPI and GooglePay both on India market.
 - IMPS - Is an India market version of the Offline Bank Transfer which differs in the data which need to be collected and used during a Payment process.
 - PROMPTPAY - A payment method which requires customer to scan QR code from the Payment application by Customer's Payment service application right after the payment is submitted.
 - QRISPAY - QRIS is a payment method intended for the Indonesian market which allows users to pay using scanning QR codes by theirs Payment application.
 - NETBANKING - Netbanking, also known as online banking or Net Banking is a payment option offered by banks and financial institutions on Indian market that allow customers to use banking services over the internet. In our case, we take advantage of the Netbanking services to offer your customer UPI Payment method to pay funds directly from your customer Net Banking account.
 - DUITNOW - A payment method which requires customer to scan QR code from the Payment application by Customer's Payment service application right after the payment is submitted.
 - CRYPTO_OFFLINE - Crypto Offline is a payment method that allows customers to pay by crypto currency from they crypto wallet to an address that they are provided.
 - UPIID - UPI (Unified Payment Method) is a method of fund transfers in India. The mechanism allows two parties to exchange funds using a UPI ID without having to share the banking information with the other party.
 - UPIQR - UPI QR is payment method intended for the Indian market. This payment method requires customers to scan QR code by Customer's Payment service application compatible with UPI payment schema.
 - EWALLET - E-Wallet is a payment option on Indonesian market that allows customers to use major Indonesian wallets for payments.

   Customer is either automatically redirected to a chosen wallet with all payment details pre-populated to finalize the payment or directly receives push notification to authorize the payment in case of OVO wallet.
 - CAPITEC_PAY - Capitec Pay is an easy, fast and safe way to pay without having to enter the bank card details or share banking login information.

   The customer can choose the account he wants to pay from and authenticate the payment safely in the banking app.
 - QRPH - QR Ph is payment method intended for Philippine market which allows customers to send payments by scanning QR codes from supported banks and e-wallets.
 - PAY_ME - Pay Me is payment method intended for South Africa market which allows customers to send payments by scanning QR codes from supported banks and e-wallets.
 - MOBILE_MONEY - Mobile Money is a payment method which allows customers to simply complete a payment from their mobile money wallet.
 - VAPAY_VERIF - Virtual Accounts With Verified Bank Account. Payment method which requires customer to copy Payment instruction with assigned Virtual Account from the Payment application right after the payment is submitted and create the Payment transfer using the instructions within customer's own payment service such as Internet or mobile banking, wallet or ATM.
   
   Each virtual account is assigned to one and only one customers bank account and is used for sending funds repeatedly.
 - CODE_PAYMENT - Convenience Store Payments. Payment method which requires customer to copy Payment Code from the Payment application right after the payment is submitted into kiosk in convenience store during the payment on the cashier.
   
   Each Payment Code is assigned to one and only pne payment and should be used only once.
 - ABSA_PAY - Absa Pay offers a simple, quick and secure way to make payments without needing to input banking login details.

   Customers can select their preferred account for payment and securely authorize the transaction through the banking app.
 *
 * 
 *
 * The model class is immutable.
 *
 * Use static {@link #of} method to create a new model class instance.
 */
public interface PayinMethodCode {


    @NotNull String getValue();

    static PayinMethodCode of(@NotNull String value) {
        return new PayinMethodCodeImpl(value);
    }
}