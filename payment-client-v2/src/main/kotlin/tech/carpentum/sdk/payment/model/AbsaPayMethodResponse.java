//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** ABSA_PAY
 *
 * 
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public interface AbsaPayMethodResponse extends PayinMethodResponse {
    /** A discriminator value of property {@link #getPaymentMethodCode}. The model class extends {@link PayinMethodResponse}. */
    PayinMethodResponse.PaymentMethodCode PAYMENT_METHOD_CODE = PayinMethodResponse.PaymentMethodCode.ABSA_PAY;

    @NotNull IdPayin getIdPayin();

    @NotNull IdPayment getIdPayment();

    @NotNull java.util.List<@NotNull AccountCallbackAbsaPay> getAccounts();

    @NotNull AccountCustomerResponseAbsaPay getAccountCustomer();

    @NotNull MoneyPaymentResponse getMoney();

    @NotNull Optional<MoneyVat> getVat();

    /** Reference number of transaction. */
    @NotNull String getReference();

    /** This is the URL where the customers will be redirected after completing a payment.

The URL must be either IP or domain-based. */
    @NotNull String getReturnUrl();

    /** Date and time when payment was accepted. */
    @NotNull java.time.OffsetDateTime getAcceptedAt();

    /** Date and time of payment expiration. If no money has been transferred to this time, payment is considered failed and callback with status change event will shortly follow. */
    @NotNull java.time.OffsetDateTime getExpireAt();

    @NotNull static Builder builder(AbsaPayMethodResponse copyOf) {
        Builder builder = builder();
        builder.idPayin(copyOf.getIdPayin());
        builder.idPayment(copyOf.getIdPayment());
        builder.accounts(copyOf.getAccounts());
        builder.accountCustomer(copyOf.getAccountCustomer());
        builder.money(copyOf.getMoney());
        builder.vat(copyOf.getVat().orElse(null));
        builder.reference(copyOf.getReference());
        builder.returnUrl(copyOf.getReturnUrl());
        builder.acceptedAt(copyOf.getAcceptedAt());
        builder.expireAt(copyOf.getExpireAt());
        return builder;
    }

    @NotNull static Builder builder() {
        return new AbsaPayMethodResponseImpl.BuilderImpl();
    }

    /** Builder for {@link AbsaPayMethodResponse} model class. */
    interface Builder {

        /**
          * Set {@link AbsaPayMethodResponse#getIdPayin} property.
          *
          * 
          */
        @NotNull Builder idPayin(IdPayin idPayin);

        boolean isIdPayinDefined();


        /**
          * Set {@link AbsaPayMethodResponse#getIdPayment} property.
          *
          * 
          */
        @NotNull Builder idPayment(IdPayment idPayment);

        boolean isIdPaymentDefined();


        /**
          * Replace all items in {@link AbsaPayMethodResponse#getAccounts} list property.
          *
          * 
          */
        @NotNull Builder accounts(java.util.List<@NotNull AccountCallbackAbsaPay> accounts);
        /**
          * Add single item to {@link AbsaPayMethodResponse#getAccounts} list property.
          *
          * 
          */
        @NotNull Builder accountsAdd(AccountCallbackAbsaPay item);
        /**
          * Add all items to {@link AbsaPayMethodResponse#getAccounts} list property.
          *
          * 
          */
        @NotNull Builder accountsAddAll(java.util.List<@NotNull AccountCallbackAbsaPay> accounts);


        /**
          * Set {@link AbsaPayMethodResponse#getAccountCustomer} property.
          *
          * 
          */
        @NotNull Builder accountCustomer(AccountCustomerResponseAbsaPay accountCustomer);

        boolean isAccountCustomerDefined();


        /**
          * Set {@link AbsaPayMethodResponse#getMoney} property.
          *
          * 
          */
        @NotNull Builder money(MoneyPaymentResponse money);

        boolean isMoneyDefined();


        /**
          * Set {@link AbsaPayMethodResponse#getVat} property.
          *
          * 
          */
        @NotNull Builder vat(MoneyVat vat);

        boolean isVatDefined();


        /**
          * Set {@link AbsaPayMethodResponse#getReference} property.
          *
          * Reference number of transaction.
          */
        @NotNull Builder reference(String reference);

        boolean isReferenceDefined();


        /**
          * Set {@link AbsaPayMethodResponse#getReturnUrl} property.
          *
          * This is the URL where the customers will be redirected after completing a payment.

The URL must be either IP or domain-based.
          */
        @NotNull Builder returnUrl(String returnUrl);

        boolean isReturnUrlDefined();


        /**
          * Set {@link AbsaPayMethodResponse#getAcceptedAt} property.
          *
          * Date and time when payment was accepted.
          */
        @NotNull Builder acceptedAt(java.time.OffsetDateTime acceptedAt);

        boolean isAcceptedAtDefined();


        /**
          * Set {@link AbsaPayMethodResponse#getExpireAt} property.
          *
          * Date and time of payment expiration. If no money has been transferred to this time, payment is considered failed and callback with status change event will shortly follow.
          */
        @NotNull Builder expireAt(java.time.OffsetDateTime expireAt);

        boolean isExpireAtDefined();


        /**
         * Create new instance of {@link AbsaPayMethodResponse} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @NotNull AbsaPayMethodResponse build();

    }
}