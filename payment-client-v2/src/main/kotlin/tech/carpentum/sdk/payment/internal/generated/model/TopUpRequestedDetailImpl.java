//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** TopUpRequestedDetail
 *
 * The requested payment.
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public class TopUpRequestedDetailImpl implements TopUpRequestedDetail {
    private final Money money;

    @Override
    public Money getMoney() {
        return money;
    }


    private final Optional<Money> exchangedFrom;

    @Override
    public Optional<Money> getExchangedFrom() {
        return exchangedFrom;
    }




    private final int hashCode;
    private final String toString;

    private TopUpRequestedDetailImpl(BuilderImpl builder) {
        this.money = Objects.requireNonNull(builder.money, "Property 'money' is required.");
        this.exchangedFrom = Optional.ofNullable(builder.exchangedFrom);

        this.hashCode = Objects.hash(money, exchangedFrom);
        this.toString = builder.type + "(" +
                "money=" + money +
                ", exchangedFrom=" + exchangedFrom +
                ')';
    }

    @Override
    public int hashCode() {
        return hashCode;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }

        if (!(obj instanceof TopUpRequestedDetailImpl)) {
            return false;
        }

        TopUpRequestedDetailImpl that = (TopUpRequestedDetailImpl) obj;
        if (!Objects.equals(this.money, that.money)) return false;
        if (!Objects.equals(this.exchangedFrom, that.exchangedFrom)) return false;

        return true;
    }

    @Override
    public String toString() {
        return toString;
    }

    /** Builder for {@link TopUpRequestedDetail} model class. */
    public static class BuilderImpl implements TopUpRequestedDetail.Builder {
        private Money money = null;
        private Money exchangedFrom = null;

        private final String type;

        public BuilderImpl(String type) {
            this.type = type;
        }

        public BuilderImpl() {
            this("TopUpRequestedDetail");
        }

        /**
          * Set {@link TopUpRequestedDetail#getMoney} property.
          *
          * 
          */
        @Override
        public BuilderImpl money(Money money) {
            this.money = money;
            return this;
        }

        @Override
        public boolean isMoneyDefined() {
            return this.money != null;
        }

        /**
          * Set {@link TopUpRequestedDetail#getExchangedFrom} property.
          *
          * 
          */
        @Override
        public BuilderImpl exchangedFrom(Money exchangedFrom) {
            this.exchangedFrom = exchangedFrom;
            return this;
        }

        @Override
        public boolean isExchangedFromDefined() {
            return this.exchangedFrom != null;
        }

        /**
         * Create new instance of {@link TopUpRequestedDetail} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @Override
        public TopUpRequestedDetailImpl build() {
            return new TopUpRequestedDetailImpl(this);
        }

    }
}