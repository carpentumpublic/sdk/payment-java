//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.JsonClass;
import tech.carpentum.sdk.payment.model.*;

@JsonClass(generateAdapter = true)
class AbsaPayMethodResponseJson {
    var idPayin: IdPayin? = null
    var idPayment: IdPayment? = null
    var accounts: List<AccountCallbackAbsaPay>? = null
    var accountCustomer: AccountCustomerResponseAbsaPay? = null
    var money: MoneyPaymentResponse? = null
    var vat: MoneyVat? = null
    var reference: String? = null
    var returnUrl: String? = null
    var acceptedAt: java.time.OffsetDateTime? = null
    var expireAt: java.time.OffsetDateTime? = null
    var paymentMethodCode: String? = null
}