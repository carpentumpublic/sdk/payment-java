//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.AuthTokenByActionRequest

class AuthTokenByActionRequestJsonAdapter {
    @FromJson
    fun fromJson(json: AuthTokenByActionRequestJson): AuthTokenByActionRequest {
        val builder = AuthTokenByActionRequest.builder()
        builder.merchantCode(json.merchantCode)
        builder.secret(json.secret)
        builder.validitySecs(json.validitySecs)
        builder.action(json.action)
        builder.idPayment(json.idPayment)
        builder.money(json.money)
        builder.moneyProvided(json.moneyProvided)
        builder.currencyCodeRequired(json.currencyCodeRequired)
        builder.moneyRequired(json.moneyRequired)
        builder.currencyCodeProvided(json.currencyCodeProvided)
        builder.settlementMethod(json.settlementMethod)
        return builder.build()
    }

    @ToJson
    fun toJson(model: AuthTokenByActionRequest): AuthTokenByActionRequestJson {
        val json = AuthTokenByActionRequestJson()
        json.merchantCode = model.merchantCode
        json.secret = model.secret
        json.validitySecs = model.validitySecs.orElse(null)
        json.action = model.action
        json.idPayment = model.idPayment
        json.money = model.money.orElse(null)
        json.moneyProvided = model.moneyProvided.orElse(null)
        json.currencyCodeRequired = model.currencyCodeRequired.orElse(null)
        json.moneyRequired = model.moneyRequired.orElse(null)
        json.currencyCodeProvided = model.currencyCodeProvided.orElse(null)
        json.settlementMethod = model.settlementMethod.orElse(null)
        return json
    }

    @FromJson
    fun fromJsonImpl(model: AuthTokenByActionRequest): AuthTokenByActionRequestImpl {
        return model as AuthTokenByActionRequestImpl
    }

    @ToJson
    fun toJsonImpl(impl: AuthTokenByActionRequestImpl): AuthTokenByActionRequest {
        return impl
    }

}