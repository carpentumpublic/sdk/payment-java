package tech.carpentum.sdk.payment.sample;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tech.carpentum.sdk.payment.AuthTokenOperations;
import tech.carpentum.sdk.payment.IncomingPaymentsApi;
import tech.carpentum.sdk.payment.PaymentContext;
import tech.carpentum.sdk.payment.ResponseException;
import tech.carpentum.sdk.payment.model.AuthTokenResponse;
import tech.carpentum.sdk.payment.model.AvailablePayinOptionList;
import tech.carpentum.sdk.payment.model.Payin;
import tech.carpentum.sdk.payment.model.PayinAcceptedResponse;
import tech.carpentum.sdk.payment.model.PayinDetail;
import tech.carpentum.sdk.payment.model.PaymentRequested;

import java.io.InterruptedIOException;
import java.time.Duration;

/**
 * Wraps all Incoming Payments API operations for single `idPayin`.
 */
public class CreatePayinScenario extends AbstractScenario {
    private final IncomingPaymentsApi incomingPaymentsApi;
    private final String idPayin;

    private CreatePayinScenario(@NotNull IncomingPaymentsApi incomingPaymentsApi, @NotNull String idPayin, @NotNull String authToken) {
        super(authToken);
        this.incomingPaymentsApi = incomingPaymentsApi;
        this.idPayin = idPayin;
    }

    /**
     * Creates new instance of the scenario with newly created Authorization token with specified validity.
     */
    public static CreatePayinScenario create(@NotNull PaymentContext context, @NotNull String idPayin, @Nullable Duration validity) throws ResponseException, InterruptedIOException {
        AuthTokenResponse authToken = context.createAuthToken(
                new AuthTokenOperations()
                        .grant(IncomingPaymentsApi.definePayinAvailablePaymentOptionsEndpoint())
                        .grant(IncomingPaymentsApi.defineCreatePayinEndpoint().forId(idPayin))
                        .grant(IncomingPaymentsApi.defineGetPayinEndpoint().forId(idPayin))
                        .grant(IncomingPaymentsApi.defineSetPayinExternalReferenceEndpoint().forId(idPayin)),
                validity
        );
        return new CreatePayinScenario(IncomingPaymentsApi.create(context, authToken.getToken()), idPayin, authToken.getToken());
    }

    /**
     * Creates new instance of the scenario with newly created Authorization token with default {@code payment} validity.
     */
    public static CreatePayinScenario create(@NotNull PaymentContext context, @NotNull String idPayin) throws ResponseException, InterruptedIOException {
        return create(context, idPayin, (Duration) null);
    }

    /**
     * Creates new instance of the scenario with existing Authorization token.
     */
    public static CreatePayinScenario create(@NotNull PaymentContext context, @NotNull String idPayin, @NotNull String authToken) throws ResponseException {
        return new CreatePayinScenario(IncomingPaymentsApi.create(context, authToken), idPayin, authToken);
    }

    public AvailablePayinOptionList availablePaymentOptions(@NotNull PaymentRequested paymentRequested) throws ResponseException, InterruptedIOException {
        return incomingPaymentsApi.payinAvailablePaymentOptions(paymentRequested);
    }

    public PayinAcceptedResponse createPayment(@NotNull Payin payin) throws ResponseException, InterruptedIOException {
        return incomingPaymentsApi.createPayin(idPayin, payin);
    }

    public void setExternalReference(@NotNull String reference) throws ResponseException, InterruptedIOException {
        incomingPaymentsApi.setPayinExternalReference(idPayin, reference);
    }

    public PayinDetail getDetail() throws ResponseException, InterruptedIOException {
        return incomingPaymentsApi.getPayin(idPayin);
    }
}
