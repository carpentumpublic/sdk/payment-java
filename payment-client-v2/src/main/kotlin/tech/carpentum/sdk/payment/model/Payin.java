//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** Payin
 *
 * For every payment method there is appropriate payment specific request object in `paymentMethod` attribute.

If you have used the [`POST /payins/!availablePaymentOptions`](#operations-Incoming_payments-availablePaymentOptions) API to get the list of the available payment options, then it is expected that you use the same input data here to make sure that the payment will be accepted.
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public interface Payin {

    @NotNull PaymentRequested getPaymentRequested();

    @NotNull PayinMethod getPaymentMethod();

    /** This is the URL where the system will send the transaction final status after payment has been completed.

The URL must be either IP or domain-based. */
    @NotNull Optional<String> getCallbackUrl();

    /** This is the URL where the customers will be redirected after completing a payment.

The URL must be either IP or domain-based. */
    @NotNull String getReturnUrl();

    /** The IP address of the customer making the payment in either the IPv4 or IPv6 format.

The IP address is used for validating against the IP address whitelists and blacklists from the merchant settings. */
    @NotNull Optional<String> getCustomerIp();

    @NotNull static Builder builder(Payin copyOf) {
        Builder builder = builder();
        builder.paymentRequested(copyOf.getPaymentRequested());
        builder.paymentMethod(copyOf.getPaymentMethod());
        builder.callbackUrl(copyOf.getCallbackUrl().orElse(null));
        builder.returnUrl(copyOf.getReturnUrl());
        builder.customerIp(copyOf.getCustomerIp().orElse(null));
        return builder;
    }

    @NotNull static Builder builder() {
        return new PayinImpl.BuilderImpl();
    }

    /** Builder for {@link Payin} model class. */
    interface Builder {

        /**
          * Set {@link Payin#getPaymentRequested} property.
          *
          * 
          */
        @NotNull Builder paymentRequested(PaymentRequested paymentRequested);

        boolean isPaymentRequestedDefined();


        /**
          * Set {@link Payin#getPaymentMethod} property.
          *
          * 
          */
        @NotNull Builder paymentMethod(PayinMethod paymentMethod);

        boolean isPaymentMethodDefined();


        /**
          * Set {@link Payin#getCallbackUrl} property.
          *
          * This is the URL where the system will send the transaction final status after payment has been completed.

The URL must be either IP or domain-based.
          */
        @NotNull Builder callbackUrl(String callbackUrl);

        boolean isCallbackUrlDefined();


        /**
          * Set {@link Payin#getReturnUrl} property.
          *
          * This is the URL where the customers will be redirected after completing a payment.

The URL must be either IP or domain-based.
          */
        @NotNull Builder returnUrl(String returnUrl);

        boolean isReturnUrlDefined();


        /**
          * Set {@link Payin#getCustomerIp} property.
          *
          * The IP address of the customer making the payment in either the IPv4 or IPv6 format.

The IP address is used for validating against the IP address whitelists and blacklists from the merchant settings.
          */
        @NotNull Builder customerIp(String customerIp);

        boolean isCustomerIpDefined();


        /**
         * Create new instance of {@link Payin} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @NotNull Payin build();

    }
}