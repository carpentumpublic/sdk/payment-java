//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.PayoutDetail

class PayoutDetailJsonAdapter {
    @FromJson
    fun fromJson(json: PayoutDetailJson): PayoutDetail {
        val builder = PayoutDetail.builder()
        builder.paymentRequested(json.paymentRequested)
        builder.process(json.process)
        builder.fee(json.fee)
        builder.paymentMethodResponse(json.paymentMethodResponse)
        return builder.build()
    }

    @ToJson
    fun toJson(model: PayoutDetail): PayoutDetailJson {
        val json = PayoutDetailJson()
        json.paymentRequested = model.paymentRequested
        json.process = model.process
        json.fee = model.fee
        json.paymentMethodResponse = model.paymentMethodResponse
        return json
    }

    @FromJson
    fun fromJsonImpl(model: PayoutDetail): PayoutDetailImpl {
        return model as PayoutDetailImpl
    }

    @ToJson
    fun toJsonImpl(impl: PayoutDetailImpl): PayoutDetail {
        return impl
    }

}