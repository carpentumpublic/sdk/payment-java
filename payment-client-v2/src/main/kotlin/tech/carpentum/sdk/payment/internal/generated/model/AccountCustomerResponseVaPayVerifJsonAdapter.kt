//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.AccountCustomerResponseVaPayVerif

class AccountCustomerResponseVaPayVerifJsonAdapter {
    @FromJson
    fun fromJson(json: AccountCustomerResponseVaPayVerifJson): AccountCustomerResponseVaPayVerif {
        val builder = AccountCustomerResponseVaPayVerif.builder()
        builder.accountName(json.accountName)
        builder.accountNumber(json.accountNumber)
        return builder.build()
    }

    @ToJson
    fun toJson(model: AccountCustomerResponseVaPayVerif): AccountCustomerResponseVaPayVerifJson {
        val json = AccountCustomerResponseVaPayVerifJson()
        json.accountName = model.accountName.orElse(null)
        json.accountNumber = model.accountNumber
        return json
    }

    @FromJson
    fun fromJsonImpl(model: AccountCustomerResponseVaPayVerif): AccountCustomerResponseVaPayVerifImpl {
        return model as AccountCustomerResponseVaPayVerifImpl
    }

    @ToJson
    fun toJsonImpl(impl: AccountCustomerResponseVaPayVerifImpl): AccountCustomerResponseVaPayVerif {
        return impl
    }

}