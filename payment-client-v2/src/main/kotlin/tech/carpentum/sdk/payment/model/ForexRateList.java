//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** ForexRateList
 *
 * 
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public interface ForexRateList {

    @NotNull java.util.List<@NotNull ForexRate> getData();

    @NotNull static ForexRateList ofData(java.util.List<@NotNull ForexRate> data) { return builder().data(data).build(); }

    @NotNull static Builder builder() {
        return new ForexRateListImpl.BuilderImpl();
    }

    /** Builder for {@link ForexRateList} model class. */
    interface Builder {

        /**
          * Replace all items in {@link ForexRateList#getData} list property.
          *
          * 
          */
        @NotNull Builder data(java.util.List<@NotNull ForexRate> data);
        /**
          * Add single item to {@link ForexRateList#getData} list property.
          *
          * 
          */
        @NotNull Builder dataAdd(ForexRate item);
        /**
          * Add all items to {@link ForexRateList#getData} list property.
          *
          * 
          */
        @NotNull Builder dataAddAll(java.util.List<@NotNull ForexRate> data);


        /**
         * Create new instance of {@link ForexRateList} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @NotNull ForexRateList build();

    }
}