package tech.carpentum.sdk.payment.sample;

import tech.carpentum.sdk.payment.ClientErrorException;
import tech.carpentum.sdk.payment.GetPaymentOptionsErrorException;
import tech.carpentum.sdk.payment.PaymentContext;
import tech.carpentum.sdk.payment.PaymentsApi.ListPaymentOptionsQuery;
import tech.carpentum.sdk.payment.PaymentsApi.ListPaymentOptionsQuery.PaymentTypeCode;
import tech.carpentum.sdk.payment.model.*;
import tech.carpentum.sdk.payment.model.PayinMethod.PaymentMethodCode;

import java.io.InterruptedIOException;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.time.Duration;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;

public class AdvancedScenariosApp {
    /* Use for example https://pipedream.com/workflows service. */
    private static final String callbackUrl = Objects.requireNonNull(System.getenv("TECH_CARPENTUM_SDK_PAYMENT_SAMPLE_CALLBACK_URL"));

    private static String scenarioCreatePayin(PaymentContext context) throws InterruptedException, InterruptedIOException {
        String idPayin = "TST-" + UUID.randomUUID();
        CreatePayinScenario createPayinScenario = CreatePayinScenario.create(context, idPayin);
        AvailablePayinOptionList availablePaymentOptions = createPayinScenario.availablePaymentOptions(
                PaymentRequested.builder()
                        .money(
                                MoneyPaymentRequested.builder()
                                        .amount(new BigDecimal("10050"))
                                        .currencyCode(CurrencyCode.of("IDR"))
                                        .build()
                        )
                        .build()
        );
        System.out.println("availablePaymentOptions= " + availablePaymentOptions);
        PayinAcceptedResponse payin = createPayinScenario.createPayment(
                Payin.builder()
                        .paymentRequested(
                                PaymentRequested.builder()
                                        .money(
                                                MoneyPaymentRequested.builder()
                                                        .amount(new BigDecimal("10050"))
                                                        .currencyCode(CurrencyCode.of("IDR"))
                                                        .build()
                                        )
                                        .build()
                        )
                        .paymentMethod(
                                VaPayMethod.builder()
                                        .account(
                                                AccountPayinRequestVaPay.builder()
                                                        .accountName("Test account")
                                                        .accountNumber("11223344")
                                                        .build()
                                        )
                                        .paymentOperatorCode("IDR_002")
                                        .emailAddress("customer@test.com")
                                        .build()
                        )
                        .returnUrl("https://example.com?id=123")
                        .callbackUrl(callbackUrl)
                        .build()
        );
        System.out.println("payin= " + payin);
        createPayinScenario.setExternalReference("my-fererence-" + UUID.randomUUID());
        Thread.sleep(2000L);
        PayinDetail detail = createPayinScenario.getDetail();
        System.out.println("detail= " + detail);
        System.out.println("paymentMethodResponse.paymentMethodCode= " + detail.getPaymentMethodResponse().getPaymentMethodCode());
        return idPayin;
    }

    private static String scenarioGetPayin(PaymentContext context, String idPayin) throws InterruptedIOException {
        GetPayinScenario getPayinScenario = GetPayinScenario.create(context, idPayin);
        PayinDetail detail = getPayinScenario.getDetail();
        System.out.println("detail= " + detail);
        System.out.println("paymentMethodResponse.paymentMethodCode= " + detail.getPaymentMethodResponse().getPaymentMethodCode());
        return idPayin;
    }

    private static String scenarioCreatePayout(PaymentContext context) throws InterruptedIOException {
        String idPayout = "TST-" + UUID.randomUUID();
        CreatePayoutScenario createPayoutScenario = CreatePayoutScenario.create(context, idPayout);
        AvailablePayoutOptionList availablePaymentOptions = createPayoutScenario.availablePaymentOptions(
                PaymentRequested.builder()
                        .money(
                                MoneyPaymentRequested.builder()
                                        .amount(new BigDecimal("10050"))
                                        .currencyCode(CurrencyCode.of("IDR"))
                                        .build()
                        )
                        .build()
        );
        System.out.println("availablePaymentOptions= " + availablePaymentOptions);
        PayoutAccepted payout = createPayoutScenario.createPayment(
                Payout.builder()
                        .paymentRequested(
                                PaymentRequested.builder()
                                        .money(
                                                MoneyPaymentRequested.builder()
                                                        .amount(new BigDecimal("50"))
                                                        .currencyCode(CurrencyCode.of("IDR"))
                                                        .build()
                                        )
                                        .build()
                        )
                        .paymentMethod(
                                BankTransferMethod.builder()
                                        .account(
                                                AccountPayoutRequestBankTransfer.builder()
                                                        .accountName("Test account")
                                                        .accountNumber("11223344")
                                                        .build()
                                        )
                                        .paymentOperatorCode("IDR_002")
                                        .emailAddress("customer@test.com")
                                        .build()
                        )
                        .callbackUrl(callbackUrl)
                        .build()
        );
        System.out.println("payout= " + payout);
        PayoutDetail detail = createPayoutScenario.getDetail();
        System.out.println("detail= " + detail);
        System.out.println("paymentMethodResponse.paymentMethodCode= " + detail.getPaymentMethodResponse().getPaymentMethodCode());

        return idPayout;
    }

    private static String scenarioCreatePayoutWithBalanceCheck(PaymentContext context) throws InterruptedIOException {
        String idPayout = "TST-" + UUID.randomUUID();
        CreatePayoutWithBalanceCheckScenario createPayoutWithBalanceCheckScenario = CreatePayoutWithBalanceCheckScenario.create(context, idPayout);
        Optional<PayoutAccepted> payout = createPayoutWithBalanceCheckScenario.createPaymentWithBalanceCheck(
                Payout.builder()
                        .paymentRequested(
                                PaymentRequested.builder()
                                        .money(
                                                MoneyPaymentRequested.builder()
                                                        .amount(new BigDecimal("5000000"))
                                                        .currencyCode(CurrencyCode.of("IDR"))
                                                        .build()
                                        )
                                        .build()
                        )
                        .paymentMethod(
                                BankTransferMethod.builder()
                                        .account(
                                                AccountPayoutRequestBankTransfer.builder()
                                                        .accountName("Test account")
                                                        .accountNumber("11223344")
                                                        .build()
                                        )
                                        .paymentOperatorCode("IDR_002")
                                        .emailAddress("customer@test.com")
                                        .build()
                        )
                        .callbackUrl(callbackUrl)
                        .build()
        );
        System.out.println("payout= " + payout);

        if (payout.isPresent()) {
            PayoutDetail detail = createPayoutWithBalanceCheckScenario.getDetail();
            System.out.println("detail= " + detail);
            System.out.println("paymentMethodResponse.paymentMethodCode= " + detail.getPaymentMethodResponse().getPaymentMethodCode());
        } else {
            System.out.println("Payout NOT created! EXPECTED");
        }
        return idPayout;
    }

    private static void scenarioGetPayout(PaymentContext context, String idPayout) throws InterruptedIOException {
        GetPayoutScenario getPayoutScenario = GetPayoutScenario.create(context, idPayout);
        PayoutDetail detail = getPayoutScenario.getDetail();
        System.out.println("detail= " + detail);
        System.out.println("paymentMethodResponse.paymentMethodCode= " + detail.getPaymentMethodResponse().getPaymentMethodCode());
    }

    private static void scenarioGetPayout410(PaymentContext context, String idPayout) throws InterruptedIOException {
        GetPayoutScenario getPayoutScenario = GetPayoutScenario.create(context, idPayout);
        try {
            getPayoutScenario.getDetail();
        } catch (ClientErrorException ex) {
            //expected exception
            System.out.println("[" + ex.getStatusCode() + "] " + "(" + (ex.getStatusCode() == HttpURLConnection.HTTP_GONE) + ") " + ex); //must be 410 Gone
        }
    }

    private static void scenarioAccount(PaymentContext context) throws InterruptedIOException {
        AccountScenario accountScenario = AccountScenario.create(context);

        System.out.println("Current Authorization token: " + accountScenario.getAuthToken());

        {
            BalanceList balances1 = accountScenario.listBalances();
            System.out.println("balances= " + balances1);
        }
        {
            BalanceList balances2 = accountScenario.listBalances("IDR", "CZK");
            System.out.println("balances= " + balances2);
        }
    }

    private static void scenarioCodelists(PaymentContext context) throws InterruptedIOException {
        CodelistsScenario codelistsScenario = CodelistsScenario.create(context, Duration.ofHours(1));
        CurrencyList currencies = codelistsScenario.listCurrencies();
        System.out.println("currencies= " + currencies);

        SegmentList segments = codelistsScenario.listSegments();
        System.out.println("segments= " + segments);
        List<Segment> data = segments.getData();
        System.out.println("data= " + data);

        PaymentOperatorList paymentOperators = codelistsScenario.listPaymentOperators();
        System.out.println("paymentOperators= " + paymentOperators);

        PaymentMethodsList paymentMethods = codelistsScenario.listPaymentMethods();
        System.out.println("paymentMethods= " + paymentMethods);

        {
            PaymentOptionsList paymentOptions1 = codelistsScenario.listPaymentOptions();
            System.out.println("paymentOptions= " + paymentOptions1);
        }
        {
            PaymentOptionsList paymentOptions2 = codelistsScenario
                    .listPaymentOptions(ListPaymentOptionsQuery.paymentTypeCode(PaymentTypeCode.PAYIN));
            System.out.println("paymentOptions= " + paymentOptions2);
        }
        {
            PaymentOptionsList paymentOptions3 = codelistsScenario
                    .listPaymentOptions(ListPaymentOptionsQuery.paymentMethodCodes(PaymentMethodCode.OFFLINE, PaymentMethodCode.ONLINE));
            System.out.println("paymentOptions= " + paymentOptions3);
        }
        {
            PaymentOptionsList paymentOptions4 = codelistsScenario
                    .listPaymentOptions(ListPaymentOptionsQuery.currencyCodes("INR", "IDR"));
            System.out.println("paymentOptions= " + paymentOptions4);
        }
        {
            PaymentOptionsList paymentOptions5 = codelistsScenario
                    .listPaymentOptions(ListPaymentOptionsQuery.segmentCodes("MEDIUM", "NEWBIE"));
            System.out.println("paymentOptions= " + paymentOptions5);
        }
        {
            try {
                PaymentOptionsList paymentOptions6 = codelistsScenario
                        .listPaymentOptions(ListPaymentOptionsQuery
                                .currencyCodes("IDR")
                                .paymentTypeCode(PaymentTypeCode.PAYIN)
                                .paymentMethodCodes(PaymentMethodCode.IMPS)
                                .segmentCodes("VIP")
                                .paymentOperatorCodes("IDR_016")
                                .get()
                        );
                System.out.println("paymentOptions= " + paymentOptions6);
            } catch (GetPaymentOptionsErrorException ex) {
                Optional.ofNullable(ex.getBusinessValidationErrors().get(GetPaymentOptionsError.CODE_MERCHANT_INACTIVE)).map(error -> {
                    throw new IllegalStateException("Fatal error: " + error);
                });
            }
        }
    }

    private static void scenarioMerchantInfo(PaymentContext context) throws InterruptedIOException {
        MerchantInfoScenario merchantInfoScenario = MerchantInfoScenario.create(context);

        System.out.println("Current Authorization token: " + merchantInfoScenario.getAuthToken());

        MerchantInfo merchantInfo = merchantInfoScenario.getMerchantInfo();
        System.out.println("merchantInfo= " + merchantInfo);
    }

    public static void main(String[] args) throws InterruptedIOException, InterruptedException {
        PaymentContext context = PaymentContext
                .builder()
                .brand("brand007")
                .defaultCallTimeout(Duration.ofMinutes(1))
                .build();

        scenarioMerchantInfo(context);
        System.out.println("---------------------------------------------------");

        scenarioCodelists(context);
        System.out.println("---------------------------------------------------");

        scenarioAccount(context);
        System.out.println("---------------------------------------------------");

        String idPayin1 = scenarioCreatePayin(context);
        System.out.println("---------------------------------------------------");
        scenarioGetPayin(context, idPayin1);
        System.out.println("---------------------------------------------------");

        String idPayout1 = scenarioCreatePayout(context);
        System.out.println("---------------------------------------------------");
        scenarioGetPayout(context, idPayout1);
        System.out.println("---------------------------------------------------");

        String idPayout2 = scenarioCreatePayoutWithBalanceCheck(context);
        System.out.println("---------------------------------------------------");
        scenarioGetPayout410(context, idPayout2);
        System.out.println("---------------------------------------------------");
    }

}
