//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** Redirection
 *
 * 
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public interface Redirection {

    // tag::methodEnum[]
    /** @see #getMethod */
    public static final String METHOD_GET = "GET";
    /** @see #getMethod */
    public static final String METHOD_POST = "POST";
    // end::methodEnum[]
    /** The HTTP method that should be used for redirection. */
    @NotNull String getMethod();

    /** The destination URL for redirect to. */
    @NotNull String getUrl();

    /** The optional data in the `application/x-www-form-urlencoded` format that should be used for redirection when POST method is used. */
    @NotNull Optional<String> getData();

    @NotNull static Builder builder(Redirection copyOf) {
        Builder builder = builder();
        builder.method(copyOf.getMethod());
        builder.url(copyOf.getUrl());
        builder.data(copyOf.getData().orElse(null));
        return builder;
    }

    @NotNull static Builder builder() {
        return new RedirectionImpl.BuilderImpl();
    }

    /** Builder for {@link Redirection} model class. */
    interface Builder {

        /**
          * Set {@link Redirection#getMethod} property.
          *
          * The HTTP method that should be used for redirection.
          */
        @NotNull Builder method(String method);

        boolean isMethodDefined();


        /**
          * Set {@link Redirection#getUrl} property.
          *
          * The destination URL for redirect to.
          */
        @NotNull Builder url(String url);

        boolean isUrlDefined();


        /**
          * Set {@link Redirection#getData} property.
          *
          * The optional data in the `application/x-www-form-urlencoded` format that should be used for redirection when POST method is used.
          */
        @NotNull Builder data(String data);

        boolean isDataDefined();


        /**
         * Create new instance of {@link Redirection} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @NotNull Redirection build();

    }
}