//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.JsonDataException
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.TopUpRequested
import tech.carpentum.sdk.payment.model.TopUpRequestedMoneyProvided
import tech.carpentum.sdk.payment.model.TopUpRequestedMoneyRequired

class TopUpRequestedJsonAdapter {

    @FromJson
    fun fromJson(json: TopUpRequestedJson): TopUpRequested {
        return if (json.moneyProvided != null) {
            val builder = TopUpRequestedMoneyProvided.builder()
            builder.moneyProvided(json.moneyProvided)
            builder.currencyRequired(json.currencyRequired)
            builder.build()
        } else if (json.moneyRequired != null) {
            val builder = TopUpRequestedMoneyRequired.builder()
            builder.moneyRequired(json.moneyRequired)
            builder.currencyProvided(json.currencyProvided)
            builder.build()
        } else {
          throw JsonDataException("Missing required attributes: [moneyProvided] or [moneyRequired].")
        }
    }

    @ToJson
    fun toJson(model: TopUpRequested): TopUpRequestedJson {
        val json = TopUpRequestedJson()
        when (model) {
            is TopUpRequestedMoneyProvided -> {
                json.moneyProvided = model.moneyProvided
                json.currencyRequired = model.currencyRequired.orElse(null)
            }
            is TopUpRequestedMoneyRequired -> {
                json.moneyRequired = model.moneyRequired
                json.currencyProvided = model.currencyProvided.orElse(null)
            }
            else -> {
                throw JsonDataException("Unsupported type [${model.javaClass.name}] of model.")
            }
        }
        return json
    }

}