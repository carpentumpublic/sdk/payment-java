//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.AvailablePayinOptionVariantCurrency

class AvailablePayinOptionVariantCurrencyJsonAdapter {
    @FromJson
    fun fromJson(json: AvailablePayinOptionVariantCurrencyJson): AvailablePayinOptionVariantCurrency {
        val builder = AvailablePayinOptionVariantCurrency.builder()
        builder.paymentMethodCode(json.paymentMethodCode)
        builder.paymentOperators(json.paymentOperators?.toList())
        builder.segmentCode(json.segmentCode)
        builder.money(json.money)
        return builder.build()
    }

    @ToJson
    fun toJson(model: AvailablePayinOptionVariantCurrency): AvailablePayinOptionVariantCurrencyJson {
        val json = AvailablePayinOptionVariantCurrencyJson()
        json.paymentMethodCode = model.paymentMethodCode
        json.paymentOperators = model.paymentOperators.ifEmpty { null }
        json.segmentCode = model.segmentCode.orElse(null)
        json.money = model.money
        return json
    }

    @FromJson
    fun fromJsonImpl(model: AvailablePayinOptionVariantCurrency): AvailablePayinOptionVariantCurrencyImpl {
        return model as AvailablePayinOptionVariantCurrencyImpl
    }

    @ToJson
    fun toJsonImpl(impl: AvailablePayinOptionVariantCurrencyImpl): AvailablePayinOptionVariantCurrency {
        return impl
    }

}