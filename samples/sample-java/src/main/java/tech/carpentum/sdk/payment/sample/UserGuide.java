package tech.carpentum.sdk.payment.sample;

import kotlin.collections.SetsKt;
import tech.carpentum.sdk.payment.AccountsApi;
import tech.carpentum.sdk.payment.AuthTokenOperations;
import tech.carpentum.sdk.payment.ClientBasicErrorException;
import tech.carpentum.sdk.payment.ClientErrorException;
import tech.carpentum.sdk.payment.IncomingPaymentsApi;
import tech.carpentum.sdk.payment.MerchantInfoApi;
import tech.carpentum.sdk.payment.OutgoingPaymentsApi;
import tech.carpentum.sdk.payment.PaymentContext;
import tech.carpentum.sdk.payment.PaymentsApi;
import tech.carpentum.sdk.payment.PaymentsApi.ListPaymentOptionsQuery.PaymentTypeCode;
import tech.carpentum.sdk.payment.PostPayoutsErrorException;
import tech.carpentum.sdk.payment.ServerBasicErrorException;
import tech.carpentum.sdk.payment.ServerErrorException;
import tech.carpentum.sdk.payment.model.*;
import tech.carpentum.sdk.payment.model.PayinMethod.PaymentMethodCode;

import java.io.InterruptedIOException;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.time.Duration;
import java.util.UUID;

/**
 * Place for code snippets referenced from {@code README.adoc} root file.
 * <p>
 * The reason to have the code snippets here is to keep the code still valid.
 */
public class UserGuide {

    private static void createPayment() {
        // tag::createPayment[]
        PaymentContext context = PaymentContext.builder()
                .domain("sandbox.carpentum.tech")                               // <1>
                .merchantCode("YOUR_MERCHANT_CODE")                             // <2>
                .secret("YOUR_SECRET")                                          // <3>
                .brand("YourBrand")                                             // <4>
                .defaultTokenValidity(Duration.ofMinutes(3))                    // <5>
                .defaultCallTimeout(Duration.ofSeconds(90))                     // <6>
                .build();
        // end::createPayment[]
    }

    private static void createPaymentApiBaseUrl() {
        // tag::createPaymentApiBaseUrl[]
        PaymentContext context = PaymentContext.builder()
                .apiBaseUrl("http://localhost:8070")                            // <1>
                // ...
                .build();
        // end::createPaymentApiBaseUrl[]
    }

    private static void createPaymentWithDefault() {
        // tag::createPaymentWithDefault[]
        PaymentContext context = PaymentContext.builder().build();              // <1>
        // end::createPaymentWithDefault[]
    }

    private static void createAuthTokenWithValidity() throws InterruptedIOException {
        PaymentContext context = PaymentContext.builder().build();
        // tag::createAuthTokenWithValidity[]
        AuthTokenResponse authToken = context
                .createAuthToken(                                               // <1>
                        new AuthTokenOperations()                               // <2>
                                .grantPost("/payins/!availablePaymentMethods")
                                .grantPost("/payins/TST-112233")
                                .grantGet("/payins/TST-112233"),
                        Duration.ofMinutes(5)                                   // <3>
                );
        // end::createAuthTokenWithValidity[]
    }

    private static void createAuthToken() throws InterruptedIOException {
        PaymentContext context = PaymentContext.builder().build();
        // tag::createAuthToken[]
        AuthTokenResponse authToken = context.createAuthToken(
                new AuthTokenOperations().grantGet("/payins/.*"));              // <1>
        // end::createAuthToken[]
    }

    private static void paymentsApiCreateAuthTokenString() throws InterruptedIOException {
        PaymentContext context = PaymentContext.builder().build();
        // tag::paymentsApiCreateAuthTokenString[]
        PaymentsApi api = PaymentsApi.create(                                   // <1>
                context,
                context.createAuthToken(
                        new AuthTokenOperations().grantGet("/currencies")       // <2>
                ).getToken()
        );
        // end::paymentsApiCreateAuthTokenString[]

        // tag::paymentsApiCreateAuthTokenStringApiCall[]
        CurrencyList currencyList = api.listCurrencies();                       // <1>
        try {
            api.listSegments();
        } catch (ClientBasicErrorException ex) {                                // <2>
            // expected 403 response because of lack of permissions
            assert ex.getStatusCode() == HttpURLConnection.HTTP_FORBIDDEN;
        }
        // end::paymentsApiCreateAuthTokenStringApiCall[]
    }

    private static void paymentsApiCreateAuthTokenEndpointDefinition() throws InterruptedIOException {
        PaymentContext context = PaymentContext.builder().build();
        // tag::paymentsApiCreateAuthTokenEndpointDefinition[]
        PaymentsApi api = PaymentsApi.create(
                context,
                context.createAuthToken(
                        new AuthTokenOperations().grant(                        // <1>
                                PaymentsApi.defineListCurrenciesEndpoint())
                ).getToken()                                                    // <2>
        );
        // end::paymentsApiCreateAuthTokenEndpointDefinition[]
    }

    private static void paymentsApiCreateAuthTokenAllEndpoints() throws InterruptedIOException {
        PaymentContext context = PaymentContext.builder().build();
        // tag::paymentsApiCreateAuthTokenAllEndpoints[]
        String token = context.createAuthToken(
                new AuthTokenOperations()                                       // <1>
                        .grant(PaymentsApi.defineListCurrenciesEndpoint())
                        .grant(PaymentsApi.defineListPaymentMethodsEndpoint())
                        .grant(PaymentsApi.defineListPaymentOperatorsEndpoint())
                        .grant(PaymentsApi.defineListPaymentOptionsEndpoint())
                        .grant(PaymentsApi.defineListSegmentsEndpoint()),
                Duration.ofHours(1)                                             // <2>
        ).getToken();
        // Using cached token to create `PaymentsApi` instance
        PaymentsApi api = PaymentsApi.create(
                context,
                token,                                                          // <3>
                Duration.ofSeconds(45)                                          // <4>
        );
        // Cache codelists
        CurrencyList currencyList = api.listCurrencies();
        PaymentMethodsList paymentMethodsList = api.listPaymentMethods();
        PaymentOperatorList paymentOperatorList = api.listPaymentOperators();
        PaymentOptionsList paymentOptionsList = api.listPaymentOptions();
        SegmentList segmentList = api.listSegments();
        // end::paymentsApiCreateAuthTokenAllEndpoints[]
    }

    private static void paymentsApiListPaymentOptionsQuery() throws InterruptedIOException {
        PaymentContext context = PaymentContext.builder().build();
        String token = context.createAuthToken(new AuthTokenOperations().grant(PaymentsApi.defineListPaymentOptionsEndpoint())).getToken();
        // Using cached token to create `PaymentsApi` instance
        PaymentsApi api = PaymentsApi.create(context, token);
        // tag::paymentsApiListPaymentOptionsQuery[]
        PaymentOptionsList paymentOptionsList = api.listPaymentOptions(
                PaymentsApi.ListPaymentOptionsQuery                             // <1>
                        .paymentTypeCode(PaymentTypeCode.PAYIN)
                        .paymentMethodCodes(PaymentMethodCode.ONLINE, PaymentMethodCode.VAPAY)
                        .currencyCodes("INR", "IDR")
                        .segmentCodes("NEWBIE", "MEDIUM")
                        .paymentOperatorCodes("BNK_000", "BNK_001")
        );
        // end::paymentsApiListPaymentOptionsQuery[]
    }

    private static void incomingPaymentsApiCreateAuthTokenAllEndpoints() throws InterruptedIOException {
        PaymentContext context = PaymentContext.builder().build();
        // tag::incomingPaymentsApiCreateAuthTokenAllEndpoints[]
        String idPayin = "TST-112233";
        String reference = UUID.randomUUID().toString();

        String token = context.createAuthToken(new AuthTokenOperations()        // <1>
                .grant(IncomingPaymentsApi.definePayinAvailablePaymentOptionsEndpoint())
                .grant(IncomingPaymentsApi.defineCreatePayinEndpoint().forId(idPayin))
                .grant(IncomingPaymentsApi.defineGetPayinEndpoint().forId(idPayin))
                .grant(IncomingPaymentsApi.defineSetPayinExternalReferenceEndpoint().forId(idPayin))
        ).getToken();
        IncomingPaymentsApi api = IncomingPaymentsApi.create(context, token);

        PaymentRequested paymentRequested = PaymentRequested.builder()
                .money(MoneyPaymentRequested.builder()
                        .amount(new BigDecimal("100"))
                        .currencyCode(CurrencyCode.of("IDR"))
                        .build())
                .build();

        AvailablePayinOptionList availablePayinOptionList =
                api.payinAvailablePaymentOptions(paymentRequested);             // <2>

        PayinAcceptedResponse payinAccepted = api.createPayin(                  // <3>
                idPayin,
                Payin.builder()
                        .paymentRequested(paymentRequested)
                        .paymentMethod(VaPayMethod.builder()
                                .account(AccountPayinRequestVaPay.builder()
                                        .accountName("Test account")
                                        .accountNumber("11223344")
                                        .build()
                                )
                                .paymentOperatorCode("BNK_000")
                                .emailAddress("customer@test.com")
                                .build()
                        )
                        .returnUrl("https://example.com?id=123")
                        .callbackUrl("https://example.com/order-status-change")
                        .build()
        );

        api.setPayinExternalReference(idPayin, reference);                      // <4>

        PayinDetail payinDetail = api.getPayin(idPayin);                        // <5>

        try {
            api.getPayin("DIFF-998877");
        } catch (ClientBasicErrorException ex) {                                // <6>
            // expected 403 response because of lack of permissions
            assert ex.getStatusCode() == HttpURLConnection.HTTP_FORBIDDEN;
        }
        // end::incomingPaymentsApiCreateAuthTokenAllEndpoints[]
    }

    private static void incomingPaymentsApiGetDetailAnyId() throws InterruptedIOException {
        PaymentContext context = PaymentContext.builder().build();
        // tag::incomingPaymentsApiGetDetailAnyId[]
        String token = context.createAuthToken(new AuthTokenOperations().grant(
                IncomingPaymentsApi.defineGetPayinEndpoint().anyId())           // <1>
        ).getToken();
        IncomingPaymentsApi api = IncomingPaymentsApi.create(
                context,
                token,
                Duration.ofSeconds(45)                                          // <2>
        );

        PaymentRequested paymentRequested = PaymentRequested.builder()
                .money(MoneyPaymentRequested.builder()
                        .amount(new BigDecimal("100"))
                        .currencyCode(CurrencyCode.of("IDR"))
                        .build())
                .build();

        PayinDetail payinDetail112233 = api.getPayin("TST-112233");             // <3>
        PayinDetail payinDetail998877 = api.getPayin("DIFF-998877");            // <4>
        // end::incomingPaymentsApiGetDetailAnyId[]
    }

    private static void outgoingPaymentsApiCreateAuthTokenAllEndpoints() throws InterruptedIOException {
        PaymentContext context = PaymentContext.builder().build();
        // tag::outgoingPaymentsApiCreateAuthTokenAllEndpoints[]
        String idPayout = "TST-223344";

        String token = context.createAuthToken(new AuthTokenOperations()        // <1>
                .grant(OutgoingPaymentsApi.defineCreatePayoutEndpoint().forId(idPayout))
                .grant(OutgoingPaymentsApi.defineGetPayoutEndpoint().forId(idPayout))
        ).getToken();
        OutgoingPaymentsApi api = OutgoingPaymentsApi.create(
                context,
                token,
                Duration.ofSeconds(45)                                          // <2>
        );

        PaymentRequested paymentRequested = PaymentRequested.builder()
                .money(MoneyPaymentRequested.builder()
                        .amount(new BigDecimal("10000"))
                        .currencyCode(CurrencyCode.of("IDR"))
                        .build())
                .build();

        PayoutAccepted payoutAccepted = api.createPayout(                       // <3>
                idPayout,
                Payout.builder()
                        .paymentRequested(paymentRequested)
                        .paymentMethod(
                                BankTransferMethod.builder()
                                        .account(AccountPayoutRequestBankTransfer.builder()
                                                .accountName("Test account")
                                                .accountNumber("11223344")
                                                .build())
                                        .paymentOperatorCode("BNK_000")
                                        .emailAddress("customer@test.com")
                                        .build()
                        )
                        .callbackUrl("https://callback.example.com")
                        .build()
        );

        PayoutDetail payoutDetail = api.getPayout(idPayout);                    // <4>
        // end::outgoingPaymentsApiCreateAuthTokenAllEndpoints[]
    }

    private static void outgoingPaymentsApiCatchResponseException() throws InterruptedIOException {
        PaymentContext context = PaymentContext.builder().build();
        String token = context.createAuthToken(
                new AuthTokenOperations().grant(OutgoingPaymentsApi.defineCreatePayoutEndpoint().anyId())
        ).getToken();
        // Using cached token to create `PaymentsApi` instance
        OutgoingPaymentsApi api = OutgoingPaymentsApi.create(context, token);
        // tag::outgoingPaymentsApiCatchResponseException[]
        try {
            api.createPayout(
                    "TST-223344",
                    Payout.builder()
                            .paymentRequested(PaymentRequested.builder()
                                    .money(MoneyPaymentRequested.builder()
                                            .amount(new BigDecimal("10000"))
                                            .currencyCode(CurrencyCode.of("CZK"))   // <1>
                                            .build())
                                    .build()
                            )
                            .paymentMethod(BankTransferMethod.builder()
                                    .account(AccountPayoutRequestBankTransfer.builder()
                                            .accountName("Test account")
                                            .accountNumber("11223344")
                                            .build()
                                    )
                                    .paymentOperatorCode("BNK_000")
                                    .emailAddress("customer@test.com")
                                    .build()
                            )
                            .callbackUrl("https://callback.example.com")
                            .build()
            );
        } catch (ClientBasicErrorException ex) {
            //TODO handle client error
            switch (ex.getStatusCode()) {
                case HttpURLConnection.HTTP_BAD_REQUEST:
                    System.err.println("400 - BAD_REQUEST");
                    break;
                case HttpURLConnection.HTTP_UNAUTHORIZED:
                    System.err.println("401 - UNAUTHORIZED");
                    break;
                case HttpURLConnection.HTTP_FORBIDDEN:
                    System.err.println("403 - FORBIDDEN");
                    break;
                case HttpURLConnection.HTTP_CONFLICT:
                    System.err.println("409 - CONFLICT");
                    break;
            }
        } catch (PostPayoutsErrorException ex) {                                    // <2>
            // expected 406 response because of lack of permissions
            assert ex.getStatusCode() == HttpURLConnection.HTTP_NOT_ACCEPTABLE;

            //TODO handle business error
            for (PostPayoutsError error : ex.getBusinessValidationErrors().values()) {
                System.err.println("BUSINESS ERROR - " + error.code);
                switch (error.code) {
                    case PostPayoutsError.CODE_CURRENCY_NOT_SUPPORTED:              // <3>
                        throw new IllegalStateException("Fatal error: " + error);
                } //TODO handle another PostPayoutsError errors
            }
        } catch (ClientErrorException ex) {
            //TODO handle generic client error
        } catch (ServerBasicErrorException ex) {
            //TODO handle server error
            System.err.println("SERVER ERROR - " + ex.getStatusCode() + " - " + ex.getBasicError().getDescription());
        } catch (ServerErrorException ex) {
            //TODO handle generic server error
        } catch (InterruptedIOException ex) {
            //TODO handle timeout error
        }
        // end::outgoingPaymentsApiCatchResponseException[]
    }

    private static void accountsApiCreateAuthTokenAllEndpoints() throws InterruptedIOException {
        PaymentContext context = PaymentContext.builder().build();
        // tag::accountsApiCreateAuthTokenAllEndpoints[]
        String token = context.createAuthToken(new AuthTokenOperations(
                AccountsApi.defineListBalancesEndpoint()                        // <1>
        )).getToken();
        AccountsApi api = AccountsApi.create(
                context,
                token,
                Duration.ofSeconds(45)                                          // <2>
        );

        BalanceList balanceList = api.listBalances(SetsKt.setOf("IDR"));        // <3>
        // end::accountsApiCreateAuthTokenAllEndpoints[]
    }

    private static void merchantInfoApiCreateAuthTokenAllEndpoints() throws InterruptedIOException {
        PaymentContext context = PaymentContext.builder().build();
        // tag::merchantInfoApiCreateAuthTokenAllEndpoints[]
        String token = context.createAuthToken(new AuthTokenOperations(
                MerchantInfoApi.defineGetMerchantInfoEndpoint()                 // <1>
        )).getToken();
        MerchantInfoApi api = MerchantInfoApi.create(
                context,
                token,
                Duration.ofSeconds(45)                                          // <2>
        );

        MerchantInfo merchantInfo = api.getMerchantInfo();                      // <3>
        // end::merchantInfoApiCreateAuthTokenAllEndpoints[]
    }

    private static void copyBuilder() {
        Payin payin = null;
        // tag::copyBuilder[]
        // Payin payin = ...
        Payin.Builder builder = Payin.builder(payin);
        builder.callbackUrl("https://example.com/order-status-change");
        Payin modified = builder.build();
        // The "modified" is copy of original "payin" with just modified "callbackUrl".
        // end::copyBuilder[]
    }

}
