//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** AccountCustomerResponseAbsaPay
 *
 * Parameters of your customer's bank or wallet account which your customer sends funds from. These account parameters are used for the sender's account verification in processing of the payment. Which parameters are mandatory depends on the payment method and the currency your customer choose to pay.
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public interface AccountCustomerResponseAbsaPay {

    /** Number of the bank account where we expect that your customer sends funds to make a payment.
This parameter is to be shown to your customer in the payment instructions. */
    @NotNull String getAccountNumber();

    @NotNull static AccountCustomerResponseAbsaPay ofAccountNumber(String accountNumber) { return builder().accountNumber(accountNumber).build(); }

    @NotNull static Builder builder() {
        return new AccountCustomerResponseAbsaPayImpl.BuilderImpl();
    }

    /** Builder for {@link AccountCustomerResponseAbsaPay} model class. */
    interface Builder {

        /**
          * Set {@link AccountCustomerResponseAbsaPay#getAccountNumber} property.
          *
          * Number of the bank account where we expect that your customer sends funds to make a payment.
This parameter is to be shown to your customer in the payment instructions.
          */
        @NotNull Builder accountNumber(String accountNumber);

        boolean isAccountNumberDefined();


        /**
         * Create new instance of {@link AccountCustomerResponseAbsaPay} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @NotNull AccountCustomerResponseAbsaPay build();

    }
}