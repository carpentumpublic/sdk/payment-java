//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** SettlementPaymentOptionsList
 *
 * List of (possibly) available settlement payment options.
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public interface SettlementPaymentOptionsList {

    @NotNull java.util.List<@NotNull SettlementPaymentOption> getData();

    @NotNull static SettlementPaymentOptionsList ofData(java.util.List<@NotNull SettlementPaymentOption> data) { return builder().data(data).build(); }

    @NotNull static Builder builder() {
        return new SettlementPaymentOptionsListImpl.BuilderImpl();
    }

    /** Builder for {@link SettlementPaymentOptionsList} model class. */
    interface Builder {

        /**
          * Replace all items in {@link SettlementPaymentOptionsList#getData} list property.
          *
          * 
          */
        @NotNull Builder data(java.util.List<@NotNull SettlementPaymentOption> data);
        /**
          * Add single item to {@link SettlementPaymentOptionsList#getData} list property.
          *
          * 
          */
        @NotNull Builder dataAdd(SettlementPaymentOption item);
        /**
          * Add all items to {@link SettlementPaymentOptionsList#getData} list property.
          *
          * 
          */
        @NotNull Builder dataAddAll(java.util.List<@NotNull SettlementPaymentOption> data);


        /**
         * Create new instance of {@link SettlementPaymentOptionsList} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @NotNull SettlementPaymentOptionsList build();

    }
}