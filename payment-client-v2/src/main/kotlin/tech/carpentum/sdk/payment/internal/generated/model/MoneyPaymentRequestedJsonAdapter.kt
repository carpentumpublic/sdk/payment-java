//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.MoneyPaymentRequested

class MoneyPaymentRequestedJsonAdapter {
    @FromJson
    fun fromJson(json: MoneyPaymentRequestedJson): MoneyPaymentRequested {
        val builder = MoneyPaymentRequested.builder()
        builder.amount(json.amount)
        builder.currencyCode(json.currencyCode)
        return builder.build()
    }

    @ToJson
    fun toJson(model: MoneyPaymentRequested): MoneyPaymentRequestedJson {
        val json = MoneyPaymentRequestedJson()
        json.amount = model.amount
        json.currencyCode = model.currencyCode
        return json
    }

    @FromJson
    fun fromJsonImpl(model: MoneyPaymentRequested): MoneyPaymentRequestedImpl {
        return model as MoneyPaymentRequestedImpl
    }

    @ToJson
    fun toJsonImpl(impl: MoneyPaymentRequestedImpl): MoneyPaymentRequested {
        return impl
    }

}