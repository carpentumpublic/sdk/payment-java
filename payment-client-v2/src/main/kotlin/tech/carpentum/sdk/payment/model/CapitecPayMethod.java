//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** CAPITEC_PAY
 *
 * Capitec Pay is an easy, fast and safe way to pay without having to enter the bank card details or share banking login information. The customer can choose the account he wants to pay from and authenticate the payment safely in the banking app.
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public interface CapitecPayMethod extends PayinMethod {
    /** A discriminator value of property {@link #getPaymentMethodCode}. The model class extends {@link PayinMethod}. */
    PayinMethod.PaymentMethodCode PAYMENT_METHOD_CODE = PayinMethod.PaymentMethodCode.CAPITEC_PAY;

    @NotNull Optional<AccountPayinRequestCapitecPay> getAccount();

    /** Your customer e-mail address in RFC 5322 format that is used for identification of the customer's payins. */
    @NotNull Optional<String> getEmailAddress();

    /** Your customer mobile phone number. */
    @NotNull Optional<String> getPhoneNumber();

    @NotNull Optional<String> getSaid();

    @NotNull static Builder builder(CapitecPayMethod copyOf) {
        Builder builder = builder();
        builder.account(copyOf.getAccount().orElse(null));
        builder.emailAddress(copyOf.getEmailAddress().orElse(null));
        builder.phoneNumber(copyOf.getPhoneNumber().orElse(null));
        builder.said(copyOf.getSaid().orElse(null));
        return builder;
    }

    @NotNull static Builder builder() {
        return new CapitecPayMethodImpl.BuilderImpl();
    }

    /** Builder for {@link CapitecPayMethod} model class. */
    interface Builder {

        /**
          * Set {@link CapitecPayMethod#getAccount} property.
          *
          * 
          */
        @NotNull Builder account(AccountPayinRequestCapitecPay account);

        boolean isAccountDefined();


        /**
          * Set {@link CapitecPayMethod#getEmailAddress} property.
          *
          * Your customer e-mail address in RFC 5322 format that is used for identification of the customer's payins.
          */
        @NotNull Builder emailAddress(String emailAddress);

        boolean isEmailAddressDefined();


        /**
          * Set {@link CapitecPayMethod#getPhoneNumber} property.
          *
          * Your customer mobile phone number.
          */
        @NotNull Builder phoneNumber(String phoneNumber);

        boolean isPhoneNumberDefined();


        /**
          * Set {@link CapitecPayMethod#getSaid} property.
          *
          * 
          */
        @NotNull Builder said(String said);

        boolean isSaidDefined();


        /**
         * Create new instance of {@link CapitecPayMethod} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @NotNull CapitecPayMethod build();

    }
}