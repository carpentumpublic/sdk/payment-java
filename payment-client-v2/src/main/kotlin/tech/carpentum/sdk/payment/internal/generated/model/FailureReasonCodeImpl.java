//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** FailureReasonCode
 *
 *  * `SYSTEM` - Internal error. Contact support for more info.
 * `PAYMENT_PROCESS_ERROR` - There has been error in payment process. Contact support for more info.
 * `ENTITY_EXPIRED` - When entity (incoming payment, outgoing payment) has expired.
 * `DIFFERENT_AMOUNT_CONFIRMED` - Customer sent different amount then requested.
 * `CURRENCY_NOT_SUPPORTED` - The selected currency is not supported by merchant.
 * `CURRENCY_PRECISION_EXCEEDED` - The provided payment amount exceeds the smallest fractional unit allowed for the specified currency.
 * `PAYMENT_METHOD_NOT_FOUND` - No payment method has been found for selected payment criteria. Verify your product configuration and contact support for further details.
 * `PAYMENT_METHOD_ERROR` - A processing error has been encountered for payment. An unexpected scenario with required operator intervention, contact support for further details.
 * `PAYMENT_OPERATOR_NOT_FOUND` - The payment operator has not been found.
 * `PAYMENT_OPERATOR_INVALID` - The selected payment operator is not supported for a used payment method.
 * `PAYMENT_OPERATOR_UNAVAILABLE` - The selected payment operator is not available for a used payment method.
 * `PAYMENT_CHANNEL_NO_ACTIVE_FOUND`- No active payment channel that can accept the payment has been found.
 * `PAYMENT_CHANNEL_NO_OPENED_FOUND` - No opened payment channel that can accept the payment has been found.
 * `PAYMENT_CHANNEL_NO_SEGMENT_FOUND` - No payment channel that can accept the payment with the specified segment has been found.
 * `PAYMENT_CHANNEL_AMOUNT_LIMITS` - No payment channel with amount limits suitable for the payment amount has been found.  Limits can be found with [`GET /payment-options`](#operations-Payments-getPaymentOptions) API.
 * `PAYMENT_CHANNEL_DAILY_LIMITS` - All the suitable channels reached their daily limits on payment amount. Select different payment method or contact support for limits adjustments.
 * `IP_DENIED` - The provided IP address is denied. It is either found in the blacklist or is not found in the whitelist.
 * `BALANCE_INSUFFICIENT` - The balance is not sufficient for payment.
 * `INVALID_ACCOUNT_NUMBER` - Account number is invalid or does not exist.
 * `INVALID_ACCOUNT_NAME` - Account name provided in the payment does not match the name assigned to the account.
 * `INVALID_PHONE_NUMBER` - Provided phone number is invalid or does not exist.
 * `INVALID_ID_NUMBER` - Provided ID number is invalid or does not exist.
 * `INCORRECT_CURRENCY` - Paid in different currency than payorder currency.
 * `CUSTOMER_VERIFICATION_REJECTED` - Customer rejected payment verification.
 * `CUSTOMER_VERIFICATION_FAILED` - Customer failed payment verification.
 * `ACCOUNT_TYPE_INELIGIBLE` - Account type of provided account cannot be used for this payment.
 * `NAME_SIMILARITY_CHECK_FAILED` - Name similarity check failed. Fraud prevention measure - name match error.
 * `INVALID_IFSC` - Invalid IFSC Code.
 * `FEATURE_UNAVAILABLE` - The payment can't be completed because the customer has either disabled a payment feature or the feature is unavailable on the customer's device.

 *
 * 
 *
 * The model class is immutable.
 *
 * Use static {@link #of} method to create a new model class instance.
 */
@JsonClass(generateAdapter = false)
public class FailureReasonCodeImpl implements FailureReasonCode {
    private final @NotNull String value;

    private final int hashCode;
    private final String toString;

    public FailureReasonCodeImpl(@NotNull String value) {
        this.value = Objects.requireNonNull(value, "Property 'FailureReasonCode' is required.");

        this.hashCode = value.hashCode();
        this.toString = String.valueOf(value);
    }

    @Override
    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return toString;
    }

    @Override
    public int hashCode() {
        return hashCode;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }

        if (!(obj instanceof FailureReasonCodeImpl)) {
            return false;
        }

        FailureReasonCodeImpl that = (FailureReasonCodeImpl) obj;
        if (!this.value.equals(that.value)) return false;

        return true;
    }
}