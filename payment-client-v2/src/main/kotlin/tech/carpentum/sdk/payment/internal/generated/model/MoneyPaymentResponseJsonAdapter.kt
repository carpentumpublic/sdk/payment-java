//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.MoneyPaymentResponse

class MoneyPaymentResponseJsonAdapter {
    @FromJson
    fun fromJson(json: MoneyPaymentResponseJson): MoneyPaymentResponse {
        val builder = MoneyPaymentResponse.builder()
        builder.amount(json.amount)
        builder.currencyCode(json.currencyCode)
        return builder.build()
    }

    @ToJson
    fun toJson(model: MoneyPaymentResponse): MoneyPaymentResponseJson {
        val json = MoneyPaymentResponseJson()
        json.amount = model.amount
        json.currencyCode = model.currencyCode
        return json
    }

    @FromJson
    fun fromJsonImpl(model: MoneyPaymentResponse): MoneyPaymentResponseImpl {
        return model as MoneyPaymentResponseImpl
    }

    @ToJson
    fun toJsonImpl(impl: MoneyPaymentResponseImpl): MoneyPaymentResponse {
        return impl
    }

}