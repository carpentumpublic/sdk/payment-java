//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.AccountCustomerResponsePayShap

class AccountCustomerResponsePayShapJsonAdapter {
    @FromJson
    fun fromJson(json: AccountCustomerResponsePayShapJson): AccountCustomerResponsePayShap {
        val builder = AccountCustomerResponsePayShap.builder()
        builder.accountName(json.accountName)
        builder.accountNumber(json.accountNumber)
        return builder.build()
    }

    @ToJson
    fun toJson(model: AccountCustomerResponsePayShap): AccountCustomerResponsePayShapJson {
        val json = AccountCustomerResponsePayShapJson()
        json.accountName = model.accountName.orElse(null)
        json.accountNumber = model.accountNumber.orElse(null)
        return json
    }

    @FromJson
    fun fromJsonImpl(model: AccountCustomerResponsePayShap): AccountCustomerResponsePayShapImpl {
        return model as AccountCustomerResponsePayShapImpl
    }

    @ToJson
    fun toJsonImpl(impl: AccountCustomerResponsePayShapImpl): AccountCustomerResponsePayShap {
        return impl
    }

}