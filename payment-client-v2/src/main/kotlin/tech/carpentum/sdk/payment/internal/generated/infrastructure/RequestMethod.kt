package tech.carpentum.sdk.payment.internal.generated.infrastructure

/**
 * Provides enumerated HTTP verbs
 */
enum class RequestMethod {
    GET, DELETE, HEAD, OPTIONS, PATCH, POST, PUT
}
