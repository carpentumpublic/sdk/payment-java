//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** FailureReasons
 *
 * 
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public interface FailureReasons {

    /** Date and time of the payment failure. */
    @NotNull java.time.OffsetDateTime getFailedAt();

    @NotNull FailureReasonCode getFailureReasonCode();

    /** Additional description of the failure reason. */
    @NotNull Optional<String> getMessage();

    @NotNull static Builder builder(FailureReasons copyOf) {
        Builder builder = builder();
        builder.failedAt(copyOf.getFailedAt());
        builder.failureReasonCode(copyOf.getFailureReasonCode());
        builder.message(copyOf.getMessage().orElse(null));
        return builder;
    }

    @NotNull static Builder builder() {
        return new FailureReasonsImpl.BuilderImpl();
    }

    /** Builder for {@link FailureReasons} model class. */
    interface Builder {

        /**
          * Set {@link FailureReasons#getFailedAt} property.
          *
          * Date and time of the payment failure.
          */
        @NotNull Builder failedAt(java.time.OffsetDateTime failedAt);

        boolean isFailedAtDefined();


        /**
          * Set {@link FailureReasons#getFailureReasonCode} property.
          *
          * 
          */
        @NotNull Builder failureReasonCode(FailureReasonCode failureReasonCode);

        boolean isFailureReasonCodeDefined();


        /**
          * Set {@link FailureReasons#getMessage} property.
          *
          * Additional description of the failure reason.
          */
        @NotNull Builder message(String message);

        boolean isMessageDefined();


        /**
         * Create new instance of {@link FailureReasons} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @NotNull FailureReasons build();

    }
}