//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.SettlementPaymentOption

class SettlementPaymentOptionJsonAdapter {
    @FromJson
    fun fromJson(json: SettlementPaymentOptionJson): SettlementPaymentOption {
        val builder = SettlementPaymentOption.builder()
        builder.paymentMethodCode(json.paymentMethodCode)
        builder.currencyCode(json.currencyCode)
        builder.transactionAmountLimit(json.transactionAmountLimit)
        builder.isAvailable(json.isAvailable)
        builder.paymentOperators(json.paymentOperators?.toList())
        return builder.build()
    }

    @ToJson
    fun toJson(model: SettlementPaymentOption): SettlementPaymentOptionJson {
        val json = SettlementPaymentOptionJson()
        json.paymentMethodCode = model.paymentMethodCode
        json.currencyCode = model.currencyCode
        json.transactionAmountLimit = model.transactionAmountLimit
        json.isAvailable = model.isAvailable
        json.paymentOperators = model.paymentOperators.ifEmpty { null }
        return json
    }

    @FromJson
    fun fromJsonImpl(model: SettlementPaymentOption): SettlementPaymentOptionImpl {
        return model as SettlementPaymentOptionImpl
    }

    @ToJson
    fun toJsonImpl(impl: SettlementPaymentOptionImpl): SettlementPaymentOption {
        return impl
    }

}