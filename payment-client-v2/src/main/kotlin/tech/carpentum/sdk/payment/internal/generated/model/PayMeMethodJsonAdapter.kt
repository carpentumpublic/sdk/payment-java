//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.PayMeMethod

class PayMeMethodJsonAdapter {
    @FromJson
    fun fromJson(json: PayMeMethodJson): PayMeMethod {
        val builder = PayMeMethod.builder()
        builder.account(json.account)
        builder.emailAddress(json.emailAddress)
        builder.phoneNumber(json.phoneNumber)
        return builder.build()
    }

    @ToJson
    fun toJson(model: PayMeMethod): PayMeMethodJson {
        val json = PayMeMethodJson()
        json.account = model.account
        json.emailAddress = model.emailAddress.orElse(null)
        json.phoneNumber = model.phoneNumber.orElse(null)
        json.paymentMethodCode = model.paymentMethodCode.name
        return json
    }

    @FromJson
    fun fromJsonImpl(model: PayMeMethod): PayMeMethodImpl {
        return model as PayMeMethodImpl
    }

    @ToJson
    fun toJsonImpl(impl: PayMeMethodImpl): PayMeMethod {
        return impl
    }

}