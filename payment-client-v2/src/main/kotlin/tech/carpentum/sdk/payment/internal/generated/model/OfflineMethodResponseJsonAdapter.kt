//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.OfflineMethodResponse

class OfflineMethodResponseJsonAdapter {
    @FromJson
    fun fromJson(json: OfflineMethodResponseJson): OfflineMethodResponse {
        val builder = OfflineMethodResponse.builder()
        builder.idPayin(json.idPayin)
        builder.idPayment(json.idPayment)
        builder.account(json.account)
        builder.accountCustomer(json.accountCustomer)
        builder.money(json.money)
        builder.moneyRequired(json.moneyRequired)
        builder.vat(json.vat)
        builder.reference(json.reference)
        builder.processor(json.processor)
        builder.qrName(json.qrName)
        builder.qrCode(json.qrCode)
        builder.returnUrl(json.returnUrl)
        builder.paymentOperator(json.paymentOperator)
        builder.productId(json.productId)
        builder.acceptedAt(json.acceptedAt)
        builder.expireAt(json.expireAt)
        return builder.build()
    }

    @ToJson
    fun toJson(model: OfflineMethodResponse): OfflineMethodResponseJson {
        val json = OfflineMethodResponseJson()
        json.idPayin = model.idPayin
        json.idPayment = model.idPayment
        json.account = model.account
        json.accountCustomer = model.accountCustomer
        json.money = model.money
        json.moneyRequired = model.moneyRequired.orElse(null)
        json.vat = model.vat.orElse(null)
        json.reference = model.reference
        json.processor = model.processor.orElse(null)
        json.qrName = model.qrName.orElse(null)
        json.qrCode = model.qrCode.orElse(null)
        json.returnUrl = model.returnUrl
        json.paymentOperator = model.paymentOperator.orElse(null)
        json.productId = model.productId.orElse(null)
        json.acceptedAt = model.acceptedAt
        json.expireAt = model.expireAt
        json.paymentMethodCode = model.paymentMethodCode.name
        return json
    }

    @FromJson
    fun fromJsonImpl(model: OfflineMethodResponse): OfflineMethodResponseImpl {
        return model as OfflineMethodResponseImpl
    }

    @ToJson
    fun toJsonImpl(impl: OfflineMethodResponseImpl): OfflineMethodResponse {
        return impl
    }

}