//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** VAPAY
 *
 * The account parameters for this payment method are used to show payment instructions to the customer.
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public interface VaPayMethodResponse extends PayinMethodResponse {
    /** A discriminator value of property {@link #getPaymentMethodCode}. The model class extends {@link PayinMethodResponse}. */
    PayinMethodResponse.PaymentMethodCode PAYMENT_METHOD_CODE = PayinMethodResponse.PaymentMethodCode.VAPAY;

    @NotNull IdPayin getIdPayin();

    @NotNull IdPayment getIdPayment();

    @NotNull AccountResponseWithBank getAccount();

    @NotNull AccountCustomerResponseVaPay getAccountCustomer();

    @NotNull MoneyPaymentResponse getMoney();

    @NotNull Optional<MoneyVat> getVat();

    /** Reference number of transaction. */
    @NotNull String getReference();

    /** This is the URL where the customers will be redirected after completing a payment.

The URL must be either IP or domain-based. */
    @NotNull String getReturnUrl();

    @NotNull Optional<SelectedPaymentOperatorIncoming> getPaymentOperator();

    /** Date and time when payment was accepted. */
    @NotNull java.time.OffsetDateTime getAcceptedAt();

    /** Date and time of payment expiration. If no money has been transferred to this time, payment is considered failed and callback with status change event will shortly follow. */
    @NotNull java.time.OffsetDateTime getExpireAt();

    @NotNull java.util.List<@NotNull PaymentInstruction> getPaymentInstructionList();

    @NotNull static Builder builder(VaPayMethodResponse copyOf) {
        Builder builder = builder();
        builder.idPayin(copyOf.getIdPayin());
        builder.idPayment(copyOf.getIdPayment());
        builder.account(copyOf.getAccount());
        builder.accountCustomer(copyOf.getAccountCustomer());
        builder.money(copyOf.getMoney());
        builder.vat(copyOf.getVat().orElse(null));
        builder.reference(copyOf.getReference());
        builder.returnUrl(copyOf.getReturnUrl());
        builder.paymentOperator(copyOf.getPaymentOperator().orElse(null));
        builder.acceptedAt(copyOf.getAcceptedAt());
        builder.expireAt(copyOf.getExpireAt());
        builder.paymentInstructionList(copyOf.getPaymentInstructionList());
        return builder;
    }

    @NotNull static Builder builder() {
        return new VaPayMethodResponseImpl.BuilderImpl();
    }

    /** Builder for {@link VaPayMethodResponse} model class. */
    interface Builder {

        /**
          * Set {@link VaPayMethodResponse#getIdPayin} property.
          *
          * 
          */
        @NotNull Builder idPayin(IdPayin idPayin);

        boolean isIdPayinDefined();


        /**
          * Set {@link VaPayMethodResponse#getIdPayment} property.
          *
          * 
          */
        @NotNull Builder idPayment(IdPayment idPayment);

        boolean isIdPaymentDefined();


        /**
          * Set {@link VaPayMethodResponse#getAccount} property.
          *
          * 
          */
        @NotNull Builder account(AccountResponseWithBank account);

        boolean isAccountDefined();


        /**
          * Set {@link VaPayMethodResponse#getAccountCustomer} property.
          *
          * 
          */
        @NotNull Builder accountCustomer(AccountCustomerResponseVaPay accountCustomer);

        boolean isAccountCustomerDefined();


        /**
          * Set {@link VaPayMethodResponse#getMoney} property.
          *
          * 
          */
        @NotNull Builder money(MoneyPaymentResponse money);

        boolean isMoneyDefined();


        /**
          * Set {@link VaPayMethodResponse#getVat} property.
          *
          * 
          */
        @NotNull Builder vat(MoneyVat vat);

        boolean isVatDefined();


        /**
          * Set {@link VaPayMethodResponse#getReference} property.
          *
          * Reference number of transaction.
          */
        @NotNull Builder reference(String reference);

        boolean isReferenceDefined();


        /**
          * Set {@link VaPayMethodResponse#getReturnUrl} property.
          *
          * This is the URL where the customers will be redirected after completing a payment.

The URL must be either IP or domain-based.
          */
        @NotNull Builder returnUrl(String returnUrl);

        boolean isReturnUrlDefined();


        /**
          * Set {@link VaPayMethodResponse#getPaymentOperator} property.
          *
          * 
          */
        @NotNull Builder paymentOperator(SelectedPaymentOperatorIncoming paymentOperator);

        boolean isPaymentOperatorDefined();


        /**
          * Set {@link VaPayMethodResponse#getAcceptedAt} property.
          *
          * Date and time when payment was accepted.
          */
        @NotNull Builder acceptedAt(java.time.OffsetDateTime acceptedAt);

        boolean isAcceptedAtDefined();


        /**
          * Set {@link VaPayMethodResponse#getExpireAt} property.
          *
          * Date and time of payment expiration. If no money has been transferred to this time, payment is considered failed and callback with status change event will shortly follow.
          */
        @NotNull Builder expireAt(java.time.OffsetDateTime expireAt);

        boolean isExpireAtDefined();


        /**
          * Replace all items in {@link VaPayMethodResponse#getPaymentInstructionList} list property.
          *
          * 
          */
        @NotNull Builder paymentInstructionList(java.util.List<@NotNull PaymentInstruction> paymentInstructionList);
        /**
          * Add single item to {@link VaPayMethodResponse#getPaymentInstructionList} list property.
          *
          * 
          */
        @NotNull Builder paymentInstructionListAdd(PaymentInstruction item);
        /**
          * Add all items to {@link VaPayMethodResponse#getPaymentInstructionList} list property.
          *
          * 
          */
        @NotNull Builder paymentInstructionListAddAll(java.util.List<@NotNull PaymentInstruction> paymentInstructionList);


        /**
         * Create new instance of {@link VaPayMethodResponse} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @NotNull VaPayMethodResponse build();

    }
}