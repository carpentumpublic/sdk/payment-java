//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** WALLET_TRANSFER
 *
 * 
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public class WalletTransferMethodResponseImpl implements WalletTransferMethodResponse {
    private final IdPayout idPayout;

    @Override
    public IdPayout getIdPayout() {
        return idPayout;
    }


    private final IdPayment idPayment;

    @Override
    public IdPayment getIdPayment() {
        return idPayment;
    }


    /** Reference number of transaction. */
    private final String reference;

    @Override
    public String getReference() {
        return reference;
    }


    @Override public PaymentMethodCode getPaymentMethodCode() { return PAYMENT_METHOD_CODE; }

    private final int hashCode;
    private final String toString;

    private WalletTransferMethodResponseImpl(BuilderImpl builder) {
        this.idPayout = Objects.requireNonNull(builder.idPayout, "Property 'idPayout' is required.");
        this.idPayment = Objects.requireNonNull(builder.idPayment, "Property 'idPayment' is required.");
        this.reference = Objects.requireNonNull(builder.reference, "Property 'reference' is required.");

        this.hashCode = Objects.hash(idPayout, idPayment, reference);
        this.toString = builder.type + "(" +
                "idPayout=" + idPayout +
                ", idPayment=" + idPayment +
                ", reference=" + reference +
                ')';
    }

    @Override
    public int hashCode() {
        return hashCode;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }

        if (!(obj instanceof WalletTransferMethodResponseImpl)) {
            return false;
        }

        WalletTransferMethodResponseImpl that = (WalletTransferMethodResponseImpl) obj;
        if (!Objects.equals(this.idPayout, that.idPayout)) return false;
        if (!Objects.equals(this.idPayment, that.idPayment)) return false;
        if (!Objects.equals(this.reference, that.reference)) return false;

        return true;
    }

    @Override
    public String toString() {
        return toString;
    }

    /** Builder for {@link WalletTransferMethodResponse} model class. */
    public static class BuilderImpl implements WalletTransferMethodResponse.Builder {
        private IdPayout idPayout = null;
        private IdPayment idPayment = null;
        private String reference = null;

        private final String type;

        public BuilderImpl(String type) {
            this.type = type;
        }

        public BuilderImpl() {
            this("WalletTransferMethodResponse");
        }

        /**
          * Set {@link WalletTransferMethodResponse#getIdPayout} property.
          *
          * 
          */
        @Override
        public BuilderImpl idPayout(IdPayout idPayout) {
            this.idPayout = idPayout;
            return this;
        }

        @Override
        public boolean isIdPayoutDefined() {
            return this.idPayout != null;
        }

        /**
          * Set {@link WalletTransferMethodResponse#getIdPayment} property.
          *
          * 
          */
        @Override
        public BuilderImpl idPayment(IdPayment idPayment) {
            this.idPayment = idPayment;
            return this;
        }

        @Override
        public boolean isIdPaymentDefined() {
            return this.idPayment != null;
        }

        /**
          * Set {@link WalletTransferMethodResponse#getReference} property.
          *
          * Reference number of transaction.
          */
        @Override
        public BuilderImpl reference(String reference) {
            this.reference = reference;
            return this;
        }

        @Override
        public boolean isReferenceDefined() {
            return this.reference != null;
        }

        /**
         * Create new instance of {@link WalletTransferMethodResponse} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @Override
        public WalletTransferMethodResponseImpl build() {
            return new WalletTransferMethodResponseImpl(this);
        }

    }
}