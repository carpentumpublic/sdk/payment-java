//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.AuthTokenRequest

class AuthTokenRequestJsonAdapter {
    @FromJson
    fun fromJson(json: AuthTokenRequestJson): AuthTokenRequest {
        val builder = AuthTokenRequest.builder()
        builder.merchantCode(json.merchantCode)
        builder.secret(json.secret)
        builder.validitySecs(json.validitySecs)
        builder.operations(json.operations?.toList())
        builder.money(json.money)
        builder.moneyProvided(json.moneyProvided)
        builder.currencyCodeRequired(json.currencyCodeRequired)
        builder.moneyRequired(json.moneyRequired)
        builder.currencyCodeProvided(json.currencyCodeProvided)
        builder.settlementMethod(json.settlementMethod)
        return builder.build()
    }

    @ToJson
    fun toJson(model: AuthTokenRequest): AuthTokenRequestJson {
        val json = AuthTokenRequestJson()
        json.merchantCode = model.merchantCode
        json.secret = model.secret
        json.validitySecs = model.validitySecs.orElse(null)
        json.operations = model.operations.ifEmpty { null }
        json.money = model.money.orElse(null)
        json.moneyProvided = model.moneyProvided.orElse(null)
        json.currencyCodeRequired = model.currencyCodeRequired.orElse(null)
        json.moneyRequired = model.moneyRequired.orElse(null)
        json.currencyCodeProvided = model.currencyCodeProvided.orElse(null)
        json.settlementMethod = model.settlementMethod.orElse(null)
        return json
    }

    @FromJson
    fun fromJsonImpl(model: AuthTokenRequest): AuthTokenRequestImpl {
        return model as AuthTokenRequestImpl
    }

    @ToJson
    fun toJsonImpl(impl: AuthTokenRequestImpl): AuthTokenRequest {
        return impl
    }

}