//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.JsonClass;
import tech.carpentum.sdk.payment.model.*;

@JsonClass(generateAdapter = true)
class PaymentProcessJson {
    var status: PaymentStatus? = null
    var failureReasons: FailureReasons? = null
    var createdAt: java.time.OffsetDateTime? = null
    var processedAt: java.time.OffsetDateTime? = null
    var isTest: Boolean? = null
    var processorStatus: ProcessorStatus? = null
}