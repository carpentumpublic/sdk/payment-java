//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** TopUpRequested
 *
 * The requested top up converts the currencies when you want to have your account topped up in one currency but are willing to make a payment in a different currency. Depending on whether you have a certain amount to send or you want to have a certain amount to be added to your account, use one of the objects in the API request body: moneyProvided with currencyRequired filled or moneyRequired with currencyProvided filled.
 *
 * @see TopUpRequestedMoneyProvided
 * @see TopUpRequestedMoneyRequired
 *
 * The model class is immutable.
 *
 *
 */
public interface TopUpRequested {
}