//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.AccountPayinRequestAbsaPay

class AccountPayinRequestAbsaPayJsonAdapter {
    @FromJson
    fun fromJson(json: AccountPayinRequestAbsaPayJson): AccountPayinRequestAbsaPay {
        val builder = AccountPayinRequestAbsaPay.builder()
        builder.accountNumber(json.accountNumber)
        return builder.build()
    }

    @ToJson
    fun toJson(model: AccountPayinRequestAbsaPay): AccountPayinRequestAbsaPayJson {
        val json = AccountPayinRequestAbsaPayJson()
        json.accountNumber = model.accountNumber
        return json
    }

    @FromJson
    fun fromJsonImpl(model: AccountPayinRequestAbsaPay): AccountPayinRequestAbsaPayImpl {
        return model as AccountPayinRequestAbsaPayImpl
    }

    @ToJson
    fun toJsonImpl(impl: AccountPayinRequestAbsaPayImpl): AccountPayinRequestAbsaPay {
        return impl
    }

}