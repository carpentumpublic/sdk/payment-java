//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** PaymentStatus
 *
 * * `PROCESSING` - payment process is not finished yet. Success or failure of payment cannot be determined at this time.
* `SUCCESS` - payment has been successfully processed.
* `FAILED` - payment has been accepted by platform and failed during processing. Details can be found in `failureReasons` attribute.
* `REFUSED` - payment has not been accepted by platform for processing. Details can be found in `failureReasons` attribute.

For every status change new callback `payment.statusChange` is sent. See more info about [callbacks](general.html#callbacks).
 *
 * 
 *
 * The model class is immutable.
 *
 * Use static {@link #of} method to create a new model class instance.
 */
@JsonClass(generateAdapter = false)
public class PaymentStatusImpl implements PaymentStatus {
    private final @NotNull String value;

    private final int hashCode;
    private final String toString;

    public PaymentStatusImpl(@NotNull String value) {
        this.value = Objects.requireNonNull(value, "Property 'PaymentStatus' is required.");

        this.hashCode = value.hashCode();
        this.toString = String.valueOf(value);
    }

    @Override
    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return toString;
    }

    @Override
    public int hashCode() {
        return hashCode;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }

        if (!(obj instanceof PaymentStatusImpl)) {
            return false;
        }

        PaymentStatusImpl that = (PaymentStatusImpl) obj;
        if (!this.value.equals(that.value)) return false;

        return true;
    }
}