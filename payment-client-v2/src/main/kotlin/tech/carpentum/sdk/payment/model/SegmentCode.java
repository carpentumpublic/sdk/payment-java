//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** SegmentCode
 *
 * A customer segment which allows you to divide your customers into groups that reflect similarity among customers in each group.

The goal of segmenting customers is to decide which payment options and limitation you would like to apply to customers in each segment in order to follow your internal business rules.

This parameter is applied only when you have configured the segmentation in your account setting.

For supported segments please refer to [`GET /segments`](#operations-Payments-getSegments) API.
 *
 * 
 *
 * The model class is immutable.
 *
 * Use static {@link #of} method to create a new model class instance.
 */
public interface SegmentCode {


    @NotNull String getValue();

    static SegmentCode of(@NotNull String value) {
        return new SegmentCodeImpl(value);
    }
}