package tech.carpentum.sdk.payment

import io.kotest.core.spec.style.FunSpec
import io.kotest.extensions.system.OverrideMode
import io.kotest.extensions.system.withEnvironment
import io.kotest.matchers.shouldBe
import java.time.Duration
import kotlin.time.Duration.Companion.minutes
import kotlin.time.Duration.Companion.seconds
import kotlin.time.toJavaDuration

/**
 * Tests [PaymentContext].
 */
class PaymentContextTest : FunSpec({

    test("PaymentContext created from environment variables with specified domain") {
        withEnvironment(
            mapOf(
                PaymentContext.Factory.ENV_TECH_CARPENTUM_SDK_PAYMENT_DOMAIN to "testing.payment.domain",
                PaymentContext.Factory.ENV_TECH_CARPENTUM_SDK_PAYMENT_MERCHANT_CODE to "MERCHANT-007",
                PaymentContext.Factory.ENV_TECH_CARPENTUM_SDK_PAYMENT_SECRET to "This 1s M3ga ?ecret!!",
                PaymentContext.Factory.ENV_TECH_CARPENTUM_SDK_PAYMENT_CALL_TIMEOUT to "42",
            ), mode = OverrideMode.SetOrOverride
        ) {
            val ctx: PaymentContext = PaymentContext.builder().build()

            ctx.apiBaseUrl shouldBe "https://api.testing.payment.domain"
            ctx.apiVersion shouldBe 2
            ctx.merchantCode shouldBe "MERCHANT-007"
            ctx.secret shouldBe "This 1s M3ga ?ecret!!"
            ctx.defaultTokenValidity shouldBe 1.minutes.toJavaDuration()
            ctx.defaultCallTimeout shouldBe 42.seconds.toJavaDuration()
        }
    }

    test("PaymentContext created from environment variables with specified api base url") {
        withEnvironment(
            mapOf(
                PaymentContext.Factory.ENV_TECH_CARPENTUM_SDK_PAYMENT_DOMAIN to "testing.payment.domain",
                PaymentContext.Factory.ENV_TECH_CARPENTUM_SDK_PAYMENT_API_BASE_URL to "http://custom.testing.payment.domain",
                PaymentContext.Factory.ENV_TECH_CARPENTUM_SDK_PAYMENT_MERCHANT_CODE to "MERCHANT-007",
                PaymentContext.Factory.ENV_TECH_CARPENTUM_SDK_PAYMENT_SECRET to "This 1s M3ga ?ecret!!",
                PaymentContext.Factory.ENV_TECH_CARPENTUM_SDK_PAYMENT_CALL_TIMEOUT to "42",
            ), mode = OverrideMode.SetOrOverride
        ) {
            val ctx: PaymentContext = PaymentContext.builder().build()

            ctx.apiBaseUrl shouldBe "http://custom.testing.payment.domain"
            ctx.apiVersion shouldBe 2
            ctx.merchantCode shouldBe "MERCHANT-007"
            ctx.secret shouldBe "This 1s M3ga ?ecret!!"
            ctx.defaultTokenValidity shouldBe 1.minutes.toJavaDuration()
            ctx.defaultCallTimeout shouldBe 42.seconds.toJavaDuration()
        }
    }

    test("PaymentContext created by Builder API with specified domain") {
        val ctx: PaymentContext = PaymentContext.builder()
            .apiBaseUrl("http://custom.testing.payment.domain") // is replaced by following apiBaseUrl call
            .domain("testing.payment.domain") //replaces previous apiBaseUrl call
            .merchantCode("MERCHANT-007")
            .secret("This 1s M3ga ?ecret!!")
            .defaultCallTimeout(Duration.ofSeconds(42))
            .build()

        ctx.apiBaseUrl shouldBe "https://api.testing.payment.domain"
        ctx.apiVersion shouldBe 2
        ctx.merchantCode shouldBe "MERCHANT-007"
        ctx.secret shouldBe "This 1s M3ga ?ecret!!"
        ctx.defaultTokenValidity shouldBe 1.minutes.toJavaDuration()
        ctx.defaultCallTimeout shouldBe 42.seconds.toJavaDuration()
    }

    test("PaymentContext created by Builder API with specified api base url") {
        val ctx: PaymentContext = PaymentContext.builder()
            .domain("testing.payment.domain") // is replaced by following apiBaseUrl call
            .apiBaseUrl("http://custom.testing.payment.domain") // replaces previous domain call
            .merchantCode("MERCHANT-007")
            .secret("This 1s M3ga ?ecret!!")
            .defaultCallTimeout(Duration.ofSeconds(42))
            .build()

        ctx.apiBaseUrl shouldBe "http://custom.testing.payment.domain"
        ctx.apiVersion shouldBe 2
        ctx.merchantCode shouldBe "MERCHANT-007"
        ctx.secret shouldBe "This 1s M3ga ?ecret!!"
        ctx.defaultTokenValidity shouldBe 1.minutes.toJavaDuration()
        ctx.defaultCallTimeout shouldBe 42.seconds.toJavaDuration()
    }

})
