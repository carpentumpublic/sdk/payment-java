//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** CustomerTransactionFee
 *
 * Additional fee for customer not included in transaction amount. E.g. gas fee for crypto transactions.
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public class CustomerTransactionFeeImpl implements CustomerTransactionFee {
    private final Optional<Money> minFee;

    @Override
    public Optional<Money> getMinFee() {
        return minFee;
    }


    private final Optional<Money> maxFee;

    @Override
    public Optional<Money> getMaxFee() {
        return maxFee;
    }




    private final int hashCode;
    private final String toString;

    private CustomerTransactionFeeImpl(BuilderImpl builder) {
        this.minFee = Optional.ofNullable(builder.minFee);
        this.maxFee = Optional.ofNullable(builder.maxFee);

        this.hashCode = Objects.hash(minFee, maxFee);
        this.toString = builder.type + "(" +
                "minFee=" + minFee +
                ", maxFee=" + maxFee +
                ')';
    }

    @Override
    public int hashCode() {
        return hashCode;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }

        if (!(obj instanceof CustomerTransactionFeeImpl)) {
            return false;
        }

        CustomerTransactionFeeImpl that = (CustomerTransactionFeeImpl) obj;
        if (!Objects.equals(this.minFee, that.minFee)) return false;
        if (!Objects.equals(this.maxFee, that.maxFee)) return false;

        return true;
    }

    @Override
    public String toString() {
        return toString;
    }

    /** Builder for {@link CustomerTransactionFee} model class. */
    public static class BuilderImpl implements CustomerTransactionFee.Builder {
        private Money minFee = null;
        private Money maxFee = null;

        private final String type;

        public BuilderImpl(String type) {
            this.type = type;
        }

        public BuilderImpl() {
            this("CustomerTransactionFee");
        }

        /**
          * Set {@link CustomerTransactionFee#getMinFee} property.
          *
          * 
          */
        @Override
        public BuilderImpl minFee(Money minFee) {
            this.minFee = minFee;
            return this;
        }

        @Override
        public boolean isMinFeeDefined() {
            return this.minFee != null;
        }

        /**
          * Set {@link CustomerTransactionFee#getMaxFee} property.
          *
          * 
          */
        @Override
        public BuilderImpl maxFee(Money maxFee) {
            this.maxFee = maxFee;
            return this;
        }

        @Override
        public boolean isMaxFeeDefined() {
            return this.maxFee != null;
        }

        /**
         * Create new instance of {@link CustomerTransactionFee} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @Override
        public CustomerTransactionFeeImpl build() {
            return new CustomerTransactionFeeImpl(this);
        }

    }
}