//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.CodePaymentMethodResponse

class CodePaymentMethodResponseJsonAdapter {
    @FromJson
    fun fromJson(json: CodePaymentMethodResponseJson): CodePaymentMethodResponse {
        val builder = CodePaymentMethodResponse.builder()
        builder.idPayin(json.idPayin)
        builder.idPayment(json.idPayment)
        builder.account(json.account)
        builder.accountCustomer(json.accountCustomer)
        builder.money(json.money)
        builder.vat(json.vat)
        builder.reference(json.reference)
        builder.returnUrl(json.returnUrl)
        builder.paymentOperator(json.paymentOperator)
        builder.acceptedAt(json.acceptedAt)
        builder.expireAt(json.expireAt)
        return builder.build()
    }

    @ToJson
    fun toJson(model: CodePaymentMethodResponse): CodePaymentMethodResponseJson {
        val json = CodePaymentMethodResponseJson()
        json.idPayin = model.idPayin
        json.idPayment = model.idPayment
        json.account = model.account
        json.accountCustomer = model.accountCustomer
        json.money = model.money
        json.vat = model.vat.orElse(null)
        json.reference = model.reference
        json.returnUrl = model.returnUrl
        json.paymentOperator = model.paymentOperator
        json.acceptedAt = model.acceptedAt
        json.expireAt = model.expireAt
        json.paymentMethodCode = model.paymentMethodCode.name
        return json
    }

    @FromJson
    fun fromJsonImpl(model: CodePaymentMethodResponse): CodePaymentMethodResponseImpl {
        return model as CodePaymentMethodResponseImpl
    }

    @ToJson
    fun toJsonImpl(impl: CodePaymentMethodResponseImpl): CodePaymentMethodResponse {
        return impl
    }

}