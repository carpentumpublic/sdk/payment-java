plugins {
    // Apply the java Plugin to add support for Java.
    java
}

repositories {
    // Use Maven Central for resolving dependencies.
    mavenCentral()
}

dependencies {
    constraints {
        // Define dependency versions as constraints
        implementation("com.squareup.moshi:moshi-adapters:1.15.1")
        implementation("com.squareup.okio:okio:3.9.0")
        implementation("com.squareup.okhttp3:okhttp:4.12.0")
        implementation("com.squareup.okhttp3:logging-interceptor:4.12.0")
        implementation("org.slf4j:slf4j-api:2.0.13")
        implementation("ch.qos.logback:logback-classic:1.5.6")
        implementation("ch.qos.logback:logback-core:1.5.6")
        implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8:2.0.0")
        implementation("org.jetbrains:annotations:24.1.0")
    }

    // Use JUnit Jupiter API for testing.
    testImplementation("org.junit.jupiter:junit-jupiter-api:5.10.2")

    // Use JUnit Jupiter Engine for testing.
    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine")
}

tasks.test {
    // Use junit platform for unit tests.
    useJUnitPlatform()
    reports {
        junitXml.required.set(true)
        html.required.set(true)
    }
    jvmArgs = listOf("--add-opens=java.base/java.util=ALL-UNNAMED")
}

java {
    toolchain {
        languageVersion.set(JavaLanguageVersion.of(17))
    }
}

tasks.register<Copy>("copyDependencies") {
    group = "Development"
    from(configurations.runtimeClasspath)
    into(layout.buildDirectory.dir("dependencies"))
}
