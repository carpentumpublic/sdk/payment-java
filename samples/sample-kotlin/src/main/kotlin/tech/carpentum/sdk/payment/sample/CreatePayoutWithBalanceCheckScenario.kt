package tech.carpentum.sdk.payment.sample

import org.slf4j.LoggerFactory
import tech.carpentum.sdk.payment.AccountsApi
import tech.carpentum.sdk.payment.AccountsApi.Factory.defineListBalancesEndpoint
import tech.carpentum.sdk.payment.AuthTokenOperations
import tech.carpentum.sdk.payment.OutgoingPaymentsApi
import tech.carpentum.sdk.payment.OutgoingPaymentsApi.Factory.defineCreatePayoutEndpoint
import tech.carpentum.sdk.payment.OutgoingPaymentsApi.Factory.defineGetPayoutEndpoint
import tech.carpentum.sdk.payment.PaymentContext
import tech.carpentum.sdk.payment.ResponseException
import tech.carpentum.sdk.payment.model.Payout
import tech.carpentum.sdk.payment.model.PayoutAccepted
import tech.carpentum.sdk.payment.model.PayoutDetail
import java.io.InterruptedIOException
import java.time.Duration
import java.util.*

/**
 * A little complex scenario that combines Accounts API and Outgoing Payments API operations
 * with Authorization token for both APIs and single `idPayout`.
 */
class CreatePayoutWithBalanceCheckScenario private constructor(
    private val accountsApi: AccountsApi,
    private val outgoingPaymentsApi: OutgoingPaymentsApi,
    private val idPayout: String,
    authToken: String
) : AbstractScenario(authToken) {

    @Throws(ResponseException::class, InterruptedIOException::class)
    fun createPaymentWithBalanceCheck(payout: Payout): Optional<PayoutAccepted> {
        val requestedBalance = payout.paymentRequested.money.amount
        return try {
            val data = accountsApi.listBalances(setOf(payout.paymentRequested.money.currencyCode.value)).data
            val availableBalance = data[0].availableBalance
            if (availableBalance.compareTo(requestedBalance) > 0) {
                Optional.of(outgoingPaymentsApi.createPayout(idPayout, payout))
            } else {
                LOGGER.debug("Insufficient balance. Available: {}, Requested: {}.", availableBalance, requestedBalance)
                Optional.empty()
            }
        } catch (ex: ResponseException) {
            LOGGER.debug("Error response while getting available balance. Detail: {}", ex.localizedMessage)
            throw ex
        }
    }

    @Throws(ResponseException::class, InterruptedIOException::class)
    fun getDetail(): PayoutDetail {
        return outgoingPaymentsApi.getPayout(idPayout)
    }

    companion object {
        private val LOGGER = LoggerFactory.getLogger(CreatePayoutWithBalanceCheckScenario::class.java)

        /**
         * Creates new instance of the scenario with newly created Authorization token with specified validity.
         */
        @Throws(ResponseException::class, InterruptedIOException::class)
        fun create(
            context: PaymentContext,
            idPayout: String,
            validity: Duration? = null
        ): CreatePayoutWithBalanceCheckScenario {
            val token = context.createAuthToken(
                AuthTokenOperations()
                    .grant(defineListBalancesEndpoint())
                    .grant(defineCreatePayoutEndpoint().forId(idPayout))
                    .grant(defineGetPayoutEndpoint().forId(idPayout)),
                validity
            ).token
            return CreatePayoutWithBalanceCheckScenario(
                AccountsApi.create(context, token, Duration.ofSeconds(10)),
                OutgoingPaymentsApi.create(context, token, Duration.ofSeconds(20)),
                idPayout,
                token
            )
        }

        /**
         * Creates new instance of the scenario with existing Authorization token.
         */
        @Throws(ResponseException::class, InterruptedIOException::class)
        fun create(context: PaymentContext, idPayout: String, authToken: String): CreatePayoutWithBalanceCheckScenario {
            return CreatePayoutWithBalanceCheckScenario(
                AccountsApi.create(context, authToken, Duration.ofSeconds(10)),
                OutgoingPaymentsApi.create(context, authToken, Duration.ofSeconds(20)),
                idPayout,
                authToken
            )
        }
    }
}