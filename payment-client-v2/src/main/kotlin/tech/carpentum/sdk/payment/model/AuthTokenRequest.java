//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** AuthTokenRequest
 *
 * 
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public interface AuthTokenRequest {

    /** Your Merchant ID. This Merchant ID is obtained upon [Merchant enrollment process](sandbox.html#sandbox-account-registration). */
    @NotNull String getMerchantCode();

    /** See [Merchant Secret Key](terminology.html#term-Merchant-secret). */
    @NotNull String getSecret();

    /** Validity of the token in seconds. Maximum value 3600, default value 1800 (30 minutes). */
    @NotNull Optional<Integer> getValiditySecs();

    /** Endpoints that can be called with the issued token. See [Fine-grained privileges](general.html#fine-grained-privileges) description. */
    @NotNull java.util.List<@NotNull String> getOperations();

    @NotNull Optional<Money> getMoney();

    @NotNull Optional<Money> getMoneyProvided();

    @NotNull Optional<CurrencyCode> getCurrencyCodeRequired();

    @NotNull Optional<Money> getMoneyRequired();

    @NotNull Optional<CurrencyCode> getCurrencyCodeProvided();

    @NotNull Optional<SettlementMethod> getSettlementMethod();

    @NotNull static Builder builder(AuthTokenRequest copyOf) {
        Builder builder = builder();
        builder.merchantCode(copyOf.getMerchantCode());
        builder.secret(copyOf.getSecret());
        builder.validitySecs(copyOf.getValiditySecs().orElse(null));
        builder.operations(copyOf.getOperations());
        builder.money(copyOf.getMoney().orElse(null));
        builder.moneyProvided(copyOf.getMoneyProvided().orElse(null));
        builder.currencyCodeRequired(copyOf.getCurrencyCodeRequired().orElse(null));
        builder.moneyRequired(copyOf.getMoneyRequired().orElse(null));
        builder.currencyCodeProvided(copyOf.getCurrencyCodeProvided().orElse(null));
        builder.settlementMethod(copyOf.getSettlementMethod().orElse(null));
        return builder;
    }

    @NotNull static Builder builder() {
        return new AuthTokenRequestImpl.BuilderImpl();
    }

    /** Builder for {@link AuthTokenRequest} model class. */
    interface Builder {

        /**
          * Set {@link AuthTokenRequest#getMerchantCode} property.
          *
          * Your Merchant ID. This Merchant ID is obtained upon [Merchant enrollment process](sandbox.html#sandbox-account-registration).
          */
        @NotNull Builder merchantCode(String merchantCode);

        boolean isMerchantCodeDefined();


        /**
          * Set {@link AuthTokenRequest#getSecret} property.
          *
          * See [Merchant Secret Key](terminology.html#term-Merchant-secret).
          */
        @NotNull Builder secret(String secret);

        boolean isSecretDefined();


        /**
          * Set {@link AuthTokenRequest#getValiditySecs} property.
          *
          * Validity of the token in seconds. Maximum value 3600, default value 1800 (30 minutes).
          */
        @NotNull Builder validitySecs(Integer validitySecs);

        boolean isValiditySecsDefined();


        /**
          * Replace all items in {@link AuthTokenRequest#getOperations} list property.
          *
          * Endpoints that can be called with the issued token. See [Fine-grained privileges](general.html#fine-grained-privileges) description.
          */
        @NotNull Builder operations(java.util.List<@NotNull String> operations);
        /**
          * Add single item to {@link AuthTokenRequest#getOperations} list property.
          *
          * Endpoints that can be called with the issued token. See [Fine-grained privileges](general.html#fine-grained-privileges) description.
          */
        @NotNull Builder operationsAdd(String item);
        /**
          * Add all items to {@link AuthTokenRequest#getOperations} list property.
          *
          * Endpoints that can be called with the issued token. See [Fine-grained privileges](general.html#fine-grained-privileges) description.
          */
        @NotNull Builder operationsAddAll(java.util.List<@NotNull String> operations);


        /**
          * Set {@link AuthTokenRequest#getMoney} property.
          *
          * 
          */
        @NotNull Builder money(Money money);

        boolean isMoneyDefined();


        /**
          * Set {@link AuthTokenRequest#getMoneyProvided} property.
          *
          * 
          */
        @NotNull Builder moneyProvided(Money moneyProvided);

        boolean isMoneyProvidedDefined();


        /**
          * Set {@link AuthTokenRequest#getCurrencyCodeRequired} property.
          *
          * 
          */
        @NotNull Builder currencyCodeRequired(CurrencyCode currencyCodeRequired);

        boolean isCurrencyCodeRequiredDefined();


        /**
          * Set {@link AuthTokenRequest#getMoneyRequired} property.
          *
          * 
          */
        @NotNull Builder moneyRequired(Money moneyRequired);

        boolean isMoneyRequiredDefined();


        /**
          * Set {@link AuthTokenRequest#getCurrencyCodeProvided} property.
          *
          * 
          */
        @NotNull Builder currencyCodeProvided(CurrencyCode currencyCodeProvided);

        boolean isCurrencyCodeProvidedDefined();


        /**
          * Set {@link AuthTokenRequest#getSettlementMethod} property.
          *
          * 
          */
        @NotNull Builder settlementMethod(SettlementMethod settlementMethod);

        boolean isSettlementMethodDefined();


        /**
         * Create new instance of {@link AuthTokenRequest} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @NotNull AuthTokenRequest build();

    }
}