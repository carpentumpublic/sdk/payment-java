//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** PayoutAccepted
 *
 * The outgoing payment has been accepted for processing.
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public class PayoutAcceptedImpl implements PayoutAccepted {
    private final PaymentRequested paymentRequested;

    @Override
    public PaymentRequested getPaymentRequested() {
        return paymentRequested;
    }


    private final PayoutMethodResponse paymentMethodResponse;

    @Override
    public PayoutMethodResponse getPaymentMethodResponse() {
        return paymentMethodResponse;
    }




    private final int hashCode;
    private final String toString;

    private PayoutAcceptedImpl(BuilderImpl builder) {
        this.paymentRequested = Objects.requireNonNull(builder.paymentRequested, "Property 'paymentRequested' is required.");
        this.paymentMethodResponse = Objects.requireNonNull(builder.paymentMethodResponse, "Property 'paymentMethodResponse' is required.");

        this.hashCode = Objects.hash(paymentRequested, paymentMethodResponse);
        this.toString = builder.type + "(" +
                "paymentRequested=" + paymentRequested +
                ", paymentMethodResponse=" + paymentMethodResponse +
                ')';
    }

    @Override
    public int hashCode() {
        return hashCode;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }

        if (!(obj instanceof PayoutAcceptedImpl)) {
            return false;
        }

        PayoutAcceptedImpl that = (PayoutAcceptedImpl) obj;
        if (!Objects.equals(this.paymentRequested, that.paymentRequested)) return false;
        if (!Objects.equals(this.paymentMethodResponse, that.paymentMethodResponse)) return false;

        return true;
    }

    @Override
    public String toString() {
        return toString;
    }

    /** Builder for {@link PayoutAccepted} model class. */
    public static class BuilderImpl implements PayoutAccepted.Builder {
        private PaymentRequested paymentRequested = null;
        private PayoutMethodResponse paymentMethodResponse = null;

        private final String type;

        public BuilderImpl(String type) {
            this.type = type;
        }

        public BuilderImpl() {
            this("PayoutAccepted");
        }

        /**
          * Set {@link PayoutAccepted#getPaymentRequested} property.
          *
          * 
          */
        @Override
        public BuilderImpl paymentRequested(PaymentRequested paymentRequested) {
            this.paymentRequested = paymentRequested;
            return this;
        }

        @Override
        public boolean isPaymentRequestedDefined() {
            return this.paymentRequested != null;
        }

        /**
          * Set {@link PayoutAccepted#getPaymentMethodResponse} property.
          *
          * 
          */
        @Override
        public BuilderImpl paymentMethodResponse(PayoutMethodResponse paymentMethodResponse) {
            this.paymentMethodResponse = paymentMethodResponse;
            return this;
        }

        @Override
        public boolean isPaymentMethodResponseDefined() {
            return this.paymentMethodResponse != null;
        }

        /**
         * Create new instance of {@link PayoutAccepted} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @Override
        public PayoutAcceptedImpl build() {
            return new PayoutAcceptedImpl(this);
        }

    }
}