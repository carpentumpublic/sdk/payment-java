//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.MobileMoneyMethodResponse

class MobileMoneyMethodResponseJsonAdapter {
    @FromJson
    fun fromJson(json: MobileMoneyMethodResponseJson): MobileMoneyMethodResponse {
        val builder = MobileMoneyMethodResponse.builder()
        builder.idPayin(json.idPayin)
        builder.idPayment(json.idPayment)
        builder.accountCustomer(json.accountCustomer)
        builder.money(json.money)
        builder.vat(json.vat)
        builder.merchantName(json.merchantName)
        builder.phoneNumber(json.phoneNumber)
        builder.reference(json.reference)
        builder.returnUrl(json.returnUrl)
        builder.acceptedAt(json.acceptedAt)
        builder.expireAt(json.expireAt)
        return builder.build()
    }

    @ToJson
    fun toJson(model: MobileMoneyMethodResponse): MobileMoneyMethodResponseJson {
        val json = MobileMoneyMethodResponseJson()
        json.idPayin = model.idPayin
        json.idPayment = model.idPayment
        json.accountCustomer = model.accountCustomer
        json.money = model.money
        json.vat = model.vat.orElse(null)
        json.merchantName = model.merchantName
        json.phoneNumber = model.phoneNumber.orElse(null)
        json.reference = model.reference
        json.returnUrl = model.returnUrl
        json.acceptedAt = model.acceptedAt
        json.expireAt = model.expireAt
        json.paymentMethodCode = model.paymentMethodCode.name
        return json
    }

    @FromJson
    fun fromJsonImpl(model: MobileMoneyMethodResponse): MobileMoneyMethodResponseImpl {
        return model as MobileMoneyMethodResponseImpl
    }

    @ToJson
    fun toJsonImpl(impl: MobileMoneyMethodResponseImpl): MobileMoneyMethodResponse {
        return impl
    }

}