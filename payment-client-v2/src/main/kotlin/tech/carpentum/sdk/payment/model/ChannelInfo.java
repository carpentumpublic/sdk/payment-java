//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** ChannelInfo
 *
 * Merchant information which is supposed to be displayed to a customer during payment process in the [Payment Web App](payin-webapp.html) or your UI supporting payment flow.
Merchant information setup is available in the Merchant BackOffice portal.
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public interface ChannelInfo {

    @NotNull String getName();

    @NotNull static ChannelInfo ofName(String name) { return builder().name(name).build(); }

    @NotNull static Builder builder() {
        return new ChannelInfoImpl.BuilderImpl();
    }

    /** Builder for {@link ChannelInfo} model class. */
    interface Builder {

        /**
          * Set {@link ChannelInfo#getName} property.
          *
          * 
          */
        @NotNull Builder name(String name);

        boolean isNameDefined();


        /**
         * Create new instance of {@link ChannelInfo} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @NotNull ChannelInfo build();

    }
}