//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** AccountPayinRequestVaPayVerif
 *
 * Parameters of your customer's bank or wallet account which your customer sends funds from. These account parameters are used for the sender's account verification in processing of the payment.
Which parameters are mandatory depends on the payment method and the currency your customer choose to pay.
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public interface AccountPayinRequestVaPayVerif {

    /** Account Name is the name of the person who holds the bank or wallet account which your customer sends funds from to make his payment.
The name should be in the same format as the account holder name of the account. Allows numbers, some special characters and UNICODE symbols, see validation pattern. */
    @NotNull Optional<String> getAccountName();

    /** Account Number is the number of your customer's bank account which your customer sends funds from to make his payment. */
    @NotNull String getAccountNumber();

    @NotNull static Builder builder(AccountPayinRequestVaPayVerif copyOf) {
        Builder builder = builder();
        builder.accountName(copyOf.getAccountName().orElse(null));
        builder.accountNumber(copyOf.getAccountNumber());
        return builder;
    }

    @NotNull static Builder builder() {
        return new AccountPayinRequestVaPayVerifImpl.BuilderImpl();
    }

    /** Builder for {@link AccountPayinRequestVaPayVerif} model class. */
    interface Builder {

        /**
          * Set {@link AccountPayinRequestVaPayVerif#getAccountName} property.
          *
          * Account Name is the name of the person who holds the bank or wallet account which your customer sends funds from to make his payment.
The name should be in the same format as the account holder name of the account. Allows numbers, some special characters and UNICODE symbols, see validation pattern.
          */
        @NotNull Builder accountName(String accountName);

        boolean isAccountNameDefined();


        /**
          * Set {@link AccountPayinRequestVaPayVerif#getAccountNumber} property.
          *
          * Account Number is the number of your customer's bank account which your customer sends funds from to make his payment.
          */
        @NotNull Builder accountNumber(String accountNumber);

        boolean isAccountNumberDefined();


        /**
         * Create new instance of {@link AccountPayinRequestVaPayVerif} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @NotNull AccountPayinRequestVaPayVerif build();

    }
}