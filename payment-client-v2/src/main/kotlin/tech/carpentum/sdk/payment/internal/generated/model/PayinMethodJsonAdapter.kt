//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import tech.carpentum.sdk.payment.model.PayinMethod
import tech.carpentum.sdk.payment.model.PayinMethod.PaymentMethodCode

import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.Moshi
import com.squareup.moshi.adapters.PolymorphicJsonAdapterFactory
import java.lang.reflect.Type

class PayinMethodJsonAdapter : JsonAdapter.Factory {
    override fun create(type: Type, annotations: MutableSet<out Annotation>, moshi: Moshi): JsonAdapter<*>? {
        return PolymorphicJsonAdapterFactory
            .of(PayinMethod::class.java, PayinMethod.DISCRIMINATOR)
            .withSubtype(AbsaPayMethodImpl::class.java, PaymentMethodCode.ABSA_PAY.toString())
            .withSubtype(CapitecPayMethodImpl::class.java, PaymentMethodCode.CAPITEC_PAY.toString())
            .withSubtype(CodePaymentMethodImpl::class.java, PaymentMethodCode.CODE_PAYMENT.toString())
            .withSubtype(CryptoOfflineMethodImpl::class.java, PaymentMethodCode.CRYPTO_OFFLINE.toString())
            .withSubtype(DuitNowMethodImpl::class.java, PaymentMethodCode.DUITNOW.toString())
            .withSubtype(EWalletMethodImpl::class.java, PaymentMethodCode.EWALLET.toString())
            .withSubtype(IMPSMethodImpl::class.java, PaymentMethodCode.IMPS.toString())
            .withSubtype(MobileMoneyMethodImpl::class.java, PaymentMethodCode.MOBILE_MONEY.toString())
            .withSubtype(NetBankingMethodImpl::class.java, PaymentMethodCode.NETBANKING.toString())
            .withSubtype(OfflineMethodImpl::class.java, PaymentMethodCode.OFFLINE.toString())
            .withSubtype(OnlineMethodImpl::class.java, PaymentMethodCode.ONLINE.toString())
            .withSubtype(P2AV2MethodImpl::class.java, PaymentMethodCode.P2A_V2.toString())
            .withSubtype(PayShapMethodImpl::class.java, PaymentMethodCode.PAYSHAP.toString())
            .withSubtype(PayMeMethodImpl::class.java, PaymentMethodCode.PAY_ME.toString())
            .withSubtype(PromptPayMethodImpl::class.java, PaymentMethodCode.PROMPTPAY.toString())
            .withSubtype(QrisPayMethodImpl::class.java, PaymentMethodCode.QRISPAY.toString())
            .withSubtype(QrPhMethodImpl::class.java, PaymentMethodCode.QRPH.toString())
            .withSubtype(UpiIdMethodImpl::class.java, PaymentMethodCode.UPIID.toString())
            .withSubtype(UpiQRMethodImpl::class.java, PaymentMethodCode.UPIQR.toString())
            .withSubtype(VaPayMethodImpl::class.java, PaymentMethodCode.VAPAY.toString())
            .withSubtype(VaPayVerifMethodImpl::class.java, PaymentMethodCode.VAPAY_VERIF.toString())
            .withSubtype(VietQRMethodImpl::class.java, PaymentMethodCode.VIETQR.toString())
            .create(type, annotations, moshi)
    }
}