//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** AvailableForexCurrencyPair
 *
 * 
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public class AvailableForexCurrencyPairImpl implements AvailableForexCurrencyPair {
    private final CurrencyCode baseCurrencyCode;

    @Override
    public CurrencyCode getBaseCurrencyCode() {
        return baseCurrencyCode;
    }


    private final CurrencyCode quoteCurrencyCode;

    @Override
    public CurrencyCode getQuoteCurrencyCode() {
        return quoteCurrencyCode;
    }




    private final int hashCode;
    private final String toString;

    private AvailableForexCurrencyPairImpl(BuilderImpl builder) {
        this.baseCurrencyCode = Objects.requireNonNull(builder.baseCurrencyCode, "Property 'baseCurrencyCode' is required.");
        this.quoteCurrencyCode = Objects.requireNonNull(builder.quoteCurrencyCode, "Property 'quoteCurrencyCode' is required.");

        this.hashCode = Objects.hash(baseCurrencyCode, quoteCurrencyCode);
        this.toString = builder.type + "(" +
                "baseCurrencyCode=" + baseCurrencyCode +
                ", quoteCurrencyCode=" + quoteCurrencyCode +
                ')';
    }

    @Override
    public int hashCode() {
        return hashCode;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }

        if (!(obj instanceof AvailableForexCurrencyPairImpl)) {
            return false;
        }

        AvailableForexCurrencyPairImpl that = (AvailableForexCurrencyPairImpl) obj;
        if (!Objects.equals(this.baseCurrencyCode, that.baseCurrencyCode)) return false;
        if (!Objects.equals(this.quoteCurrencyCode, that.quoteCurrencyCode)) return false;

        return true;
    }

    @Override
    public String toString() {
        return toString;
    }

    /** Builder for {@link AvailableForexCurrencyPair} model class. */
    public static class BuilderImpl implements AvailableForexCurrencyPair.Builder {
        private CurrencyCode baseCurrencyCode = null;
        private CurrencyCode quoteCurrencyCode = null;

        private final String type;

        public BuilderImpl(String type) {
            this.type = type;
        }

        public BuilderImpl() {
            this("AvailableForexCurrencyPair");
        }

        /**
          * Set {@link AvailableForexCurrencyPair#getBaseCurrencyCode} property.
          *
          * 
          */
        @Override
        public BuilderImpl baseCurrencyCode(CurrencyCode baseCurrencyCode) {
            this.baseCurrencyCode = baseCurrencyCode;
            return this;
        }

        @Override
        public boolean isBaseCurrencyCodeDefined() {
            return this.baseCurrencyCode != null;
        }

        /**
          * Set {@link AvailableForexCurrencyPair#getQuoteCurrencyCode} property.
          *
          * 
          */
        @Override
        public BuilderImpl quoteCurrencyCode(CurrencyCode quoteCurrencyCode) {
            this.quoteCurrencyCode = quoteCurrencyCode;
            return this;
        }

        @Override
        public boolean isQuoteCurrencyCodeDefined() {
            return this.quoteCurrencyCode != null;
        }

        /**
         * Create new instance of {@link AvailableForexCurrencyPair} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @Override
        public AvailableForexCurrencyPairImpl build() {
            return new AvailableForexCurrencyPairImpl(this);
        }

    }
}