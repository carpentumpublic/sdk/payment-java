package tech.carpentum.sdk.payment.internal.api

import tech.carpentum.sdk.payment.internal.generated.api.AuthApi
import java.time.Duration

internal class EnhancedAuthApi(
    basePath: String = defaultBasePath,
    callTimeout: Duration
) : AuthApi(basePath, ApiUtils.getClient(callTimeout))
