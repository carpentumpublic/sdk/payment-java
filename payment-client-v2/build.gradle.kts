import org.jetbrains.dokka.gradle.DokkaTask
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile
import java.net.URI

plugins {
    id("tech.carpentum.sdk.payment.kotlin-library-conventions")
    id("org.jetbrains.dokka") version "1.9.20"
    id("maven-publish")
    id("signing")
    id("com.google.devtools.ksp") version "2.0.0-1.0.21"
}

repositories {
    // Use Maven Central for resolving dependencies.
    mavenCentral()
}

dependencies {
    ksp("com.squareup.moshi:moshi-kotlin-codegen:1.15.1")

    implementation("com.squareup.moshi:moshi-adapters")
    implementation("com.squareup.okio:okio")
    implementation("com.squareup.okhttp3:okhttp")
    implementation("com.squareup.okhttp3:logging-interceptor")
    implementation("org.slf4j:slf4j-api")

    testImplementation("io.kotest:kotest-runner-junit5")
}

java {
    withSourcesJar()
}

tasks.withType<KotlinCompile>().configureEach {
    kotlinOptions {
        freeCompilerArgs = listOf(
            "-java-parameters",
            //"-Xopt-in=kotlin.RequiresOptIn", //see ApiClient, method annotated by @OptIn(ExperimentalStdlibApi::class)
        )
    }
}

tasks.withType<JavaCompile>().configureEach {
    options.compilerArgs.add("-parameters")
}

configure<SourceSetContainer> {
    named("main") {
        java.srcDir("src/main/kotlin")
    }
}
tasks.withType<DokkaTask>().configureEach {
    dokkaSourceSets {
        named("main") {
            moduleName.set("Carpentum Payment system Java SDK")
            includes.from("src/main/dokka/Module.md")
            jdkVersion.set(17)
            sourceLink {
                localDirectory.set(file("src/main/kotlin"))
                remoteUrl.set(URI("https://gitlab.com/carpentumpublic/sdk/payment-java/-/blob/master/payment-client-v2/src/main/kotlin").toURL())
                remoteLineSuffix.set("#L")
            }
            externalDocumentationLink {
                url.set(uri("https://gitlab.com/carpentumpublic/sdk/payment-java").toURL())
                packageListUrl.set(uri("file:///${layout.buildDirectory}/dokka/javadoc/package-list").toURL())
            }
            perPackageOption {
                matchingRegex.set(""".*\.internal.*""") // will match all .internal packages and sub-packages
                suppress.set(true)
            }
        }
    }
}

val dokkaJavadocJar by tasks.register<Jar>("dokkaJavadocJar") {
    dependsOn(tasks.dokkaJavadoc)
    from(tasks.dokkaJavadoc.flatMap { it.outputDirectory })
    archiveClassifier.set("javadoc")
}

publishing {
    publications {
        register<MavenPublication>("mavenJava") {
            from(components["java"])
            artifact(dokkaJavadocJar)
            pom {
                name.set("Carpentum Payment system Java SDK")
                description.set("Carpentum Payment system Java SDK")
                url.set("https://gitlab.com/carpentumpublic/sdk/payment-java/")
                organization { 
                    name.set("Carpentum Systems")
                }
                licenses {
                    license {
                        name.set("The MIT License")
                        url.set("https://tldrlegal.com/license/mit-license")
                    }
                }
                developers {
                    developer {
                        id.set("liborkramolis")
                        name.set("Libor Kramolis")
                        email.set("l.kramolis@psp.team")
                    }
                }
                scm {
                    connection.set("scm:git:git://gitlab.com:carpentumpublic/sdk/payment-java.git")
                    developerConnection.set("scm:git:ssh://gitlab.com:carpentumpublic/sdk/payment-java.git")
                    url.set("https://gitlab.com/carpentumpublic/sdk/payment-java/")
                }
            }
        }
    }
    repositories {
        maven("https://gitlab.com/api/v4/projects/27322734/packages/maven") {
            name = "gitlab"
            credentials(HttpHeaderCredentials::class.java) {
                val jobToken = System.getenv("CI_JOB_TOKEN")
                if (jobToken != null) {
                    name = "Job-Token"
                    value = System.getenv("CI_JOB_TOKEN")
                } else {
                    name = "Private-Token"
                    value = System.getenv("GITLAB_TOKEN")
                }
            }
            authentication {
                register<HttpHeaderAuthentication>("header")
            }
        }
    }
}

signing {
    val signingKeyId: String? by project
    val signingKey: String? by project
    val signingPassword: String? by project
    useInMemoryPgpKeys(signingKeyId, signingKey, signingPassword)
    sign(publishing.publications["mavenJava"])
}

tasks.register("publishToGitlabRepository") {
    group = "publishing"
    description = "Publishes all Maven publications to the Gitlab Maven repository."
    dependsOn(tasks.withType<PublishToMavenRepository>().matching {
        it.repository == publishing.repositories["gitlab"]
    })
}
