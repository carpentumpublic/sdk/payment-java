//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.AvailableTopUpOptions

class AvailableTopUpOptionsJsonAdapter {
    @FromJson
    fun fromJson(json: AvailableTopUpOptionsJson): AvailableTopUpOptions {
        val builder = AvailableTopUpOptions.builder()
        builder.options(json.options?.toList())
        return builder.build()
    }

    @ToJson
    fun toJson(model: AvailableTopUpOptions): AvailableTopUpOptionsJson {
        val json = AvailableTopUpOptionsJson()
        json.options = model.options.ifEmpty { null }
        return json
    }

    @FromJson
    fun fromJsonImpl(model: AvailableTopUpOptions): AvailableTopUpOptionsImpl {
        return model as AvailableTopUpOptionsImpl
    }

    @ToJson
    fun toJsonImpl(impl: AvailableTopUpOptionsImpl): AvailableTopUpOptions {
        return impl
    }

}