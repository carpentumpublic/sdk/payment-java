//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** PayoutMethodResponse
 *
 * 
 *
 * @see BankTransferMethodResponse
 * @see CryptoTransferMethodResponse
 * @see WalletTransferMethodResponse
 *
 * The model class is immutable.
 *
 *
 */
public interface PayoutMethodResponse {
    /** Name of discriminator property used to distinguish between "one-of" subclasses. */
    String DISCRIMINATOR = "paymentMethodCode";

    /**
     * @see PaymentMethodCode
     */
    @NotNull PaymentMethodCode getPaymentMethodCode();

    /**
     * Enumeration of "one-of" subclasses distinguished by {@code paymentMethodCode} discriminator value.
     */
    enum PaymentMethodCode {
        BANK_TRANSFER,
        CRYPTO_TRANSFER,
        WALLET_TRANSFER,
    }
}