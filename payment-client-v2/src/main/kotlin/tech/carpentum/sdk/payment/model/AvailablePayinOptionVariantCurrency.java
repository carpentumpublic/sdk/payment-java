//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** AvailablePayinOptionVariantCurrency
 *
 * 
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public interface AvailablePayinOptionVariantCurrency {

    @NotNull PayinMethodCode getPaymentMethodCode();

    @NotNull java.util.List<@NotNull PaymentOperatorIncoming> getPaymentOperators();

    @NotNull Optional<SegmentCode> getSegmentCode();

    @NotNull Money getMoney();

    @NotNull static Builder builder(AvailablePayinOptionVariantCurrency copyOf) {
        Builder builder = builder();
        builder.paymentMethodCode(copyOf.getPaymentMethodCode());
        builder.paymentOperators(copyOf.getPaymentOperators());
        builder.segmentCode(copyOf.getSegmentCode().orElse(null));
        builder.money(copyOf.getMoney());
        return builder;
    }

    @NotNull static Builder builder() {
        return new AvailablePayinOptionVariantCurrencyImpl.BuilderImpl();
    }

    /** Builder for {@link AvailablePayinOptionVariantCurrency} model class. */
    interface Builder {

        /**
          * Set {@link AvailablePayinOptionVariantCurrency#getPaymentMethodCode} property.
          *
          * 
          */
        @NotNull Builder paymentMethodCode(PayinMethodCode paymentMethodCode);

        boolean isPaymentMethodCodeDefined();


        /**
          * Replace all items in {@link AvailablePayinOptionVariantCurrency#getPaymentOperators} list property.
          *
          * 
          */
        @NotNull Builder paymentOperators(java.util.List<@NotNull PaymentOperatorIncoming> paymentOperators);
        /**
          * Add single item to {@link AvailablePayinOptionVariantCurrency#getPaymentOperators} list property.
          *
          * 
          */
        @NotNull Builder paymentOperatorsAdd(PaymentOperatorIncoming item);
        /**
          * Add all items to {@link AvailablePayinOptionVariantCurrency#getPaymentOperators} list property.
          *
          * 
          */
        @NotNull Builder paymentOperatorsAddAll(java.util.List<@NotNull PaymentOperatorIncoming> paymentOperators);


        /**
          * Set {@link AvailablePayinOptionVariantCurrency#getSegmentCode} property.
          *
          * 
          */
        @NotNull Builder segmentCode(SegmentCode segmentCode);

        boolean isSegmentCodeDefined();


        /**
          * Set {@link AvailablePayinOptionVariantCurrency#getMoney} property.
          *
          * 
          */
        @NotNull Builder money(Money money);

        boolean isMoneyDefined();


        /**
         * Create new instance of {@link AvailablePayinOptionVariantCurrency} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @NotNull AvailablePayinOptionVariantCurrency build();

    }
}