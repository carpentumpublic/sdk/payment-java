//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.AvailableTopUpOption

class AvailableTopUpOptionJsonAdapter {
    @FromJson
    fun fromJson(json: AvailableTopUpOptionJson): AvailableTopUpOption {
        val builder = AvailableTopUpOption.builder()
        builder.moneyPay(json.moneyPay)
        builder.moneyReceive(json.moneyReceive)
        builder.paymentMethodCode(json.paymentMethodCode)
        builder.paymentOperators(json.paymentOperators?.toList())
        return builder.build()
    }

    @ToJson
    fun toJson(model: AvailableTopUpOption): AvailableTopUpOptionJson {
        val json = AvailableTopUpOptionJson()
        json.moneyPay = model.moneyPay
        json.moneyReceive = model.moneyReceive
        json.paymentMethodCode = model.paymentMethodCode
        json.paymentOperators = model.paymentOperators.ifEmpty { null }
        return json
    }

    @FromJson
    fun fromJsonImpl(model: AvailableTopUpOption): AvailableTopUpOptionImpl {
        return model as AvailableTopUpOptionImpl
    }

    @ToJson
    fun toJsonImpl(impl: AvailableTopUpOptionImpl): AvailableTopUpOption {
        return impl
    }

}