//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** TopUpDetail
 *
 * 
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public class TopUpDetailImpl implements TopUpDetail {
    private final TopUpRequestedDetail topUpRequested;

    @Override
    public TopUpRequestedDetail getTopUpRequested() {
        return topUpRequested;
    }


    private final PaymentProcess process;

    @Override
    public PaymentProcess getProcess() {
        return process;
    }


    private final MoneyFee fee;

    @Override
    public MoneyFee getFee() {
        return fee;
    }


    private final PayinMethodResponse paymentMethodResponse;

    @Override
    public PayinMethodResponse getPaymentMethodResponse() {
        return paymentMethodResponse;
    }


    private final Optional<SettlementMethod> settlement;

    @Override
    public Optional<SettlementMethod> getSettlement() {
        return settlement;
    }




    private final int hashCode;
    private final String toString;

    private TopUpDetailImpl(BuilderImpl builder) {
        this.topUpRequested = Objects.requireNonNull(builder.topUpRequested, "Property 'topUpRequested' is required.");
        this.process = Objects.requireNonNull(builder.process, "Property 'process' is required.");
        this.fee = Objects.requireNonNull(builder.fee, "Property 'fee' is required.");
        this.paymentMethodResponse = Objects.requireNonNull(builder.paymentMethodResponse, "Property 'paymentMethodResponse' is required.");
        this.settlement = Optional.ofNullable(builder.settlement);

        this.hashCode = Objects.hash(topUpRequested, process, fee, paymentMethodResponse, settlement);
        this.toString = builder.type + "(" +
                "topUpRequested=" + topUpRequested +
                ", process=" + process +
                ", fee=" + fee +
                ", paymentMethodResponse=" + paymentMethodResponse +
                ", settlement=" + settlement +
                ')';
    }

    @Override
    public int hashCode() {
        return hashCode;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }

        if (!(obj instanceof TopUpDetailImpl)) {
            return false;
        }

        TopUpDetailImpl that = (TopUpDetailImpl) obj;
        if (!Objects.equals(this.topUpRequested, that.topUpRequested)) return false;
        if (!Objects.equals(this.process, that.process)) return false;
        if (!Objects.equals(this.fee, that.fee)) return false;
        if (!Objects.equals(this.paymentMethodResponse, that.paymentMethodResponse)) return false;
        if (!Objects.equals(this.settlement, that.settlement)) return false;

        return true;
    }

    @Override
    public String toString() {
        return toString;
    }

    /** Builder for {@link TopUpDetail} model class. */
    public static class BuilderImpl implements TopUpDetail.Builder {
        private TopUpRequestedDetail topUpRequested = null;
        private PaymentProcess process = null;
        private MoneyFee fee = null;
        private PayinMethodResponse paymentMethodResponse = null;
        private SettlementMethod settlement = null;

        private final String type;

        public BuilderImpl(String type) {
            this.type = type;
        }

        public BuilderImpl() {
            this("TopUpDetail");
        }

        /**
          * Set {@link TopUpDetail#getTopUpRequested} property.
          *
          * 
          */
        @Override
        public BuilderImpl topUpRequested(TopUpRequestedDetail topUpRequested) {
            this.topUpRequested = topUpRequested;
            return this;
        }

        @Override
        public boolean isTopUpRequestedDefined() {
            return this.topUpRequested != null;
        }

        /**
          * Set {@link TopUpDetail#getProcess} property.
          *
          * 
          */
        @Override
        public BuilderImpl process(PaymentProcess process) {
            this.process = process;
            return this;
        }

        @Override
        public boolean isProcessDefined() {
            return this.process != null;
        }

        /**
          * Set {@link TopUpDetail#getFee} property.
          *
          * 
          */
        @Override
        public BuilderImpl fee(MoneyFee fee) {
            this.fee = fee;
            return this;
        }

        @Override
        public boolean isFeeDefined() {
            return this.fee != null;
        }

        /**
          * Set {@link TopUpDetail#getPaymentMethodResponse} property.
          *
          * 
          */
        @Override
        public BuilderImpl paymentMethodResponse(PayinMethodResponse paymentMethodResponse) {
            this.paymentMethodResponse = paymentMethodResponse;
            return this;
        }

        @Override
        public boolean isPaymentMethodResponseDefined() {
            return this.paymentMethodResponse != null;
        }

        /**
          * Set {@link TopUpDetail#getSettlement} property.
          *
          * 
          */
        @Override
        public BuilderImpl settlement(SettlementMethod settlement) {
            this.settlement = settlement;
            return this;
        }

        @Override
        public boolean isSettlementDefined() {
            return this.settlement != null;
        }

        /**
         * Create new instance of {@link TopUpDetail} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @Override
        public TopUpDetailImpl build() {
            return new TopUpDetailImpl(this);
        }

    }
}