//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.AccountPayinRequestPayShap

class AccountPayinRequestPayShapJsonAdapter {
    @FromJson
    fun fromJson(json: AccountPayinRequestPayShapJson): AccountPayinRequestPayShap {
        val builder = AccountPayinRequestPayShap.builder()
        builder.accountNumber(json.accountNumber)
        return builder.build()
    }

    @ToJson
    fun toJson(model: AccountPayinRequestPayShap): AccountPayinRequestPayShapJson {
        val json = AccountPayinRequestPayShapJson()
        json.accountNumber = model.accountNumber.orElse(null)
        return json
    }

    @FromJson
    fun fromJsonImpl(model: AccountPayinRequestPayShap): AccountPayinRequestPayShapImpl {
        return model as AccountPayinRequestPayShapImpl
    }

    @ToJson
    fun toJsonImpl(impl: AccountPayinRequestPayShapImpl): AccountPayinRequestPayShap {
        return impl
    }

}