//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.AccountResponseCryptoOffline

class AccountResponseCryptoOfflineJsonAdapter {
    @FromJson
    fun fromJson(json: AccountResponseCryptoOfflineJson): AccountResponseCryptoOffline {
        val builder = AccountResponseCryptoOffline.builder()
        builder.accountNumber(json.accountNumber)
        return builder.build()
    }

    @ToJson
    fun toJson(model: AccountResponseCryptoOffline): AccountResponseCryptoOfflineJson {
        val json = AccountResponseCryptoOfflineJson()
        json.accountNumber = model.accountNumber
        return json
    }

    @FromJson
    fun fromJsonImpl(model: AccountResponseCryptoOffline): AccountResponseCryptoOfflineImpl {
        return model as AccountResponseCryptoOfflineImpl
    }

    @ToJson
    fun toJsonImpl(impl: AccountResponseCryptoOfflineImpl): AccountResponseCryptoOffline {
        return impl
    }

}