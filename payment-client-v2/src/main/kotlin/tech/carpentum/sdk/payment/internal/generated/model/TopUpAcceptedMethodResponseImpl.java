//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** TopUpMethodResponse
 *
 * For every payment method there is appropriate payment specific response object in `paymentMethodResponse` attribute.

Use data from `paymentMethodResponse` for payment completion (for example show to the customer).
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public class TopUpAcceptedMethodResponseImpl implements TopUpAcceptedMethodResponse {
    private final TopUpRequested topUpRequested;

    @Override
    public TopUpRequested getTopUpRequested() {
        return topUpRequested;
    }


    private final Money moneyReceive;

    @Override
    public Money getMoneyReceive() {
        return moneyReceive;
    }


    private final PayinMethodResponse paymentMethodResponse;

    @Override
    public PayinMethodResponse getPaymentMethodResponse() {
        return paymentMethodResponse;
    }




    private final int hashCode;
    private final String toString;

    private TopUpAcceptedMethodResponseImpl(BuilderImpl builder) {
        this.topUpRequested = Objects.requireNonNull(builder.topUpRequested, "Property 'topUpRequested' is required.");
        this.moneyReceive = Objects.requireNonNull(builder.moneyReceive, "Property 'moneyReceive' is required.");
        this.paymentMethodResponse = Objects.requireNonNull(builder.paymentMethodResponse, "Property 'paymentMethodResponse' is required.");

        this.hashCode = Objects.hash(topUpRequested, moneyReceive, paymentMethodResponse);
        this.toString = builder.type + "(" +
                "topUpRequested=" + topUpRequested +
                ", moneyReceive=" + moneyReceive +
                ", paymentMethodResponse=" + paymentMethodResponse +
                ')';
    }

    @Override
    public int hashCode() {
        return hashCode;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }

        if (!(obj instanceof TopUpAcceptedMethodResponseImpl)) {
            return false;
        }

        TopUpAcceptedMethodResponseImpl that = (TopUpAcceptedMethodResponseImpl) obj;
        if (!Objects.equals(this.topUpRequested, that.topUpRequested)) return false;
        if (!Objects.equals(this.moneyReceive, that.moneyReceive)) return false;
        if (!Objects.equals(this.paymentMethodResponse, that.paymentMethodResponse)) return false;

        return true;
    }

    @Override
    public String toString() {
        return toString;
    }

    /** Builder for {@link TopUpAcceptedMethodResponse} model class. */
    public static class BuilderImpl implements TopUpAcceptedMethodResponse.Builder {
        private TopUpRequested topUpRequested = null;
        private Money moneyReceive = null;
        private PayinMethodResponse paymentMethodResponse = null;

        private final String type;

        public BuilderImpl(String type) {
            this.type = type;
        }

        public BuilderImpl() {
            this("TopUpAcceptedMethodResponse");
        }

        /**
          * Set {@link TopUpAcceptedMethodResponse#getTopUpRequested} property.
          *
          * 
          */
        @Override
        public BuilderImpl topUpRequested(TopUpRequested topUpRequested) {
            this.topUpRequested = topUpRequested;
            return this;
        }

        @Override
        public boolean isTopUpRequestedDefined() {
            return this.topUpRequested != null;
        }

        /**
          * Set {@link TopUpAcceptedMethodResponse#getMoneyReceive} property.
          *
          * 
          */
        @Override
        public BuilderImpl moneyReceive(Money moneyReceive) {
            this.moneyReceive = moneyReceive;
            return this;
        }

        @Override
        public boolean isMoneyReceiveDefined() {
            return this.moneyReceive != null;
        }

        /**
          * Set {@link TopUpAcceptedMethodResponse#getPaymentMethodResponse} property.
          *
          * 
          */
        @Override
        public BuilderImpl paymentMethodResponse(PayinMethodResponse paymentMethodResponse) {
            this.paymentMethodResponse = paymentMethodResponse;
            return this;
        }

        @Override
        public boolean isPaymentMethodResponseDefined() {
            return this.paymentMethodResponse != null;
        }

        /**
         * Create new instance of {@link TopUpAcceptedMethodResponse} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @Override
        public TopUpAcceptedMethodResponseImpl build() {
            return new TopUpAcceptedMethodResponseImpl(this);
        }

    }
}