package tech.carpentum.sdk.payment.sample;

import kotlin.collections.SetsKt;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import tech.carpentum.sdk.payment.AccountsApi;
import tech.carpentum.sdk.payment.AuthTokenOperations;
import tech.carpentum.sdk.payment.OutgoingPaymentsApi;
import tech.carpentum.sdk.payment.PaymentContext;
import tech.carpentum.sdk.payment.ResponseException;
import tech.carpentum.sdk.payment.model.AuthTokenResponse;
import tech.carpentum.sdk.payment.model.BalanceList;
import tech.carpentum.sdk.payment.model.Payout;
import tech.carpentum.sdk.payment.model.PayoutAccepted;
import tech.carpentum.sdk.payment.model.PayoutDetail;

import java.io.InterruptedIOException;
import java.math.BigDecimal;
import java.time.Duration;
import java.util.Optional;

/**
 * A little complex scenario that combines Accounts API and Outgoing Payments API operations
 * with Authorization token for both APIs and single `idPayout`.
 */
public class CreatePayoutWithBalanceCheckScenario extends AbstractScenario {
    private static final Logger LOGGER = LoggerFactory.getLogger(CreatePayoutWithBalanceCheckScenario.class);

    private final AccountsApi accountsApi;
    private final OutgoingPaymentsApi outgoingPaymentsApi;
    private final String idPayout;

    private CreatePayoutWithBalanceCheckScenario(@NotNull AccountsApi accountsApi, @NotNull OutgoingPaymentsApi outgoingPaymentsApi, @NotNull String idPayout, @NotNull String authToken) {
        super(authToken);
        this.accountsApi = accountsApi;
        this.outgoingPaymentsApi = outgoingPaymentsApi;
        this.idPayout = idPayout;
    }

    /**
     * Creates new instance of the scenario with newly created Authorization token with specified validity.
     */
    public static CreatePayoutWithBalanceCheckScenario create(@NotNull PaymentContext context, @NotNull String idPayout, @Nullable Duration validity) throws ResponseException, InterruptedIOException {
        AuthTokenResponse authToken = context.createAuthToken(
                new AuthTokenOperations()
                        .grant(AccountsApi.defineListBalancesEndpoint())
                        .grant(OutgoingPaymentsApi.defineCreatePayoutEndpoint().forId(idPayout))
                        .grant(OutgoingPaymentsApi.defineGetPayoutEndpoint().forId(idPayout)),
                validity
        );
        return new CreatePayoutWithBalanceCheckScenario(
                AccountsApi.create(context, authToken.getToken(), Duration.ofSeconds(10)),
                OutgoingPaymentsApi.create(context, authToken.getToken(), Duration.ofSeconds(20)),
                idPayout,
                authToken.getToken()
        );
    }

    /**
     * Creates new instance of the scenario with newly created Authorization token with default {@code payment} validity.
     */
    public static CreatePayoutWithBalanceCheckScenario create(@NotNull PaymentContext context, @NotNull String idPayout) throws ResponseException, InterruptedIOException {
        return create(context, idPayout, (Duration) null);
    }

    /**
     * Creates new instance of the scenario with existing Authorization token.
     */
    public static CreatePayoutWithBalanceCheckScenario create(@NotNull PaymentContext context, @NotNull String idPayout, @NotNull String authToken) throws ResponseException {
        return new CreatePayoutWithBalanceCheckScenario(
                AccountsApi.create(context, authToken, Duration.ofSeconds(10)),
                OutgoingPaymentsApi.create(context, authToken, Duration.ofSeconds(20)),
                idPayout,
                authToken
        );
    }

    public Optional<PayoutAccepted> createPaymentWithBalanceCheck(@NotNull Payout payout) throws ResponseException, InterruptedIOException {
        BigDecimal requestedBalance = payout.getPaymentRequested().getMoney().getAmount();
        try {
            BalanceList balancesResponse = accountsApi.listBalances(SetsKt.setOf((payout.getPaymentRequested().getMoney().getCurrencyCode().getValue())));
            BigDecimal availableBalance = balancesResponse.getData().get(0).getAvailableBalance();

            if (availableBalance.compareTo(requestedBalance) > 0) {
                return Optional.of(outgoingPaymentsApi.createPayout(idPayout, payout));
            } else {
                LOGGER.debug("Insufficient balance. Available: {}, Requested: {}.", availableBalance, requestedBalance);
                return Optional.empty();
            }
        } catch (ResponseException ex) {
            LOGGER.debug("Error response while getting available balance. Detail: {}", ex.getLocalizedMessage());
            throw ex;
        } catch (InterruptedIOException ex) {
            throw ex;
        }
    }

    public PayoutDetail getDetail() throws ResponseException, InterruptedIOException {
        return outgoingPaymentsApi.getPayout(idPayout);
    }
}
