//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** RedirectResponse
 *
 * Customer should be redirected to specified URL for the next payment process step.
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public interface PayinAcceptedRedirectResponse extends PayinAcceptedResponse {

    @NotNull PaymentRequested getPaymentRequested();

    @NotNull Redirection getRedirectTo();

    @NotNull static Builder builder(PayinAcceptedRedirectResponse copyOf) {
        Builder builder = builder();
        builder.paymentRequested(copyOf.getPaymentRequested());
        builder.redirectTo(copyOf.getRedirectTo());
        return builder;
    }

    @NotNull static Builder builder() {
        return new PayinAcceptedRedirectResponseImpl.BuilderImpl();
    }

    /** Builder for {@link PayinAcceptedRedirectResponse} model class. */
    interface Builder {

        /**
          * Set {@link PayinAcceptedRedirectResponse#getPaymentRequested} property.
          *
          * 
          */
        @NotNull Builder paymentRequested(PaymentRequested paymentRequested);

        boolean isPaymentRequestedDefined();


        /**
          * Set {@link PayinAcceptedRedirectResponse#getRedirectTo} property.
          *
          * 
          */
        @NotNull Builder redirectTo(Redirection redirectTo);

        boolean isRedirectToDefined();


        /**
         * Create new instance of {@link PayinAcceptedRedirectResponse} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @NotNull PayinAcceptedRedirectResponse build();

    }
}