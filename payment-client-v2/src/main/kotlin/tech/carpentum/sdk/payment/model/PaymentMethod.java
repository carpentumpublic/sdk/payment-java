//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** PaymentMethod
 *
 * 
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public interface PaymentMethod {

    @NotNull String getCode();

    @NotNull static PaymentMethod ofCode(String code) { return builder().code(code).build(); }

    @NotNull static Builder builder() {
        return new PaymentMethodImpl.BuilderImpl();
    }

    /** Builder for {@link PaymentMethod} model class. */
    interface Builder {

        /**
          * Set {@link PaymentMethod#getCode} property.
          *
          * 
          */
        @NotNull Builder code(String code);

        boolean isCodeDefined();


        /**
         * Create new instance of {@link PaymentMethod} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @NotNull PaymentMethod build();

    }
}