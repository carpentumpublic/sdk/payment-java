//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.AvailableForexCurrencyPair

class AvailableForexCurrencyPairJsonAdapter {
    @FromJson
    fun fromJson(json: AvailableForexCurrencyPairJson): AvailableForexCurrencyPair {
        val builder = AvailableForexCurrencyPair.builder()
        builder.baseCurrencyCode(json.baseCurrencyCode)
        builder.quoteCurrencyCode(json.quoteCurrencyCode)
        return builder.build()
    }

    @ToJson
    fun toJson(model: AvailableForexCurrencyPair): AvailableForexCurrencyPairJson {
        val json = AvailableForexCurrencyPairJson()
        json.baseCurrencyCode = model.baseCurrencyCode
        json.quoteCurrencyCode = model.quoteCurrencyCode
        return json
    }

    @FromJson
    fun fromJsonImpl(model: AvailableForexCurrencyPair): AvailableForexCurrencyPairImpl {
        return model as AvailableForexCurrencyPairImpl
    }

    @ToJson
    fun toJsonImpl(impl: AvailableForexCurrencyPairImpl): AvailableForexCurrencyPair {
        return impl
    }

}