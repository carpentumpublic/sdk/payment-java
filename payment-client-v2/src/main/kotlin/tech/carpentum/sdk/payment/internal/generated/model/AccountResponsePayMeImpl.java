//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** AccountResponsePayMe
 *
 * Parameters of a bank account where we expect that your customer send funds to make a payment. These account parameters has to be provided to your customer in form of an payment instructions.
The returned parameters are depended on the payment method and currency your customer choose to pay.
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public class AccountResponsePayMeImpl implements AccountResponsePayMe {
    /** Name of the bank account where we expect that your customer sends funds to make a payment.
This parameter is to be shown to your customer in the payment instructions. */
    private final Optional<String> accountName;

    @Override
    public Optional<String> getAccountName() {
        return accountName;
    }




    private final int hashCode;
    private final String toString;

    private AccountResponsePayMeImpl(BuilderImpl builder) {
        this.accountName = Optional.ofNullable(builder.accountName);

        this.hashCode = Objects.hash(accountName);
        this.toString = builder.type + "(" +
                "accountName=" + accountName +
                ')';
    }

    @Override
    public int hashCode() {
        return hashCode;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }

        if (!(obj instanceof AccountResponsePayMeImpl)) {
            return false;
        }

        AccountResponsePayMeImpl that = (AccountResponsePayMeImpl) obj;
        if (!Objects.equals(this.accountName, that.accountName)) return false;

        return true;
    }

    @Override
    public String toString() {
        return toString;
    }

    /** Builder for {@link AccountResponsePayMe} model class. */
    public static class BuilderImpl implements AccountResponsePayMe.Builder {
        private String accountName = null;

        private final String type;

        public BuilderImpl(String type) {
            this.type = type;
        }

        public BuilderImpl() {
            this("AccountResponsePayMe");
        }

        /**
          * Set {@link AccountResponsePayMe#getAccountName} property.
          *
          * Name of the bank account where we expect that your customer sends funds to make a payment.
This parameter is to be shown to your customer in the payment instructions.
          */
        @Override
        public BuilderImpl accountName(String accountName) {
            this.accountName = accountName;
            return this;
        }

        @Override
        public boolean isAccountNameDefined() {
            return this.accountName != null;
        }

        /**
         * Create new instance of {@link AccountResponsePayMe} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @Override
        public AccountResponsePayMeImpl build() {
            return new AccountResponsePayMeImpl(this);
        }

    }
}