//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.JsonClass;
import tech.carpentum.sdk.payment.model.*;

@JsonClass(generateAdapter = true)
class TopUpAcceptedRedirectResponseJson {
    var topUpRequested: TopUpRequested? = null
    var redirectTo: Redirection? = null
}