package tech.carpentum.sdk.payment.sample

import tech.carpentum.sdk.payment.model.MerchantInfo
import tech.carpentum.sdk.payment.{AuthTokenOperations, MerchantInfoApi, PaymentContext, ResponseException}

import java.time.Duration

/**
 * Wraps all MerchantInfo API operations.
 */
object MerchantInfoScenario {
  /**
   * Creates new instance of the scenario with newly created Authorization token with specified validity.
   */
  @throws[ResponseException]
  def create(context: PaymentContext, validity: Duration = null): MerchantInfoScenario = {
    val authToken = context.createAuthToken(new AuthTokenOperations(MerchantInfoApi.defineGetMerchantInfoEndpoint), validity).getToken
    new MerchantInfoScenario(MerchantInfoApi.create(context, authToken), authToken)
  }

  /**
   * Creates new instance of the scenario with existing Authorization token.
   */
  @throws[ResponseException]
  def create(context: PaymentContext, authToken: String) =
    new MerchantInfoScenario(MerchantInfoApi.create(context, authToken), authToken)
}

class MerchantInfoScenario private(val merchantInfoApi: MerchantInfoApi, override val authToken: String) extends AbstractScenario(authToken) {
  @throws[ResponseException]
  def getMerchantInfo: MerchantInfo = merchantInfoApi.getMerchantInfo
}
