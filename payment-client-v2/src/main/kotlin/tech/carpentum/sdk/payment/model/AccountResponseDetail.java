//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** AccountResponseDetail
 *
 * 
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public interface AccountResponseDetail {

    /** Name of the bank account. */
    @NotNull Optional<String> getAccountName();

    /** Number of the bank account. */
    @NotNull Optional<String> getAccountNumber();

    /** Type of the bank account. */
    @NotNull Optional<String> getAccountType();

    /** Bank code of the bank. */
    @NotNull Optional<String> getBankCode();

    @NotNull Optional<PaymentOperatorIncoming> getPaymentOperator();

    /** Name of the bank. */
    @NotNull Optional<String> getBankName();

    /** Branch name of the bank. */
    @NotNull Optional<String> getBankBranch();

    /** City of the bank. */
    @NotNull Optional<String> getBankCity();

    /** Province of the bank. */
    @NotNull Optional<String> getBankProvince();

    @NotNull static Builder builder(AccountResponseDetail copyOf) {
        Builder builder = builder();
        builder.accountName(copyOf.getAccountName().orElse(null));
        builder.accountNumber(copyOf.getAccountNumber().orElse(null));
        builder.accountType(copyOf.getAccountType().orElse(null));
        builder.bankCode(copyOf.getBankCode().orElse(null));
        builder.paymentOperator(copyOf.getPaymentOperator().orElse(null));
        builder.bankName(copyOf.getBankName().orElse(null));
        builder.bankBranch(copyOf.getBankBranch().orElse(null));
        builder.bankCity(copyOf.getBankCity().orElse(null));
        builder.bankProvince(copyOf.getBankProvince().orElse(null));
        return builder;
    }

    @NotNull static Builder builder() {
        return new AccountResponseDetailImpl.BuilderImpl();
    }

    /** Builder for {@link AccountResponseDetail} model class. */
    interface Builder {

        /**
          * Set {@link AccountResponseDetail#getAccountName} property.
          *
          * Name of the bank account.
          */
        @NotNull Builder accountName(String accountName);

        boolean isAccountNameDefined();


        /**
          * Set {@link AccountResponseDetail#getAccountNumber} property.
          *
          * Number of the bank account.
          */
        @NotNull Builder accountNumber(String accountNumber);

        boolean isAccountNumberDefined();


        /**
          * Set {@link AccountResponseDetail#getAccountType} property.
          *
          * Type of the bank account.
          */
        @NotNull Builder accountType(String accountType);

        boolean isAccountTypeDefined();


        /**
          * Set {@link AccountResponseDetail#getBankCode} property.
          *
          * Bank code of the bank.
          */
        @NotNull Builder bankCode(String bankCode);

        boolean isBankCodeDefined();


        /**
          * Set {@link AccountResponseDetail#getPaymentOperator} property.
          *
          * 
          */
        @NotNull Builder paymentOperator(PaymentOperatorIncoming paymentOperator);

        boolean isPaymentOperatorDefined();


        /**
          * Set {@link AccountResponseDetail#getBankName} property.
          *
          * Name of the bank.
          */
        @NotNull Builder bankName(String bankName);

        boolean isBankNameDefined();


        /**
          * Set {@link AccountResponseDetail#getBankBranch} property.
          *
          * Branch name of the bank.
          */
        @NotNull Builder bankBranch(String bankBranch);

        boolean isBankBranchDefined();


        /**
          * Set {@link AccountResponseDetail#getBankCity} property.
          *
          * City of the bank.
          */
        @NotNull Builder bankCity(String bankCity);

        boolean isBankCityDefined();


        /**
          * Set {@link AccountResponseDetail#getBankProvince} property.
          *
          * Province of the bank.
          */
        @NotNull Builder bankProvince(String bankProvince);

        boolean isBankProvinceDefined();


        /**
         * Create new instance of {@link AccountResponseDetail} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @NotNull AccountResponseDetail build();

    }
}