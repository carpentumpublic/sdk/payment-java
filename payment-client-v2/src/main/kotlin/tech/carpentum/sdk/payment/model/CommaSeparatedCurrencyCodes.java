//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** CommaSeparatedCurrencyCodes
 *
 * List of requested currencies.

How to send multiple values please refer to  [Query for multiple values](general.html#query-for-multiple-values) section.

For supported currencies please refer to [`GET /currencies`](#operations-Payments-getCurrencies).
 *
 * 
 *
 * The model class is immutable.
 *
 * Use static {@link #of} method to create a new model class instance.
 */
public interface CommaSeparatedCurrencyCodes {


    @NotNull String getValue();

    static CommaSeparatedCurrencyCodes of(@NotNull String value) {
        return new CommaSeparatedCurrencyCodesImpl(value);
    }
}