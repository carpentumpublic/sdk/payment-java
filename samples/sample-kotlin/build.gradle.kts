plugins {
    id("tech.carpentum.sdk.payment.kotlin-application-conventions")
}

dependencies {
    implementation(project(":payment-client-v2"))
    implementation("ch.qos.logback:logback-classic")
}

application {
    // Define the main class for the application.
    mainClass.set("tech.carpentum.sdk.payment.sample.AdvancedScenariosApp")
}
