//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** AccountResponseWithBank
 *
 * Parameters of a bank account where we expect that your customer send funds to make a payment. These account parameters has to be provided to your customer in form of an payment instructions.
The returned parameters are depended on the payment method and currency your customer choose to pay.
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public interface AccountResponseWithBank {

    /** Name of the bank account where we expect that your customer sends funds to make a payment.
This parameter is to be shown to your customer in the payment instructions. */
    @NotNull String getAccountName();

    /** Number of the bank account where we expect that your customer sends funds to make a payment.
This parameter is to be shown to your customer in the payment instructions. */
    @NotNull String getAccountNumber();

    /** Bank code of the bank where we expect that your customer sends funds to make a payment.
If this parameter contains any value then show it to your customer in the payment instructions. */
    @NotNull Optional<String> getBankCode();

    /** Name of the bank where we expect that your customer sends funds to make a payment.
If this parameter contains any value then show it to your customer in the payment instructions. */
    @NotNull Optional<String> getBankName();

    /** Branch name of the bank where we expect that your customer sends funds to make a payment.
If this parameter contains any value then show it to your customer in the payment instructions. */
    @NotNull Optional<String> getBankBranch();

    /** City of the bank where we expect that your customer sends funds to make a payment.
If this parameter contains any value then show it to your customer in the payment instructions. */
    @NotNull Optional<String> getBankCity();

    /** Province of the bank where we expect that your customer sends funds to make a payment.
If this parameter contains any value then show it to your customer in the payment instructions. */
    @NotNull Optional<String> getBankProvince();

    @NotNull static Builder builder(AccountResponseWithBank copyOf) {
        Builder builder = builder();
        builder.accountName(copyOf.getAccountName());
        builder.accountNumber(copyOf.getAccountNumber());
        builder.bankCode(copyOf.getBankCode().orElse(null));
        builder.bankName(copyOf.getBankName().orElse(null));
        builder.bankBranch(copyOf.getBankBranch().orElse(null));
        builder.bankCity(copyOf.getBankCity().orElse(null));
        builder.bankProvince(copyOf.getBankProvince().orElse(null));
        return builder;
    }

    @NotNull static Builder builder() {
        return new AccountResponseOfflineImpl.BuilderImpl("AccountResponseWithBank");
    }

    /** Builder for {@link AccountResponseWithBank} model class. */
    interface Builder {

        /**
          * Set {@link AccountResponseWithBank#getAccountName} property.
          *
          * Name of the bank account where we expect that your customer sends funds to make a payment.
This parameter is to be shown to your customer in the payment instructions.
          */
        @NotNull Builder accountName(String accountName);

        boolean isAccountNameDefined();


        /**
          * Set {@link AccountResponseWithBank#getAccountNumber} property.
          *
          * Number of the bank account where we expect that your customer sends funds to make a payment.
This parameter is to be shown to your customer in the payment instructions.
          */
        @NotNull Builder accountNumber(String accountNumber);

        boolean isAccountNumberDefined();


        /**
          * Set {@link AccountResponseWithBank#getBankCode} property.
          *
          * Bank code of the bank where we expect that your customer sends funds to make a payment.
If this parameter contains any value then show it to your customer in the payment instructions.
          */
        @NotNull Builder bankCode(String bankCode);

        boolean isBankCodeDefined();


        /**
          * Set {@link AccountResponseWithBank#getBankName} property.
          *
          * Name of the bank where we expect that your customer sends funds to make a payment.
If this parameter contains any value then show it to your customer in the payment instructions.
          */
        @NotNull Builder bankName(String bankName);

        boolean isBankNameDefined();


        /**
          * Set {@link AccountResponseWithBank#getBankBranch} property.
          *
          * Branch name of the bank where we expect that your customer sends funds to make a payment.
If this parameter contains any value then show it to your customer in the payment instructions.
          */
        @NotNull Builder bankBranch(String bankBranch);

        boolean isBankBranchDefined();


        /**
          * Set {@link AccountResponseWithBank#getBankCity} property.
          *
          * City of the bank where we expect that your customer sends funds to make a payment.
If this parameter contains any value then show it to your customer in the payment instructions.
          */
        @NotNull Builder bankCity(String bankCity);

        boolean isBankCityDefined();


        /**
          * Set {@link AccountResponseWithBank#getBankProvince} property.
          *
          * Province of the bank where we expect that your customer sends funds to make a payment.
If this parameter contains any value then show it to your customer in the payment instructions.
          */
        @NotNull Builder bankProvince(String bankProvince);

        boolean isBankProvinceDefined();


        /**
         * Create new instance of {@link AccountResponseWithBank} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @NotNull AccountResponseWithBank build();

    }
}