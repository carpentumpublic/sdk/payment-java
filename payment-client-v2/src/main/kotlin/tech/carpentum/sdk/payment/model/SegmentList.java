//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** SegmentList
 *
 * List of available segments.
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public interface SegmentList {

    @NotNull java.util.List<@NotNull Segment> getData();

    @NotNull static SegmentList ofData(java.util.List<@NotNull Segment> data) { return builder().data(data).build(); }

    @NotNull static Builder builder() {
        return new SegmentListImpl.BuilderImpl();
    }

    /** Builder for {@link SegmentList} model class. */
    interface Builder {

        /**
          * Replace all items in {@link SegmentList#getData} list property.
          *
          * 
          */
        @NotNull Builder data(java.util.List<@NotNull Segment> data);
        /**
          * Add single item to {@link SegmentList#getData} list property.
          *
          * 
          */
        @NotNull Builder dataAdd(Segment item);
        /**
          * Add all items to {@link SegmentList#getData} list property.
          *
          * 
          */
        @NotNull Builder dataAddAll(java.util.List<@NotNull Segment> data);


        /**
         * Create new instance of {@link SegmentList} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @NotNull SegmentList build();

    }
}