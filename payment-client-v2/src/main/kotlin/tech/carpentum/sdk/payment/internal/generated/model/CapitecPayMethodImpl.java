//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** CAPITEC_PAY
 *
 * Capitec Pay is an easy, fast and safe way to pay without having to enter the bank card details or share banking login information. The customer can choose the account he wants to pay from and authenticate the payment safely in the banking app.
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public class CapitecPayMethodImpl implements CapitecPayMethod {
    private final Optional<AccountPayinRequestCapitecPay> account;

    @Override
    public Optional<AccountPayinRequestCapitecPay> getAccount() {
        return account;
    }


    /** Your customer e-mail address in RFC 5322 format that is used for identification of the customer's payins. */
    private final Optional<String> emailAddress;

    @Override
    public Optional<String> getEmailAddress() {
        return emailAddress;
    }


    /** Your customer mobile phone number. */
    private final Optional<String> phoneNumber;

    @Override
    public Optional<String> getPhoneNumber() {
        return phoneNumber;
    }


    private final Optional<String> said;

    @Override
    public Optional<String> getSaid() {
        return said;
    }


    @Override public PaymentMethodCode getPaymentMethodCode() { return PAYMENT_METHOD_CODE; }

    private final int hashCode;
    private final String toString;

    private CapitecPayMethodImpl(BuilderImpl builder) {
        this.account = Optional.ofNullable(builder.account);
        this.emailAddress = Optional.ofNullable(builder.emailAddress);
        this.phoneNumber = Optional.ofNullable(builder.phoneNumber);
        this.said = Optional.ofNullable(builder.said);

        this.hashCode = Objects.hash(account, emailAddress, phoneNumber, said);
        this.toString = builder.type + "(" +
                "account=" + account +
                ", emailAddress=" + emailAddress +
                ", phoneNumber=" + phoneNumber +
                ", said=" + said +
                ')';
    }

    @Override
    public int hashCode() {
        return hashCode;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }

        if (!(obj instanceof CapitecPayMethodImpl)) {
            return false;
        }

        CapitecPayMethodImpl that = (CapitecPayMethodImpl) obj;
        if (!Objects.equals(this.account, that.account)) return false;
        if (!Objects.equals(this.emailAddress, that.emailAddress)) return false;
        if (!Objects.equals(this.phoneNumber, that.phoneNumber)) return false;
        if (!Objects.equals(this.said, that.said)) return false;

        return true;
    }

    @Override
    public String toString() {
        return toString;
    }

    /** Builder for {@link CapitecPayMethod} model class. */
    public static class BuilderImpl implements CapitecPayMethod.Builder {
        private AccountPayinRequestCapitecPay account = null;
        private String emailAddress = null;
        private String phoneNumber = null;
        private String said = null;

        private final String type;

        public BuilderImpl(String type) {
            this.type = type;
        }

        public BuilderImpl() {
            this("CapitecPayMethod");
        }

        /**
          * Set {@link CapitecPayMethod#getAccount} property.
          *
          * 
          */
        @Override
        public BuilderImpl account(AccountPayinRequestCapitecPay account) {
            this.account = account;
            return this;
        }

        @Override
        public boolean isAccountDefined() {
            return this.account != null;
        }

        /**
          * Set {@link CapitecPayMethod#getEmailAddress} property.
          *
          * Your customer e-mail address in RFC 5322 format that is used for identification of the customer's payins.
          */
        @Override
        public BuilderImpl emailAddress(String emailAddress) {
            this.emailAddress = emailAddress;
            return this;
        }

        @Override
        public boolean isEmailAddressDefined() {
            return this.emailAddress != null;
        }

        /**
          * Set {@link CapitecPayMethod#getPhoneNumber} property.
          *
          * Your customer mobile phone number.
          */
        @Override
        public BuilderImpl phoneNumber(String phoneNumber) {
            this.phoneNumber = phoneNumber;
            return this;
        }

        @Override
        public boolean isPhoneNumberDefined() {
            return this.phoneNumber != null;
        }

        /**
          * Set {@link CapitecPayMethod#getSaid} property.
          *
          * 
          */
        @Override
        public BuilderImpl said(String said) {
            this.said = said;
            return this;
        }

        @Override
        public boolean isSaidDefined() {
            return this.said != null;
        }

        /**
         * Create new instance of {@link CapitecPayMethod} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @Override
        public CapitecPayMethodImpl build() {
            return new CapitecPayMethodImpl(this);
        }

    }
}