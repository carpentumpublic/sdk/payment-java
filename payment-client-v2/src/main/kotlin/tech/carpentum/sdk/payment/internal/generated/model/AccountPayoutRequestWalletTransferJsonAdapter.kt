//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.AccountPayoutRequestWalletTransfer

class AccountPayoutRequestWalletTransferJsonAdapter {
    @FromJson
    fun fromJson(json: AccountPayoutRequestWalletTransferJson): AccountPayoutRequestWalletTransfer {
        val builder = AccountPayoutRequestWalletTransfer.builder()
        builder.accountName(json.accountName)
        builder.accountNumber(json.accountNumber)
        return builder.build()
    }

    @ToJson
    fun toJson(model: AccountPayoutRequestWalletTransfer): AccountPayoutRequestWalletTransferJson {
        val json = AccountPayoutRequestWalletTransferJson()
        json.accountName = model.accountName.orElse(null)
        json.accountNumber = model.accountNumber.orElse(null)
        return json
    }

    @FromJson
    fun fromJsonImpl(model: AccountPayoutRequestWalletTransfer): AccountPayoutRequestWalletTransferImpl {
        return model as AccountPayoutRequestWalletTransferImpl
    }

    @ToJson
    fun toJsonImpl(impl: AccountPayoutRequestWalletTransferImpl): AccountPayoutRequestWalletTransfer {
        return impl
    }

}