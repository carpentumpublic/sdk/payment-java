//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.JsonClass;
import tech.carpentum.sdk.payment.model.*;

@JsonClass(generateAdapter = true)
class WalletTopUpOptionJson {
    var paymentTypeCode: String? = null
    var paymentMethodCode: PayinMethodCode? = null
    var currencyCode: CurrencyCode? = null
    var segmentCode: SegmentCode? = null
    var transactionAmountLimit: IntervalNumberTo? = null
    var isAvailable: Boolean? = null
    var paymentOperators: List<PaymentOperatorOption>? = null
}