//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.AccountPayinRequestCapitecPay

class AccountPayinRequestCapitecPayJsonAdapter {
    @FromJson
    fun fromJson(json: AccountPayinRequestCapitecPayJson): AccountPayinRequestCapitecPay {
        val builder = AccountPayinRequestCapitecPay.builder()
        builder.accountName(json.accountName)
        builder.accountNumber(json.accountNumber)
        return builder.build()
    }

    @ToJson
    fun toJson(model: AccountPayinRequestCapitecPay): AccountPayinRequestCapitecPayJson {
        val json = AccountPayinRequestCapitecPayJson()
        json.accountName = model.accountName.orElse(null)
        json.accountNumber = model.accountNumber
        return json
    }

    @FromJson
    fun fromJsonImpl(model: AccountPayinRequestCapitecPay): AccountPayinRequestCapitecPayImpl {
        return model as AccountPayinRequestCapitecPayImpl
    }

    @ToJson
    fun toJsonImpl(impl: AccountPayinRequestCapitecPayImpl): AccountPayinRequestCapitecPay {
        return impl
    }

}