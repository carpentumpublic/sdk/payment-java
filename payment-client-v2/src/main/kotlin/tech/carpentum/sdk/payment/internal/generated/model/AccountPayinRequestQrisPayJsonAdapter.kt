//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.AccountPayinRequestQrisPay

class AccountPayinRequestQrisPayJsonAdapter {
    @FromJson
    fun fromJson(json: AccountPayinRequestQrisPayJson): AccountPayinRequestQrisPay {
        val builder = AccountPayinRequestQrisPay.builder()
        builder.accountName(json.accountName)
        builder.accountNumber(json.accountNumber)
        return builder.build()
    }

    @ToJson
    fun toJson(model: AccountPayinRequestQrisPay): AccountPayinRequestQrisPayJson {
        val json = AccountPayinRequestQrisPayJson()
        json.accountName = model.accountName.orElse(null)
        json.accountNumber = model.accountNumber.orElse(null)
        return json
    }

    @FromJson
    fun fromJsonImpl(model: AccountPayinRequestQrisPay): AccountPayinRequestQrisPayImpl {
        return model as AccountPayinRequestQrisPayImpl
    }

    @ToJson
    fun toJsonImpl(impl: AccountPayinRequestQrisPayImpl): AccountPayinRequestQrisPay {
        return impl
    }

}