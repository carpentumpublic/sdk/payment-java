//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.JsonClass;
import tech.carpentum.sdk.payment.model.*;

@JsonClass(generateAdapter = true)
class ForexRateJson {
    var baseCurrencyCode: CurrencyCode? = null
    var quoteCurrencyCode: CurrencyCode? = null
    var buyRate: java.math.BigDecimal? = null
    var sellRate: java.math.BigDecimal? = null
    var validUpTo: java.time.OffsetDateTime? = null
}