//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.NetBankingMethodResponse

class NetBankingMethodResponseJsonAdapter {
    @FromJson
    fun fromJson(json: NetBankingMethodResponseJson): NetBankingMethodResponse {
        val builder = NetBankingMethodResponse.builder()
        builder.idPayin(json.idPayin)
        builder.idPayment(json.idPayment)
        builder.money(json.money)
        builder.vat(json.vat)
        builder.merchantName(json.merchantName)
        builder.reference(json.reference)
        builder.returnUrl(json.returnUrl)
        builder.upiId(json.upiId)
        builder.deepLink(json.deepLink)
        builder.paymentOperator(json.paymentOperator)
        builder.acceptedAt(json.acceptedAt)
        builder.expireAt(json.expireAt)
        return builder.build()
    }

    @ToJson
    fun toJson(model: NetBankingMethodResponse): NetBankingMethodResponseJson {
        val json = NetBankingMethodResponseJson()
        json.idPayin = model.idPayin
        json.idPayment = model.idPayment
        json.money = model.money
        json.vat = model.vat.orElse(null)
        json.merchantName = model.merchantName
        json.reference = model.reference
        json.returnUrl = model.returnUrl
        json.upiId = model.upiId.orElse(null)
        json.deepLink = model.deepLink.orElse(null)
        json.paymentOperator = model.paymentOperator.orElse(null)
        json.acceptedAt = model.acceptedAt
        json.expireAt = model.expireAt
        json.paymentMethodCode = model.paymentMethodCode.name
        return json
    }

    @FromJson
    fun fromJsonImpl(model: NetBankingMethodResponse): NetBankingMethodResponseImpl {
        return model as NetBankingMethodResponseImpl
    }

    @ToJson
    fun toJsonImpl(impl: NetBankingMethodResponseImpl): NetBankingMethodResponse {
        return impl
    }

}