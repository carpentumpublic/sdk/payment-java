//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.AccountSettlementRequestCryptoTransfer

class AccountSettlementRequestCryptoTransferJsonAdapter {
    @FromJson
    fun fromJson(json: AccountSettlementRequestCryptoTransferJson): AccountSettlementRequestCryptoTransfer {
        val builder = AccountSettlementRequestCryptoTransfer.builder()
        builder.accountNumber(json.accountNumber)
        return builder.build()
    }

    @ToJson
    fun toJson(model: AccountSettlementRequestCryptoTransfer): AccountSettlementRequestCryptoTransferJson {
        val json = AccountSettlementRequestCryptoTransferJson()
        json.accountNumber = model.accountNumber
        return json
    }

    @FromJson
    fun fromJsonImpl(model: AccountSettlementRequestCryptoTransfer): AccountSettlementRequestCryptoTransferImpl {
        return model as AccountSettlementRequestCryptoTransferImpl
    }

    @ToJson
    fun toJsonImpl(impl: AccountSettlementRequestCryptoTransferImpl): AccountSettlementRequestCryptoTransfer {
        return impl
    }

}