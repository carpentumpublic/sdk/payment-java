//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** AccountPayinRequestAbsaPay
 *
 * Parameters of your customer's bank or wallet account which your customer sends funds from. These account parameters are used for the sender's account verification in processing of the payment.
Which parameters are mandatory depends on the payment method and the currency your customer choose to pay.
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public interface AccountPayinRequestAbsaPay {

    /** Account Number is the number of your customer's bank account which your customer sends funds from to make his payment. Must contain only numbers. */
    @NotNull String getAccountNumber();

    @NotNull static AccountPayinRequestAbsaPay ofAccountNumber(String accountNumber) { return builder().accountNumber(accountNumber).build(); }

    @NotNull static Builder builder() {
        return new AccountPayinRequestAbsaPayImpl.BuilderImpl();
    }

    /** Builder for {@link AccountPayinRequestAbsaPay} model class. */
    interface Builder {

        /**
          * Set {@link AccountPayinRequestAbsaPay#getAccountNumber} property.
          *
          * Account Number is the number of your customer's bank account which your customer sends funds from to make his payment. Must contain only numbers.
          */
        @NotNull Builder accountNumber(String accountNumber);

        boolean isAccountNumberDefined();


        /**
         * Create new instance of {@link AccountPayinRequestAbsaPay} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @NotNull AccountPayinRequestAbsaPay build();

    }
}