//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import tech.carpentum.sdk.payment.model.SettlementMethod
import tech.carpentum.sdk.payment.model.SettlementMethod.PaymentMethodCode

import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.Moshi
import com.squareup.moshi.adapters.PolymorphicJsonAdapterFactory
import java.lang.reflect.Type

class SettlementMethodJsonAdapter : JsonAdapter.Factory {
    override fun create(type: Type, annotations: MutableSet<out Annotation>, moshi: Moshi): JsonAdapter<*>? {
        return PolymorphicJsonAdapterFactory
            .of(SettlementMethod::class.java, SettlementMethod.DISCRIMINATOR)
            .withSubtype(SettlementCryptoTransferMethodImpl::class.java, PaymentMethodCode.CRYPTO_TRANSFER.toString())
            .create(type, annotations, moshi)
    }
}