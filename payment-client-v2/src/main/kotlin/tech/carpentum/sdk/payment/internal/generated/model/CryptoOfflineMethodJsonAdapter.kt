//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.CryptoOfflineMethod

class CryptoOfflineMethodJsonAdapter {
    @FromJson
    fun fromJson(json: CryptoOfflineMethodJson): CryptoOfflineMethod {
        val builder = CryptoOfflineMethod.builder()
        builder.paymentOperatorCode(json.paymentOperatorCode)
        builder.emailAddress(json.emailAddress)
        return builder.build()
    }

    @ToJson
    fun toJson(model: CryptoOfflineMethod): CryptoOfflineMethodJson {
        val json = CryptoOfflineMethodJson()
        json.paymentOperatorCode = model.paymentOperatorCode
        json.emailAddress = model.emailAddress.orElse(null)
        json.paymentMethodCode = model.paymentMethodCode.name
        return json
    }

    @FromJson
    fun fromJsonImpl(model: CryptoOfflineMethod): CryptoOfflineMethodImpl {
        return model as CryptoOfflineMethodImpl
    }

    @ToJson
    fun toJsonImpl(impl: CryptoOfflineMethodImpl): CryptoOfflineMethod {
        return impl
    }

}