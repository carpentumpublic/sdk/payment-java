package tech.carpentum.sdk.payment.sample;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tech.carpentum.sdk.payment.AuthTokenOperations;
import tech.carpentum.sdk.payment.OutgoingPaymentsApi;
import tech.carpentum.sdk.payment.PaymentContext;
import tech.carpentum.sdk.payment.ResponseException;
import tech.carpentum.sdk.payment.model.AuthTokenResponse;
import tech.carpentum.sdk.payment.model.AvailablePayoutOptionList;
import tech.carpentum.sdk.payment.model.PaymentRequested;
import tech.carpentum.sdk.payment.model.Payout;
import tech.carpentum.sdk.payment.model.PayoutAccepted;
import tech.carpentum.sdk.payment.model.PayoutDetail;

import java.io.InterruptedIOException;
import java.time.Duration;

/**
 * Wraps all Outgoing Payments API operations for single `idPayout`.
 */
public class CreatePayoutScenario extends AbstractScenario {
    private final OutgoingPaymentsApi outgoingPaymentsApi;
    private final String idPayout;

    private CreatePayoutScenario(@NotNull OutgoingPaymentsApi outgoingPaymentsApi, @NotNull String idPayout, @NotNull String authToken) throws ResponseException {
        super(authToken);
        this.outgoingPaymentsApi = outgoingPaymentsApi;
        this.idPayout = idPayout;
    }

    /**
     * Creates new instance of the scenario with newly created Authorization token with specified validity.
     */
    public static CreatePayoutScenario create(@NotNull PaymentContext context, @NotNull String idPayout, @Nullable Duration validity) throws ResponseException, InterruptedIOException {
        AuthTokenResponse authToken = context.createAuthToken(
                new AuthTokenOperations()
                        .grant(OutgoingPaymentsApi.definePayoutAvailablePaymentOptionsEndpoint())
                        .grant(OutgoingPaymentsApi.defineCreatePayoutEndpoint().forId(idPayout))
                        .grant(OutgoingPaymentsApi.defineGetPayoutEndpoint().forId(idPayout)),
                validity
        );
        return new CreatePayoutScenario(OutgoingPaymentsApi.create(context, authToken.getToken()), idPayout, authToken.getToken());
    }

    /**
     * Creates new instance of the scenario with newly created Authorization token with default {@code payment} validity.
     */
    public static CreatePayoutScenario create(@NotNull PaymentContext context, @NotNull String idPayout) throws ResponseException, InterruptedIOException {
        return create(context, idPayout, (Duration) null);
    }

    /**
     * Creates new instance of the scenario with existing Authorization token.
     */
    public static CreatePayoutScenario create(@NotNull PaymentContext context, @NotNull String idPayout, @NotNull String authToken) throws ResponseException {
        return new CreatePayoutScenario(OutgoingPaymentsApi.create(context, authToken), idPayout, authToken);
    }

    public AvailablePayoutOptionList availablePaymentOptions(@NotNull PaymentRequested paymentRequested) throws ResponseException, InterruptedIOException {
        return outgoingPaymentsApi.payoutAvailablePaymentOptions(paymentRequested);
    }

    public PayoutAccepted createPayment(@NotNull Payout payout) throws ResponseException, InterruptedIOException {
        return outgoingPaymentsApi.createPayout(idPayout, payout);
    }

    public PayoutDetail getDetail() throws ResponseException, InterruptedIOException {
        return outgoingPaymentsApi.getPayout(idPayout);
    }
}
