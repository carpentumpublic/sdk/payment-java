//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.JsonClass;
import tech.carpentum.sdk.payment.model.*;

@JsonClass(generateAdapter = true)
class AvailableTopUpOptionJson {
    var moneyPay: Money? = null
    var moneyReceive: Money? = null
    var paymentMethodCode: PayinMethodCode? = null
    var paymentOperators: List<PaymentOperatorIncoming>? = null
}