//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** PayoutMethod
 *
 * Payment method is a payment instrument that your customers use (such as online bank transfers, VA payments, UPI and so on).
Select a payment method from the list of the methods that you have configured in your account.

The `paymentMethodCode` represents payment method available for payout payment.
 *
 * @see BankTransferMethod
 * @see CryptoTransferMethod
 * @see WalletTransferMethod
 *
 * The model class is immutable.
 *
 *
 */
public interface PayoutMethod {
    /** Name of discriminator property used to distinguish between "one-of" subclasses. */
    String DISCRIMINATOR = "paymentMethodCode";

    /**
     * @see PaymentMethodCode
     */
    @NotNull PaymentMethodCode getPaymentMethodCode();

    /**
     * Enumeration of "one-of" subclasses distinguished by {@code paymentMethodCode} discriminator value.
     */
    enum PaymentMethodCode {
        BANK_TRANSFER,
        CRYPTO_TRANSFER,
        WALLET_TRANSFER,
    }
}