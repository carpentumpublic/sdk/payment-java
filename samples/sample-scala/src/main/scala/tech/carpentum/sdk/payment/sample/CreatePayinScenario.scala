package tech.carpentum.sdk.payment.sample

import tech.carpentum.sdk.payment.model.{ AvailablePayinOptionList, Payin, PayinAcceptedResponse, PayinDetail, PaymentRequested }
import tech.carpentum.sdk.payment.{ AuthTokenOperations, IncomingPaymentsApi, PaymentContext, ResponseException }

import java.time.Duration

/**
 * Wraps all Incoming Payments API operations for single `idPayin`.
 */
object CreatePayinScenario {

  /**
   * Creates new instance of the scenario with newly created Authorization token with specified validity.
   */
  @throws[ResponseException]
  def create(context: PaymentContext, idPayin: String, validity: Duration = null): CreatePayinScenario = {
    val authToken = context
      .createAuthToken(
        new AuthTokenOperations()
          .grant(IncomingPaymentsApi.definePayinAvailablePaymentOptionsEndpoint)
          .grant(IncomingPaymentsApi.defineCreatePayinEndpoint.forId(idPayin))
          .grant(IncomingPaymentsApi.defineGetPayinEndpoint.forId(idPayin))
          .grant(IncomingPaymentsApi.defineSetPayinExternalReferenceEndpoint.forId(idPayin)),
        validity,
      )
      .getToken
    new CreatePayinScenario(IncomingPaymentsApi.create(context, authToken), idPayin, authToken)
  }

  /**
   * Creates new instance of the scenario with existing Authorization token.
   */
  @throws[ResponseException]
  def create(context: PaymentContext, idPayin: String, authToken: String) =
    new CreatePayinScenario(IncomingPaymentsApi.create(context, authToken), idPayin, authToken)
}

class CreatePayinScenario private (val incomingPaymentsApi: IncomingPaymentsApi, val idPayin: String, override val authToken: String)
    extends AbstractScenario(authToken) {
  @throws[ResponseException]
  def availablePaymentOptions(paymentRequested: PaymentRequested): AvailablePayinOptionList =
    incomingPaymentsApi.payinAvailablePaymentOptions(paymentRequested)

  @throws[ResponseException]
  def createPayment(payin: Payin): PayinAcceptedResponse = incomingPaymentsApi.createPayin(idPayin, payin)

  @throws[ResponseException]
  def setExternalReference(reference: String): Unit = incomingPaymentsApi.setPayinExternalReference(idPayin, reference)

  @throws[ResponseException]
  def getDetail: PayinDetail = incomingPaymentsApi.getPayin(idPayin)
}
