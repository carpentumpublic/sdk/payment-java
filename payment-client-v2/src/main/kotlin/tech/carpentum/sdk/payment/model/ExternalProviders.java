//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** ExternalProviders
 *
 * 
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public interface ExternalProviders {

    @NotNull java.util.List<@NotNull ExternalProvider> getProviders();

    @NotNull static ExternalProviders ofProviders(java.util.List<@NotNull ExternalProvider> providers) { return builder().providers(providers).build(); }

    @NotNull static Builder builder() {
        return new ExternalProvidersImpl.BuilderImpl();
    }

    /** Builder for {@link ExternalProviders} model class. */
    interface Builder {

        /**
          * Replace all items in {@link ExternalProviders#getProviders} list property.
          *
          * 
          */
        @NotNull Builder providers(java.util.List<@NotNull ExternalProvider> providers);
        /**
          * Add single item to {@link ExternalProviders#getProviders} list property.
          *
          * 
          */
        @NotNull Builder providersAdd(ExternalProvider item);
        /**
          * Add all items to {@link ExternalProviders#getProviders} list property.
          *
          * 
          */
        @NotNull Builder providersAddAll(java.util.List<@NotNull ExternalProvider> providers);


        /**
         * Create new instance of {@link ExternalProviders} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @NotNull ExternalProviders build();

    }
}