//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.CodePaymentMethod

class CodePaymentMethodJsonAdapter {
    @FromJson
    fun fromJson(json: CodePaymentMethodJson): CodePaymentMethod {
        val builder = CodePaymentMethod.builder()
        builder.account(json.account)
        builder.paymentOperatorCode(json.paymentOperatorCode)
        builder.emailAddress(json.emailAddress)
        return builder.build()
    }

    @ToJson
    fun toJson(model: CodePaymentMethod): CodePaymentMethodJson {
        val json = CodePaymentMethodJson()
        json.account = model.account.orElse(null)
        json.paymentOperatorCode = model.paymentOperatorCode
        json.emailAddress = model.emailAddress.orElse(null)
        json.paymentMethodCode = model.paymentMethodCode.name
        return json
    }

    @FromJson
    fun fromJsonImpl(model: CodePaymentMethod): CodePaymentMethodImpl {
        return model as CodePaymentMethodImpl
    }

    @ToJson
    fun toJsonImpl(impl: CodePaymentMethodImpl): CodePaymentMethod {
        return impl
    }

}