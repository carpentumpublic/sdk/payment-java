package tech.carpentum.sdk.payment.sample

import tech.carpentum.sdk.payment.model.{CurrencyList, PaymentMethodsList, PaymentOperatorList, PaymentOptionsList, SegmentList}
import tech.carpentum.sdk.payment.{AuthTokenOperations, PaymentContext, PaymentsApi, ResponseException}

import java.time.Duration
import java.util.function.Supplier

/**
 * Wraps all Payments API operations.
 */
object CodelistsScenario {
  /**
   * Creates new instance of the scenario with newly created Authorization token with specified validity.
   */
  @throws[ResponseException]
  def create(context: PaymentContext, validity: Duration = null): CodelistsScenario = {
    val authToken = context.createAuthToken(
      new AuthTokenOperations()
        .grant(PaymentsApi.defineListCurrenciesEndpoint)
        .grant(PaymentsApi.defineListPaymentMethodsEndpoint)
        .grant(PaymentsApi.defineListPaymentOperatorsEndpoint)
        .grant(PaymentsApi.defineListPaymentOptionsEndpoint)
        .grant(PaymentsApi.defineListSegmentsEndpoint),
      validity
    ).getToken
    new CodelistsScenario(PaymentsApi.create(context, authToken), authToken)
  }

  /**
   * Creates new instance of the scenario with existing Authorization token.
   */
  @throws[ResponseException]
  def create(context: PaymentContext, authToken: String) =
    new CodelistsScenario(PaymentsApi.create(context, authToken), authToken)
}

class CodelistsScenario private(val paymentsApi: PaymentsApi, override val authToken: String) extends AbstractScenario(authToken) {
  @throws[ResponseException]
  def listCurrencies: CurrencyList = paymentsApi.listCurrencies

  @throws[ResponseException]
  def listSegments: SegmentList = paymentsApi.listSegments

  @throws[ResponseException]
  def listPaymentOperators: PaymentOperatorList = paymentsApi.listPaymentOperators

  @throws[ResponseException]
  def listPaymentMethods: PaymentMethodsList = paymentsApi.listPaymentMethods

  @throws[ResponseException]
  def listPaymentOptions: PaymentOptionsList = paymentsApi.listPaymentOptions

  @throws[ResponseException]
  def listPaymentOptions(query: PaymentsApi.ListPaymentOptionsQuery): PaymentOptionsList = paymentsApi.listPaymentOptions(query)

  @throws[ResponseException]
  def listPaymentOptions(querySupplier: Supplier[PaymentsApi.ListPaymentOptionsQuery]): PaymentOptionsList = paymentsApi.listPaymentOptions(querySupplier)
}
