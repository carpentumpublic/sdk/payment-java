//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.AccountPayinRequestNetBanking

class AccountPayinRequestNetBankingJsonAdapter {
    @FromJson
    fun fromJson(json: AccountPayinRequestNetBankingJson): AccountPayinRequestNetBanking {
        val builder = AccountPayinRequestNetBanking.builder()
        builder.accountName(json.accountName)
        builder.accountNumber(json.accountNumber)
        return builder.build()
    }

    @ToJson
    fun toJson(model: AccountPayinRequestNetBanking): AccountPayinRequestNetBankingJson {
        val json = AccountPayinRequestNetBankingJson()
        json.accountName = model.accountName.orElse(null)
        json.accountNumber = model.accountNumber.orElse(null)
        return json
    }

    @FromJson
    fun fromJsonImpl(model: AccountPayinRequestNetBanking): AccountPayinRequestNetBankingImpl {
        return model as AccountPayinRequestNetBankingImpl
    }

    @ToJson
    fun toJsonImpl(impl: AccountPayinRequestNetBankingImpl): AccountPayinRequestNetBanking {
        return impl
    }

}