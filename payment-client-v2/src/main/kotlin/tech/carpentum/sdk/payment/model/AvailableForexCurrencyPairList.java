//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** AvailableForexCurrencyPairList
 *
 * 
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public interface AvailableForexCurrencyPairList {

    @NotNull java.util.List<@NotNull AvailableForexCurrencyPair> getData();

    @NotNull static AvailableForexCurrencyPairList ofData(java.util.List<@NotNull AvailableForexCurrencyPair> data) { return builder().data(data).build(); }

    @NotNull static Builder builder() {
        return new AvailableForexCurrencyPairListImpl.BuilderImpl();
    }

    /** Builder for {@link AvailableForexCurrencyPairList} model class. */
    interface Builder {

        /**
          * Replace all items in {@link AvailableForexCurrencyPairList#getData} list property.
          *
          * 
          */
        @NotNull Builder data(java.util.List<@NotNull AvailableForexCurrencyPair> data);
        /**
          * Add single item to {@link AvailableForexCurrencyPairList#getData} list property.
          *
          * 
          */
        @NotNull Builder dataAdd(AvailableForexCurrencyPair item);
        /**
          * Add all items to {@link AvailableForexCurrencyPairList#getData} list property.
          *
          * 
          */
        @NotNull Builder dataAddAll(java.util.List<@NotNull AvailableForexCurrencyPair> data);


        /**
         * Create new instance of {@link AvailableForexCurrencyPairList} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @NotNull AvailableForexCurrencyPairList build();

    }
}