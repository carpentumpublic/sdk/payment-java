//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** PaymentOperatorList
 *
 * List of available payment operators.
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public interface PaymentOperatorList {

    @NotNull java.util.List<@NotNull PaymentOperatorIncoming> getData();

    @NotNull static PaymentOperatorList ofData(java.util.List<@NotNull PaymentOperatorIncoming> data) { return builder().data(data).build(); }

    @NotNull static Builder builder() {
        return new PaymentOperatorListImpl.BuilderImpl();
    }

    /** Builder for {@link PaymentOperatorList} model class. */
    interface Builder {

        /**
          * Replace all items in {@link PaymentOperatorList#getData} list property.
          *
          * 
          */
        @NotNull Builder data(java.util.List<@NotNull PaymentOperatorIncoming> data);
        /**
          * Add single item to {@link PaymentOperatorList#getData} list property.
          *
          * 
          */
        @NotNull Builder dataAdd(PaymentOperatorIncoming item);
        /**
          * Add all items to {@link PaymentOperatorList#getData} list property.
          *
          * 
          */
        @NotNull Builder dataAddAll(java.util.List<@NotNull PaymentOperatorIncoming> data);


        /**
         * Create new instance of {@link PaymentOperatorList} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @NotNull PaymentOperatorList build();

    }
}