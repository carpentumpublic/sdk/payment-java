package tech.carpentum.sdk.payment

object CompilationTestSupport {
    val context: PaymentContext = PaymentContext.builder().build()
    val token: String = context.createAuthToken(AuthTokenOperations()).token

    val accountsApi: AccountsApi = AccountsApi.Factory.create(context, token)
    val incomingPaymentsApi: IncomingPaymentsApi = IncomingPaymentsApi.Factory.create(context, token)
    val merchantInfoApi: MerchantInfoApi = MerchantInfoApi.Factory.create(context, token)
    val outgoingPaymentsApi: OutgoingPaymentsApi = OutgoingPaymentsApi.Factory.create(context, token)
    val paymentsApi: PaymentsApi = PaymentsApi.Factory.create(context, token)
    val rateListsApi: RateListsApi = RateListsApi.Factory.create(context, token)
    val settlementsApi: SettlementsApi = SettlementsApi.Factory.create(context, token)
}
