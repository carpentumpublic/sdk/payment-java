//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** PayoutAccepted
 *
 * The outgoing payment has been accepted for processing.
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public interface PayoutAccepted {

    @NotNull PaymentRequested getPaymentRequested();

    @NotNull PayoutMethodResponse getPaymentMethodResponse();

    @NotNull static Builder builder(PayoutAccepted copyOf) {
        Builder builder = builder();
        builder.paymentRequested(copyOf.getPaymentRequested());
        builder.paymentMethodResponse(copyOf.getPaymentMethodResponse());
        return builder;
    }

    @NotNull static Builder builder() {
        return new PayoutAcceptedImpl.BuilderImpl();
    }

    /** Builder for {@link PayoutAccepted} model class. */
    interface Builder {

        /**
          * Set {@link PayoutAccepted#getPaymentRequested} property.
          *
          * 
          */
        @NotNull Builder paymentRequested(PaymentRequested paymentRequested);

        boolean isPaymentRequestedDefined();


        /**
          * Set {@link PayoutAccepted#getPaymentMethodResponse} property.
          *
          * 
          */
        @NotNull Builder paymentMethodResponse(PayoutMethodResponse paymentMethodResponse);

        boolean isPaymentMethodResponseDefined();


        /**
         * Create new instance of {@link PayoutAccepted} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @NotNull PayoutAccepted build();

    }
}