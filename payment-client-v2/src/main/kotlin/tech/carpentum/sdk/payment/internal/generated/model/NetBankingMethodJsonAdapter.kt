//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.NetBankingMethod

class NetBankingMethodJsonAdapter {
    @FromJson
    fun fromJson(json: NetBankingMethodJson): NetBankingMethod {
        val builder = NetBankingMethod.builder()
        builder.account(json.account)
        builder.emailAddress(json.emailAddress)
        builder.phoneNumber(json.phoneNumber)
        builder.paymentOperatorCode(json.paymentOperatorCode)
        return builder.build()
    }

    @ToJson
    fun toJson(model: NetBankingMethod): NetBankingMethodJson {
        val json = NetBankingMethodJson()
        json.account = model.account.orElse(null)
        json.emailAddress = model.emailAddress.orElse(null)
        json.phoneNumber = model.phoneNumber.orElse(null)
        json.paymentOperatorCode = model.paymentOperatorCode.orElse(null)
        json.paymentMethodCode = model.paymentMethodCode.name
        return json
    }

    @FromJson
    fun fromJsonImpl(model: NetBankingMethod): NetBankingMethodImpl {
        return model as NetBankingMethodImpl
    }

    @ToJson
    fun toJsonImpl(impl: NetBankingMethodImpl): NetBankingMethod {
        return impl
    }

}