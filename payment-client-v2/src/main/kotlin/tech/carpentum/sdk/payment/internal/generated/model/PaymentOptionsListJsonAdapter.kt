//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.PaymentOptionsList

class PaymentOptionsListJsonAdapter {
    @FromJson
    fun fromJson(json: PaymentOptionsListJson): PaymentOptionsList {
        val builder = PaymentOptionsList.builder()
        builder.data(json.data?.toList())
        return builder.build()
    }

    @ToJson
    fun toJson(model: PaymentOptionsList): PaymentOptionsListJson {
        val json = PaymentOptionsListJson()
        json.data = model.data.ifEmpty { null }
        return json
    }

    @FromJson
    fun fromJsonImpl(model: PaymentOptionsList): PaymentOptionsListImpl {
        return model as PaymentOptionsListImpl
    }

    @ToJson
    fun toJsonImpl(impl: PaymentOptionsListImpl): PaymentOptionsList {
        return impl
    }

}