//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.OfflineMethod

class OfflineMethodJsonAdapter {
    @FromJson
    fun fromJson(json: OfflineMethodJson): OfflineMethod {
        val builder = OfflineMethod.builder()
        builder.account(json.account)
        builder.paymentOperatorCode(json.paymentOperatorCode)
        builder.emailAddress(json.emailAddress)
        builder.phoneNumber(json.phoneNumber)
        builder.productId(json.productId)
        return builder.build()
    }

    @ToJson
    fun toJson(model: OfflineMethod): OfflineMethodJson {
        val json = OfflineMethodJson()
        json.account = model.account.orElse(null)
        json.paymentOperatorCode = model.paymentOperatorCode.orElse(null)
        json.emailAddress = model.emailAddress.orElse(null)
        json.phoneNumber = model.phoneNumber.orElse(null)
        json.productId = model.productId.orElse(null)
        json.paymentMethodCode = model.paymentMethodCode.name
        return json
    }

    @FromJson
    fun fromJsonImpl(model: OfflineMethod): OfflineMethodImpl {
        return model as OfflineMethodImpl
    }

    @ToJson
    fun toJsonImpl(impl: OfflineMethodImpl): OfflineMethod {
        return impl
    }

}