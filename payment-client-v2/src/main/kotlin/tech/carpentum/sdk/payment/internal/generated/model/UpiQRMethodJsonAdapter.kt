//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.UpiQRMethod

class UpiQRMethodJsonAdapter {
    @FromJson
    fun fromJson(json: UpiQRMethodJson): UpiQRMethod {
        val builder = UpiQRMethod.builder()
        builder.account(json.account)
        builder.upiId(json.upiId)
        builder.emailAddress(json.emailAddress)
        builder.phoneNumber(json.phoneNumber)
        builder.paymentOperatorCode(json.paymentOperatorCode)
        return builder.build()
    }

    @ToJson
    fun toJson(model: UpiQRMethod): UpiQRMethodJson {
        val json = UpiQRMethodJson()
        json.account = model.account.orElse(null)
        json.upiId = model.upiId.orElse(null)
        json.emailAddress = model.emailAddress.orElse(null)
        json.phoneNumber = model.phoneNumber.orElse(null)
        json.paymentOperatorCode = model.paymentOperatorCode.orElse(null)
        json.paymentMethodCode = model.paymentMethodCode.name
        return json
    }

    @FromJson
    fun fromJsonImpl(model: UpiQRMethod): UpiQRMethodImpl {
        return model as UpiQRMethodImpl
    }

    @ToJson
    fun toJsonImpl(impl: UpiQRMethodImpl): UpiQRMethod {
        return impl
    }

}