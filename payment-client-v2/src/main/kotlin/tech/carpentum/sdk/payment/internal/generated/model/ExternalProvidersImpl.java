//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** ExternalProviders
 *
 * 
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public class ExternalProvidersImpl implements ExternalProviders {
    private final java.util.List<@NotNull ExternalProvider> providers;

    @Override
    public java.util.List<@NotNull ExternalProvider> getProviders() {
        return providers;
    }




    private final int hashCode;
    private final String toString;

    private ExternalProvidersImpl(BuilderImpl builder) {
        this.providers = java.util.Collections.unmodifiableList(builder.providers);

        this.hashCode = Objects.hash(providers);
        this.toString = builder.type + "(" +
                "providers=" + providers +
                ')';
    }

    @Override
    public int hashCode() {
        return hashCode;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }

        if (!(obj instanceof ExternalProvidersImpl)) {
            return false;
        }

        ExternalProvidersImpl that = (ExternalProvidersImpl) obj;
        if (!Objects.equals(this.providers, that.providers)) return false;

        return true;
    }

    @Override
    public String toString() {
        return toString;
    }

    /** Builder for {@link ExternalProviders} model class. */
    public static class BuilderImpl implements ExternalProviders.Builder {
        private java.util.List<@NotNull ExternalProvider> providers = new java.util.ArrayList<>();

        private final String type;

        public BuilderImpl(String type) {
            this.type = type;
        }

        public BuilderImpl() {
            this("ExternalProviders");
        }

        /**
          * Replace all items in {@link ExternalProviders#getProviders} list property.
          *
          * 
          */
        @Override
        public BuilderImpl providers(java.util.List<@NotNull ExternalProvider> providers) {
            this.providers.clear();
            if (providers != null) {
                this.providers.addAll(providers);
            }
            return this;
        }
        /**
          * Add single item to {@link ExternalProviders#getProviders} list property.
          *
          * 
          */
        @Override
        public BuilderImpl providersAdd(ExternalProvider item) {
            if (item != null) {
                this.providers.add(item);
            }
            return this;
        }
        /**
          * Add all items to {@link ExternalProviders#getProviders} list property.
          *
          * 
          */
        @Override
        public BuilderImpl providersAddAll(java.util.List<@NotNull ExternalProvider> providers) {
            if (providers != null) {
                this.providers.addAll(providers);
            }
            return this;
        }


        /**
         * Create new instance of {@link ExternalProviders} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @Override
        public ExternalProvidersImpl build() {
            return new ExternalProvidersImpl(this);
        }

    }
}