//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.PayShapMethod

class PayShapMethodJsonAdapter {
    @FromJson
    fun fromJson(json: PayShapMethodJson): PayShapMethod {
        val builder = PayShapMethod.builder()
        builder.account(json.account)
        builder.phoneNumber(json.phoneNumber)
        builder.paymentOperatorCode(json.paymentOperatorCode)
        builder.externalProviderCode(json.externalProviderCode)
        return builder.build()
    }

    @ToJson
    fun toJson(model: PayShapMethod): PayShapMethodJson {
        val json = PayShapMethodJson()
        json.account = model.account.orElse(null)
        json.phoneNumber = model.phoneNumber.orElse(null)
        json.paymentOperatorCode = model.paymentOperatorCode
        json.externalProviderCode = model.externalProviderCode.orElse(null)
        json.paymentMethodCode = model.paymentMethodCode.name
        return json
    }

    @FromJson
    fun fromJsonImpl(model: PayShapMethod): PayShapMethodImpl {
        return model as PayShapMethodImpl
    }

    @ToJson
    fun toJsonImpl(impl: PayShapMethodImpl): PayShapMethod {
        return impl
    }

}