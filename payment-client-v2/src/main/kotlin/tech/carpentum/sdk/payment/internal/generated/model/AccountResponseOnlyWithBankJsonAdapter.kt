//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.AccountResponseOnlyWithBank

class AccountResponseOnlyWithBankJsonAdapter {
    @FromJson
    fun fromJson(json: AccountResponseOnlyWithBankJson): AccountResponseOnlyWithBank {
        val builder = AccountResponseOnlyWithBank.builder()
        builder.accountName(json.accountName)
        builder.accountNumber(json.accountNumber)
        builder.bankCode(json.bankCode)
        builder.bankName(json.bankName)
        builder.bankBranch(json.bankBranch)
        builder.bankCity(json.bankCity)
        builder.bankProvince(json.bankProvince)
        return builder.build()
    }

    @ToJson
    fun toJson(model: AccountResponseOnlyWithBank): AccountResponseOnlyWithBankJson {
        val json = AccountResponseOnlyWithBankJson()
        json.accountName = model.accountName.orElse(null)
        json.accountNumber = model.accountNumber.orElse(null)
        json.bankCode = model.bankCode.orElse(null)
        json.bankName = model.bankName.orElse(null)
        json.bankBranch = model.bankBranch.orElse(null)
        json.bankCity = model.bankCity.orElse(null)
        json.bankProvince = model.bankProvince.orElse(null)
        return json
    }

    @FromJson
    fun fromJsonImpl(model: AccountResponseOnlyWithBank): AccountResponseOnlyWithBankImpl {
        return model as AccountResponseOnlyWithBankImpl
    }

    @ToJson
    fun toJsonImpl(impl: AccountResponseOnlyWithBankImpl): AccountResponseOnlyWithBank {
        return impl
    }

}