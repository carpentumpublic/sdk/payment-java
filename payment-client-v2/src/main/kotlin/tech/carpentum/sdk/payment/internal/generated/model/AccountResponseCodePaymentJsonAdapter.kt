//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.AccountResponseCodePayment

class AccountResponseCodePaymentJsonAdapter {
    @FromJson
    fun fromJson(json: AccountResponseCodePaymentJson): AccountResponseCodePayment {
        val builder = AccountResponseCodePayment.builder()
        builder.accountNumber(json.accountNumber)
        return builder.build()
    }

    @ToJson
    fun toJson(model: AccountResponseCodePayment): AccountResponseCodePaymentJson {
        val json = AccountResponseCodePaymentJson()
        json.accountNumber = model.accountNumber
        return json
    }

    @FromJson
    fun fromJsonImpl(model: AccountResponseCodePayment): AccountResponseCodePaymentImpl {
        return model as AccountResponseCodePaymentImpl
    }

    @ToJson
    fun toJsonImpl(impl: AccountResponseCodePaymentImpl): AccountResponseCodePayment {
        return impl
    }

}