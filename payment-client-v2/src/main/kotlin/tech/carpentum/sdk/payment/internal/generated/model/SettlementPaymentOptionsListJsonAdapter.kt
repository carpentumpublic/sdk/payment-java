//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.SettlementPaymentOptionsList

class SettlementPaymentOptionsListJsonAdapter {
    @FromJson
    fun fromJson(json: SettlementPaymentOptionsListJson): SettlementPaymentOptionsList {
        val builder = SettlementPaymentOptionsList.builder()
        builder.data(json.data?.toList())
        return builder.build()
    }

    @ToJson
    fun toJson(model: SettlementPaymentOptionsList): SettlementPaymentOptionsListJson {
        val json = SettlementPaymentOptionsListJson()
        json.data = model.data.ifEmpty { null }
        return json
    }

    @FromJson
    fun fromJsonImpl(model: SettlementPaymentOptionsList): SettlementPaymentOptionsListImpl {
        return model as SettlementPaymentOptionsListImpl
    }

    @ToJson
    fun toJsonImpl(impl: SettlementPaymentOptionsListImpl): SettlementPaymentOptionsList {
        return impl
    }

}