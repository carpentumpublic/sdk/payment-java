package tech.carpentum.sdk.payment.model

import io.kotest.core.spec.style.FunSpec
import io.kotest.matchers.shouldBe

/**
 * Tests [AvailablePayinOption].
 */
class AvailablePayinOptionTest : FunSpec({

    test("Property PayinMethodCode should be String - compilation test") {
        AvailablePayinOption.builder()
            .paymentMethodCode(PayinMethodCode.of("payment1"))
            .paymentOperatorsAdd(
                PaymentOperatorIncoming.builder()
                    .code("operation1")
                    .name("Operation 1")
                    .build()
            )
            .paymentOperatorsAdd(
                PaymentOperatorIncoming.builder()
                    .code("operation2")
                    .name("Operation 2")
                    .build()
            )
            .segmentCode(SegmentCode.of("segment1"))
            .build()
            .toString() shouldBe "AvailablePayinOption(paymentMethodCode=payment1, paymentOperators=[PaymentOperatorIncoming(code=operation1, name=Operation 1, externalProviders=Optional.empty), PaymentOperatorIncoming(code=operation2, name=Operation 2, externalProviders=Optional.empty)], segmentCode=Optional[segment1])"
    }

})
