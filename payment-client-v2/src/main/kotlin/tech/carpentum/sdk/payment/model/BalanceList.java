//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** BalanceList
 *
 * The list of account balances by currency.
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public interface BalanceList {

    @NotNull java.util.List<@NotNull Balance> getData();

    @NotNull static BalanceList ofData(java.util.List<@NotNull Balance> data) { return builder().data(data).build(); }

    @NotNull static Builder builder() {
        return new BalanceListImpl.BuilderImpl();
    }

    /** Builder for {@link BalanceList} model class. */
    interface Builder {

        /**
          * Replace all items in {@link BalanceList#getData} list property.
          *
          * 
          */
        @NotNull Builder data(java.util.List<@NotNull Balance> data);
        /**
          * Add single item to {@link BalanceList#getData} list property.
          *
          * 
          */
        @NotNull Builder dataAdd(Balance item);
        /**
          * Add all items to {@link BalanceList#getData} list property.
          *
          * 
          */
        @NotNull Builder dataAddAll(java.util.List<@NotNull Balance> data);


        /**
         * Create new instance of {@link BalanceList} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @NotNull BalanceList build();

    }
}