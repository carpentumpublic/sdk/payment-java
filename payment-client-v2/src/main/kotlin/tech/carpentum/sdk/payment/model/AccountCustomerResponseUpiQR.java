//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** AccountCustomerResponseUpiQR
 *
 * Parameters of your customer's bank or wallet account which your customer sends funds from. These account parameters are used for the sender's account verification in processing of the payment.
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public interface AccountCustomerResponseUpiQR {

    /** Account Name is the name of the person who holds the bank or wallet account which your customer sends funds from to make his payment. */
    @NotNull Optional<String> getAccountName();

    @NotNull static AccountCustomerResponseUpiQR ofAccountName(String accountName) { return builder().accountName(accountName).build(); }

    @NotNull static Builder builder() {
        return new AccountCustomerResponseUpiQRImpl.BuilderImpl();
    }

    /** Builder for {@link AccountCustomerResponseUpiQR} model class. */
    interface Builder {

        /**
          * Set {@link AccountCustomerResponseUpiQR#getAccountName} property.
          *
          * Account Name is the name of the person who holds the bank or wallet account which your customer sends funds from to make his payment.
          */
        @NotNull Builder accountName(String accountName);

        boolean isAccountNameDefined();


        /**
         * Create new instance of {@link AccountCustomerResponseUpiQR} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @NotNull AccountCustomerResponseUpiQR build();

    }
}