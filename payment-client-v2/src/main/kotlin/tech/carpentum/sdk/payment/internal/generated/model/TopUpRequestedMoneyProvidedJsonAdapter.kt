//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.TopUpRequestedMoneyProvided

class TopUpRequestedMoneyProvidedJsonAdapter {
    @FromJson
    fun fromJson(json: TopUpRequestedMoneyProvidedJson): TopUpRequestedMoneyProvided {
        val builder = TopUpRequestedMoneyProvided.builder()
        builder.moneyProvided(json.moneyProvided)
        builder.currencyRequired(json.currencyRequired)
        return builder.build()
    }

    @ToJson
    fun toJson(model: TopUpRequestedMoneyProvided): TopUpRequestedMoneyProvidedJson {
        val json = TopUpRequestedMoneyProvidedJson()
        json.moneyProvided = model.moneyProvided
        json.currencyRequired = model.currencyRequired.orElse(null)
        return json
    }

    @FromJson
    fun fromJsonImpl(model: TopUpRequestedMoneyProvided): TopUpRequestedMoneyProvidedImpl {
        return model as TopUpRequestedMoneyProvidedImpl
    }

    @ToJson
    fun toJsonImpl(impl: TopUpRequestedMoneyProvidedImpl): TopUpRequestedMoneyProvided {
        return impl
    }

}