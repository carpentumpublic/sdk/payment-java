//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** AccountCustomerResponseQrPh
 *
 * Parameters of your customer's bank or wallet account which your customer sends funds from. These account parameters are used for the sender's account verification in processing of the payment.
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public interface AccountCustomerResponseQrPh {

    /** Account Name is the name of the person who holds the bank or wallet account which your customer sends funds from to make his payment. */
    @NotNull Optional<String> getAccountName();

    /** Account Number is the number of your customer's bank account which your customer sends funds from to make his payment. */
    @NotNull Optional<String> getAccountNumber();

    @NotNull static Builder builder(AccountCustomerResponseQrPh copyOf) {
        Builder builder = builder();
        builder.accountName(copyOf.getAccountName().orElse(null));
        builder.accountNumber(copyOf.getAccountNumber().orElse(null));
        return builder;
    }

    @NotNull static Builder builder() {
        return new AccountCustomerResponseQrPhImpl.BuilderImpl();
    }

    /** Builder for {@link AccountCustomerResponseQrPh} model class. */
    interface Builder {

        /**
          * Set {@link AccountCustomerResponseQrPh#getAccountName} property.
          *
          * Account Name is the name of the person who holds the bank or wallet account which your customer sends funds from to make his payment.
          */
        @NotNull Builder accountName(String accountName);

        boolean isAccountNameDefined();


        /**
          * Set {@link AccountCustomerResponseQrPh#getAccountNumber} property.
          *
          * Account Number is the number of your customer's bank account which your customer sends funds from to make his payment.
          */
        @NotNull Builder accountNumber(String accountNumber);

        boolean isAccountNumberDefined();


        /**
         * Create new instance of {@link AccountCustomerResponseQrPh} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @NotNull AccountCustomerResponseQrPh build();

    }
}