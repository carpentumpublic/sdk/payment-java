package tech.carpentum.sdk.payment.sample

import tech.carpentum.sdk.payment.AuthTokenOperations
import tech.carpentum.sdk.payment.PaymentContext
import tech.carpentum.sdk.payment.PaymentsApi
import tech.carpentum.sdk.payment.PaymentsApi.Factory.defineListCurrenciesEndpoint
import tech.carpentum.sdk.payment.PaymentsApi.Factory.defineListPaymentMethodsEndpoint
import tech.carpentum.sdk.payment.PaymentsApi.Factory.defineListPaymentOperatorsEndpoint
import tech.carpentum.sdk.payment.PaymentsApi.Factory.defineListPaymentOptionsEndpoint
import tech.carpentum.sdk.payment.PaymentsApi.Factory.defineListSegmentsEndpoint
import tech.carpentum.sdk.payment.PaymentsApi.ListPaymentOptionsQuery
import tech.carpentum.sdk.payment.ResponseException
import tech.carpentum.sdk.payment.model.CurrencyList
import tech.carpentum.sdk.payment.model.PaymentMethodsList
import tech.carpentum.sdk.payment.model.PaymentOperatorList
import tech.carpentum.sdk.payment.model.PaymentOptionsList
import tech.carpentum.sdk.payment.model.SegmentList
import java.io.InterruptedIOException
import java.time.Duration
import java.util.function.Supplier

/**
 * Wraps all Payments API operations.
 */
class CodelistsScenario private constructor(private val paymentsApi: PaymentsApi, authToken: String) :
    AbstractScenario(authToken) {

    @Throws(ResponseException::class, InterruptedIOException::class)
    fun listCurrencies(): CurrencyList {
        return paymentsApi.listCurrencies()
    }

    @Throws(ResponseException::class, InterruptedIOException::class)
    fun listSegments(): SegmentList {
        return paymentsApi.listSegments()
    }

    @Throws(ResponseException::class, InterruptedIOException::class)
    fun listPaymentOperators(): PaymentOperatorList {
        return paymentsApi.listPaymentOperators()
    }

    @Throws(ResponseException::class, InterruptedIOException::class)
    fun listPaymentMethods(): PaymentMethodsList {
        return paymentsApi.listPaymentMethods()
    }

    @Throws(ResponseException::class, InterruptedIOException::class)
    fun listPaymentOptions(): PaymentOptionsList {
        return paymentsApi.listPaymentOptions()
    }

    @Throws(ResponseException::class, InterruptedIOException::class)
    fun listPaymentOptions(query: ListPaymentOptionsQuery): PaymentOptionsList {
        return paymentsApi.listPaymentOptions(query)
    }

    @Throws(ResponseException::class, InterruptedIOException::class)
    fun listPaymentOptions(querySupplier: Supplier<ListPaymentOptionsQuery>): PaymentOptionsList {
        return paymentsApi.listPaymentOptions(querySupplier)
    }

    companion object {
        /**
         * Creates new instance of the scenario with newly created Authorization token with specified validity.
         */
        @Throws(ResponseException::class, InterruptedIOException::class)
        fun create(context: PaymentContext, validity: Duration? = null): CodelistsScenario {
            val token = context.createAuthToken(
                AuthTokenOperations()
                    .grant(defineListCurrenciesEndpoint())
                    .grant(defineListPaymentMethodsEndpoint())
                    .grant(defineListPaymentOperatorsEndpoint())
                    .grant(defineListPaymentOptionsEndpoint())
                    .grant(defineListSegmentsEndpoint()),
                validity
            ).token
            return CodelistsScenario(PaymentsApi.create(context, token), token)
        }

        /**
         * Creates new instance of the scenario with existing Authorization token.
         */
        @Throws(ResponseException::class, InterruptedIOException::class)
        fun create(context: PaymentContext, authToken: String): CodelistsScenario {
            return CodelistsScenario(PaymentsApi.create(context, authToken), authToken)
        }
    }
}
