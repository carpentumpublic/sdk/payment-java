//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** AccountSettlementRequestCryptoTransfer
 *
 * Parameters of a customer's crypto wallet information where your customer would like his funds to be transferred.
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public interface AccountSettlementRequestCryptoTransfer {

    /** Address of a customer's crypto wallet where your customer would like his funds to be transferred. */
    @NotNull String getAccountNumber();

    @NotNull static AccountSettlementRequestCryptoTransfer ofAccountNumber(String accountNumber) { return builder().accountNumber(accountNumber).build(); }

    @NotNull static Builder builder() {
        return new AccountSettlementRequestCryptoTransferImpl.BuilderImpl();
    }

    /** Builder for {@link AccountSettlementRequestCryptoTransfer} model class. */
    interface Builder {

        /**
          * Set {@link AccountSettlementRequestCryptoTransfer#getAccountNumber} property.
          *
          * Address of a customer's crypto wallet where your customer would like his funds to be transferred.
          */
        @NotNull Builder accountNumber(String accountNumber);

        boolean isAccountNumberDefined();


        /**
         * Create new instance of {@link AccountSettlementRequestCryptoTransfer} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @NotNull AccountSettlementRequestCryptoTransfer build();

    }
}