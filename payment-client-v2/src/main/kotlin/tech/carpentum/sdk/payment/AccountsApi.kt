@file:JvmName("AccountsApiUtils")

package tech.carpentum.sdk.payment

import tech.carpentum.sdk.payment.AccountsApi.Factory
import tech.carpentum.sdk.payment.EndpointDefinition.Method.GET
import tech.carpentum.sdk.payment.internal.api.CommaSeparatedCodes
import tech.carpentum.sdk.payment.internal.api.EnhancedAccountsApi
import tech.carpentum.sdk.payment.internal.api.GetPayoutErrorExceptionFactory
import tech.carpentum.sdk.payment.internal.api.ResponseExceptionUtils
import tech.carpentum.sdk.payment.model.BalanceList
import tech.carpentum.sdk.payment.model.GetPayoutError
import java.io.InterruptedIOException
import java.time.Duration

/**
 * The Accounts RESTful API.
 *
 * Use [Factory] to create new instance of the class.
 */
class AccountsApi private constructor(
    private val apiVersion: Int,
    private val api: EnhancedAccountsApi
) {

    /**
     * Throws [GetPayoutErrorException] ("406" response) with one of defined
     * [GetPayoutError] business validation error code.
     * Throws [InterruptedIOException] in case of timeout.
     */
    @JvmOverloads
    @Throws(ResponseException::class, InterruptedIOException::class)
    // tag::userGuidePublicApi[]
    fun listBalances(currencyCodes: Set<String>? = null): BalanceList
    // end::userGuidePublicApi[]
    {
        return ResponseExceptionUtils.wrap(GetPayoutErrorExceptionFactory.instance) {
            api.getBalances(currencyCodes = CommaSeparatedCodes.format(currencyCodes), xAPIVersion = apiVersion)
        }
    }

    /**
     * Factory to create a new instance of [AccountsApi].
     */
    companion object Factory {
        /**
         * Endpoint definition for [AccountsApi.listBalances] method.
         */
        @JvmStatic
        fun defineListBalancesEndpoint(): EndpointDefinition = EndpointDefinition(GET, "/balances")

        @JvmStatic
        @JvmOverloads
        fun create(context: PaymentContext, accessToken: String, callTimeout: Duration? = null): AccountsApi {
            return AccountsApi(
                context.apiVersion,
                EnhancedAccountsApi(basePath = context.apiBaseUrl, accessToken = accessToken, brand = context.brand, callTimeout = callTimeout ?: context.defaultCallTimeout)
            )
        }
    }

}

/**
 * Grants [AccountsApi.listBalances] endpoint, see [AccountsApi.defineListBalancesEndpoint] definition.
 */
@JvmName("grantListBalancesEndpoint")
fun AuthTokenOperations.grantAccountsApiListBalancesEndpoint(): AuthTokenOperations =
    this.grant(AccountsApi.defineListBalancesEndpoint())
