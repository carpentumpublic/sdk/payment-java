//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** OFFLINE
 *
 * Offline Bank Transfer. Payment method which requires customer to copy Payment instruction from the Payment application right after the incoming payment is submitted and create the Payment transfer using the instructions within customer's own payment service such as Internet or mobile banking or wallet.

As the Payment relies on the customer offline Payment transfer processing it can take minutes for Payment to be confirmed and also number of Payment expiration ratio can be higher.
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public class OfflineMethodImpl implements OfflineMethod {
    private final Optional<AccountPayinRequestOffline> account;

    @Override
    public Optional<AccountPayinRequestOffline> getAccount() {
        return account;
    }


    /** One of following can serve as Payment Operator:

 * Financial or other institution (such as bank, card payment processor, ...) that manages transactions for your customers
 * Mobile wallet
 * Blockchain protocol for crypto currency payments

Customer is informed with the payment instructions where funds have to be transferred efficiently based on the selected Payment operator.
For getting list of the available payment options for payins use [POST /payins/!availablePaymentOptions](#operations-Incoming_payments-availablePaymentOptions) API, for payouts use [POST /payouts/!availablePaymentOptions](#operations-Outgoing_payments-availablePaymentOptions) API. */
    private final Optional<String> paymentOperatorCode;

    @Override
    public Optional<String> getPaymentOperatorCode() {
        return paymentOperatorCode;
    }


    /** Your customer e-mail address in RFC 5322 format that is used for identification of the customer's payins.

If currency is JPY, then emailAddress field is required. Otherwise, it is optional. */
    private final Optional<String> emailAddress;

    @Override
    public Optional<String> getEmailAddress() {
        return emailAddress;
    }


    /** Your customer mobile phone number in full international telephone number format, including country code. */
    private final Optional<String> phoneNumber;

    @Override
    public Optional<String> getPhoneNumber() {
        return phoneNumber;
    }


    /** Identification of a product related to a payment order. This field is returned only if it is provided in a payment order request. */
    private final Optional<String> productId;

    @Override
    public Optional<String> getProductId() {
        return productId;
    }


    @Override public PaymentMethodCode getPaymentMethodCode() { return PAYMENT_METHOD_CODE; }

    private final int hashCode;
    private final String toString;

    private OfflineMethodImpl(BuilderImpl builder) {
        this.account = Optional.ofNullable(builder.account);
        this.paymentOperatorCode = Optional.ofNullable(builder.paymentOperatorCode);
        this.emailAddress = Optional.ofNullable(builder.emailAddress);
        this.phoneNumber = Optional.ofNullable(builder.phoneNumber);
        this.productId = Optional.ofNullable(builder.productId);

        this.hashCode = Objects.hash(account, paymentOperatorCode, emailAddress, phoneNumber, productId);
        this.toString = builder.type + "(" +
                "account=" + account +
                ", paymentOperatorCode=" + paymentOperatorCode +
                ", emailAddress=" + emailAddress +
                ", phoneNumber=" + phoneNumber +
                ", productId=" + productId +
                ')';
    }

    @Override
    public int hashCode() {
        return hashCode;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }

        if (!(obj instanceof OfflineMethodImpl)) {
            return false;
        }

        OfflineMethodImpl that = (OfflineMethodImpl) obj;
        if (!Objects.equals(this.account, that.account)) return false;
        if (!Objects.equals(this.paymentOperatorCode, that.paymentOperatorCode)) return false;
        if (!Objects.equals(this.emailAddress, that.emailAddress)) return false;
        if (!Objects.equals(this.phoneNumber, that.phoneNumber)) return false;
        if (!Objects.equals(this.productId, that.productId)) return false;

        return true;
    }

    @Override
    public String toString() {
        return toString;
    }

    /** Builder for {@link OfflineMethod} model class. */
    public static class BuilderImpl implements OfflineMethod.Builder {
        private AccountPayinRequestOffline account = null;
        private String paymentOperatorCode = null;
        private String emailAddress = null;
        private String phoneNumber = null;
        private String productId = null;

        private final String type;

        public BuilderImpl(String type) {
            this.type = type;
        }

        public BuilderImpl() {
            this("OfflineMethod");
        }

        /**
          * Set {@link OfflineMethod#getAccount} property.
          *
          * 
          */
        @Override
        public BuilderImpl account(AccountPayinRequestOffline account) {
            this.account = account;
            return this;
        }

        @Override
        public boolean isAccountDefined() {
            return this.account != null;
        }

        /**
          * Set {@link OfflineMethod#getPaymentOperatorCode} property.
          *
          * One of following can serve as Payment Operator:

 * Financial or other institution (such as bank, card payment processor, ...) that manages transactions for your customers
 * Mobile wallet
 * Blockchain protocol for crypto currency payments

Customer is informed with the payment instructions where funds have to be transferred efficiently based on the selected Payment operator.
For getting list of the available payment options for payins use [POST /payins/!availablePaymentOptions](#operations-Incoming_payments-availablePaymentOptions) API, for payouts use [POST /payouts/!availablePaymentOptions](#operations-Outgoing_payments-availablePaymentOptions) API.
          */
        @Override
        public BuilderImpl paymentOperatorCode(String paymentOperatorCode) {
            this.paymentOperatorCode = paymentOperatorCode;
            return this;
        }

        @Override
        public boolean isPaymentOperatorCodeDefined() {
            return this.paymentOperatorCode != null;
        }

        /**
          * Set {@link OfflineMethod#getEmailAddress} property.
          *
          * Your customer e-mail address in RFC 5322 format that is used for identification of the customer's payins.

If currency is JPY, then emailAddress field is required. Otherwise, it is optional.
          */
        @Override
        public BuilderImpl emailAddress(String emailAddress) {
            this.emailAddress = emailAddress;
            return this;
        }

        @Override
        public boolean isEmailAddressDefined() {
            return this.emailAddress != null;
        }

        /**
          * Set {@link OfflineMethod#getPhoneNumber} property.
          *
          * Your customer mobile phone number in full international telephone number format, including country code.
          */
        @Override
        public BuilderImpl phoneNumber(String phoneNumber) {
            this.phoneNumber = phoneNumber;
            return this;
        }

        @Override
        public boolean isPhoneNumberDefined() {
            return this.phoneNumber != null;
        }

        /**
          * Set {@link OfflineMethod#getProductId} property.
          *
          * Identification of a product related to a payment order. This field is returned only if it is provided in a payment order request.
          */
        @Override
        public BuilderImpl productId(String productId) {
            this.productId = productId;
            return this;
        }

        @Override
        public boolean isProductIdDefined() {
            return this.productId != null;
        }

        /**
         * Create new instance of {@link OfflineMethod} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @Override
        public OfflineMethodImpl build() {
            return new OfflineMethodImpl(this);
        }

    }
}