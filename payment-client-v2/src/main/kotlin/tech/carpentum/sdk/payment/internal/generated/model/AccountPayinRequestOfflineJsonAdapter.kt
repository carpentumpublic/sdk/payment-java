//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.AccountPayinRequestOffline

class AccountPayinRequestOfflineJsonAdapter {
    @FromJson
    fun fromJson(json: AccountPayinRequestOfflineJson): AccountPayinRequestOffline {
        val builder = AccountPayinRequestOffline.builder()
        builder.accountName(json.accountName)
        builder.accountNumber(json.accountNumber)
        return builder.build()
    }

    @ToJson
    fun toJson(model: AccountPayinRequestOffline): AccountPayinRequestOfflineJson {
        val json = AccountPayinRequestOfflineJson()
        json.accountName = model.accountName.orElse(null)
        json.accountNumber = model.accountNumber.orElse(null)
        return json
    }

    @FromJson
    fun fromJsonImpl(model: AccountPayinRequestOffline): AccountPayinRequestOfflineImpl {
        return model as AccountPayinRequestOfflineImpl
    }

    @ToJson
    fun toJsonImpl(impl: AccountPayinRequestOfflineImpl): AccountPayinRequestOffline {
        return impl
    }

}