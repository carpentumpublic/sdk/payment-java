//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** AvailablePayinOptionList
 *
 * The list of possible payment options.
The list always contains at least one payment option.
If no available payment options are found, an HTTP 406 response is returned with the error code.
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public class AvailablePayinOptionListImpl implements AvailablePayinOptionList {
    private final java.util.List<@NotNull AvailablePayinOption> data;

    @Override
    public java.util.List<@NotNull AvailablePayinOption> getData() {
        return data;
    }


    private final java.util.List<@NotNull AvailablePayinOptionVariantCurrency> dataVariantCurrencies;

    @Override
    public java.util.List<@NotNull AvailablePayinOptionVariantCurrency> getDataVariantCurrencies() {
        return dataVariantCurrencies;
    }




    private final int hashCode;
    private final String toString;

    private AvailablePayinOptionListImpl(BuilderImpl builder) {
        this.data = java.util.Collections.unmodifiableList(builder.data);
        this.dataVariantCurrencies = java.util.Collections.unmodifiableList(builder.dataVariantCurrencies);

        this.hashCode = Objects.hash(data, dataVariantCurrencies);
        this.toString = builder.type + "(" +
                "data=" + data +
                ", dataVariantCurrencies=" + dataVariantCurrencies +
                ')';
    }

    @Override
    public int hashCode() {
        return hashCode;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }

        if (!(obj instanceof AvailablePayinOptionListImpl)) {
            return false;
        }

        AvailablePayinOptionListImpl that = (AvailablePayinOptionListImpl) obj;
        if (!Objects.equals(this.data, that.data)) return false;
        if (!Objects.equals(this.dataVariantCurrencies, that.dataVariantCurrencies)) return false;

        return true;
    }

    @Override
    public String toString() {
        return toString;
    }

    /** Builder for {@link AvailablePayinOptionList} model class. */
    public static class BuilderImpl implements AvailablePayinOptionList.Builder {
        private java.util.List<@NotNull AvailablePayinOption> data = new java.util.ArrayList<>();
        private java.util.List<@NotNull AvailablePayinOptionVariantCurrency> dataVariantCurrencies = new java.util.ArrayList<>();

        private final String type;

        public BuilderImpl(String type) {
            this.type = type;
        }

        public BuilderImpl() {
            this("AvailablePayinOptionList");
        }

        /**
          * Replace all items in {@link AvailablePayinOptionList#getData} list property.
          *
          * 
          */
        @Override
        public BuilderImpl data(java.util.List<@NotNull AvailablePayinOption> data) {
            this.data.clear();
            if (data != null) {
                this.data.addAll(data);
            }
            return this;
        }
        /**
          * Add single item to {@link AvailablePayinOptionList#getData} list property.
          *
          * 
          */
        @Override
        public BuilderImpl dataAdd(AvailablePayinOption item) {
            if (item != null) {
                this.data.add(item);
            }
            return this;
        }
        /**
          * Add all items to {@link AvailablePayinOptionList#getData} list property.
          *
          * 
          */
        @Override
        public BuilderImpl dataAddAll(java.util.List<@NotNull AvailablePayinOption> data) {
            if (data != null) {
                this.data.addAll(data);
            }
            return this;
        }


        /**
          * Replace all items in {@link AvailablePayinOptionList#getDataVariantCurrencies} list property.
          *
          * 
          */
        @Override
        public BuilderImpl dataVariantCurrencies(java.util.List<@NotNull AvailablePayinOptionVariantCurrency> dataVariantCurrencies) {
            this.dataVariantCurrencies.clear();
            if (dataVariantCurrencies != null) {
                this.dataVariantCurrencies.addAll(dataVariantCurrencies);
            }
            return this;
        }
        /**
          * Add single item to {@link AvailablePayinOptionList#getDataVariantCurrencies} list property.
          *
          * 
          */
        @Override
        public BuilderImpl dataVariantCurrenciesAdd(AvailablePayinOptionVariantCurrency item) {
            if (item != null) {
                this.dataVariantCurrencies.add(item);
            }
            return this;
        }
        /**
          * Add all items to {@link AvailablePayinOptionList#getDataVariantCurrencies} list property.
          *
          * 
          */
        @Override
        public BuilderImpl dataVariantCurrenciesAddAll(java.util.List<@NotNull AvailablePayinOptionVariantCurrency> dataVariantCurrencies) {
            if (dataVariantCurrencies != null) {
                this.dataVariantCurrencies.addAll(dataVariantCurrencies);
            }
            return this;
        }


        /**
         * Create new instance of {@link AvailablePayinOptionList} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @Override
        public AvailablePayinOptionListImpl build() {
            return new AvailablePayinOptionListImpl(this);
        }

    }
}