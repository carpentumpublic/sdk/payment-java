//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** IdPayout
 *
 * Unique Merchant Order ID provided by merchant. See [Request IDs](general.html#request-ids).
 *
 * 
 *
 * The model class is immutable.
 *
 * Use static {@link #of} method to create a new model class instance.
 */
public interface IdPayout {


    @NotNull String getValue();

    static IdPayout of(@NotNull String value) {
        return new IdPayoutImpl(value);
    }
}