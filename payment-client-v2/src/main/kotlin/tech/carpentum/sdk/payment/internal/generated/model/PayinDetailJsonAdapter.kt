//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.PayinDetail

class PayinDetailJsonAdapter {
    @FromJson
    fun fromJson(json: PayinDetailJson): PayinDetail {
        val builder = PayinDetail.builder()
        builder.paymentRequested(json.paymentRequested)
        builder.process(json.process)
        builder.fee(json.fee)
        builder.paymentMethodResponse(json.paymentMethodResponse)
        builder.settlement(json.settlement)
        return builder.build()
    }

    @ToJson
    fun toJson(model: PayinDetail): PayinDetailJson {
        val json = PayinDetailJson()
        json.paymentRequested = model.paymentRequested
        json.process = model.process
        json.fee = model.fee
        json.paymentMethodResponse = model.paymentMethodResponse
        json.settlement = model.settlement.orElse(null)
        return json
    }

    @FromJson
    fun fromJsonImpl(model: PayinDetail): PayinDetailImpl {
        return model as PayinDetailImpl
    }

    @ToJson
    fun toJsonImpl(impl: PayinDetailImpl): PayinDetail {
        return impl
    }

}