package tech.carpentum.sdk.payment.sample

import tech.carpentum.sdk.payment.AuthTokenOperations
import tech.carpentum.sdk.payment.OutgoingPaymentsApi
import tech.carpentum.sdk.payment.OutgoingPaymentsApi.Factory.defineCreatePayoutEndpoint
import tech.carpentum.sdk.payment.OutgoingPaymentsApi.Factory.defineGetPayoutEndpoint
import tech.carpentum.sdk.payment.PaymentContext
import tech.carpentum.sdk.payment.ResponseException
import tech.carpentum.sdk.payment.model.AvailablePayoutOptionList
import tech.carpentum.sdk.payment.model.PaymentRequested
import tech.carpentum.sdk.payment.model.Payout
import tech.carpentum.sdk.payment.model.PayoutAccepted
import tech.carpentum.sdk.payment.model.PayoutDetail
import java.io.InterruptedIOException
import java.time.Duration

/**
 * Wraps all Outgoing Payments API operations for single `idPayout`.
 */
class CreatePayoutScenario private constructor(
    private val outgoingPaymentsApi: OutgoingPaymentsApi,
    private val idPayout: String,
    authToken: String
) : AbstractScenario(authToken) {

    @Throws(ResponseException::class, InterruptedIOException::class)
    fun availablePaymentOptions(paymentRequested: PaymentRequested): AvailablePayoutOptionList {
        return outgoingPaymentsApi.payoutAvailablePaymentOptions(paymentRequested)
    }

    @Throws(ResponseException::class, InterruptedIOException::class)
    fun createPayment(payout: Payout): PayoutAccepted {
        return outgoingPaymentsApi.createPayout(idPayout, payout)
    }

    @Throws(ResponseException::class, InterruptedIOException::class)
    fun getDetail(): PayoutDetail {
        return outgoingPaymentsApi.getPayout(idPayout)
    }

    companion object {
        /**
         * Creates new instance of the scenario with newly created Authorization token with specified validity.
         */
        @Throws(ResponseException::class, InterruptedIOException::class)
        fun create(
            context: PaymentContext,
            idPayout: String,
            validity: Duration? = null
        ): CreatePayoutScenario {
            val token = context.createAuthToken(
                AuthTokenOperations()
                    .grant(defineCreatePayoutEndpoint().forId(idPayout))
                    .grant(defineGetPayoutEndpoint().forId(idPayout)),
                validity
            ).token
            return CreatePayoutScenario(OutgoingPaymentsApi.create(context, token), idPayout, token)
        }

        /**
         * Creates new instance of the scenario with existing Authorization token.
         */
        @Throws(ResponseException::class, InterruptedIOException::class)
        fun create(context: PaymentContext, idPayout: String, authToken: String): CreatePayoutScenario {
            return CreatePayoutScenario(OutgoingPaymentsApi.create(context, authToken), idPayout, authToken)
        }
    }
}