package tech.carpentum.sdk.payment.internal.api

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import okhttp3.logging.HttpLoggingInterceptor.Level
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import tech.carpentum.sdk.payment.internal.generated.infrastructure.ApiClient
import tech.carpentum.sdk.payment.internal.generated.infrastructure.RequestConfig
import java.time.Duration

internal fun <T> RequestConfig<T>.addAuthorizationHeader(accessToken: String): RequestConfig<T> = apply {
    headers[ApiClient.Authorization] = "Bearer $accessToken"
}

internal fun <T> RequestConfig<T>.addBrandHeader(brand: String): RequestConfig<T> = apply {
    headers["X-brand"] = brand
}

object ApiUtils {

    private const val loggerName = "tech.carpentum.sdk.payment.api"
    private val noneLogger = LoggerFactory.getLogger(loggerName)

    private const val enableBasic = "$loggerName.basic"
    private val basicLogger = LoggerFactory.getLogger(enableBasic)
    private const val enableHeaders = "$loggerName.headers"
    private val headersLogger = LoggerFactory.getLogger(enableHeaders)
    private const val enableBody = "$loggerName.body"
    private val bodyLogger = LoggerFactory.getLogger(enableBody)

    private val loggingInterceptor: HttpLoggingInterceptor = HttpLoggingInterceptor(HttpLogger(getLogger()))

    init {
        loggingInterceptor.setLevel(getLevel())
        loggingInterceptor.redactHeader(ApiClient.Authorization)
    }

    fun getClient(callTimeout: Duration): OkHttpClient {
        val builder: OkHttpClient.Builder = OkHttpClient.Builder()
            .addInterceptor(loggingInterceptor)
            .connectTimeout(callTimeout)
            .readTimeout(callTimeout)
            .writeTimeout(callTimeout)
            .callTimeout(callTimeout)
        return builder.build()
    }

    private fun getLogger(): Logger {
        return when {
            bodyLogger.isTraceEnabled -> bodyLogger
            headersLogger.isTraceEnabled -> headersLogger
            basicLogger.isTraceEnabled -> basicLogger
            else -> noneLogger
        }
    }

    private fun getLevel(): Level {
        return when {
            bodyLogger.isTraceEnabled -> Level.BODY
            headersLogger.isTraceEnabled -> Level.HEADERS
            basicLogger.isTraceEnabled -> Level.BASIC
            else -> Level.NONE
        }
    }

    class HttpLogger(private val logger: Logger) : HttpLoggingInterceptor.Logger {
        override fun log(message: String) {
            logger.trace(message)
        }
    }

}
