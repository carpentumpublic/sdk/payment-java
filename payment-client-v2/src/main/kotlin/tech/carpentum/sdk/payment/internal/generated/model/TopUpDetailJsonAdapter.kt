//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.TopUpDetail

class TopUpDetailJsonAdapter {
    @FromJson
    fun fromJson(json: TopUpDetailJson): TopUpDetail {
        val builder = TopUpDetail.builder()
        builder.topUpRequested(json.topUpRequested)
        builder.process(json.process)
        builder.fee(json.fee)
        builder.paymentMethodResponse(json.paymentMethodResponse)
        builder.settlement(json.settlement)
        return builder.build()
    }

    @ToJson
    fun toJson(model: TopUpDetail): TopUpDetailJson {
        val json = TopUpDetailJson()
        json.topUpRequested = model.topUpRequested
        json.process = model.process
        json.fee = model.fee
        json.paymentMethodResponse = model.paymentMethodResponse
        json.settlement = model.settlement.orElse(null)
        return json
    }

    @FromJson
    fun fromJsonImpl(model: TopUpDetail): TopUpDetailImpl {
        return model as TopUpDetailImpl
    }

    @ToJson
    fun toJsonImpl(impl: TopUpDetailImpl): TopUpDetail {
        return impl
    }

}