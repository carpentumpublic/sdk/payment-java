//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** AuthTokenResponse
 *
 * Auth token for required operations. The Auth token payload can be decoded at [JWT official webiste](https://jwt.io/).
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public interface AuthTokenResponse {

    /** The JWT, its content can be [decoded at jwt.io](https://jwt.io/). See also [token content explanation](https://jwt.io/introduction).

Example content

```
{
  "operations": [
    "POST /payins/123"
  ],
  "exp": 1619518389,
  "jti": "53ee5f14-5ca5-4034-815e-815de85b96c5",
  "iat": 1619514789,
  "sub": "YOUR_MERCHANT_ID"
}
```

The token contains the following data
 - operations - allowed endpoints, see [Fine-grained privileges](general.html#fine-grained-privileges) for details.
 - exp - expiration time as number of seconds from 1970-01-01T00:00:00Z UTC
 - jti - JWT id
 - iat - issued at, time when the token was created as number of seconds from 1970-01-01T00:00:00Z UTC
 - sub - subject, the [merchant code](terminology.html#term-Merchant-code) */
    @NotNull String getToken();

    @NotNull static AuthTokenResponse ofToken(String token) { return builder().token(token).build(); }

    @NotNull static Builder builder() {
        return new AuthTokenResponseImpl.BuilderImpl();
    }

    /** Builder for {@link AuthTokenResponse} model class. */
    interface Builder {

        /**
          * Set {@link AuthTokenResponse#getToken} property.
          *
          * The JWT, its content can be [decoded at jwt.io](https://jwt.io/). See also [token content explanation](https://jwt.io/introduction).

Example content

```
{
  "operations": [
    "POST /payins/123"
  ],
  "exp": 1619518389,
  "jti": "53ee5f14-5ca5-4034-815e-815de85b96c5",
  "iat": 1619514789,
  "sub": "YOUR_MERCHANT_ID"
}
```

The token contains the following data
 - operations - allowed endpoints, see [Fine-grained privileges](general.html#fine-grained-privileges) for details.
 - exp - expiration time as number of seconds from 1970-01-01T00:00:00Z UTC
 - jti - JWT id
 - iat - issued at, time when the token was created as number of seconds from 1970-01-01T00:00:00Z UTC
 - sub - subject, the [merchant code](terminology.html#term-Merchant-code)
          */
        @NotNull Builder token(String token);

        boolean isTokenDefined();


        /**
         * Create new instance of {@link AuthTokenResponse} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @NotNull AuthTokenResponse build();

    }
}