//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** AccountResponseDetail
 *
 * 
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public class AccountResponseDetailImpl implements AccountResponseDetail {
    /** Name of the bank account. */
    private final Optional<String> accountName;

    @Override
    public Optional<String> getAccountName() {
        return accountName;
    }


    /** Number of the bank account. */
    private final Optional<String> accountNumber;

    @Override
    public Optional<String> getAccountNumber() {
        return accountNumber;
    }


    /** Type of the bank account. */
    private final Optional<String> accountType;

    @Override
    public Optional<String> getAccountType() {
        return accountType;
    }


    /** Bank code of the bank. */
    private final Optional<String> bankCode;

    @Override
    public Optional<String> getBankCode() {
        return bankCode;
    }


    private final Optional<PaymentOperatorIncoming> paymentOperator;

    @Override
    public Optional<PaymentOperatorIncoming> getPaymentOperator() {
        return paymentOperator;
    }


    /** Name of the bank. */
    private final Optional<String> bankName;

    @Override
    public Optional<String> getBankName() {
        return bankName;
    }


    /** Branch name of the bank. */
    private final Optional<String> bankBranch;

    @Override
    public Optional<String> getBankBranch() {
        return bankBranch;
    }


    /** City of the bank. */
    private final Optional<String> bankCity;

    @Override
    public Optional<String> getBankCity() {
        return bankCity;
    }


    /** Province of the bank. */
    private final Optional<String> bankProvince;

    @Override
    public Optional<String> getBankProvince() {
        return bankProvince;
    }




    private final int hashCode;
    private final String toString;

    private AccountResponseDetailImpl(BuilderImpl builder) {
        this.accountName = Optional.ofNullable(builder.accountName);
        this.accountNumber = Optional.ofNullable(builder.accountNumber);
        this.accountType = Optional.ofNullable(builder.accountType);
        this.bankCode = Optional.ofNullable(builder.bankCode);
        this.paymentOperator = Optional.ofNullable(builder.paymentOperator);
        this.bankName = Optional.ofNullable(builder.bankName);
        this.bankBranch = Optional.ofNullable(builder.bankBranch);
        this.bankCity = Optional.ofNullable(builder.bankCity);
        this.bankProvince = Optional.ofNullable(builder.bankProvince);

        this.hashCode = Objects.hash(accountName, accountNumber, accountType, bankCode, paymentOperator, bankName, bankBranch, bankCity, bankProvince);
        this.toString = builder.type + "(" +
                "accountName=" + accountName +
                ", accountNumber=" + accountNumber +
                ", accountType=" + accountType +
                ", bankCode=" + bankCode +
                ", paymentOperator=" + paymentOperator +
                ", bankName=" + bankName +
                ", bankBranch=" + bankBranch +
                ", bankCity=" + bankCity +
                ", bankProvince=" + bankProvince +
                ')';
    }

    @Override
    public int hashCode() {
        return hashCode;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }

        if (!(obj instanceof AccountResponseDetailImpl)) {
            return false;
        }

        AccountResponseDetailImpl that = (AccountResponseDetailImpl) obj;
        if (!Objects.equals(this.accountName, that.accountName)) return false;
        if (!Objects.equals(this.accountNumber, that.accountNumber)) return false;
        if (!Objects.equals(this.accountType, that.accountType)) return false;
        if (!Objects.equals(this.bankCode, that.bankCode)) return false;
        if (!Objects.equals(this.paymentOperator, that.paymentOperator)) return false;
        if (!Objects.equals(this.bankName, that.bankName)) return false;
        if (!Objects.equals(this.bankBranch, that.bankBranch)) return false;
        if (!Objects.equals(this.bankCity, that.bankCity)) return false;
        if (!Objects.equals(this.bankProvince, that.bankProvince)) return false;

        return true;
    }

    @Override
    public String toString() {
        return toString;
    }

    /** Builder for {@link AccountResponseDetail} model class. */
    public static class BuilderImpl implements AccountResponseDetail.Builder {
        private String accountName = null;
        private String accountNumber = null;
        private String accountType = null;
        private String bankCode = null;
        private PaymentOperatorIncoming paymentOperator = null;
        private String bankName = null;
        private String bankBranch = null;
        private String bankCity = null;
        private String bankProvince = null;

        private final String type;

        public BuilderImpl(String type) {
            this.type = type;
        }

        public BuilderImpl() {
            this("AccountResponseDetail");
        }

        /**
          * Set {@link AccountResponseDetail#getAccountName} property.
          *
          * Name of the bank account.
          */
        @Override
        public BuilderImpl accountName(String accountName) {
            this.accountName = accountName;
            return this;
        }

        @Override
        public boolean isAccountNameDefined() {
            return this.accountName != null;
        }

        /**
          * Set {@link AccountResponseDetail#getAccountNumber} property.
          *
          * Number of the bank account.
          */
        @Override
        public BuilderImpl accountNumber(String accountNumber) {
            this.accountNumber = accountNumber;
            return this;
        }

        @Override
        public boolean isAccountNumberDefined() {
            return this.accountNumber != null;
        }

        /**
          * Set {@link AccountResponseDetail#getAccountType} property.
          *
          * Type of the bank account.
          */
        @Override
        public BuilderImpl accountType(String accountType) {
            this.accountType = accountType;
            return this;
        }

        @Override
        public boolean isAccountTypeDefined() {
            return this.accountType != null;
        }

        /**
          * Set {@link AccountResponseDetail#getBankCode} property.
          *
          * Bank code of the bank.
          */
        @Override
        public BuilderImpl bankCode(String bankCode) {
            this.bankCode = bankCode;
            return this;
        }

        @Override
        public boolean isBankCodeDefined() {
            return this.bankCode != null;
        }

        /**
          * Set {@link AccountResponseDetail#getPaymentOperator} property.
          *
          * 
          */
        @Override
        public BuilderImpl paymentOperator(PaymentOperatorIncoming paymentOperator) {
            this.paymentOperator = paymentOperator;
            return this;
        }

        @Override
        public boolean isPaymentOperatorDefined() {
            return this.paymentOperator != null;
        }

        /**
          * Set {@link AccountResponseDetail#getBankName} property.
          *
          * Name of the bank.
          */
        @Override
        public BuilderImpl bankName(String bankName) {
            this.bankName = bankName;
            return this;
        }

        @Override
        public boolean isBankNameDefined() {
            return this.bankName != null;
        }

        /**
          * Set {@link AccountResponseDetail#getBankBranch} property.
          *
          * Branch name of the bank.
          */
        @Override
        public BuilderImpl bankBranch(String bankBranch) {
            this.bankBranch = bankBranch;
            return this;
        }

        @Override
        public boolean isBankBranchDefined() {
            return this.bankBranch != null;
        }

        /**
          * Set {@link AccountResponseDetail#getBankCity} property.
          *
          * City of the bank.
          */
        @Override
        public BuilderImpl bankCity(String bankCity) {
            this.bankCity = bankCity;
            return this;
        }

        @Override
        public boolean isBankCityDefined() {
            return this.bankCity != null;
        }

        /**
          * Set {@link AccountResponseDetail#getBankProvince} property.
          *
          * Province of the bank.
          */
        @Override
        public BuilderImpl bankProvince(String bankProvince) {
            this.bankProvince = bankProvince;
            return this;
        }

        @Override
        public boolean isBankProvinceDefined() {
            return this.bankProvince != null;
        }

        /**
         * Create new instance of {@link AccountResponseDetail} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @Override
        public AccountResponseDetailImpl build() {
            return new AccountResponseDetailImpl(this);
        }

    }
}