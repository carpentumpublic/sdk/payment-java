//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** CurrencyCode
 *
 * For supported currencies please refer to [`GET /currencies`](#operations-Payments-getCurrencies).
 *
 * 
 *
 * The model class is immutable.
 *
 * Use static {@link #of} method to create a new model class instance.
 */
@JsonClass(generateAdapter = false)
public class CurrencyCodeImpl implements CurrencyCode {
    private final @NotNull String value;

    private final int hashCode;
    private final String toString;

    public CurrencyCodeImpl(@NotNull String value) {
        this.value = Objects.requireNonNull(value, "Property 'CurrencyCode' is required.");

        this.hashCode = value.hashCode();
        this.toString = String.valueOf(value);
    }

    @Override
    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return toString;
    }

    @Override
    public int hashCode() {
        return hashCode;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }

        if (!(obj instanceof CurrencyCodeImpl)) {
            return false;
        }

        CurrencyCodeImpl that = (CurrencyCodeImpl) obj;
        if (!this.value.equals(that.value)) return false;

        return true;
    }
}