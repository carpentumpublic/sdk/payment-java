//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.JsonClass;
import tech.carpentum.sdk.payment.model.*;

@JsonClass(generateAdapter = true)
class AuthTokenRequestJson {
    var merchantCode: String? = null
    var secret: String? = null
    var validitySecs: Int? = null
    var operations: List<String>? = null
    var money: Money? = null
    var moneyProvided: Money? = null
    var currencyCodeRequired: CurrencyCode? = null
    var moneyRequired: Money? = null
    var currencyCodeProvided: CurrencyCode? = null
    var settlementMethod: SettlementMethod? = null
}