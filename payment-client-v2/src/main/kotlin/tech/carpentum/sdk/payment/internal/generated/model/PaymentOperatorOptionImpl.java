//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** PaymentOperatorOption
 *
 * 
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public class PaymentOperatorOptionImpl implements PaymentOperatorOption {
    /** One of following can serve as Payment Operator:

 * Financial or other institution (such as bank, card payment processor, ...) that manages transactions for your customers
 * Mobile wallet
 * Blockchain protocol for crypto currency payments

Customer is informed with the payment instructions where funds have to be transferred efficiently based on the selected Payment operator.
For getting list of the available payment options for payins use [POST /payins/!availablePaymentOptions](#operations-Incoming_payments-availablePaymentOptions) API, for payouts use [POST /payouts/!availablePaymentOptions](#operations-Outgoing_payments-availablePaymentOptions) API. */
    private final String code;

    @Override
    public String getCode() {
        return code;
    }


    private final Optional<String> name;

    @Override
    public Optional<String> getName() {
        return name;
    }


    /** If set to false, the payment operator is not actually available, please contact merchant support to solve this problem. */
    private final Boolean isAvailable;

    @Override
    public Boolean getIsAvailable() {
        return isAvailable;
    }


    private final IntervalNumberTo transactionAmountLimit;

    @Override
    public IntervalNumberTo getTransactionAmountLimit() {
        return transactionAmountLimit;
    }




    private final int hashCode;
    private final String toString;

    private PaymentOperatorOptionImpl(BuilderImpl builder) {
        this.code = Objects.requireNonNull(builder.code, "Property 'code' is required.");
        this.name = Optional.ofNullable(builder.name);
        this.isAvailable = Objects.requireNonNull(builder.isAvailable, "Property 'isAvailable' is required.");
        this.transactionAmountLimit = Objects.requireNonNull(builder.transactionAmountLimit, "Property 'transactionAmountLimit' is required.");

        this.hashCode = Objects.hash(code, name, isAvailable, transactionAmountLimit);
        this.toString = builder.type + "(" +
                "code=" + code +
                ", name=" + name +
                ", isAvailable=" + isAvailable +
                ", transactionAmountLimit=" + transactionAmountLimit +
                ')';
    }

    @Override
    public int hashCode() {
        return hashCode;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }

        if (!(obj instanceof PaymentOperatorOptionImpl)) {
            return false;
        }

        PaymentOperatorOptionImpl that = (PaymentOperatorOptionImpl) obj;
        if (!Objects.equals(this.code, that.code)) return false;
        if (!Objects.equals(this.name, that.name)) return false;
        if (!Objects.equals(this.isAvailable, that.isAvailable)) return false;
        if (!Objects.equals(this.transactionAmountLimit, that.transactionAmountLimit)) return false;

        return true;
    }

    @Override
    public String toString() {
        return toString;
    }

    /** Builder for {@link PaymentOperatorOption} model class. */
    public static class BuilderImpl implements PaymentOperatorOption.Builder {
        private String code = null;
        private String name = null;
        private Boolean isAvailable = null;
        private IntervalNumberTo transactionAmountLimit = null;

        private final String type;

        public BuilderImpl(String type) {
            this.type = type;
        }

        public BuilderImpl() {
            this("PaymentOperatorOption");
        }

        /**
          * Set {@link PaymentOperatorOption#getCode} property.
          *
          * One of following can serve as Payment Operator:

 * Financial or other institution (such as bank, card payment processor, ...) that manages transactions for your customers
 * Mobile wallet
 * Blockchain protocol for crypto currency payments

Customer is informed with the payment instructions where funds have to be transferred efficiently based on the selected Payment operator.
For getting list of the available payment options for payins use [POST /payins/!availablePaymentOptions](#operations-Incoming_payments-availablePaymentOptions) API, for payouts use [POST /payouts/!availablePaymentOptions](#operations-Outgoing_payments-availablePaymentOptions) API.
          */
        @Override
        public BuilderImpl code(String code) {
            this.code = code;
            return this;
        }

        @Override
        public boolean isCodeDefined() {
            return this.code != null;
        }

        /**
          * Set {@link PaymentOperatorOption#getName} property.
          *
          * 
          */
        @Override
        public BuilderImpl name(String name) {
            this.name = name;
            return this;
        }

        @Override
        public boolean isNameDefined() {
            return this.name != null;
        }

        /**
          * Set {@link PaymentOperatorOption#getIsAvailable} property.
          *
          * If set to false, the payment operator is not actually available, please contact merchant support to solve this problem.
          */
        @Override
        public BuilderImpl isAvailable(Boolean isAvailable) {
            this.isAvailable = isAvailable;
            return this;
        }

        @Override
        public boolean isIsAvailableDefined() {
            return this.isAvailable != null;
        }

        /**
          * Set {@link PaymentOperatorOption#getTransactionAmountLimit} property.
          *
          * 
          */
        @Override
        public BuilderImpl transactionAmountLimit(IntervalNumberTo transactionAmountLimit) {
            this.transactionAmountLimit = transactionAmountLimit;
            return this;
        }

        @Override
        public boolean isTransactionAmountLimitDefined() {
            return this.transactionAmountLimit != null;
        }

        /**
         * Create new instance of {@link PaymentOperatorOption} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @Override
        public PaymentOperatorOptionImpl build() {
            return new PaymentOperatorOptionImpl(this);
        }

    }
}