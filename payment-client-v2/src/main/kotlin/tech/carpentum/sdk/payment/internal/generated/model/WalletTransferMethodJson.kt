//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.JsonClass;
import tech.carpentum.sdk.payment.model.*;

@JsonClass(generateAdapter = true)
class WalletTransferMethodJson {
    var account: AccountPayoutRequestWalletTransfer? = null
    var paymentOperatorCode: String? = null
    var phoneNumber: String? = null
    var remark: String? = null
    var paymentMethodCode: String? = null
}