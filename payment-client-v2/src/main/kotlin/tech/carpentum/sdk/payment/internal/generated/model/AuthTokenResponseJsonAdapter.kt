//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.AuthTokenResponse

class AuthTokenResponseJsonAdapter {
    @FromJson
    fun fromJson(json: AuthTokenResponseJson): AuthTokenResponse {
        val builder = AuthTokenResponse.builder()
        builder.token(json.token)
        return builder.build()
    }

    @ToJson
    fun toJson(model: AuthTokenResponse): AuthTokenResponseJson {
        val json = AuthTokenResponseJson()
        json.token = model.token
        return json
    }

    @FromJson
    fun fromJsonImpl(model: AuthTokenResponse): AuthTokenResponseImpl {
        return model as AuthTokenResponseImpl
    }

    @ToJson
    fun toJsonImpl(impl: AuthTokenResponseImpl): AuthTokenResponse {
        return impl
    }

}