//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** PostPayoutsError
 *
 * Input does not meet business validations. The payment has not been accepted. See [406 Not Acceptable](responseCodes.html#term-406-Not-Acceptable)
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public class PostPayoutsError extends BusinessValidationError {
    // tag::codeEnum[]
    /** @see #getCode */
    public static final String CODE_BALANCE_INSUFFICIENT = "BALANCE_INSUFFICIENT";
    /** @see #getCode */
    public static final String CODE_BANK_CODE_REQUIRED = "BANK_CODE_REQUIRED";
    /** @see #getCode */
    public static final String CODE_CLIENT_IP_INVALID = "CLIENT_IP_INVALID";
    /** @see #getCode */
    public static final String CODE_CURRENCY_NOT_SUPPORTED = "CURRENCY_NOT_SUPPORTED";
    /** @see #getCode */
    public static final String CODE_CURRENCY_PRECISION_EXCEEDED = "CURRENCY_PRECISION_EXCEEDED";
    /** @see #getCode */
    public static final String CODE_INVALID_IFSC = "INVALID_IFSC";
    /** @see #getCode */
    public static final String CODE_IP_DENIED = "IP_DENIED";
    /** @see #getCode */
    public static final String CODE_PAYMENT_CHANNEL_AMOUNT_LIMITS = "PAYMENT_CHANNEL_AMOUNT_LIMITS";
    /** @see #getCode */
    public static final String CODE_PAYMENT_CHANNEL_DAILY_LIMITS = "PAYMENT_CHANNEL_DAILY_LIMITS";
    /** @see #getCode */
    public static final String CODE_PAYMENT_CHANNEL_NO_ACTIVE_FOUND = "PAYMENT_CHANNEL_NO_ACTIVE_FOUND";
    /** @see #getCode */
    public static final String CODE_PAYMENT_CHANNEL_NO_OPENED_FOUND = "PAYMENT_CHANNEL_NO_OPENED_FOUND";
    /** @see #getCode */
    public static final String CODE_PAYMENT_CHANNEL_NO_SEGMENT_FOUND = "PAYMENT_CHANNEL_NO_SEGMENT_FOUND";
    /** @see #getCode */
    public static final String CODE_PAYMENT_METHOD_ERROR = "PAYMENT_METHOD_ERROR";
    /** @see #getCode */
    public static final String CODE_PAYMENT_METHOD_NOT_FOUND = "PAYMENT_METHOD_NOT_FOUND";
    /** @see #getCode */
    public static final String CODE_PAYMENT_OPERATOR_INVALID = "PAYMENT_OPERATOR_INVALID";
    /** @see #getCode */
    public static final String CODE_PAYMENT_OPERATOR_NOT_FOUND = "PAYMENT_OPERATOR_NOT_FOUND";
    /** @see #getCode */
    public static final String CODE_PAYMENT_OPERATOR_REQUIRED = "PAYMENT_OPERATOR_REQUIRED";
    /** @see #getCode */
    public static final String CODE_PAYMENT_OPERATOR_UNAVAILABLE = "PAYMENT_OPERATOR_UNAVAILABLE";
    // end::codeEnum[]








    private PostPayoutsError(PostPayoutsError.Builder builder) {
        super(builder);
    }

    @NotNull public static Builder builder() {
        return new Builder();
    }

    /** Builder for {@link PostPayoutsError} model class. */
    public static class Builder extends BusinessValidationError.Builder<PostPayoutsError, Builder> {
        private Builder() {}

        /**
         * Create new instance of {@link PostPayoutsError} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @NotNull public PostPayoutsError build() {
            return new PostPayoutsError(this);
        }
    }
}