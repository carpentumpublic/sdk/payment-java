//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** PaymentInstruction
 *
 * If not empty then it specifies special instructions for different banking services your customers has to follow when entering payment in their banking service.
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public interface PaymentInstruction {

    /** It specifies your customer banking service to which companyCode belongs. e.g. ATM-MANDIRI - Mandiri Bank ATM banking service. */
    @NotNull String getBankingService();

    /** It informs your customers that companyCode has to be used in their banking service together with Virtual Account Number from accountNumber parameter e.g. 82017 + 12903437820 for particular bankingService  provided that it is specified. */
    @NotNull String getCompanyCode();

    @NotNull static Builder builder(PaymentInstruction copyOf) {
        Builder builder = builder();
        builder.bankingService(copyOf.getBankingService());
        builder.companyCode(copyOf.getCompanyCode());
        return builder;
    }

    @NotNull static Builder builder() {
        return new PaymentInstructionImpl.BuilderImpl();
    }

    /** Builder for {@link PaymentInstruction} model class. */
    interface Builder {

        /**
          * Set {@link PaymentInstruction#getBankingService} property.
          *
          * It specifies your customer banking service to which companyCode belongs. e.g. ATM-MANDIRI - Mandiri Bank ATM banking service.
          */
        @NotNull Builder bankingService(String bankingService);

        boolean isBankingServiceDefined();


        /**
          * Set {@link PaymentInstruction#getCompanyCode} property.
          *
          * It informs your customers that companyCode has to be used in their banking service together with Virtual Account Number from accountNumber parameter e.g. 82017 + 12903437820 for particular bankingService  provided that it is specified.
          */
        @NotNull Builder companyCode(String companyCode);

        boolean isCompanyCodeDefined();


        /**
         * Create new instance of {@link PaymentInstruction} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @NotNull PaymentInstruction build();

    }
}