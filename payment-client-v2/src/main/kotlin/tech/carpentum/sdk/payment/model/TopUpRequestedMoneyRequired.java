//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** TopUpRequestedMoneyRequired
 *
 * Provide the currency in which you want to pay (currencyCode) to have your account topped up with a certain amount of money (amount) in a different currency (currencyProvided). The API will return how much money you will pay in your original currency (currencyCode).
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public interface TopUpRequestedMoneyRequired extends TopUpRequested {

    @NotNull Money getMoneyRequired();

    @NotNull Optional<CurrencyCode> getCurrencyProvided();

    @NotNull static Builder builder(TopUpRequestedMoneyRequired copyOf) {
        Builder builder = builder();
        builder.moneyRequired(copyOf.getMoneyRequired());
        builder.currencyProvided(copyOf.getCurrencyProvided().orElse(null));
        return builder;
    }

    @NotNull static Builder builder() {
        return new TopUpRequestedMoneyRequiredImpl.BuilderImpl();
    }

    /** Builder for {@link TopUpRequestedMoneyRequired} model class. */
    interface Builder {

        /**
          * Set {@link TopUpRequestedMoneyRequired#getMoneyRequired} property.
          *
          * 
          */
        @NotNull Builder moneyRequired(Money moneyRequired);

        boolean isMoneyRequiredDefined();


        /**
          * Set {@link TopUpRequestedMoneyRequired#getCurrencyProvided} property.
          *
          * 
          */
        @NotNull Builder currencyProvided(CurrencyCode currencyProvided);

        boolean isCurrencyProvidedDefined();


        /**
         * Create new instance of {@link TopUpRequestedMoneyRequired} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @NotNull TopUpRequestedMoneyRequired build();

    }
}