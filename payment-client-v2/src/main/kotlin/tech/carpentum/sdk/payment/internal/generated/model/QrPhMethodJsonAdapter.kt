//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.QrPhMethod

class QrPhMethodJsonAdapter {
    @FromJson
    fun fromJson(json: QrPhMethodJson): QrPhMethod {
        val builder = QrPhMethod.builder()
        builder.account(json.account)
        builder.emailAddress(json.emailAddress)
        builder.paymentOperatorCode(json.paymentOperatorCode)
        builder.phoneNumber(json.phoneNumber)
        return builder.build()
    }

    @ToJson
    fun toJson(model: QrPhMethod): QrPhMethodJson {
        val json = QrPhMethodJson()
        json.account = model.account.orElse(null)
        json.emailAddress = model.emailAddress.orElse(null)
        json.paymentOperatorCode = model.paymentOperatorCode.orElse(null)
        json.phoneNumber = model.phoneNumber.orElse(null)
        json.paymentMethodCode = model.paymentMethodCode.name
        return json
    }

    @FromJson
    fun fromJsonImpl(model: QrPhMethod): QrPhMethodImpl {
        return model as QrPhMethodImpl
    }

    @ToJson
    fun toJsonImpl(impl: QrPhMethodImpl): QrPhMethod {
        return impl
    }

}