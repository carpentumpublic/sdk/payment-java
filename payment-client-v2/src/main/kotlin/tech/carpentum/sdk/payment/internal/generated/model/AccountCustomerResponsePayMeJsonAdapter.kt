//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.AccountCustomerResponsePayMe

class AccountCustomerResponsePayMeJsonAdapter {
    @FromJson
    fun fromJson(json: AccountCustomerResponsePayMeJson): AccountCustomerResponsePayMe {
        val builder = AccountCustomerResponsePayMe.builder()
        builder.accountName(json.accountName)
        builder.accountNumber(json.accountNumber)
        return builder.build()
    }

    @ToJson
    fun toJson(model: AccountCustomerResponsePayMe): AccountCustomerResponsePayMeJson {
        val json = AccountCustomerResponsePayMeJson()
        json.accountName = model.accountName
        json.accountNumber = model.accountNumber.orElse(null)
        return json
    }

    @FromJson
    fun fromJsonImpl(model: AccountCustomerResponsePayMe): AccountCustomerResponsePayMeImpl {
        return model as AccountCustomerResponsePayMeImpl
    }

    @ToJson
    fun toJsonImpl(impl: AccountCustomerResponsePayMeImpl): AccountCustomerResponsePayMe {
        return impl
    }

}