//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** PAYSHAP
 *
 * PayShap offers a quick, easy, and secure way to send money without having to input banking login details.

Customers can make payments using just a phone number or account number, with transactions processed instantly. 
It is a convenient solution for everyday payments, designed to simplify and speed up the payment experience.
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public interface PayShapMethod extends PayinMethod {
    /** A discriminator value of property {@link #getPaymentMethodCode}. The model class extends {@link PayinMethod}. */
    PayinMethod.PaymentMethodCode PAYMENT_METHOD_CODE = PayinMethod.PaymentMethodCode.PAYSHAP;

    @NotNull Optional<AccountPayinRequestPayShap> getAccount();

    /** Your customer mobile phone number. */
    @NotNull Optional<String> getPhoneNumber();

    /** One of following can serve as Payment Operator:

 * Financial or other institution (such as bank, card payment processor, ...) that manages transactions for your customers
 * Mobile wallet
 * Blockchain protocol for crypto currency payments

Customer is informed with the payment instructions where funds have to be transferred efficiently based on the selected Payment operator.
For getting list of the available payment options for payins use [POST /payins/!availablePaymentOptions](#operations-Incoming_payments-availablePaymentOptions) API, for payouts use [POST /payouts/!availablePaymentOptions](#operations-Outgoing_payments-availablePaymentOptions) API. */
    @NotNull String getPaymentOperatorCode();

    @NotNull Optional<String> getExternalProviderCode();

    @NotNull static Builder builder(PayShapMethod copyOf) {
        Builder builder = builder();
        builder.account(copyOf.getAccount().orElse(null));
        builder.phoneNumber(copyOf.getPhoneNumber().orElse(null));
        builder.paymentOperatorCode(copyOf.getPaymentOperatorCode());
        builder.externalProviderCode(copyOf.getExternalProviderCode().orElse(null));
        return builder;
    }

    @NotNull static Builder builder() {
        return new PayShapMethodImpl.BuilderImpl();
    }

    /** Builder for {@link PayShapMethod} model class. */
    interface Builder {

        /**
          * Set {@link PayShapMethod#getAccount} property.
          *
          * 
          */
        @NotNull Builder account(AccountPayinRequestPayShap account);

        boolean isAccountDefined();


        /**
          * Set {@link PayShapMethod#getPhoneNumber} property.
          *
          * Your customer mobile phone number.
          */
        @NotNull Builder phoneNumber(String phoneNumber);

        boolean isPhoneNumberDefined();


        /**
          * Set {@link PayShapMethod#getPaymentOperatorCode} property.
          *
          * One of following can serve as Payment Operator:

 * Financial or other institution (such as bank, card payment processor, ...) that manages transactions for your customers
 * Mobile wallet
 * Blockchain protocol for crypto currency payments

Customer is informed with the payment instructions where funds have to be transferred efficiently based on the selected Payment operator.
For getting list of the available payment options for payins use [POST /payins/!availablePaymentOptions](#operations-Incoming_payments-availablePaymentOptions) API, for payouts use [POST /payouts/!availablePaymentOptions](#operations-Outgoing_payments-availablePaymentOptions) API.
          */
        @NotNull Builder paymentOperatorCode(String paymentOperatorCode);

        boolean isPaymentOperatorCodeDefined();


        /**
          * Set {@link PayShapMethod#getExternalProviderCode} property.
          *
          * 
          */
        @NotNull Builder externalProviderCode(String externalProviderCode);

        boolean isExternalProviderCodeDefined();


        /**
         * Create new instance of {@link PayShapMethod} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @NotNull PayShapMethod build();

    }
}