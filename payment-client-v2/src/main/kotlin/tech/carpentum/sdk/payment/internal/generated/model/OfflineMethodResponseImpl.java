//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** OFFLINE
 *
 * The account parameters for this payment method are used to show payment instructions to the customer.
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public class OfflineMethodResponseImpl implements OfflineMethodResponse {
    private final IdPayin idPayin;

    @Override
    public IdPayin getIdPayin() {
        return idPayin;
    }


    private final IdPayment idPayment;

    @Override
    public IdPayment getIdPayment() {
        return idPayment;
    }


    private final AccountResponseOffline account;

    @Override
    public AccountResponseOffline getAccount() {
        return account;
    }


    private final AccountCustomerResponseOffline accountCustomer;

    @Override
    public AccountCustomerResponseOffline getAccountCustomer() {
        return accountCustomer;
    }


    private final MoneyPaymentResponse money;

    @Override
    public MoneyPaymentResponse getMoney() {
        return money;
    }


    private final Optional<MoneyRequired> moneyRequired;

    @Override
    public Optional<MoneyRequired> getMoneyRequired() {
        return moneyRequired;
    }


    private final Optional<MoneyVat> vat;

    @Override
    public Optional<MoneyVat> getVat() {
        return vat;
    }


    /** Reference number of transaction. */
    private final String reference;

    @Override
    public String getReference() {
        return reference;
    }


    private final Optional<PaymentProcessor> processor;

    @Override
    public Optional<PaymentProcessor> getProcessor() {
        return processor;
    }


    /** The name of the QR code image to be scanned by a wallet or payment service compatible with this payment method. The QR code of the image can be labeled by qrName to increase the clarity of the payment instruction.
If this parameter contains any value, include it in the payment instructions for your customer. */
    private final Optional<String> qrName;

    @Override
    public Optional<String> getQrName() {
        return qrName;
    }


    /** The URL of the QR code image to be scanned by a wallet or payment service compatible with this payment method. The QR code encodes the instructions how make a payment.
If this parameter contains any value, include it in the payment instructions for your customer.

This parameter is returned for the LAK and VND currency. */
    private final Optional<String> qrCode;

    @Override
    public Optional<String> getQrCode() {
        return qrCode;
    }


    /** This is the URL where the customers will be redirected after completing a payment.

The URL must be either IP or domain-based. */
    private final String returnUrl;

    @Override
    public String getReturnUrl() {
        return returnUrl;
    }


    private final Optional<SelectedPaymentOperatorIncoming> paymentOperator;

    @Override
    public Optional<SelectedPaymentOperatorIncoming> getPaymentOperator() {
        return paymentOperator;
    }


    /** Identification of a product related to a payment order. This field is returned only if it is provided in a payment order request. */
    private final Optional<String> productId;

    @Override
    public Optional<String> getProductId() {
        return productId;
    }


    /** Date and time when payment was accepted. */
    private final java.time.OffsetDateTime acceptedAt;

    @Override
    public java.time.OffsetDateTime getAcceptedAt() {
        return acceptedAt;
    }


    /** Date and time of payment expiration. If no money has been transferred to this time, payment is considered failed and callback with status change event will shortly follow. */
    private final java.time.OffsetDateTime expireAt;

    @Override
    public java.time.OffsetDateTime getExpireAt() {
        return expireAt;
    }


    @Override public PaymentMethodCode getPaymentMethodCode() { return PAYMENT_METHOD_CODE; }

    private final int hashCode;
    private final String toString;

    private OfflineMethodResponseImpl(BuilderImpl builder) {
        this.idPayin = Objects.requireNonNull(builder.idPayin, "Property 'idPayin' is required.");
        this.idPayment = Objects.requireNonNull(builder.idPayment, "Property 'idPayment' is required.");
        this.account = Objects.requireNonNull(builder.account, "Property 'account' is required.");
        this.accountCustomer = Objects.requireNonNull(builder.accountCustomer, "Property 'accountCustomer' is required.");
        this.money = Objects.requireNonNull(builder.money, "Property 'money' is required.");
        this.moneyRequired = Optional.ofNullable(builder.moneyRequired);
        this.vat = Optional.ofNullable(builder.vat);
        this.reference = Objects.requireNonNull(builder.reference, "Property 'reference' is required.");
        this.processor = Optional.ofNullable(builder.processor);
        this.qrName = Optional.ofNullable(builder.qrName);
        this.qrCode = Optional.ofNullable(builder.qrCode);
        this.returnUrl = Objects.requireNonNull(builder.returnUrl, "Property 'returnUrl' is required.");
        this.paymentOperator = Optional.ofNullable(builder.paymentOperator);
        this.productId = Optional.ofNullable(builder.productId);
        this.acceptedAt = Objects.requireNonNull(builder.acceptedAt, "Property 'acceptedAt' is required.");
        this.expireAt = Objects.requireNonNull(builder.expireAt, "Property 'expireAt' is required.");

        this.hashCode = Objects.hash(idPayin, idPayment, account, accountCustomer, money, moneyRequired, vat, reference, processor, qrName, qrCode, returnUrl, paymentOperator, productId, acceptedAt, expireAt);
        this.toString = builder.type + "(" +
                "idPayin=" + idPayin +
                ", idPayment=" + idPayment +
                ", account=" + account +
                ", accountCustomer=" + accountCustomer +
                ", money=" + money +
                ", moneyRequired=" + moneyRequired +
                ", vat=" + vat +
                ", reference=" + reference +
                ", processor=" + processor +
                ", qrName=" + qrName +
                ", qrCode=" + qrCode +
                ", returnUrl=" + returnUrl +
                ", paymentOperator=" + paymentOperator +
                ", productId=" + productId +
                ", acceptedAt=" + acceptedAt +
                ", expireAt=" + expireAt +
                ')';
    }

    @Override
    public int hashCode() {
        return hashCode;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }

        if (!(obj instanceof OfflineMethodResponseImpl)) {
            return false;
        }

        OfflineMethodResponseImpl that = (OfflineMethodResponseImpl) obj;
        if (!Objects.equals(this.idPayin, that.idPayin)) return false;
        if (!Objects.equals(this.idPayment, that.idPayment)) return false;
        if (!Objects.equals(this.account, that.account)) return false;
        if (!Objects.equals(this.accountCustomer, that.accountCustomer)) return false;
        if (!Objects.equals(this.money, that.money)) return false;
        if (!Objects.equals(this.moneyRequired, that.moneyRequired)) return false;
        if (!Objects.equals(this.vat, that.vat)) return false;
        if (!Objects.equals(this.reference, that.reference)) return false;
        if (!Objects.equals(this.processor, that.processor)) return false;
        if (!Objects.equals(this.qrName, that.qrName)) return false;
        if (!Objects.equals(this.qrCode, that.qrCode)) return false;
        if (!Objects.equals(this.returnUrl, that.returnUrl)) return false;
        if (!Objects.equals(this.paymentOperator, that.paymentOperator)) return false;
        if (!Objects.equals(this.productId, that.productId)) return false;
        if (!Objects.equals(this.acceptedAt, that.acceptedAt)) return false;
        if (!Objects.equals(this.expireAt, that.expireAt)) return false;

        return true;
    }

    @Override
    public String toString() {
        return toString;
    }

    /** Builder for {@link OfflineMethodResponse} model class. */
    public static class BuilderImpl implements OfflineMethodResponse.Builder {
        private IdPayin idPayin = null;
        private IdPayment idPayment = null;
        private AccountResponseOffline account = null;
        private AccountCustomerResponseOffline accountCustomer = null;
        private MoneyPaymentResponse money = null;
        private MoneyRequired moneyRequired = null;
        private MoneyVat vat = null;
        private String reference = null;
        private PaymentProcessor processor = null;
        private String qrName = null;
        private String qrCode = null;
        private String returnUrl = null;
        private SelectedPaymentOperatorIncoming paymentOperator = null;
        private String productId = null;
        private java.time.OffsetDateTime acceptedAt = null;
        private java.time.OffsetDateTime expireAt = null;

        private final String type;

        public BuilderImpl(String type) {
            this.type = type;
        }

        public BuilderImpl() {
            this("OfflineMethodResponse");
        }

        /**
          * Set {@link OfflineMethodResponse#getIdPayin} property.
          *
          * 
          */
        @Override
        public BuilderImpl idPayin(IdPayin idPayin) {
            this.idPayin = idPayin;
            return this;
        }

        @Override
        public boolean isIdPayinDefined() {
            return this.idPayin != null;
        }

        /**
          * Set {@link OfflineMethodResponse#getIdPayment} property.
          *
          * 
          */
        @Override
        public BuilderImpl idPayment(IdPayment idPayment) {
            this.idPayment = idPayment;
            return this;
        }

        @Override
        public boolean isIdPaymentDefined() {
            return this.idPayment != null;
        }

        /**
          * Set {@link OfflineMethodResponse#getAccount} property.
          *
          * 
          */
        @Override
        public BuilderImpl account(AccountResponseOffline account) {
            this.account = account;
            return this;
        }

        @Override
        public boolean isAccountDefined() {
            return this.account != null;
        }

        /**
          * Set {@link OfflineMethodResponse#getAccountCustomer} property.
          *
          * 
          */
        @Override
        public BuilderImpl accountCustomer(AccountCustomerResponseOffline accountCustomer) {
            this.accountCustomer = accountCustomer;
            return this;
        }

        @Override
        public boolean isAccountCustomerDefined() {
            return this.accountCustomer != null;
        }

        /**
          * Set {@link OfflineMethodResponse#getMoney} property.
          *
          * 
          */
        @Override
        public BuilderImpl money(MoneyPaymentResponse money) {
            this.money = money;
            return this;
        }

        @Override
        public boolean isMoneyDefined() {
            return this.money != null;
        }

        /**
          * Set {@link OfflineMethodResponse#getMoneyRequired} property.
          *
          * 
          */
        @Override
        public BuilderImpl moneyRequired(MoneyRequired moneyRequired) {
            this.moneyRequired = moneyRequired;
            return this;
        }

        @Override
        public boolean isMoneyRequiredDefined() {
            return this.moneyRequired != null;
        }

        /**
          * Set {@link OfflineMethodResponse#getVat} property.
          *
          * 
          */
        @Override
        public BuilderImpl vat(MoneyVat vat) {
            this.vat = vat;
            return this;
        }

        @Override
        public boolean isVatDefined() {
            return this.vat != null;
        }

        /**
          * Set {@link OfflineMethodResponse#getReference} property.
          *
          * Reference number of transaction.
          */
        @Override
        public BuilderImpl reference(String reference) {
            this.reference = reference;
            return this;
        }

        @Override
        public boolean isReferenceDefined() {
            return this.reference != null;
        }

        /**
          * Set {@link OfflineMethodResponse#getProcessor} property.
          *
          * 
          */
        @Override
        public BuilderImpl processor(PaymentProcessor processor) {
            this.processor = processor;
            return this;
        }

        @Override
        public boolean isProcessorDefined() {
            return this.processor != null;
        }

        /**
          * Set {@link OfflineMethodResponse#getQrName} property.
          *
          * The name of the QR code image to be scanned by a wallet or payment service compatible with this payment method. The QR code of the image can be labeled by qrName to increase the clarity of the payment instruction.
If this parameter contains any value, include it in the payment instructions for your customer.
          */
        @Override
        public BuilderImpl qrName(String qrName) {
            this.qrName = qrName;
            return this;
        }

        @Override
        public boolean isQrNameDefined() {
            return this.qrName != null;
        }

        /**
          * Set {@link OfflineMethodResponse#getQrCode} property.
          *
          * The URL of the QR code image to be scanned by a wallet or payment service compatible with this payment method. The QR code encodes the instructions how make a payment.
If this parameter contains any value, include it in the payment instructions for your customer.

This parameter is returned for the LAK and VND currency.
          */
        @Override
        public BuilderImpl qrCode(String qrCode) {
            this.qrCode = qrCode;
            return this;
        }

        @Override
        public boolean isQrCodeDefined() {
            return this.qrCode != null;
        }

        /**
          * Set {@link OfflineMethodResponse#getReturnUrl} property.
          *
          * This is the URL where the customers will be redirected after completing a payment.

The URL must be either IP or domain-based.
          */
        @Override
        public BuilderImpl returnUrl(String returnUrl) {
            this.returnUrl = returnUrl;
            return this;
        }

        @Override
        public boolean isReturnUrlDefined() {
            return this.returnUrl != null;
        }

        /**
          * Set {@link OfflineMethodResponse#getPaymentOperator} property.
          *
          * 
          */
        @Override
        public BuilderImpl paymentOperator(SelectedPaymentOperatorIncoming paymentOperator) {
            this.paymentOperator = paymentOperator;
            return this;
        }

        @Override
        public boolean isPaymentOperatorDefined() {
            return this.paymentOperator != null;
        }

        /**
          * Set {@link OfflineMethodResponse#getProductId} property.
          *
          * Identification of a product related to a payment order. This field is returned only if it is provided in a payment order request.
          */
        @Override
        public BuilderImpl productId(String productId) {
            this.productId = productId;
            return this;
        }

        @Override
        public boolean isProductIdDefined() {
            return this.productId != null;
        }

        /**
          * Set {@link OfflineMethodResponse#getAcceptedAt} property.
          *
          * Date and time when payment was accepted.
          */
        @Override
        public BuilderImpl acceptedAt(java.time.OffsetDateTime acceptedAt) {
            this.acceptedAt = acceptedAt;
            return this;
        }

        @Override
        public boolean isAcceptedAtDefined() {
            return this.acceptedAt != null;
        }

        /**
          * Set {@link OfflineMethodResponse#getExpireAt} property.
          *
          * Date and time of payment expiration. If no money has been transferred to this time, payment is considered failed and callback with status change event will shortly follow.
          */
        @Override
        public BuilderImpl expireAt(java.time.OffsetDateTime expireAt) {
            this.expireAt = expireAt;
            return this;
        }

        @Override
        public boolean isExpireAtDefined() {
            return this.expireAt != null;
        }

        /**
         * Create new instance of {@link OfflineMethodResponse} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @Override
        public OfflineMethodResponseImpl build() {
            return new OfflineMethodResponseImpl(this);
        }

    }
}