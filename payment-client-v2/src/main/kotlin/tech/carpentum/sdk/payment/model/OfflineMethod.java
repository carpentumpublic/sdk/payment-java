//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** OFFLINE
 *
 * Offline Bank Transfer. Payment method which requires customer to copy Payment instruction from the Payment application right after the incoming payment is submitted and create the Payment transfer using the instructions within customer's own payment service such as Internet or mobile banking or wallet.

As the Payment relies on the customer offline Payment transfer processing it can take minutes for Payment to be confirmed and also number of Payment expiration ratio can be higher.
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public interface OfflineMethod extends PayinMethod {
    /** A discriminator value of property {@link #getPaymentMethodCode}. The model class extends {@link PayinMethod}. */
    PayinMethod.PaymentMethodCode PAYMENT_METHOD_CODE = PayinMethod.PaymentMethodCode.OFFLINE;

    @NotNull Optional<AccountPayinRequestOffline> getAccount();

    /** One of following can serve as Payment Operator:

 * Financial or other institution (such as bank, card payment processor, ...) that manages transactions for your customers
 * Mobile wallet
 * Blockchain protocol for crypto currency payments

Customer is informed with the payment instructions where funds have to be transferred efficiently based on the selected Payment operator.
For getting list of the available payment options for payins use [POST /payins/!availablePaymentOptions](#operations-Incoming_payments-availablePaymentOptions) API, for payouts use [POST /payouts/!availablePaymentOptions](#operations-Outgoing_payments-availablePaymentOptions) API. */
    @NotNull Optional<String> getPaymentOperatorCode();

    /** Your customer e-mail address in RFC 5322 format that is used for identification of the customer's payins.

If currency is JPY, then emailAddress field is required. Otherwise, it is optional. */
    @NotNull Optional<String> getEmailAddress();

    /** Your customer mobile phone number in full international telephone number format, including country code. */
    @NotNull Optional<String> getPhoneNumber();

    /** Identification of a product related to a payment order. This field is returned only if it is provided in a payment order request. */
    @NotNull Optional<String> getProductId();

    @NotNull static Builder builder(OfflineMethod copyOf) {
        Builder builder = builder();
        builder.account(copyOf.getAccount().orElse(null));
        builder.paymentOperatorCode(copyOf.getPaymentOperatorCode().orElse(null));
        builder.emailAddress(copyOf.getEmailAddress().orElse(null));
        builder.phoneNumber(copyOf.getPhoneNumber().orElse(null));
        builder.productId(copyOf.getProductId().orElse(null));
        return builder;
    }

    @NotNull static Builder builder() {
        return new OfflineMethodImpl.BuilderImpl();
    }

    /** Builder for {@link OfflineMethod} model class. */
    interface Builder {

        /**
          * Set {@link OfflineMethod#getAccount} property.
          *
          * 
          */
        @NotNull Builder account(AccountPayinRequestOffline account);

        boolean isAccountDefined();


        /**
          * Set {@link OfflineMethod#getPaymentOperatorCode} property.
          *
          * One of following can serve as Payment Operator:

 * Financial or other institution (such as bank, card payment processor, ...) that manages transactions for your customers
 * Mobile wallet
 * Blockchain protocol for crypto currency payments

Customer is informed with the payment instructions where funds have to be transferred efficiently based on the selected Payment operator.
For getting list of the available payment options for payins use [POST /payins/!availablePaymentOptions](#operations-Incoming_payments-availablePaymentOptions) API, for payouts use [POST /payouts/!availablePaymentOptions](#operations-Outgoing_payments-availablePaymentOptions) API.
          */
        @NotNull Builder paymentOperatorCode(String paymentOperatorCode);

        boolean isPaymentOperatorCodeDefined();


        /**
          * Set {@link OfflineMethod#getEmailAddress} property.
          *
          * Your customer e-mail address in RFC 5322 format that is used for identification of the customer's payins.

If currency is JPY, then emailAddress field is required. Otherwise, it is optional.
          */
        @NotNull Builder emailAddress(String emailAddress);

        boolean isEmailAddressDefined();


        /**
          * Set {@link OfflineMethod#getPhoneNumber} property.
          *
          * Your customer mobile phone number in full international telephone number format, including country code.
          */
        @NotNull Builder phoneNumber(String phoneNumber);

        boolean isPhoneNumberDefined();


        /**
          * Set {@link OfflineMethod#getProductId} property.
          *
          * Identification of a product related to a payment order. This field is returned only if it is provided in a payment order request.
          */
        @NotNull Builder productId(String productId);

        boolean isProductIdDefined();


        /**
         * Create new instance of {@link OfflineMethod} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @NotNull OfflineMethod build();

    }
}