//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** ExternalProvider
 *
 * TODO: Add description
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public interface ExternalProvider {

    @NotNull String getCode();

    @NotNull String getName();

    @NotNull static Builder builder(ExternalProvider copyOf) {
        Builder builder = builder();
        builder.code(copyOf.getCode());
        builder.name(copyOf.getName());
        return builder;
    }

    @NotNull static Builder builder() {
        return new ExternalProviderImpl.BuilderImpl();
    }

    /** Builder for {@link ExternalProvider} model class. */
    interface Builder {

        /**
          * Set {@link ExternalProvider#getCode} property.
          *
          * 
          */
        @NotNull Builder code(String code);

        boolean isCodeDefined();


        /**
          * Set {@link ExternalProvider#getName} property.
          *
          * 
          */
        @NotNull Builder name(String name);

        boolean isNameDefined();


        /**
         * Create new instance of {@link ExternalProvider} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @NotNull ExternalProvider build();

    }
}