//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.MobileMoneyMethod

class MobileMoneyMethodJsonAdapter {
    @FromJson
    fun fromJson(json: MobileMoneyMethodJson): MobileMoneyMethod {
        val builder = MobileMoneyMethod.builder()
        builder.account(json.account)
        builder.emailAddress(json.emailAddress)
        builder.phoneNumber(json.phoneNumber)
        return builder.build()
    }

    @ToJson
    fun toJson(model: MobileMoneyMethod): MobileMoneyMethodJson {
        val json = MobileMoneyMethodJson()
        json.account = model.account
        json.emailAddress = model.emailAddress.orElse(null)
        json.phoneNumber = model.phoneNumber
        json.paymentMethodCode = model.paymentMethodCode.name
        return json
    }

    @FromJson
    fun fromJsonImpl(model: MobileMoneyMethod): MobileMoneyMethodImpl {
        return model as MobileMoneyMethodImpl
    }

    @ToJson
    fun toJsonImpl(impl: MobileMoneyMethodImpl): MobileMoneyMethod {
        return impl
    }

}