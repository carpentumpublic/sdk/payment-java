//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.AccountPayinRequestPayMe

class AccountPayinRequestPayMeJsonAdapter {
    @FromJson
    fun fromJson(json: AccountPayinRequestPayMeJson): AccountPayinRequestPayMe {
        val builder = AccountPayinRequestPayMe.builder()
        builder.accountName(json.accountName)
        builder.accountNumber(json.accountNumber)
        return builder.build()
    }

    @ToJson
    fun toJson(model: AccountPayinRequestPayMe): AccountPayinRequestPayMeJson {
        val json = AccountPayinRequestPayMeJson()
        json.accountName = model.accountName
        json.accountNumber = model.accountNumber.orElse(null)
        return json
    }

    @FromJson
    fun fromJsonImpl(model: AccountPayinRequestPayMe): AccountPayinRequestPayMeImpl {
        return model as AccountPayinRequestPayMeImpl
    }

    @ToJson
    fun toJsonImpl(impl: AccountPayinRequestPayMeImpl): AccountPayinRequestPayMe {
        return impl
    }

}