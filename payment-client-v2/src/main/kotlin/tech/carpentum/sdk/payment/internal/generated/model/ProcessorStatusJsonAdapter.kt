//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.ProcessorStatus

class ProcessorStatusJsonAdapter {

    @FromJson
    fun fromJson(json: String): ProcessorStatus {
        return ProcessorStatus.of(json)
    }

    @ToJson
    fun toJson(model: ProcessorStatus): String {
        return model.value
    }

    @FromJson
    fun fromJsonImpl(model: ProcessorStatus): ProcessorStatusImpl {
        return model as ProcessorStatusImpl
    }

    @ToJson
    fun toJsonImpl(impl: ProcessorStatusImpl): ProcessorStatus {
        return impl
    }

}