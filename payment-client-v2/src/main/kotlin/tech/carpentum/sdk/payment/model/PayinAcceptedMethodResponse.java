//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** MethodResponse
 *
 * For every payment method there is appropriate payment specific response object in `paymentMethodResponse` attribute.

Use data from `paymentMethodResponse` for payment completion (for example show to the customer).
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public interface PayinAcceptedMethodResponse extends PayinAcceptedResponse {

    @NotNull PaymentRequested getPaymentRequested();

    @NotNull PayinMethodResponse getPaymentMethodResponse();

    @NotNull static Builder builder(PayinAcceptedMethodResponse copyOf) {
        Builder builder = builder();
        builder.paymentRequested(copyOf.getPaymentRequested());
        builder.paymentMethodResponse(copyOf.getPaymentMethodResponse());
        return builder;
    }

    @NotNull static Builder builder() {
        return new PayinAcceptedMethodResponseImpl.BuilderImpl();
    }

    /** Builder for {@link PayinAcceptedMethodResponse} model class. */
    interface Builder {

        /**
          * Set {@link PayinAcceptedMethodResponse#getPaymentRequested} property.
          *
          * 
          */
        @NotNull Builder paymentRequested(PaymentRequested paymentRequested);

        boolean isPaymentRequestedDefined();


        /**
          * Set {@link PayinAcceptedMethodResponse#getPaymentMethodResponse} property.
          *
          * 
          */
        @NotNull Builder paymentMethodResponse(PayinMethodResponse paymentMethodResponse);

        boolean isPaymentMethodResponseDefined();


        /**
         * Create new instance of {@link PayinAcceptedMethodResponse} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @NotNull PayinAcceptedMethodResponse build();

    }
}