//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** CustomerTransactionFee
 *
 * Additional fee for customer not included in transaction amount. E.g. gas fee for crypto transactions.
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public interface CustomerTransactionFee {

    @NotNull Optional<Money> getMinFee();

    @NotNull Optional<Money> getMaxFee();

    @NotNull static Builder builder(CustomerTransactionFee copyOf) {
        Builder builder = builder();
        builder.minFee(copyOf.getMinFee().orElse(null));
        builder.maxFee(copyOf.getMaxFee().orElse(null));
        return builder;
    }

    @NotNull static Builder builder() {
        return new CustomerTransactionFeeImpl.BuilderImpl();
    }

    /** Builder for {@link CustomerTransactionFee} model class. */
    interface Builder {

        /**
          * Set {@link CustomerTransactionFee#getMinFee} property.
          *
          * 
          */
        @NotNull Builder minFee(Money minFee);

        boolean isMinFeeDefined();


        /**
          * Set {@link CustomerTransactionFee#getMaxFee} property.
          *
          * 
          */
        @NotNull Builder maxFee(Money maxFee);

        boolean isMaxFeeDefined();


        /**
         * Create new instance of {@link CustomerTransactionFee} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @NotNull CustomerTransactionFee build();

    }
}