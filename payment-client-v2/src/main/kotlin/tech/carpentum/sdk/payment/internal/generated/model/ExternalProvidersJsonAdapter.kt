//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.ExternalProviders

class ExternalProvidersJsonAdapter {
    @FromJson
    fun fromJson(json: ExternalProvidersJson): ExternalProviders {
        val builder = ExternalProviders.builder()
        builder.providers(json.providers?.toList())
        return builder.build()
    }

    @ToJson
    fun toJson(model: ExternalProviders): ExternalProvidersJson {
        val json = ExternalProvidersJson()
        json.providers = model.providers.ifEmpty { null }
        return json
    }

    @FromJson
    fun fromJsonImpl(model: ExternalProviders): ExternalProvidersImpl {
        return model as ExternalProvidersImpl
    }

    @ToJson
    fun toJsonImpl(impl: ExternalProvidersImpl): ExternalProviders {
        return impl
    }

}