//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.AvailablePayinOption

class AvailablePayinOptionJsonAdapter {
    @FromJson
    fun fromJson(json: AvailablePayinOptionJson): AvailablePayinOption {
        val builder = AvailablePayinOption.builder()
        builder.paymentMethodCode(json.paymentMethodCode)
        builder.paymentOperators(json.paymentOperators?.toList())
        builder.segmentCode(json.segmentCode)
        return builder.build()
    }

    @ToJson
    fun toJson(model: AvailablePayinOption): AvailablePayinOptionJson {
        val json = AvailablePayinOptionJson()
        json.paymentMethodCode = model.paymentMethodCode
        json.paymentOperators = model.paymentOperators.ifEmpty { null }
        json.segmentCode = model.segmentCode.orElse(null)
        return json
    }

    @FromJson
    fun fromJsonImpl(model: AvailablePayinOption): AvailablePayinOptionImpl {
        return model as AvailablePayinOptionImpl
    }

    @ToJson
    fun toJsonImpl(impl: AvailablePayinOptionImpl): AvailablePayinOption {
        return impl
    }

}