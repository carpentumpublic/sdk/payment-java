package tech.carpentum.sdk.payment.sample

import tech.carpentum.sdk.payment.ClientErrorException
import tech.carpentum.sdk.payment.GetPaymentOptionsErrorException
import tech.carpentum.sdk.payment.PaymentContext
import tech.carpentum.sdk.payment.PaymentsApi.ListPaymentOptionsQuery.Factory.currencyCodes
import tech.carpentum.sdk.payment.PaymentsApi.ListPaymentOptionsQuery.Factory.paymentMethodCodes
import tech.carpentum.sdk.payment.PaymentsApi.ListPaymentOptionsQuery.Factory.paymentTypeCode
import tech.carpentum.sdk.payment.PaymentsApi.ListPaymentOptionsQuery.Factory.segmentCodes
import tech.carpentum.sdk.payment.PaymentsApi.ListPaymentOptionsQuery.PaymentTypeCode
import tech.carpentum.sdk.payment.model.AccountPayinRequestVaPay
import tech.carpentum.sdk.payment.model.AccountPayoutRequestBankTransfer
import tech.carpentum.sdk.payment.model.BankTransferMethod
import tech.carpentum.sdk.payment.model.CurrencyCode
import tech.carpentum.sdk.payment.model.GetPaymentOptionsError
import tech.carpentum.sdk.payment.model.MoneyPaymentRequested
import tech.carpentum.sdk.payment.model.Payin
import tech.carpentum.sdk.payment.model.PayinMethod.PaymentMethodCode
import tech.carpentum.sdk.payment.model.PaymentRequested
import tech.carpentum.sdk.payment.model.Payout
import tech.carpentum.sdk.payment.model.VaPayMethod
import java.math.BigDecimal
import java.net.HttpURLConnection
import java.time.Duration
import java.util.*

object AdvancedScenariosApp {
    /* Use for example https://pipedream.com/workflows service. */
    private val callbackUrl = Objects.requireNonNull(System.getenv("TECH_CARPENTUM_SDK_PAYMENT_SAMPLE_CALLBACK_URL"))

    private fun scenarioCreatePayin(context: PaymentContext): String {
        val idPayin = "TST-" + UUID.randomUUID()
        val createPayinScenario: CreatePayinScenario = CreatePayinScenario.create(context, idPayin)
        val availablePaymentOptions = createPayinScenario.availablePaymentOptions(
            PaymentRequested.builder()
                .money(
                    MoneyPaymentRequested.builder()
                        .amount(BigDecimal("10050"))
                        .currencyCode(CurrencyCode.of("IDR"))
                        .build()
                )
                .build()
        )
        println("availablePaymentOptions= $availablePaymentOptions")
        val payin = createPayinScenario.createPayment(
            Payin.builder()
                .paymentRequested(
                    PaymentRequested.builder()
                        .money(
                            MoneyPaymentRequested.builder()
                                .amount(BigDecimal("10050"))
                                .currencyCode(CurrencyCode.of("IDR"))
                                .build()
                        )
                        .build()
                )
                .paymentMethod(
                    VaPayMethod.builder()
                        .account(
                            AccountPayinRequestVaPay.builder()
                                .accountName("Test account")
                                .accountNumber("11223344")
                                .build()
                        )
                        .paymentOperatorCode("IDR_002")
                        .emailAddress("customer@test.com")
                        .build()
                )
                .returnUrl("https://example.com?id=123")
                .callbackUrl(callbackUrl)
                .build()
        )
        println("payin= $payin")
        createPayinScenario.setExternalReference("my-fererence-" + UUID.randomUUID())
        Thread.sleep(2000L)
        val detail = createPayinScenario.getDetail()
        println("detail= $detail")
        println("paymentMethodResponse.paymentMethodCode= " + detail.paymentMethodResponse.paymentMethodCode)
        return idPayin
    }

    private fun scenarioGetPayin(context: PaymentContext, idPayin: String): String {
        val getPayinScenario = GetPayinScenario.create(context, idPayin)
        val detail = getPayinScenario.getDetail()
        println("detail= $detail")
        println("paymentMethodResponse.paymentMethodCode= " + detail.paymentMethodResponse.paymentMethodCode)
        return idPayin
    }

    private fun scenarioCreatePayout(context: PaymentContext): String {
        val idPayout = "TST-" + UUID.randomUUID()
        val createPayoutScenario = CreatePayoutScenario.create(context, idPayout)
        val payout = createPayoutScenario.createPayment(
            Payout.builder()
                .paymentRequested(
                    PaymentRequested.builder()
                        .money(
                            MoneyPaymentRequested.builder()
                                .amount(BigDecimal("50"))
                                .currencyCode(CurrencyCode.of("IDR"))
                                .build()
                        )
                        .build()
                )
                .paymentMethod(
                    BankTransferMethod.builder()
                        .account(
                            AccountPayoutRequestBankTransfer.builder()
                                .accountName("Test account")
                                .accountNumber("11223344")
                                .build()
                        )
                        .paymentOperatorCode("IDR_002")
                        .emailAddress("customer@test.com")
                        .build()
                )
                .callbackUrl(callbackUrl)
                .build()
        )
        println("payout= $payout")
        val detail = createPayoutScenario.getDetail()
        println("detail= $detail")
        println("paymentMethodResponse.paymentMethodCode= " + detail.paymentMethodResponse.paymentMethodCode)
        return idPayout
    }

    private fun scenarioCreatePayoutWithBalanceCheck(context: PaymentContext): String {
        val idPayout = "TST-" + UUID.randomUUID()
        val createPayoutWithBalanceCheckScenario = CreatePayoutWithBalanceCheckScenario.create(context, idPayout)
        val payout = createPayoutWithBalanceCheckScenario.createPaymentWithBalanceCheck(
            Payout.builder()
                .paymentRequested(
                    PaymentRequested.builder()
                        .money(
                            MoneyPaymentRequested.builder()
                                .amount(BigDecimal("5000000"))
                                .currencyCode(CurrencyCode.of("IDR"))
                                .build()
                        )
                        .build()
                )
                .paymentMethod(
                    BankTransferMethod.builder()
                        .account(
                            AccountPayoutRequestBankTransfer.builder()
                                .accountName("Test account")
                                .accountNumber("11223344")
                                .build()
                        )
                        .paymentOperatorCode("IDR_002")
                        .emailAddress("customer@test.com")
                        .build()
                )
                .callbackUrl(callbackUrl)
                .build()
        )
        println("payout= $payout")
        if (payout.isPresent) {
            val detail = createPayoutWithBalanceCheckScenario.getDetail()
            println("detail= $detail")
            println("paymentMethodResponse.paymentMethodCode= " + detail.paymentMethodResponse.paymentMethodCode)
        } else {
            println("Payout NOT created! EXPECTED")
        }
        return idPayout
    }

    private fun scenarioGetPayout(context: PaymentContext, idPayout: String) {
        val getPayoutScenario = GetPayoutScenario.create(context, idPayout)
        val detail = getPayoutScenario.getDetail()
        println("detail= $detail")
        println("paymentMethodResponse.paymentMethodCode= " + detail.paymentMethodResponse.paymentMethodCode)
    }

    private fun scenarioGetPayout410(context: PaymentContext, idPayout: String) {
        val getPayoutScenario = GetPayoutScenario.create(context, idPayout)
        try {
            getPayoutScenario.getDetail()
        } catch (ex: ClientErrorException) {
            //expected exception
            println("[" + ex.statusCode + "] " + "(" + (ex.statusCode == HttpURLConnection.HTTP_GONE) + ") " + ex) //must be 410 Gone
        }
    }

    private fun scenarioAccount(context: PaymentContext) {
        val accountScenario = AccountScenario.create(context)
        println("Current Authorization token: " + accountScenario.authToken)
        run {
            val balances1 = accountScenario.listBalances()
            println("balances= $balances1")
        }
        run {
            val balances2 = accountScenario.listBalances("IDR", "CZK")
            println("balances= $balances2")
        }
    }

    private fun scenarioCodelists(context: PaymentContext) {
        val codelistsScenario = CodelistsScenario.create(context, Duration.ofHours(1))
        val currencies = codelistsScenario.listCurrencies()
        println("currencies= $currencies")
        val segments = codelistsScenario.listSegments()
        println("segments= $segments")
        val data = segments.data
        println("data= $data")
        val paymentOperators = codelistsScenario.listPaymentOperators()
        println("paymentOperators= $paymentOperators")
        val paymentMethods = codelistsScenario.listPaymentMethods()
        println("paymentMethods= $paymentMethods")
        run {
            val paymentOptions1 = codelistsScenario.listPaymentOptions()
            println("paymentOptions= $paymentOptions1")
        }
        run {
            val paymentOptions2 = codelistsScenario
                .listPaymentOptions(paymentTypeCode(PaymentTypeCode.PAYIN))
            println("paymentOptions= $paymentOptions2")
        }
        run {
            val paymentOptions3 = codelistsScenario
                .listPaymentOptions(paymentMethodCodes(PaymentMethodCode.OFFLINE, PaymentMethodCode.ONLINE))
            println("paymentOptions= $paymentOptions3")
        }
        run {
            val paymentOptions4 = codelistsScenario
                .listPaymentOptions(currencyCodes("INR", "IDR"))
            println("paymentOptions= $paymentOptions4")
        }
        run {
            val paymentOptions5 = codelistsScenario
                .listPaymentOptions(segmentCodes("MEDIUM", "NEWBIE"))
            println("paymentOptions= $paymentOptions5")
        }
        run {
            try {
                val paymentOptions6 = codelistsScenario
                    .listPaymentOptions(
                        currencyCodes("IDR")
                            .paymentTypeCode(PaymentTypeCode.PAYIN)
                            .paymentMethodCodes(PaymentMethodCode.IMPS)
                            .segmentCodes("VIP")
                            .paymentOperatorCodes("IDR_016")
                            .get()
                    )
                println("paymentOptions= $paymentOptions6")
            } catch (ex: GetPaymentOptionsErrorException) {
                Optional.ofNullable(ex.businessValidationErrors[GetPaymentOptionsError.CODE_MERCHANT_INACTIVE]).map { error ->
                    throw IllegalStateException("Fatal error: $error")
                }
            }
        }
    }

    private fun scenarioMerchantInfo(context: PaymentContext) {
        val merchantInfoScenario = MerchantInfoScenario.create(context)
        println("Current Authorization token: " + merchantInfoScenario.authToken)
        val merchantInfo = merchantInfoScenario.getMerchantInfo()
        println("merchantInfo= $merchantInfo")
    }

    @JvmStatic
    fun main(args: Array<String>) {
        val context = PaymentContext
            .builder()
            .brand("brand007")
            .defaultCallTimeout(Duration.ofMinutes(1))
            .build()

        scenarioMerchantInfo(context)
        println("---------------------------------------------------")

        scenarioCodelists(context)
        println("---------------------------------------------------")

        scenarioAccount(context)
        println("---------------------------------------------------")

        val idPayin1 = scenarioCreatePayin(context)
        println("---------------------------------------------------")

        scenarioGetPayin(context, idPayin1)
        println("---------------------------------------------------")

        val idPayout1 = scenarioCreatePayout(context)
        println("---------------------------------------------------")

        scenarioGetPayout(context, idPayout1)
        println("---------------------------------------------------")

        val idPayout2 = scenarioCreatePayoutWithBalanceCheck(context)
        println("---------------------------------------------------")
        scenarioGetPayout410(context, idPayout2)
        println("---------------------------------------------------")
    }
}