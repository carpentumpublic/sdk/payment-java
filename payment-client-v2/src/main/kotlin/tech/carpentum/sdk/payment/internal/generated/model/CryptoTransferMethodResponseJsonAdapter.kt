//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.CryptoTransferMethodResponse

class CryptoTransferMethodResponseJsonAdapter {
    @FromJson
    fun fromJson(json: CryptoTransferMethodResponseJson): CryptoTransferMethodResponse {
        val builder = CryptoTransferMethodResponse.builder()
        builder.idPayout(json.idPayout)
        builder.idPayment(json.idPayment)
        builder.reference(json.reference)
        return builder.build()
    }

    @ToJson
    fun toJson(model: CryptoTransferMethodResponse): CryptoTransferMethodResponseJson {
        val json = CryptoTransferMethodResponseJson()
        json.idPayout = model.idPayout
        json.idPayment = model.idPayment
        json.paymentMethodCode = model.paymentMethodCode.name
        json.reference = model.reference
        return json
    }

    @FromJson
    fun fromJsonImpl(model: CryptoTransferMethodResponse): CryptoTransferMethodResponseImpl {
        return model as CryptoTransferMethodResponseImpl
    }

    @ToJson
    fun toJsonImpl(impl: CryptoTransferMethodResponseImpl): CryptoTransferMethodResponse {
        return impl
    }

}