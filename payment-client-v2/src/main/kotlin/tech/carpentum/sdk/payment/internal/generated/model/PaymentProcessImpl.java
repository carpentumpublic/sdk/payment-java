//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** PaymentProcess
 *
 * 
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public class PaymentProcessImpl implements PaymentProcess {
    private final PaymentStatus status;

    @Override
    public PaymentStatus getStatus() {
        return status;
    }


    private final Optional<FailureReasons> failureReasons;

    @Override
    public Optional<FailureReasons> getFailureReasons() {
        return failureReasons;
    }


    /** Date and time when payment was accepted by platform. */
    private final java.time.OffsetDateTime createdAt;

    @Override
    public java.time.OffsetDateTime getCreatedAt() {
        return createdAt;
    }


    /** Date and time when payment was processed by platform. */
    private final Optional<java.time.OffsetDateTime> processedAt;

    @Override
    public Optional<java.time.OffsetDateTime> getProcessedAt() {
        return processedAt;
    }


    /** This flag is set to `true` when payment was done with testing merchant or testing channel. */
    private final Boolean isTest;

    @Override
    public Boolean getIsTest() {
        return isTest;
    }


    private final Optional<ProcessorStatus> processorStatus;

    @Override
    public Optional<ProcessorStatus> getProcessorStatus() {
        return processorStatus;
    }




    private final int hashCode;
    private final String toString;

    private PaymentProcessImpl(BuilderImpl builder) {
        this.status = Objects.requireNonNull(builder.status, "Property 'status' is required.");
        this.failureReasons = Optional.ofNullable(builder.failureReasons);
        this.createdAt = Objects.requireNonNull(builder.createdAt, "Property 'createdAt' is required.");
        this.processedAt = Optional.ofNullable(builder.processedAt);
        this.isTest = Objects.requireNonNull(builder.isTest, "Property 'isTest' is required.");
        this.processorStatus = Optional.ofNullable(builder.processorStatus);

        this.hashCode = Objects.hash(status, failureReasons, createdAt, processedAt, isTest, processorStatus);
        this.toString = builder.type + "(" +
                "status=" + status +
                ", failureReasons=" + failureReasons +
                ", createdAt=" + createdAt +
                ", processedAt=" + processedAt +
                ", isTest=" + isTest +
                ", processorStatus=" + processorStatus +
                ')';
    }

    @Override
    public int hashCode() {
        return hashCode;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }

        if (!(obj instanceof PaymentProcessImpl)) {
            return false;
        }

        PaymentProcessImpl that = (PaymentProcessImpl) obj;
        if (!Objects.equals(this.status, that.status)) return false;
        if (!Objects.equals(this.failureReasons, that.failureReasons)) return false;
        if (!Objects.equals(this.createdAt, that.createdAt)) return false;
        if (!Objects.equals(this.processedAt, that.processedAt)) return false;
        if (!Objects.equals(this.isTest, that.isTest)) return false;
        if (!Objects.equals(this.processorStatus, that.processorStatus)) return false;

        return true;
    }

    @Override
    public String toString() {
        return toString;
    }

    /** Builder for {@link PaymentProcess} model class. */
    public static class BuilderImpl implements PaymentProcess.Builder {
        private PaymentStatus status = null;
        private FailureReasons failureReasons = null;
        private java.time.OffsetDateTime createdAt = null;
        private java.time.OffsetDateTime processedAt = null;
        private Boolean isTest = null;
        private ProcessorStatus processorStatus = null;

        private final String type;

        public BuilderImpl(String type) {
            this.type = type;
        }

        public BuilderImpl() {
            this("PaymentProcess");
        }

        /**
          * Set {@link PaymentProcess#getStatus} property.
          *
          * 
          */
        @Override
        public BuilderImpl status(PaymentStatus status) {
            this.status = status;
            return this;
        }

        @Override
        public boolean isStatusDefined() {
            return this.status != null;
        }

        /**
          * Set {@link PaymentProcess#getFailureReasons} property.
          *
          * 
          */
        @Override
        public BuilderImpl failureReasons(FailureReasons failureReasons) {
            this.failureReasons = failureReasons;
            return this;
        }

        @Override
        public boolean isFailureReasonsDefined() {
            return this.failureReasons != null;
        }

        /**
          * Set {@link PaymentProcess#getCreatedAt} property.
          *
          * Date and time when payment was accepted by platform.
          */
        @Override
        public BuilderImpl createdAt(java.time.OffsetDateTime createdAt) {
            this.createdAt = createdAt;
            return this;
        }

        @Override
        public boolean isCreatedAtDefined() {
            return this.createdAt != null;
        }

        /**
          * Set {@link PaymentProcess#getProcessedAt} property.
          *
          * Date and time when payment was processed by platform.
          */
        @Override
        public BuilderImpl processedAt(java.time.OffsetDateTime processedAt) {
            this.processedAt = processedAt;
            return this;
        }

        @Override
        public boolean isProcessedAtDefined() {
            return this.processedAt != null;
        }

        /**
          * Set {@link PaymentProcess#getIsTest} property.
          *
          * This flag is set to `true` when payment was done with testing merchant or testing channel.
          */
        @Override
        public BuilderImpl isTest(Boolean isTest) {
            this.isTest = isTest;
            return this;
        }

        @Override
        public boolean isIsTestDefined() {
            return this.isTest != null;
        }

        /**
          * Set {@link PaymentProcess#getProcessorStatus} property.
          *
          * 
          */
        @Override
        public BuilderImpl processorStatus(ProcessorStatus processorStatus) {
            this.processorStatus = processorStatus;
            return this;
        }

        @Override
        public boolean isProcessorStatusDefined() {
            return this.processorStatus != null;
        }

        /**
         * Create new instance of {@link PaymentProcess} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @Override
        public PaymentProcessImpl build() {
            return new PaymentProcessImpl(this);
        }

    }
}