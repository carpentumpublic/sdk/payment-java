@file:JvmName("RateListsApiUtils")

package tech.carpentum.sdk.payment

import tech.carpentum.sdk.payment.EndpointDefinition.Method.GET
import tech.carpentum.sdk.payment.RateListsApi.Factory
import tech.carpentum.sdk.payment.internal.api.EnhancedRateListsApi
import tech.carpentum.sdk.payment.internal.api.GetFxRatesErrorExceptionFactory
import tech.carpentum.sdk.payment.internal.api.ResponseExceptionUtils
import tech.carpentum.sdk.payment.model.AvailableForexCurrencyPairList
import tech.carpentum.sdk.payment.model.ForexRateList
import java.io.InterruptedIOException
import java.time.Duration

/**
 *  RESTful API, Common methods that can be used for settlements.
 *
 * Use [Factory] to create new instance of the class.
 */
class RateListsApi private constructor(
    private val apiVersion: Int,
    private val api: EnhancedRateListsApi
) {

    /**
     * Throws [InterruptedIOException] in case of timeout.
     */
    @Throws(ResponseException::class, InterruptedIOException::class)
    // tag::userGuidePublicApi[]
    fun getFxRatePairs(tenantCode: String, merchantCode: String): AvailableForexCurrencyPairList
    // end::userGuidePublicApi[]
    {
        return api.getFxRatePairs(xAPIVersion = apiVersion, tenantCode = tenantCode, merchantCode = merchantCode)
    }

    /**
     * Throws [GetFxRatesErrorException] ("406" response) with one of defined
     * [GetFxRatesError] business validation error code.
     * Throws [InterruptedIOException] in case of timeout.
     */
    @Throws(ResponseException::class, InterruptedIOException::class)
    // tag::userGuidePublicApi[]
    fun getFxRates(
        tenantCode: String,
        merchantCode: String,
        baseCurrencyCode: String? = null,
        quoteCurrencyCode: String? = null
    ): ForexRateList
    // end::userGuidePublicApi[]
    {
        return ResponseExceptionUtils.wrap(GetFxRatesErrorExceptionFactory.instance) {
            api.getFxRates(
                xAPIVersion = apiVersion,
                tenantCode = tenantCode,
                merchantCode = merchantCode,
                baseCurrencyCode = baseCurrencyCode,
                quoteCurrencyCode = quoteCurrencyCode
            )
        }
    }

    /**
     * Factory to create a new instance of [RateListsApi].
     */
    companion object Factory {
        /**
         * Endpoint definition for [RateListsApi.getFxRatePairs] method.
         */
        @JvmStatic
        fun defineGetFxRatePairsEndpoint(): EndpointDefinition = EndpointDefinition(GET, "/fx-rate-pairs")

        /**
         * Endpoint definition for [RateListsApi.getFxRates] method.
         */
        @JvmStatic
        fun defineGetFxRatesEndpoint(): EndpointDefinition = EndpointDefinition(GET, "/fx-rates")

        @JvmStatic
        @JvmOverloads
        fun create(context: PaymentContext, accessToken: String, callTimeout: Duration? = null): RateListsApi {
            return RateListsApi(
                apiVersion = context.apiVersion,
                api = EnhancedRateListsApi(
                    basePath = context.apiBaseUrl,
                    accessToken = accessToken,
                    brand = context.brand,
                    callTimeout = callTimeout ?: context.defaultCallTimeout
                )
            )
        }
    }

}

/**
 * Grants [RateListsApi.getFxRatePairs] endpoint, see [RateListsApi.defineGetFxRatePairsEndpoint] definition.
 */
@JvmName("grantGetFxRatePairsEndpoint")
fun AuthTokenOperations.grantRateListsApiGetFxRatePairsEndpoint(): AuthTokenOperations =
    this.grant(RateListsApi.defineGetFxRatePairsEndpoint())

/**
 * Grants [RateListsApi.getFxRates] endpoint, see [RateListsApi.defineGetFxRatesEndpoint] definition.
 */
@JvmName("grantGetFxRatesEndpoint")
fun AuthTokenOperations.grantRateListsApiGetFxRatesEndpoint(): AuthTokenOperations =
    this.grant(RateListsApi.defineGetFxRatesEndpoint())
