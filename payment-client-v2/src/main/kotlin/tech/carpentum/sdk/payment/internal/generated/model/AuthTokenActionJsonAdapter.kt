//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.AuthTokenAction

class AuthTokenActionJsonAdapter {

    @FromJson
    fun fromJson(json: String): AuthTokenAction {
        return AuthTokenAction.of(json)
    }

    @ToJson
    fun toJson(model: AuthTokenAction): String {
        return model.value
    }

    @FromJson
    fun fromJsonImpl(model: AuthTokenAction): AuthTokenActionImpl {
        return model as AuthTokenActionImpl
    }

    @ToJson
    fun toJsonImpl(impl: AuthTokenActionImpl): AuthTokenAction {
        return impl
    }

}