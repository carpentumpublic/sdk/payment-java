//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.JsonClass;
import tech.carpentum.sdk.payment.model.*;

@JsonClass(generateAdapter = true)
class TopUpDetailJson {
    var topUpRequested: TopUpRequestedDetail? = null
    var process: PaymentProcess? = null
    var fee: MoneyFee? = null
    var paymentMethodResponse: PayinMethodResponse? = null
    var settlement: SettlementMethod? = null
}