//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** AuthTokenAction
 *
 * Action code specifying endpoints that can be called with the issued token.
 *
 * 
 *
 * The model class is immutable.
 *
 * Use static {@link #of} method to create a new model class instance.
 */
public interface AuthTokenAction {
    AuthTokenAction PAYOUT_INFO = AuthTokenAction.of("PAYOUT_INFO");
    AuthTokenAction WALLET_OPERATION_CREATE = AuthTokenAction.of("WALLET_OPERATION_CREATE");
    AuthTokenAction PAYIN_INFO = AuthTokenAction.of("PAYIN_INFO");
    AuthTokenAction PAYIN_CREATE = AuthTokenAction.of("PAYIN_CREATE");
    AuthTokenAction PAYOUT_CREATE = AuthTokenAction.of("PAYOUT_CREATE");

    @NotNull String getValue();

    static AuthTokenAction of(@NotNull String value) {
        return new AuthTokenActionImpl(value);
    }
}