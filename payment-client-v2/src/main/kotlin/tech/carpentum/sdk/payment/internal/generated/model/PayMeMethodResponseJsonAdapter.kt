//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.PayMeMethodResponse

class PayMeMethodResponseJsonAdapter {
    @FromJson
    fun fromJson(json: PayMeMethodResponseJson): PayMeMethodResponse {
        val builder = PayMeMethodResponse.builder()
        builder.idPayin(json.idPayin)
        builder.idPayment(json.idPayment)
        builder.account(json.account)
        builder.accountCustomer(json.accountCustomer)
        builder.money(json.money)
        builder.moneyRequired(json.moneyRequired)
        builder.vat(json.vat)
        builder.merchantName(json.merchantName)
        builder.reference(json.reference)
        builder.qrName(json.qrName)
        builder.qrCode(json.qrCode)
        builder.payMeDeepLink(json.payMeDeepLink)
        builder.returnUrl(json.returnUrl)
        builder.acceptedAt(json.acceptedAt)
        builder.expireAt(json.expireAt)
        return builder.build()
    }

    @ToJson
    fun toJson(model: PayMeMethodResponse): PayMeMethodResponseJson {
        val json = PayMeMethodResponseJson()
        json.idPayin = model.idPayin
        json.idPayment = model.idPayment
        json.account = model.account
        json.accountCustomer = model.accountCustomer
        json.money = model.money
        json.moneyRequired = model.moneyRequired.orElse(null)
        json.vat = model.vat.orElse(null)
        json.merchantName = model.merchantName
        json.reference = model.reference
        json.qrName = model.qrName
        json.qrCode = model.qrCode
        json.payMeDeepLink = model.payMeDeepLink.orElse(null)
        json.returnUrl = model.returnUrl
        json.acceptedAt = model.acceptedAt
        json.expireAt = model.expireAt
        json.paymentMethodCode = model.paymentMethodCode.name
        return json
    }

    @FromJson
    fun fromJsonImpl(model: PayMeMethodResponse): PayMeMethodResponseImpl {
        return model as PayMeMethodResponseImpl
    }

    @ToJson
    fun toJsonImpl(impl: PayMeMethodResponseImpl): PayMeMethodResponse {
        return impl
    }

}