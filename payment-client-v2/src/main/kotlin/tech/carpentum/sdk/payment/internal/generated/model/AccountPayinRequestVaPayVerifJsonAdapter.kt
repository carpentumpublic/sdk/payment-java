//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.AccountPayinRequestVaPayVerif

class AccountPayinRequestVaPayVerifJsonAdapter {
    @FromJson
    fun fromJson(json: AccountPayinRequestVaPayVerifJson): AccountPayinRequestVaPayVerif {
        val builder = AccountPayinRequestVaPayVerif.builder()
        builder.accountName(json.accountName)
        builder.accountNumber(json.accountNumber)
        return builder.build()
    }

    @ToJson
    fun toJson(model: AccountPayinRequestVaPayVerif): AccountPayinRequestVaPayVerifJson {
        val json = AccountPayinRequestVaPayVerifJson()
        json.accountName = model.accountName.orElse(null)
        json.accountNumber = model.accountNumber
        return json
    }

    @FromJson
    fun fromJsonImpl(model: AccountPayinRequestVaPayVerif): AccountPayinRequestVaPayVerifImpl {
        return model as AccountPayinRequestVaPayVerifImpl
    }

    @ToJson
    fun toJsonImpl(impl: AccountPayinRequestVaPayVerifImpl): AccountPayinRequestVaPayVerif {
        return impl
    }

}