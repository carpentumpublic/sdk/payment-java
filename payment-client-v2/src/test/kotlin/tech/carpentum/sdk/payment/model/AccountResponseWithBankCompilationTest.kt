package tech.carpentum.sdk.payment.model

/**
 * Tests backward compatible changes in [AccountResponseWithBank].
 */
object AccountResponseWithBankCompilationTest {

    fun compilationTest() {
        // OfflineMethodResponse
        val offlineMethodResponse: OfflineMethodResponse = OfflineMethodResponse.builder().build()
        val accountWithBank: AccountResponseWithBank = offlineMethodResponse.account
        val accountOffline: AccountResponseOffline = offlineMethodResponse.account
    }

}
