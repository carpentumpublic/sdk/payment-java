//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** NETBANKING
 *
 * Netbanking, also known as online banking or Net Banking is a payment option offered by banks and financial institutions on Indian market that allow customers to use banking services over the internet. In our case, we take advantage of the Netbanking services to offer your customer UPI Payment method to pay funds directly from your customer Net Banking account.
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public interface NetBankingMethod extends PayinMethod {
    /** A discriminator value of property {@link #getPaymentMethodCode}. The model class extends {@link PayinMethod}. */
    PayinMethod.PaymentMethodCode PAYMENT_METHOD_CODE = PayinMethod.PaymentMethodCode.NETBANKING;

    @NotNull Optional<AccountPayinRequestNetBanking> getAccount();

    /** Your customer e-mail address in RFC 5322 format that is used for identification of the customer's payins. */
    @NotNull Optional<String> getEmailAddress();

    /** Your customer mobile phone number in full international telephone number format, including country code. */
    @NotNull Optional<String> getPhoneNumber();

    /** One of following can serve as Payment Operator:

 * Financial or other institution (such as bank, card payment processor, ...) that manages transactions for your customers
 * Mobile wallet
 * Blockchain protocol for crypto currency payments

Customer is informed with the payment instructions where funds have to be transferred efficiently based on the selected Payment operator.
For getting list of the available payment options for payins use [POST /payins/!availablePaymentOptions](#operations-Incoming_payments-availablePaymentOptions) API, for payouts use [POST /payouts/!availablePaymentOptions](#operations-Outgoing_payments-availablePaymentOptions) API. */
    @NotNull Optional<String> getPaymentOperatorCode();

    @NotNull static Builder builder(NetBankingMethod copyOf) {
        Builder builder = builder();
        builder.account(copyOf.getAccount().orElse(null));
        builder.emailAddress(copyOf.getEmailAddress().orElse(null));
        builder.phoneNumber(copyOf.getPhoneNumber().orElse(null));
        builder.paymentOperatorCode(copyOf.getPaymentOperatorCode().orElse(null));
        return builder;
    }

    @NotNull static Builder builder() {
        return new NetBankingMethodImpl.BuilderImpl();
    }

    /** Builder for {@link NetBankingMethod} model class. */
    interface Builder {

        /**
          * Set {@link NetBankingMethod#getAccount} property.
          *
          * 
          */
        @NotNull Builder account(AccountPayinRequestNetBanking account);

        boolean isAccountDefined();


        /**
          * Set {@link NetBankingMethod#getEmailAddress} property.
          *
          * Your customer e-mail address in RFC 5322 format that is used for identification of the customer's payins.
          */
        @NotNull Builder emailAddress(String emailAddress);

        boolean isEmailAddressDefined();


        /**
          * Set {@link NetBankingMethod#getPhoneNumber} property.
          *
          * Your customer mobile phone number in full international telephone number format, including country code.
          */
        @NotNull Builder phoneNumber(String phoneNumber);

        boolean isPhoneNumberDefined();


        /**
          * Set {@link NetBankingMethod#getPaymentOperatorCode} property.
          *
          * One of following can serve as Payment Operator:

 * Financial or other institution (such as bank, card payment processor, ...) that manages transactions for your customers
 * Mobile wallet
 * Blockchain protocol for crypto currency payments

Customer is informed with the payment instructions where funds have to be transferred efficiently based on the selected Payment operator.
For getting list of the available payment options for payins use [POST /payins/!availablePaymentOptions](#operations-Incoming_payments-availablePaymentOptions) API, for payouts use [POST /payouts/!availablePaymentOptions](#operations-Outgoing_payments-availablePaymentOptions) API.
          */
        @NotNull Builder paymentOperatorCode(String paymentOperatorCode);

        boolean isPaymentOperatorCodeDefined();


        /**
         * Create new instance of {@link NetBankingMethod} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @NotNull NetBankingMethod build();

    }
}